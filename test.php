<?php
/**
 * 将所有的部分结合起来绘制
 */
require_once('vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuDescPaint;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuTreePaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;
use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Model\EventData;
use FamilyTreePDF\Model\GraveData;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Model\PhotoData;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuBigEventPaint;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuGravePaint;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPhotoOnePaint;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPhotoTwoPaint;

use FamilyTreePDF\Util\DataFilter;

// 绘制方向
$direction = SysConst::$RIGHT_TO_LEFT;
// $direction = SysConst::$LEFT_TO_RIGHT;

$isHD = true;
$isPageNumDisabled = false;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $config = PaintConfig::getHDR2LConfig();
    } else {
        $config = PaintConfig::getR2LConfig();
    }

} else {
    if ($isHD) {
        $config = PaintConfig::getHDL2RConfig();
    } else {
        $config = PaintConfig::getL2RConfig();
    }

}

$options = [
    'isShowRanking' => 1,
    'showAdoption' =>1
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/mqb.json");
//$content = file_get_contents(__DIR__ . "/199person.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

// //start
// $startPersonId = 0;
// $options['isShowStartPersonFathers'] = 1;
// $dataFilter = new DataFilter($persons);
//     //添加startPersonId 和 isShowStartPersonFathers
//     $dataFilter->setOptions($options);
//     if ($startPersonId != 0) {
//         $dataFilter->setStartPersonId($startPersonId);
//     }
//     $persons = $dataFilter->getPersonSet();
//end


$xinhuoTreePaint = new TraditionalSuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装

$xinhuoDescPaint = new TraditionalSuDescPaint();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setDirection($direction);         // 必须要设置排版方向

$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->paint();

$xinhuoTreePaint->paint();

//// 大事件
//$eventText = '**哈哈**大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿';
//
//$eventData = new EventData('大事件名', $eventText, '2018-09-09');
//$eventData2 = new EventData('大事件名', '大事件内容但是大号是滴啊会松动为安徽神雕华盛顿', '2018-09-09');
//
//$events = [];
//$events[] = $eventData;
//$events[] = $eventData2;
//
//$eventPaint = new TraditionalSuBigEventPaint();
//$eventPaint->setContext($context);
//$eventPaint->setConfig($config);
//$eventPaint->input($events);
//$eventPaint->paint();
//
//// 单张相册
//$photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');
//
//$photoOnePaint = new TraditionalSuPhotoOnePaint();
//$photoOnePaint->setContext($context);
//$photoOnePaint->setConfig($config);
//$photoOnePaint->input($photoData);
//$photoOnePaint->paint();
//
//// 两张相册
//$photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');
//$photoData2 = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');
//
//$photos = [];
//$photos[] = $photoData;
//$photos[] = $photoData2;
//
//$photoTwoPaint = new TraditionalSuPhotoTwoPaint();
//$photoTwoPaint->setContext($context);
//$photoTwoPaint->setConfig($config);
//$photoTwoPaint->input($photos);
//$photoTwoPaint->paint();


//// 宗祠
//$graveData = new GraveData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈', '上海闵行');
//$gravePaint = new TraditionalSuGravePaint();
//$gravePaint->setContext($context);
//$gravePaint->setConfig($config);
//$gravePaint->input($graveData);
//$gravePaint->paint();


$builder = new PDFBuilder($direction, true);
//$builder = new A4PDFBuilder($direction, $isHD);
$GLOBALS['branchName'] = [1=>'魏王總派系·辛丑年', 2=>'魏王總派系·辛丑年'];
$branchFamilyName = "魏王總派系·辛丑年";
$hallName = "涿郡堂";
$totalPage = $builder->merge('赵氏宗谱', "childName",$branchFamilyName,2,$hallName);
//生成封面图
$arr = ['ChildName' => '这是默认支系谱名'];
$builder->toCover('赵', 3, 'lan', $arr);
$countPDF = $builder->toPdf($totalPage, 80);


$builder->clear();
