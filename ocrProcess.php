<?php
/**
 * 从Redis队列中轮询要执行的任务，并执行
 *
 */
require_once('vendor/autoload.php');
date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);

use DB\CDBOCRTask;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use Util\Util;
use ThirdParty\OCRClient;

/**
 * 异常处理函数
 *
 * @param Exception $exception 异常
 *
 * @return null
 */
function Exception_handler($exception)
{
    global $logger;
    $logger->error('ocrProcess:'.Util::exceptionFormat($exception));
    exit;
}

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

/**
 * 在这里注册异常捕获函数，如果出现异常则进行处理
 */
set_exception_handler('Exception_handler');


$ocrDB = new CDBOCRTask();
$ocrClient = new OCRClient();

$callback = function($msg) use ($ocrDB, $ocrClient) {
    $ocrTask = json_decode($msg->body, true);   //这里将json递归深度限制为1

    $photo = $ocrTask['p'];
    $taskId = $ocrTask['t'];

    list($result,$ocrType) = $ocrClient->ocr($photo);

    $ocrDB->updateOCRResult($taskId, $result, $ocrType);

    return true;
};

$ocrDB->getOCRTask($callback);
