<?php

namespace Util\TaskQueue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Util\Logger;
use Util\Util;

class Consumer {

    private $connection;
    private $channel;
    private $logger;

    public function __construct() {
        $this->logger = Logger::getInstance();
    }

    private function connect() {
        // $host,
        // $port,
        // $user,
        // $password,
        // $vhost = '/',
        // $insist = false,
        // $login_method = 'AMQPLAIN',
        // $login_response = null,
        // $locale = 'en_US',
        // $connection_timeout = 3.0,
        // $read_write_timeout = 3.0,
        // $context = null,
        // $keepalive = false,
        // $heartbeat = 0

        $heartBeat = 30;

        $this->connection = new AMQPStreamConnection(
            $GLOBALS['rabbitmq_ip'],
            $GLOBALS['rabbitmq_port'],
            $GLOBALS['rabbitmq_user'],
            $GLOBALS['rabbitmq_pass'],
            '/',false,'AMQPLAIN',null,'en_US',60,60,null,false,$heartBeat
        );
        $this->channel = $this->connection->channel();
    }

    public function cleanup_connection() {
        // Connection might already be closed.
        // Ignoring exceptions.
        try {
            if($this->connection !== null) {
                $this->connection->close();
            }
        } catch (\ErrorException $e) {
        }
    }

    /**
     * 消费任务
     * @param $queueName 任务队列名
     * @param $callback  回调函数
     * @param $preAck    预先回复
     */
    public function consume($queueName, $callback, $preAck = false) {
        while(true){
            try {
                $this->connect();
                $this->channel->queue_declare($queueName, false, true, false, false);
                $this->channel->basic_qos(null, 1, null);

                $ackCallback = function($msg) use ($callback, $preAck) {
                    if ($preAck) {
                        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                    }
                    $success = call_user_func($callback, $msg);
                    if ($success) {
                        //ack
                        if (!$preAck) {
                            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                        }
                    } else {
                        // 执行失败，不应答
                        $context['tags'] = array('alert'=>'yes');
                        $context['filename'] = 'task queue';
                        $this->logger->error("任务执行失败".$msg->body, $context);
                    }
                };

                $this->channel->basic_consume($queueName, '', false, false, false, false, $ackCallback);

                while(count($this->channel->callbacks)) {
                    $this->channel->wait();
                }
            } catch(AMQPIOException $e) {
                echo "AMQP IO exception " . PHP_EOL;
                $this->cleanup_connection();
                usleep(1000000);
            } catch(\RuntimeException $e) {
                echo "Runtime exception " . PHP_EOL;
                $this->cleanup_connection();
                usleep(1000000);
            } catch(\ErrorException $e) {
                echo "Error exception，Consumer出错($e)" . PHP_EOL;
                $this->logger->error("任务执行出错".Util::exceptionFormat($e) ,[]);
                $this->cleanup_connection();
                usleep(1000000);
            }
        
        }
    }

    public function close() {
        $this->channel->close();
        $this->connection->close();
    }

}