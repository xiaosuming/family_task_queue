<?php
require_once('../../vendor/autoload.php');
use Util\TaskQueue\Consumer;

$callback = function($msg) {
    var_dump($msg->body);
    sleep(5);
};

$consumer = new Consumer();
$consumer->consume("test_task", $callback);