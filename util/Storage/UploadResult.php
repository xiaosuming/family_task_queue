<?php
/**
 * 上传文件的返回值
 * @author: jiangpengfei
 * @date: 2018-04-19
 */

namespace Util\Storage;

class UploadResult {
    public $location;
    public $sourceIp;
    public $bucketName;
    public $remoteFileName;
    public $size;
    public $success = true;
}