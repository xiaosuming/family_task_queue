<?php
/**
 * 要绘制的相片数据的封装
 * @author jiangpengfei
 * @date   2018-08-27
 */

namespace Util\FamilyTree;

class PhotoData {
    public $url;
    public $personNames;       // 相册中的人物名数组
    public $desc;               // 描述
    public $address;            // 地址

    public function __construct($url, $personNames, $desc, $address) {
        $this->url = $url;
        $this->personNames = $personNames;
        $this->desc = $desc;
        $this->address = $address;
    }
}
