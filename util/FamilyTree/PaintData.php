<?php
/**
 * 要绘制的数据的封装
 * @author jiangpengfei
 * @date   2018-07-03
 */

namespace Util\FamilyTree;

class PaintData {
    public $levelList;  // $levelList在绘制过程中，是用来按照辈分存储所有人物的容器
    public $rect;       // 代表整张图的矩形
    public $path;       // 绘制中的路径
    public $startLevel; // 绘制的起始辈分

    public function __construct($levelList, $rect, $path, $startLevel) {
        $this->levelList = $levelList;
        $this->rect = $rect;
        $this->path = $path;
        $this->startLevel = $startLevel;
    }
}
