<?php
/**
 * 要绘制的宗祠数据的封装
 * @author jiangpengfei
 * @date   2018-08-27
 */

namespace Util\FamilyTree;

class GraveData {
    public $photo;
    public $desc;               // 描述
    public $address;            // 地址
    public $personName;

    public function __construct($photo, $personName, $desc, $address) {
        $this->photo = $photo;
        $this->personName = $personName;
        $this->desc = $desc;
        $this->address = $address;
    }
}
