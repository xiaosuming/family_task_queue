<?php
/**
 * 族谱pdf绘制的封装
 * @author jiangpengfei
 * @date   2018-07-02
 */

namespace Util;

define("DEBUG", true);
define("LEFT_TO_RIGHT", 0);
define("RIGHT_TO_LEFT", 1);

use Dv\Dv;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Util\FamilyTree\PaintData;
use Util\FileUtil;
use Util\FamilyTree\PhotoData;

class FamilyTree
{
    private $options;
    private $config;
    private $xoffset;       // 绘制人物时的整体x方向偏移
    private $yoffset;       // 绘制人物时的整体y方向偏移
    private $pageWidth;     // 每页的宽度（指的是halfblank.png的尺寸)
    private $pageHeight;    // 每页的高度
    private $pageCenterWidth;//每页中间的间隔长
    private $fontSize;      //人物名字的字体大小
    private $fontXOffset;   //绘制人物名字时的字体x方向偏移
    private $fontYOffset;   //绘制人物名字时的字体y方向偏移
    private $copyXOffset;   //复制图片时y方向的偏移
    private $copyYOffset;   //复制图片时x方向的偏移
    private $lineWidth; // 线宽
    private $font; // 字体
    private $levelTextX; // 第几世文字的x位置
    private $levelTextY; // 第几世文字的y位置
    private $levelTextDistance; // 第几世文字的间隔
    private $levelTextFontSize; // 世代的文字大小
    private $direction; // 排版方向

    private $paintDataQueue; // 等待绘制的人物队列
    private $totalPage; // 总页数
    private $calculatePage; //计算用的页数变量
    private $descPageMap; // 用来存储描述的一个map, 是personId和desc的页码对应
    private $personMap; // id和人物对应
    private $rootPersonQueue; // 绘制吊线图的根人物队列
    private $allLevelList; // 所有人物的levelList
    private $minLevel; // 最低的level，level越小，辈分越高
    private $maxLevel; // 最高的level, level越大，辈分越小

    private $descNormalFontSize; // 描述的普通字体大小
    private $descNameFontSize; // 描述名字的字体大小
    private $descLevelFontSize; // 辈分的字体大小
    private $descStartX; // 描述部分正文的X起点
    private $descStartY; // 描述部分正文的Y起点

    private $extraSpace;        // 页面绘制时多出来的旁白

    private $logger;            // 日志组件

    public function __construct($config)
    {
        $this->config = $config;
        $this->xoffset = $config['xoffset'];
        $this->yoffset = $config['yoffset'];
        $this->pageWidth = $config['pageWidth'];
        $this->pageHeight = $config['pageHeight'];
        $this->pageCenterWidth = $config['pageCenterWidth'];
        $this->fontSize = $config['fontSize'];
        $this->fontXOffset = $config['fontXOffset'];
        $this->fontYOffset = $config['fontYOffset'];
        $this->copyXOffset = $config['copyXOffset'];
        $this->copyYOffset = $config['copyYOffset'];
        $this->lineWidth = $config['lineWidth'];
        $this->levelTextX = $config['levelTextX'];
        $this->levelTextY = $config['levelTextY'];
        $this->levelTextDistance = $config['levelTextDistance'];
        $this->levelTextFontSize = $config['levelTextFontSize'];
        $this->font = __DIR__ . "/1.TTC";
        $this->direction = LEFT_TO_RIGHT;

        $this->descStartX = $config['descStartX'];
        $this->descStartY = $config['descStartY'];
        $this->descNormalFontSize = $config['descNormalFontSize'];
        $this->descNameFontSize = $config['descNameFontSize'];
        $this->descLevelFontSize = $config['descLevelFontSize'];

        $this->totalPage = 0;
        $this->paintDataQueue = array();
        $this->minLevel = PHP_INT_MAX;
        $this->maxLevel = PHP_INT_MIN;

        $this->extraSpace = $config['extraSpace'];
    }

    /**
     * 配置选项
     * * @param array $options ["isShowRanking" => 1]
     */
    public function setOptions($options) {
        $this->options = $options;
    }

    /**
     * 初始化目录。需要halfoutput,mergeoutput,pdfoutput,pdfoutput/tmp这四个文件夹
     */
    public function initDir() {
        if (!is_dir(__DIR__."/halfoutput")) {
            mkdir(__DIR__."/halfoutput", 0777);
        }

        if (!is_dir(__DIR__."/mergeoutput")) {
            mkdir(__DIR__."/mergeoutput", 0777);
        }

        if (!is_dir(__DIR__."/pdfoutput")) {
            mkdir(__DIR__."/pdfoutput", 0777);
        }

        if (!is_dir(__DIR__."/pdfoutput/tmp")) {
            mkdir(__DIR__."/pdfoutput/tmp", 0777);
        }

    }

    /**
     * 注入 logger 组件
     */
    public function setLogger($logger) {
        $this->logger = $logger;
    }

    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * 绘制人物姓名
     * @param $canvas 画布
     * @param $x    绘制的横坐标
     * @param $y    绘制的纵坐标
     * @param $name 绘制的名字
     * @param $shouldSmall 是否应该更小
     * @param $bold 是否是粗体
     */
    private function paintText($canvas, $x, $y, $name, $shouldSmall = false, $bold = true)
    {
        $fontSize = $this->fontSize;
        $distance = $this->config['textYDistance'];
        $color = 0x000000;

        if ($shouldSmall) {
            $fontSize = $fontSize-$this->config['smallFontSizeDiff'];
            $distance = $this->config['smallFontDistance'];
        }

        $len = mb_strlen($name);

        if (!$shouldSmall) {
            if ($len > 4) {
                $name = mb_substr($name, 0, 4);
                $len = 4;
            }
        }

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($name, $i, 1);
            imagettftext($canvas, $fontSize, 0,
                $x + $this->fontXOffset + $this->xoffset,
                $y + $this->fontYOffset - $this->yoffset, $color, $this->font, $c);
            
            if ($bold) {
                imagettftext($canvas, $fontSize, 0,
                $x + 1 + $this->fontXOffset + $this->xoffset,
                $y + $this->fontYOffset - $this->yoffset, $color, $this->font, $c);
            }

            $y = $y + $distance + $fontSize;
        }
    }

    /**
     * 绘制人物描述页的文字
     * @param $canvas 画布
     * @param $x    绘制的横坐标
     * @param $y    绘制的纵坐标
     * @param $desc 绘制的名字
     * @param $type 类型，1是世代，2是名字，3是普通描述
     * @param $bold 是否是粗体，默认是
     */
    private function paintDescText(&$canvas, &$x, &$y, $desc, $type, $bold = true, $topDesc = '')
    {
        $maxLen = $this->config['descMaxLen'];
        $xLimit = 0;

        $xStep = $this->config['descTextXStep'];

        switch ($type) {
            case 1:
                $fontSize = $this->descLevelFontSize;
                $xOffset = $this->config['descLevelTextXOffset'];
                $yOffset = $this->config['descLevelTextYOffset'];
                break;
            case 2:
                $fontSize = $this->descNameFontSize;
                $xOffset = $this->config['descNameTextXOffset'];
                $yOffset = $this->config['descNameTextYOffset'];
                break;
            case 3:
                $yOffset = 0;
                $xOffset = $this->config['descNormalTextXOffset'];

                $fontSize = $this->descNormalFontSize;
                break;
        }

        if ($type == 2 && $topDesc != '') {
            // echo "绘制topDesc---------------------------\n";
            // 要绘制顶部的长子之类的
            $len = mb_strlen($topDesc);

            for ($i = 0; $i < $len; $i++) {
                $c = mb_substr($topDesc, $i, 1);
                imagettftext($canvas, $this->config['descTopTextFontSize'], 0,
                    $x + $xOffset,
                    $y + $yOffset + $this->config['descTopTextYOffset'], 0x000000, $this->font, $c);
    
                
                $y = $y + $this->config['descTopTextFontDistance'] + $this->config['descTopTextFontSize'];
            }

        }

        $len = mb_strlen($desc);

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($desc, $i, 1);
            imagettftext($canvas, $fontSize, 0,
                $x + $xOffset,
                $y + $yOffset, 0x000000, $this->font, $c);

            if ($bold) {
                imagettftext($canvas, $fontSize, 0,
                $x + 1 + $xOffset,
                $y + $yOffset, 0x000000, $this->font, $c);
            }
            
            $y = $y + $this->config['descTextFontYDistance'] + $this->fontSize;

            if ($i < $len - 1 && $y > $maxLen) {
                // 到达底部并且不是最后一个字，换行
                $x += $xStep;
                $y = $this->descStartY;

                if ($x < 0 && $this->direction == RIGHT_TO_LEFT ||
                  $x > $this->pageWidth && $this->direction == LEFT_TO_RIGHT) {
                    // 重开一页
                    // 先保存当前的
                    imagepng($canvas, __DIR__ . "/halfoutput/output" . $this->totalPage . ".png");
                    echo "绘制描述第{$this->totalPage}页\n";
                    $this->totalPage++;
                    $canvas = imagecreatefrompng(__DIR__ . $this->config['deschalfImgPath']);
                    $x = $this->descStartX;
                    $y = $this->descStartY;
                }
            }
        }

        // 写完一次后，必须换行
        $x += $xStep;
        $y = $this->descStartY;
        if ($x < 0 && $this->direction == RIGHT_TO_LEFT ||
        $x > $this->pageWidth && $this->direction == LEFT_TO_RIGHT) {
            // 重开一页
            imagepng($canvas, __DIR__ . "/halfoutput/output" . $this->totalPage . ".png");
            echo "绘制描述第{$this->totalPage}页\n";
            $this->totalPage++;
            $canvas = imagecreatefrompng(__DIR__ . $this->config['deschalfImgPath']);
            $x = $this->descStartX;
            $y = $this->descStartY;
        }
    }

    /**
     * 获取要绘制的线的路径
     * @param 从dv库中输出的pathStr
     */
    private function getPath($pathStr)
    {
        $paths = array();

        $cs = explode(' ', $pathStr);

        for ($i = 1; $i < count($cs); $i = $i + 6) {
            $path = array();
            $path['sx'] = $cs[$i + 1];
            $path['sy'] = $cs[$i + 2];
            $path['ex'] = $cs[$i + 4];
            $path['ey'] = $cs[$i + 5];

            $paths[] = $path;
        }

        return $paths;
    }

    /**
     * 绘制有宽度的线
     * @param $canvas 画布
     * @param $sx     起点的横坐标
     * @param $sy     起点的纵坐标
     * @param $ex     终点的横坐标
     * @param $ey     终点的纵坐标
     * @param $width  宽度
     */
    public function imagelineWithWidth($canvas, $sx, $sy, $ex, $ey, $width)
    {
        $sx = $sx + $this->xoffset;
        $sy = $sy - $this->yoffset;
        $ex = $ex + $this->xoffset;
        $ey = $ey - $this->yoffset;

        if ($width == 1) {
            imageline($canvas, $sx, $sy, $ex, $ey, 0x000000);
        } else {
            if ($sx == $ex) {
                // 纵向
                imagefilledrectangle($canvas, $sx, $sy, $ex + $width, $ey, 0x000000);
            } else {
                // 横向
                imagefilledrectangle($canvas, $sx, $sy, $ex, $ey + $width, 0x000000);
            }

        }
    }

    /**
     * 在一张画布上绘制所有的人物
     * @param $allPersons 所有的人物集合
     * @param $page       页数
     * @param $startLevel 绘制的辈分起点
     */
    private function paint($paintData, &$page)
    {
        $innerPage = 0;
        $xoffset = $this->config['xoffset'];
        $yoffset = $this->config['yoffset'];
        $pageWidth = $this->config['pageWidth'];
        $pageHeight = $this->config['pageHeight'];
        $pageCenterWidth = $this->config['pageCenterWidth'];
        $fontSize = $this->config['fontSize'];
        $fontXOffset = $this->config['fontXOffset'];
        $fontYOffset = $this->config['fontYOffset'];
        $copyXOffset = $this->config['copyXOffset'];
        $copyYOffset = $this->config['copyYOffset'];
        $lineWidth = $this->config['lineWidth'];

        $rect = $paintData->rect;
        $levelList = $paintData->levelList;
        $paths = $paintData->path;
        $startLevel = $paintData->startLevel;

        $maxWidth = $rect['maxWidth']+$this->extraSpace;   // 多留20的旁白
        if ($this->direction == RIGHT_TO_LEFT) {
            // 从右往左
            foreach ($levelList as &$level) {
                foreach ($level as &$item) {
                    $item['x'] = $maxWidth - $item['x'];
                }
            }

            foreach ($paths as $key => $path) {
                $paths[$key]['ex'] = $maxWidth - $path['ex'];
                $paths[$key]['sx'] = $maxWidth - $path['sx'];
            }
        }

        $canvas = imagecreatetruecolor($maxWidth, $rect['maxHeight']);

        imagefilledrectangle($canvas, 0, 0, $maxWidth, $rect['maxHeight'], 0xffffff);

        foreach ($levelList as &$level) {
            foreach ($level as &$item) {
                if ($item['name'] != 'virtual') {
                    $x = $item['x'];
                    $y = $item['y'];

                    // imagefilledrectangle($canvas, $x+$xoffset, $y,
                    //                     $item['x']+$xoffset+$item['nodeWidth'], $item['y']+$item['nodeHeight'],
                    //                     0xcccccc
                    //                     );

                    $this->paintText($canvas, $x, $y, $item['name']);

                    // if ($this->direction == LEFT_TO_RIGHT) {
                        // 在旁边绘制见多少页
                    $this->paintText($canvas, $x+$this->config['pageTextXoffset'],$y+$this->config['pageTextYoffset'],
                        "见".Util::number2chinese($this->descPageMap[$item['id']])."页", true, false);
                    // } else {
                    //     // 在旁边绘制见多少页
                    //     $this->paintText($canvas, $x-45,$y-15,
                    //     "见".Util::number2chinese($this->descPageMap[$item['id']])."页", true, false);
                    // }

                    // // 画这个人的配偶
                    // foreach($item['spouses'] as $spouse) {
                    //     echo "画配偶\n";
                    //     $sx = $x + $this->config['spouseXOffset'];
                    //     $sy = $y;
                    //     $this->paintText($canvas, $sx, $sy, $spouse['name']);

                    //     $this->paintText($canvas, $sx+$this->config['pageTextXoffset'],$sy+$this->config['pageTextYoffset'],
                    //     "见".Util::number2chinese($this->descPageMap[$spouse['id']])."页", true, false);

                    //     // 绘制配偶的连接线
                    //     $this->imagelineWithWidth($canvas, $x, $y+$this->config['spouseLineYOffset'],
                    //      $x+$this->config['spouseLineXOffset'], $y+$this->config['spouseLineYOffset'], $lineWidth);

                    //     $x = $sx;
                    //     $y = $sy;
                        
                    // }
                }
            }
        }

        // 绘制路径
        foreach ($paths as $path) {
            $this->imagelineWithWidth($canvas, $path['sx'], $path['sy'], $path['ex'], $path['ey'], $lineWidth);
        }

        // 这里开始分页
        $leftWidth = $maxWidth;
        $leftHeight = $rect['maxHeight'];

        while ($leftWidth > 0) {

            $copyWidth = $pageWidth < $leftWidth ? $pageWidth : $leftWidth;
            $copyHeight = $pageHeight < $leftHeight ? $pageHeight : $leftHeight;

            $dstCanvas = imagecreatefrompng(__DIR__ . $this->config['halfblankImgPath']);

            $srcCopyX = ($innerPage * $pageWidth);

            if ($this->direction == RIGHT_TO_LEFT) {
                // 裁剪的位置从又向左
                $srcCopyX = $maxWidth - $srcCopyX - $copyWidth;

                // 粘贴的位置右对齐
                $copyXOffset = $pageWidth - $copyWidth + $copyXOffset;
            }

            imagecopy($dstCanvas, $canvas, $copyXOffset, $copyYOffset, $srcCopyX, 0, $copyWidth, $copyHeight);

            $leftWidth = $leftWidth - $copyWidth;

            $this->paintLevel($dstCanvas, $startLevel); // 写上第几世
            imagepng($dstCanvas, __DIR__ . "/halfoutput/output{$page}.png");
            if (DEBUG) {
                echo "绘制第{$page}页" . PHP_EOL;
            }
            // 释放内存
            imagedestroy($dstCanvas);
            $page++;
            $innerPage++;
        }
        imagedestroy($canvas);
    }

    /**
     * 绘制世代的文字
     * @param $canvas 画布
     * @param $startLevel 开始的辈分
     */
    private function paintLevel($canvas, $startLevel)
    {

        $x = $this->levelTextX;
        $y = $this->levelTextY;
        $text = "第" . Util::number2chinese($startLevel) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->levelTextFontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->levelTextFontSize;
        }

        $y = $this->levelTextY + $this->levelTextDistance;
        $text = "第" . Util::number2chinese($startLevel + 1) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->levelTextFontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->levelTextFontSize;
        }

        $y = $this->levelTextY + $this->levelTextDistance * 2;
        $text = "第" . Util::number2chinese($startLevel + 2) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->levelTextFontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->levelTextFontSize;
        }

        $y = $this->levelTextY + $this->levelTextDistance * 3;
        $text = "第" . Util::number2chinese($startLevel + 3) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->levelTextFontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->levelTextFontSize;
        }

        $y = $this->levelTextY + $this->levelTextDistance * 4;
        $text = "第" . Util::number2chinese($startLevel + 4) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->levelTextFontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->levelTextFontSize;
        }
    }

    /**
     * 数据适配器
     * @param 所有的人物数组
     */
    private function dataAdaptor($persons)
    {

        $allPersons = array();

        foreach ($persons as $person) {
            $personbj = array();
            $personbj['id'] = $person['id'];
            $personbj['name'] = $person['name'];
            $personbj['image'] = $person['photo'];
            $personbj['level'] = $person['level'];
            $personbj['gender'] = $person['gender'];
            $personbj['type'] = $person['type'];
            $personbj['ranking'] = $person['ranking'] ?? 0;
            
            if (count($person['father']) > 0) {
                $personbj['fathers'] = $person['father'];
            }

            if (count($person['mother']) > 0) {
                $personbj['mothers'] = $person['mother'];
            }

            if (count($person['brother']) > 0) {
                $personbj['brothers'] = $person['brother'];
            }

            if (count($person['sister']) > 0) {
                $personbj['sisters'] = $person['sister'];
            }

            if (count($person['son']) > 0) {
                $personbj['sons'] = $person['son'];
            }

            if (count($person['daughter']) > 0) {
                $personbj['daughters'] = $person['daughter'];
            }

            // 配偶
            if (count($person['spouse']) > 0) {
                $personbj['spouses'] = $person['spouse'];
            }

            $allPersons[] = $personbj;
        }

        return $allPersons;
    }

    /**
     * 向人物数组中添加亲属, (只添加子代)
     * @param $corePerson 核心人物
     * @param $persons    人物数组
     * @param $depth      当前的递归深度
     * @param $limit      递归深度的限制
     */
    private function addRelative($corePerson, &$persons, $depth, $limit)
    {
        if ($depth > $limit) {
            // 如果深度超过,则不再查找
            return;
        }

        // echo "深度".$depth.PHP_EOL;
        // echo "level".$corePerson['level'];

        if (!isset($persons[$corePerson['id']])) {
            $persons[$corePerson['id']] = $corePerson;


            // 添加这个人的配偶
            foreach($corePerson['spouse'] as $spouseId) {
                if (isset($this->personMap[$spouseId])) {
                    $persons[$spouseId] = $this->personMap[$spouseId];
                }
            }

            // echo "儿子数量".count($corePerson['son']);
            // echo "女儿数量".count($corePerson['daughter']);
            if ($depth == $limit && (count($corePerson['son']) > 0 || count($corePerson['daughter']) > 0)) {
                // 最后一代,放到rootPerson队列中
                array_push($this->rootPersonQueue, $corePerson);
            }
        }



        // 添加儿子
        foreach ($corePerson['son'] as $id) {
            if (!isset($persons[$id])) {
                $this->addRelative($this->personMap[$id], $persons, $depth + 1, $limit);
            }
        }

        // 添加女儿
        foreach ($corePerson['daughter'] as $id) {
            if (!isset($persons[$id])) {
                $this->addRelative($this->personMap[$id], $persons, $depth + 1, $limit);
            }
        }

    }

    /**
     * 输入，
     * @param $allPersons  所有待绘制的人物
     */
    public function input($allPersons)
    {
        if (count($allPersons) == 0) {
            return;
        }
        
        $page = 0;
        $this->personMap = array();

        // root person,在族谱的绘制中,是作为一页的起点来做的
        $this->rootPersonQueue = array();
        $rootPerson;

        // 构建personMap,id和person对应
        foreach ($allPersons as $person) {
            $this->personMap[$person['id']] = $person;

            $level = $person['level'];

            if ($level < $this->minLevel) {
                $this->minLevel = $level;
                $rootPerson = $person;
            }

            if ($level > $this->maxLevel) {
                $this->maxLevel = $level;
            }

            if (!isset($this->allLevelList[$level])) {
                $this->allLevelList[$level] = array();
            }

            $this->allLevelList[$level][] = $person;
        }

        $needPersons = array();

        $this->addRelative($rootPerson, $needPersons, 1, 5);

        $needPersons = array_values($needPersons);

        $needPaintPersons = $this->dataAdaptor($needPersons);

        $dv = new Dv();
        $dv->setConfig($this->config);

        $dv->input($needPaintPersons);
        $dv->setDisplayStyle(2);
        $levelList = $dv->output();

        $rect = $dv->getRect();
        $paths = $this->getPath($dv->getPath());
        //释放内存
        $dv->free();

        // 更新总页数
        $this->totalPage += ceil(($rect['maxWidth'] + $this->extraSpace) / $this->pageWidth);

        $startLevel = $rootPerson['level'] - $this->minLevel + 1;
        $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

        $this->paintDataQueue[] = $paintData;

        // 检查rootPersonQueue是否为空
        while (count($this->rootPersonQueue) > 0) {
            $rootPerson = $this->rootPersonQueue[0];
            $rootPerson['father'] = array();
            $rootPerson['mother'] = array();

            $needPersons = array();

            $this->addRelative($rootPerson, $needPersons, 1, 5);

            $needPersons = array_values($needPersons);

            if (count($needPersons) > 0) {

                $needPaintPersons = $this->dataAdaptor($needPersons);
                $dv = new Dv();
                $dv->setConfig($this->config);

                $dv->input($needPaintPersons);
                $dv->setDisplayStyle(2);
                $levelList = $dv->output();

                $rect = $dv->getRect();
                $paths = $this->getPath($dv->getPath());
                //释放内存
                $dv->free();

                // 这里的页数计算可能有问题
                $this->totalPage += ceil(($rect['maxWidth'] + $this->extraSpace) / $this->pageWidth);
                $startLevel = $rootPerson['level'] - $this->minLevel + 1;
                $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

                $this->paintDataQueue[] = $paintData;
            }
            $this->rootPersonQueue = array_slice($this->rootPersonQueue, 1);
        }

    }

    /**
     * 计算描述的页码，主要目的是计算每个人物所在的页码
     */
    private function calculateDesc(&$x, &$y, $desc, $type, $bold = true) {
        $maxLen = 472;
        $xLimit = 0;
        $initPage = $this->calculatePage;

        if ($this->direction == LEFT_TO_RIGHT) {
            $xStep = 23;
        }else{
            $xStep = -23;
        }
        switch ($type) {
            case 1:
                $fontSize = $this->descLevelFontSize;
                if ($this->direction == LEFT_TO_RIGHT) {
                    $xOffset = 3;
                } else {
                    $xOffset = 1;
                }
                $yOffset = 40;
                break;
            case 2:
                $fontSize = $this->descNameFontSize;
                if ($this->direction == LEFT_TO_RIGHT) {
                    $xOffset = 4;
                } else {
                    $xOffset = 0;
                }
                $yOffset = -14;
                break;
            case 3:
                $yOffset = 0;
                if ($this->direction == LEFT_TO_RIGHT) {
                    $xOffset = 6;
                } else {
                    $xOffset = 2;
                }
                $fontSize = $this->descNormalFontSize;
                break;
        }

        $len = mb_strlen($desc);

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($desc, $i, 1);
            
            $y = $y + 2 + $this->fontSize;

            if ($i < $len - 1 && $y > $maxLen) {
                // 到达底部并且不是最后一个字，换行
                $x += $xStep;
                $y = $this->descStartY;

                if ($x < 0 && $this->direction == RIGHT_TO_LEFT ||
                  $x > $this->pageWidth && $this->direction == LEFT_TO_RIGHT) {
                    $this->calculatePage++;
                    $x = $this->descStartX;
                    $y = $this->descStartY;
                }
            }
        }

        // 写完一次后，必须换行
        $x += $xStep;
        $y = $this->descStartY;
        if ($x < 0 && $this->direction == RIGHT_TO_LEFT ||
        $x > $this->pageWidth && $this->direction == LEFT_TO_RIGHT) {
            // 重开一页
            $this->calculatePage++;
            $x = $this->descStartX;
            $y = $this->descStartY;
        }

        return $initPage;
    }

    /**
     * 将排行转换成文字
     */
    private function convertRanking($ranking, $gender) {
        switch($ranking) {
            case 1:
                if ($gender == 0) {
                    return '长女';
                } else {
                    return '长子';
                }
                break;
            case 2:
                if ($gender == 0) {
                    return '次女';
                } else {
                    return '次子';
                }
                break;

            default:
                if ($gender == 0) {
                    return Util::number2chinese($ranking).'女';
                } else {
                    return Util::number2chinese($ranking).'子';
                }
        }
    }

    /**
     * 获取一个人物的排行称呼，如长子，次子，长女等
     * @param $person 人物信息
     */
    private function getRanking($person) {
        // 先检查人物是否有ranking
        if($person['ranking'] > 0) {
            return $this->convertRanking($person['ranking'],$person['gender']);
        }

        // 没有排行，目前根据加入的顺序来判断
        $siblings = array_merge($person['brother'], $person['sister']);

        sort($siblings);

        foreach($siblings as $index => $personId) {
            if ($person['id'] == $personId) {
                return $this->convertRanking($index+1, $person['gender']);
            }
        }
    }

    /**
     * 绘制描述
     * 绘制描述的遍历顺序应该是按照levelList的数据
     * 在绘制描述的时候，要更新待绘制item的描述内容
     */
    public function paintDesc()
    {
        $calculateX = $this->descStartX;
        $calculateY = $this->descStartY;

        // 先计算一遍
        $this->calculatePage = $this->totalPage;
        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {
            $level = $this->allLevelList[$key];
            $levelText = "第" . Util::number2chinese($key - $this->minLevel + 1) . "世";

            $this->calculateDesc($calculateX, $calculateY, $levelText, 1);

            foreach ($level as $person) {
                // 生于
                // 出生地
                // 孩子
                // 妻/夫
                $birthday = '';
                $address = '';
                $children = array();
                $childrenId = array();
                $spouse = '';
                if ($person['birthday'] == null) {
                    $birthday = "出生日期不详,";
                } else {
                    $birthday = "出生日期" . $person['birthday'] . ",";
                }

                if ($person['address'] == '') {
                    $address = "出生地不详,";
                } else {
                    $address = "出生地" . $person['address'] . ",";
                }

                if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                    $children[] = "无子，";
                } else {
                    $total = count($person['son']) + count($person['daughter']);
                    $children[] = "生" . Util::number2chinese($total) . "子 ";

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'] . "、";
                        $childrenId[] = $personId;
                    }

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'] . "、";
                        $childrenId[] = $personId;
                    }
                }

                // 配偶
                foreach ($person['spouse'] as $personId) {
                    if ($person['gender'] == 0) {
                        //女
                        $spouse .= "夫" . $this->personMap[$personId]['name'] . "、";
                    } else {
                        $spouse .= "妻" . $this->personMap[$personId]['name'] . "、";
                    }
                }

                // 当前人物的描述已经好了
                $name = $person['name'];
                $personPage = $this->calculateDesc($calculateX, $calculateY, $name, 2);
                $this->descPageMap[$person['id']] = $personPage;

                $this->calculateDesc($calculateX, $calculateY, $birthday, 3, false);
                $this->calculateDesc($calculateX, $calculateY, $address, 3, false);

                // 孩子部分特殊计算
                $childrenLen = count($children);

                if ($childrenLen == 1) {    // 没有孩子
                    $this->calculateDesc($calculateX, $calculateY, $children[0], 3, false);
                } else {
                    $isFirst = true;
                    for ($i = 0; $i < $childrenLen;) {
                        if ($isFirst) {
                            $paintText = $children[$i];

                            if (isset($children[$i+1]) ) {
                                $paintText .= $children[$i+1]."*三十页、";  // 这里的文字仅仅用作占位计算，无任何意义
                            }

                            if (isset($children[$i+2]) ) {
                                $paintText .= $children[$i+2]."*三十页、";
                            }

                            // 计算文字占用范围
                            $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);

                            $i = $i + 3;
                            $isFirst = false;
                        } else {
                            $paintText = $children[$i]."**三十页、";

                            if (isset($children[$i+1]) ) {
                                $paintText .= $children[$i+1]."*三十页、";
                            }

                            $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);
                            $i = $i + 2;
                        }
                    }
                }
                // else {
                //     $isFirst = true;
                //     for ($i = 0; $i < $childrenLen;) {
                //         if ($isFirst) {
                //             $paintText = $children[$i] . $children[$i+1] ?? '' . $children[$i+2] ?? '';
                //             $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);

                //             $i = $i + 3;
                //             $isFirst = false;
                //         } else {
                //             $paintText = $children[$i] . $children[$i+1] ?? '';
                //             $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);
                //             $i = $i + 2;
                //         }
                //     }
                // }

                if ($spouse != '') {
                    $this->calculateDesc($calculateX, $calculateY, $spouse, 3, false);
                }
            }
        }

        // 实际绘制
        $x = $this->descStartX;
        $y = $this->descStartY;
        $canvas = null;
        $canvas = imagecreatefrompng(__DIR__ . $this->config['deschalfImgPath']);

        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {
            $level = $this->allLevelList[$key];
            $levelText = "第" . Util::number2chinese($key - $this->minLevel + 1) . "世";

            $this->paintDescText($canvas, $x, $y, $levelText, 1);

            foreach ($level as $person) {
                // 生于
                // 出生地
                // 孩子
                // 妻/夫
                $birthday = '';
                $address = '';
                $children = array();
                $childrenId = array();
                $spouse = '';
                if ($person['birthday'] == null) {
                    $birthday = "出生日期不详,";
                } else {
                    $birthday = "出生日期" . $person['birthday'] . ",";
                }

                if ($person['address'] == '') {
                    $address = "出生地不详,";
                } else {
                    $address = "出生地" . $person['address'] . ",";
                }

                if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                    $children[] = "无子";
                } else {
                    $total = count($person['son']) + count($person['daughter']);
                    $children[] = "生" . Util::number2chinese($total) . "子 ";

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                }

                // 配偶
                foreach ($person['spouse'] as $personId) {
                    if ($person['gender'] == 0) {
                        //女
                        $spouse .= "夫" . $this->personMap[$personId]['name'] . "、";
                    } else {
                        $spouse .= "妻" . $this->personMap[$personId]['name'] . "、";
                    }
                }

                // 当前人物的描述已经好了
                $name = $person['name'];

                // 排行的绘制
                if ( isset($this->options['isShowRanking']) && $this->options['isShowRanking'] == 1) {
                    // 绘制排行
                    if (  isset($person['father']) && count($person['father']) > 0 
                    && isset($this->personMap[$person['father'][0]]) &&  $this->personMap[$person['father'][0]]['type'] == 1) {
                        // 父亲是本家人

                        $fatherName = $this->personMap[$person['father'][0]]['name'];
                        $this->paintDescText($canvas, $x, $y, $name, 2, true, $fatherName.' '.$this->getRanking($person));
                    } else if (isset($person['mother']) && count($person['mother']) > 0 
                    && isset($this->personMap[$person['mother'][0]]) &&  $this->personMap[$person['mother'][0]]['type'] == 1) {
                        // 母亲是本家人
                        $motherName = $this->personMap[$person['mother'][0]]['name'];
                        $this->paintDescText($canvas, $x, $y, $name, 2, true, $motherName.' '.$this->getRanking($person));

                    } else {
                        $this->paintDescText($canvas, $x, $y, $name, 2, true);
                    }
                } else {
                    // 不绘制排行
                    $this->paintDescText($canvas, $x, $y, $name, 2, true);
                }

                $this->paintDescText($canvas, $x, $y, $birthday, 3, false);
                $this->paintDescText($canvas, $x, $y, $address, 3, false);

                $childrenLen = count($children);

                if ($childrenLen == 1) {    // 没有孩子
                    $this->paintDescText($canvas, $x, $y, $children[0], 3, false);
                } else {
                    $isFirst = true;
                    for ($i = 0; $i < $childrenLen;) {
                        if ($isFirst) {
                            $paintText = $children[$i];

                            if (isset($children[$i+1]) ) {
                                $paintText .= $children[$i+1]."*".
                                Util::number2chinese($this->descPageMap[$childrenId[$i]])
                                ."页、";
                            }

                            if (isset($children[$i+2]) ) {
                                $paintText .= $children[$i+2]."*".
                                Util::number2chinese($this->descPageMap[$childrenId[$i+1]])
                                ."页、";
                            }

                            $this->paintDescText($canvas, $x, $y, $paintText, 3, false);

                            $i = $i + 3;
                            $isFirst = false;
                        } else {
                            $paintText = $children[$i]."*".
                            Util::number2chinese($this->descPageMap[$childrenId[$i-1]])
                            ."页、";

                            if (isset($children[$i+1]) ) {
                                $paintText .= $children[$i+1]."*".
                                Util::number2chinese($this->descPageMap[$childrenId[$i]])
                                ."页、";
                            }

                            $this->paintDescText($canvas, $x, $y, $paintText, 3, false);
                            $i = $i + 2;
                        }
                    }
                }
                

                if ($spouse != '') {
                    $this->paintDescText($canvas, $x, $y, $spouse, 3, false);
                }
            }
        }
    }

    /**
     * 输出半页
     */
    public function paintTree()
    {
        $page = 0;
        foreach ($this->paintDataQueue as $data) {
            $this->paint($data, $page);
        }
    }

    /**
     * 绘制大事件
     */
    public function paintBigEvent($events) {
        $options = [
            "paddingLeft" => $this->config['eventPaddingLevel'], // 20
            "paddingRight" => $this->config['eventPaddingRight'], // 20
            "paddingTop" => $this->config['eventPaddingTop'],  // 40
            "paddingBottom" => $this->config['eventPaddingBottom'], // 40
            "lineheight" => $this->config['eventLineheight'],   // 30
            "fontsize" => $this->config['eventFontsize'],      // 14
            "fontDistance" => $this->config['eventFontDistance'], // 字体间距
          ];
        foreach($events as $event) {
            $title = $event['expName'];
            $content = $event['expContent'];
            $time = $event['expTime'];

            if ($time == "0001-01-01") {
                $time = "时间不详";
            }

            $all = '# '.$title."\n\n**".$time."**\n\n".$content;
            // 参数: 字体路径(char*), 风格(0,1), 输出的起始索引(int), 输出路径(char*), 内容(char*)
            $pages = md2pic($this->font, 1, $this->totalPage, __DIR__ . '/halfoutput/', $all, $this->config['eventPageWidth'], $this->config['eventPageHeigth'],$options);
            
            $this->totalPage += $pages;
        }
    }

    // 下载图片，可以通过width和height指定大小
    public function downloadImage($url, $outfile, $width, $height) {

        if ( ($pos = strpos($url, "?")) > 0) {
            $url = substr($url,0, $pos);
            $url = $url."?imageView2/2/w/$width/h/$height";
        } else {
            $url = $url."?imageView2/2/w/$width/h/$height";
        }

        $ch = curl_init();

        $fp=fopen($outfile, 'w');
    
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); 
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        $output = curl_exec($ch); 
        $info = curl_getinfo($ch);
        fclose($fp);
        curl_close($ch); 

        return true;
    }

    // 绘制图片
    private function paintPhoto($canvas, $photoUrl, $width, $height, $marginTop) {
        $outfile =  __DIR__.'/halfoutput/photo';


        if ($this->downloadImage($photoUrl, $outfile, $width, $height) ) {
            $file['name'] = 'photo';
            $file['tmp_name'] = $outfile;
            $filetype = FileUtil::getFileType($file);
            if ($filetype == FileUtil::FILE_TYPE_PNG) {
                $paintImage = imagecreatefrompng($outfile);
            } else if ($filetype == FileUtil::FILE_TYPE_JPG) {
                $paintImage = imagecreatefromjpeg($outfile);
            } else {
                return;
            }

            list($iwidth, $iheight, $type, $attr) = getimagesize($outfile);

            $startX = ($this->config['photoPageWidth'] - $iwidth) / 2;

            imagecopymerge($canvas, $paintImage, $startX, $marginTop, 0, 0, $iwidth, $iheight, 100);

            return $canvas;
        } else {
            // 图片下载出错
            if ($this->logger != null) {
                $this->logger->error("error when download image:{$url}");
            } else {
                echo "图片下载出错";
            }

            return null;
        }
    }

    // 绘制photoData对象
    private function paintPhotoData($canvas, $photoData, $width, $height, $marginTop) {
        $url = $photoData->url;
        $personNames = $photoData->personNames;
        $desc = $photoData->desc;
        $address = $photoData->address;

        $this->paintPhoto($canvas, $url, $width, $height, $marginTop);
    }

    /**
     * 一页一张图片
     */
    public function paintOnePhoto($photoData) {
        $canvas = imagecreatefrompng(__DIR__.$this->config['totalblankImgPath']);
        $this->paintPhotoData($canvas, $photoData, $this->config['photoMaxWidth'], $this->config['photoOneMaxHeight'], $this->config['photoOneMarginTop']);
        imagepng ($canvas, __DIR__.'/halfoutput/output'.$this->totalPage.'.png');
        imagedestroy($canvas);

        echo "绘制相册第{$this->totalPage}页\n";

        $this->totalPage += 1;
    }


    /**
     * 一页两张图片
     * @param $photos 照片数组
     */
    public function paintTwoPhoto($photos) {
        $canvas = imagecreatefrompng(__DIR__.$this->config['totalblankImgPath']);
        $marginTop = $this->config['photoTwoMarginTop1'];
        $width = $this->config['photoMaxWidth'];
        $height = $this->config['photoTwoMaxHeight'];
        
        foreach($photos as $photo) {
            $this->paintPhotoData($canvas, $photo, $width, $height, $marginTop);
            $marginTop += $this->config['photoTwoMarginTop2'];
        }

        imagepng ($canvas, __DIR__.'/halfoutput/output'.$this->totalPage.'.png');
        imagedestroy($canvas);

        echo "绘制相册第{$this->totalPage}页\n";
        $this->totalPage += 1;
    }

    /**
     * 绘制普通的段落文本
     */
    private function paintNormalText($canvas, $text, $fontSize, $startX, $startY, $width) {
        $len = mb_strlen($text);
        $x = $startX;
        $y = $startY;
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $x = $x + $fontSize + intval($fontSize / 2);

            if ($x > $width + $startX) {
                $x = $startX;
                $y = $y + $fontSize + intval($fontSize / 2);
            }
        }

        return $y + $fontSize + intval($fontSize / 2);
    }

    /**
     * 绘制宗祠
     */
    public function paintGrave($graveData) {
        $canvas = imagecreatefrompng(__DIR__.$this->config['totalblankImgPath']);
        $personName = $graveData->personName;
        $address = $graveData->address;
        $desc = $graveData->desc;
        $photo = $graveData->photo;

        $startX = $this->config['graveStartX'];
        $startY = $this->config['graveStartY'];

        // 绘制宗祠标题
        $startY = $this->paintNormalText($canvas, $personName, $this->config['graveNameFontSize'],  $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠地址
        $startY = $this->paintNormalText($canvas, '地址:'.$address, $this->config['graveAddressFontSize'],  $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠描述
        $startY = $this->paintNormalText($canvas, '描述:'.$desc, $this->config['graveDescFontSize'],  $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠图片
        if ($photo != null) {
            $width = $this->config['graveMaxWidth'];
            $height = $this->config['graveMaxHeight'] - $startY-$this->config['gravePhotoMargin'];
            $tmp = $this->paintPhoto($canvas, $photo, $width, $height, $startY+$this->config['gravePhotoMargin']);

            if ($tmp != null) {
                $canvas = $tmp;
            } else {
                echo "绘制宗祠图片出错";
            }

        }

        imagepng ($canvas, __DIR__.'/halfoutput/output'.$this->totalPage.'.png');
        imagedestroy($canvas);

        echo "绘制宗祠第{$this->totalPage}页\n";
        $this->totalPage += 1;
    }

    /**
     * 绘制族谱的标题
     * @param $canvas 画布
     * @param $title  标题文字
     */
    private function paintTitle($canvas, $title)
    {
        $titleX = $this->config['titleX'];
        $titleY = $this->config['titleY'];
        $fontSize = $this->config['titleFontSize'];

        $x = $titleX;
        $y = $titleY;
        $len = mb_strlen($title);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($title, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $this->config['titleTextYDistance'] + $fontSize;
        }

    }

    /**
     * 绘制页码
     * @param $canvas 画布
     * @param $page   页数
     */
    private function paintPage($canvas, $page)
    {
        $config = $this->config;
        $pageNumLeftX = $config['pageNumLeftX'];
        $pageNumRightX = $config['pageNumRightX'];
        $pageNumY = $config['pageNumY'];
        $pageFontSize = $config['pageFontSize'];
        $pageFontMargin = $config['pageFontMargin'];

        $fontSize = $pageFontSize;

        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = Util::number2chinese($page * 2) . "页";
        } else {
            $pageText = Util::number2chinese($page * 2 - 1) . "页";
        }
        $x = $pageNumLeftX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }

        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = Util::number2chinese($page * 2 - 1) . "页";
        } else {
            $pageText = Util::number2chinese($page * 2) . "页";
        }
        $x = $pageNumRightX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }
    }

    /**
     * 合并半页的图片
     * @param 族谱的标题
     */
    public function merge($titleText)
    {
        $pageCenterWidth = $this->config['pageCenterWidth'];
        $pageWidth = $this->config['pageWidth'];
        $page = 0;
        while (true) {
            $outputPageIndex = ($page / 2 + 1);
            if (DEBUG) {
                echo "合并" . $outputPageIndex . "页" . PHP_EOL;
            }
            $dstCanvas = imagecreatefrompng(__DIR__ . $this->config['allblankImgPath']);
            $this->paintTitle($dstCanvas, $titleText);
            $this->paintPage($dstCanvas, $outputPageIndex);

            if ($this->direction == 1) {
                // 从右往左，顺序颠倒
                $filepath = __DIR__ . "/halfoutput/output" . ($page + 1) . ".png";
            } else {
                $filepath = __DIR__ . "/halfoutput/output{$page}.png";
            }
            if (file_exists($filepath)) {
                // 复制第一页
                $size = getimagesize($filepath);
                $width = $size[0];
                $height = $size[1];

                $srcCanvas = imagecreatefrompng($filepath);
                imagecopy($dstCanvas, $srcCanvas, $this->config['mergePageOneXOffset'], $this->config['mergePageOneYOffset'], 0, 0, $width, $height);
                imagedestroy($srcCanvas);
            } else {
                imagepng($dstCanvas, __DIR__ . "/mergeoutput/output{$outputPageIndex}.png");
            }

            if ($this->direction == 1) {
                // 从右往左，顺序颠倒
                $filepath = __DIR__ . "/halfoutput/output" . ($page) . ".png";
            } else {
                $filepath = __DIR__ . "/halfoutput/output" . ($page + 1) . ".png";
            }
            if (file_exists($filepath)) {
                // 复制第二页
                $size = getimagesize($filepath);
                $width = $size[0];
                $height = $size[1];

                $srcCanvas = imagecreatefrompng($filepath);
                imagecopy($dstCanvas, $srcCanvas, $pageCenterWidth + $pageWidth, $this->config['mergePageTwoYOffset'], 0, 0, $width, $height);
                imagedestroy($srcCanvas);
            } else {
                imagepng($dstCanvas, __DIR__ . "/mergeoutput/output{$outputPageIndex}.png");
                break;
            }

            imagepng($dstCanvas, __DIR__ . "/mergeoutput/output{$outputPageIndex}.png");
            imagedestroy($dstCanvas);
            $page += 2;
        }
    }

    /**
     * 将merge后的图片转换为pdf
     */
    public function toPdf()
    {
        /**************生产预览版***************/
        $mpdf = new Mpdf();

        $html = '';

        $page = 1;
        while (true) {

            $filepath = __DIR__ . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $html .= "<img style='width:100%' src='" . $filepath . "'>";
            $page++;
        }

        $mpdf->WriteHTML($html);
        $mpdf->Output(__DIR__ . "/pdfoutput/preview.pdf", Destination::FILE);

/**************生产打印版***************/

        $mpdf = new Mpdf();

        $html = '';

        $page = 1;
        while (true) {

            $filepath = __DIR__ . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $source = imagecreatefrompng($filepath);
            $rotate = imagerotate($source, 90, 0);

            $tmpFile = __DIR__ . "/pdfoutput/tmp/tmp" . $page . ".png";
            imagepng($rotate, $tmpFile);

            $html .= "<img style='width:100%' src='" . $tmpFile . "'>";
            $page++;
        }

        $mpdf->WriteHTML($html);
        $mpdf->Output(__DIR__ . "/pdfoutput/print.pdf", Destination::FILE);
    }

    /**
     * 清除中间文件
     */
    public function clear()
    {
        $page = 0;
        while (true) {

            $filepath = __DIR__ . "/halfoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = __DIR__ . "/mergeoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = __DIR__ . "/pdfoutput/tmp/tmp" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }
    }

}
