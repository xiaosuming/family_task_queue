<?php
/**
 * 用来存放person的描述的类
 * @author jiangpengfei
 * @date   2018-07-03
 */
namespace std;

class Desc {

    // 绘制在前面的文本,具体在左边还是右边，根据direction得出，如果是从左往右，
    // 则在左边，否则在右边
    public $frontText;     
    
    // 绘制在后面的文本
    public $endText;
}