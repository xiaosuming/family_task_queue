<?php
/**
 * 数据过滤器类，主要负责从所有的人物集合中获取需要的分支
 * @Author: jiangpengfei
 * @Date: 2018-09-25
 */

namespace Util\FamilyTree;

class DataFilter {

    private $personSet;         // 人物的集合
    private $personMap;         // id和人物的对应
    private $startPersonId;     // 分支的起始点, 有这个值的时候，是用户选择导出部分分支
    private $startPersonIdArray;  // 在用户选择全部导出时，会有个起始点的数组
    private $isShowDaughterOffspring;   // 是否显示外戚

    public function __construct(array $persons)
    {
        $this->startPersonId = 0;
        $this->startPersonIdArray = [];
        $this->personSet = $persons;
        
        $minLevel = 99999;
        foreach($persons as $person) {
            $this->personMap[$person['id']] = $person;
            if ($person['level'] < $minLevel) {
                // 辈分更高
                $this->startPersonIdArray = [];
                $this->startPersonIdArray[] = $person['id'];
                $minLevel = $person['level'];
            } else if ($person['level'] == $minLevel) {
                // 辈分相等
                $this->startPersonIdArray[] = $person['id'];
                $minLevel = $person['level'];
            }
        }
    }

    /**
     * 设置配置项，根据配置项进行数据的过滤
     * @param array $options ["isShowDaughterOffspring" => 0]
     */
    public function setOptions($options) {
        $this->isShowDaughterOffspring = $options['isShowDaughterOffspring'] ?? 1;
    }

    public function setStartPersonId(int $startPersonId)
    {
        $this->startPersonId = $startPersonId;
    }

    private function pushValidPerson($corePerson, &$validPersonSet) {

        // 不显示配偶
        $corePerson['spouse'] = [];

        // 检查是否显示外戚
        if ($this->isShowDaughterOffspring == 0 && $corePerson['gender'] == 0) {
            // 不显示外戚
            $corePerson['son'] = [];
            $corePerson['daughter'] = [];
        }
        array_push($validPersonSet, $corePerson);


        $sonIds = $corePerson['son'];
        foreach($sonIds as $sonId) {
            if (isset($this->personMap[$sonId])) {
                $this->pushValidPerson($this->personMap[$sonId], $validPersonSet);
            }
        }

        $daughterIds = $corePerson['daughter'];
        foreach($daughterIds as $daughterId) {
            if (isset($this->personMap[$daughterId])) {
                $this->pushValidPerson($this->personMap[$daughterId], $validPersonSet);
            }
        }
      
    }

    public function getPersonSet() : array {

        if ($this->startPersonId != 0) {
            // 一个分支的导出
            $startPerson = $this->personMap[$this->startPersonId];
            $startPerson['father'] = [];
            $startPerson['mother'] = [];
            $startPerson['sister'] = [];
            $startPerson['brother'] = [];

            $validPersonSet = array();

            $this->pushValidPerson($startPerson, $validPersonSet);

            return $validPersonSet;
        } else {
            // 全部导出
            foreach($this->startPersonIdArray as $startPersonId) {
                $startPerson = $this->personMap[$startPersonId];

                $validPersonSet = array();

                $this->pushValidPerson($startPerson, $validPersonSet);

                return $validPersonSet;
            }
        }
    }
}