<?php
require_once('/home/jiang/projects/family/processor/vendor/autoload.php');

use Util\FamilyTree;
use Util\FamilyTree\PhotoData;

$config = [
    "nodeWidth" => 30,              // 人名处的宽度
    "nodeHeight" => 70,             // 人名处的高度
    "siblingSeparation" => 26,      // 兄弟之间的间隔
    "subtreeSeparation" => 26,      // 隔代之间的间隔
    "spouseSeparation" => 26,       // 配偶之间的间隔
    "levelSeparation" => 40,        // 世代之间的间隔
    "fontSize" => 14,               // 人名字体大小
    "fontXOffset" => 5,             // 绘制人物名字的左偏移量
    "fontYOffset" => 20,            // 绘制人物名字的上偏移
    "xoffset" => 6,                 // 粘贴图片时的偏移
    "yoffset" => 100,               // 粘贴图片时的偏移
    "pageWidth" => 334,             // 每页的宽度,除去世代的部分
    "pageHeight" => 540,            // 每页的高度
    "pageCenterWidth" => 130,       // 每页的中间宽度
    "copyXOffset" => 0,             
    "copyYOffset" => 0,
    "lineWidth" => 1,
    "pageNumLeftX" => 395,          // 左边页码的位置X
    "pageNumRightX" => 430,         // 右边页码的位置X
    "pageNumY" => 380,              // 页码的位置Y
    "pageFontSize" => 10,           // 页码的字体大小   
    "titleX" =>  395,
    "titleY" => 80,
    "titleFontSize" => 30,           // 标题文字的大小
    "pageFontMargin" => 6,           // 页码文字之间的距离
    "levelTextX" => 343,             // 第几世文字的x位置   
    "levelTextY" => 34,              // 第几世文字的y位置
    "levelTextDistance" => 108,       // 第几世文字的距离
    "levelTextFontSize" => 8,
    "descStartX" => 5,            // 描述部分正文的X起点       
    "descStartY" => 114,            // 描述部分正文的Y起点    
    "descNormalFontSize" => 10,     // 描述的普通字体大小
    "descNameFontSize" => 12,       // 描述名字的字体大小
    "descLevelFontSize" => 14       // 辈分的字体大小
];

$familyTree = new FamilyTree($config);

$photo1 = new PhotoData("https://image.izuqun.com/yZyN5xSptB19R1BdEq7m1534927231.jpg?width=690&amp;height=920", ["蒋鹏飞", "大声道", "大叔大婶"], "大叔大婶大所大所", "地址");
$photo2 = new PhotoData("https://image.izuqun.com/eQvBEAQfZUWeemMruYXJ1530773329.png", ["蒋鹏飞2", "大声道2", "大叔大婶2"], "大叔大婶大所大所2", "地址2");

$photos = [$photo1, $photo2];

$familyTree->paintTwoPhoto($photos);