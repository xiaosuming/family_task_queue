<?php

namespace Util\FamilyTree;

class PaintConfig
{

    /**
     * 获取普通清晰度的从左往右的配置文件
     */
    public static function getL2RConfig()
    {
        return [
            "nodeWidth" => 30,              // 人名处的宽度
            "nodeHeight" => 70,             // 人名处的高度
            "siblingSeparation" => 26,      // 兄弟之间的间隔
            "subtreeSeparation" => 26,      // 隔代之间的间隔
            "spouseSeparation" => 26,       // 配偶之间的间隔
            // "spouseXOffset" => 56,       // 画配偶时的偏移
            // "spouseLineXOffset" => 26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 40,        // 世代之间的间隔
            "fontSize" => 12,               // 人名字体大小
            "fontXOffset" => 5,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => 6,                 // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 334,             // 每页的宽度,除去世代的部分
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 0,
            "lineWidth" => 1,
            "pageNumLeftX" => 395,          // 左边页码的位置X
            "pageNumRightX" => 430,         // 右边页码的位置X
            "pageNumY" => 380,              // 页码的位置Y
            "pageFontSize" => 10,           // 页码的字体大小   
            "titleX" => 395,
            "titleY" => 80,
            "titleFontSize" => 30,           // 标题文字的大小
            "pageFontMargin" => 6,           // 页码文字之间的距离
            "levelTextX" => 343,             // 第几世文字的x位置   
            "levelTextY" => 34,              // 第几世文字的y位置
            "levelTextDistance" => 108,       // 第几世文字的距离
            "levelTextFontSize" => 8,
            "descStartX" => 5,            // 描述部分正文的X起点       
            "descStartY" => 114,            // 描述部分正文的Y起点    
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 12,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小

            "extraSpace" => 20,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "pageTextXoffset" => 22, // 见多少页的X轴偏移
            "pageTextYoffset" => -5,  // 见多少页的Y轴偏移
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "titleTextYDistance" => 10,     // 绘制族谱标题的y轴偏移

            "mergePageOneXOffset" => 10,    // 合并图片时第一页X轴偏移
            "mergePageOneYOffset" => 20,    // 合并图片时第一页Y轴偏移
            "mergePageTwoYOffset" => 20,    // 合并图片时第二页Y轴偏移

            "allblankImgPath" => '/allblank.png',
            "deschalfImgPath" => '/deschalf.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',

            'descMaxLen' => 472,            // 描述部分文字的最大长度
            'descTextXStep' => 23,          // 描述文字X轴上每一个的位移距离
            'descLevelTextXOffset' => 3,    // 描述第几世文字的X轴偏移量
            'descLevelTextYOffset' => 40,   // 描述第几世文字的Y轴偏移量
            'descNameTextXOffset' => 4,     // 描述姓名文字的X轴偏移量
            'descNameTextYOffset' => -14,   // 描述姓名文字的Y轴偏移量
            'descNormalTextXOffset' => 6,   // 描述普通文字的X轴偏移量
            'descTextFontYDistance' => 6,   // 描述文字的Y轴上的距离
            'descTopTextFontSize' => 8,     // 顶部的描述文字的大小
            'descTopTextFontDistance' => 3,  // 顶部的描述文字的间距
            'descTopTextYOffset' => -74,    // 顶部的描述文字的y轴偏移


            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,        // 行高
            'eventFontsize' => 14,          // 字体大小
            'eventFontDistance' => 6,       // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取普通清晰度的从右往左的配置文件
     */
    public static function getR2LConfig()
    {
        return [
            "nodeWidth" => 30,              // 人名处的宽度
            "nodeHeight" => 70,             // 人名处的高度
            "siblingSeparation" => 26,      // 兄弟之间的间隔
            "subtreeSeparation" => 26,      // 隔代之间的间隔
            "spouseSeparation" => 26,       // 配偶之间的间隔
            // "spouseXOffset" => -56,       // 画配偶时的偏移
            // "spouseLineXOffset" => -26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 40,        // 世代之间的间隔
            "fontSize" => 12,               // 人名字体大小
            "fontXOffset" => -25,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => 6,                // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 332,             // 每页的宽度
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 0,
            "lineWidth" => 1,
            "pageNumLeftX" => 395,          // 左边页码的位置X
            "pageNumRightX" => 430,         // 右边页码的位置X
            "pageNumY" => 380,              // 页码的位置Y
            "pageFontSize" => 10,           // 页码的字体大小   
            "titleX" => 395,
            "titleY" => 80,
            "titleFontSize" => 30,           // 标题文字的大小
            "pageFontMargin" => 6,           // 页码文字之间的距离
            "levelTextX" => 343,             // 第几世文字的x位置   
            "levelTextY" => 34,              // 第几世文字的y位置
            "levelTextDistance" => 108,       // 第几世文字的距离
            "levelTextFontSize" => 8,         //辈分的字体大小
            "descStartX" => 332,            // 描述部分正文的X起点       
            "descStartY" => 114,            // 描述部分正文的Y起点    
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 12,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小


            "extraSpace" => 20,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "pageTextXoffset" => -15, // 见多少页的X轴偏移
            "pageTextYoffset" => -5,  // 见多少页的Y轴偏移
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "titleTextYDistance" => 10,     // 绘制族谱标题的y轴偏移

            "mergePageOneXOffset" => 10,    // 合并图片时第一页X轴偏移
            "mergePageOneYOffset" => 20,    // 合并图片时第一页Y轴偏移
            "mergePageTwoYOffset" => 20,    // 合并图片时第二页Y轴偏移

            "allblankImgPath" => '/allblank.png',
            "deschalfImgPath" => '/deschalf.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',


            'descMaxLen' => 472,            // 描述部分文字的最大长度
            'descTextXStep' => -23,          // 描述文字X轴上每一个的位移距离
            'descLevelTextXOffset' => 1,    // 描述第几世文字的X轴偏移量
            'descLevelTextYOffset' => 40,   // 描述第几世文字的Y轴偏移量
            'descNameTextXOffset' => 0,     // 描述姓名文字的X轴偏移量
            'descNameTextYOffset' => -14,   // 描述姓名文字的Y轴偏移量
            'descNormalTextXOffset' => 2,   // 描述普通文字的X轴偏移量
            'descTextFontYDistance' => 6,   // 描述文字的Y轴上的距离
            'descTopTextFontSize' => 8,     // 顶部的描述文字的大小
            'descTopTextFontDistance' => 3,  // 顶部的描述文字的间距
            'descTopTextYOffset' => -74,    // 顶部的描述文字的y轴偏移

            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,             // 行高
            'eventFontsize' => 14,               // 字体大小
            'eventFontDistance' => 6,            // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从左往右的配置文件
     */
    public static function getHDL2RConfig()
    {
        return [
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 210,             // 人名处的高度
            "siblingSeparation" => 78,      // 兄弟之间的间隔
            "subtreeSeparation" => 78,      // 隔代之间的间隔
            "spouseSeparation" => 78,       // 配偶之间的间隔
            // "spouseXOffset" => 168,       // 画配偶时的偏移
            // "spouseLineXOffset" => 78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 120,        // 世代之间的间隔
            "fontSize" => 36,               // 人名字体大小
            "fontXOffset" => 22,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => 18,                 // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 1002,             // 每页的宽度,除去世代的部分
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,             
            "copyYOffset" => 0,
            "lineWidth" => 1,
            "pageNumLeftX" => 1185,          // 左边页码的位置X
            "pageNumRightX" => 1290,         // 右边页码的位置X
            "pageNumY" => 1140,              // 页码的位置Y
            "pageFontSize" => 30,           // 页码的字体大小   
            "titleX" =>  1185,
            "titleY" => 240,
            "titleFontSize" => 90,           // 标题文字的大小
            "pageFontMargin" => 18,           // 页码文字之间的距离
            "levelTextX" => 1029,             // 第几世文字的x位置   
            "levelTextY" => 102,              // 第几世文字的y位置
            "levelTextDistance" => 324,       // 第几世文字的距离
            "levelTextFontSize" => 24,
            "descStartX" => 15,            // 描述部分正文的X起点       
            "descStartY" => 342,            // 描述部分正文的Y起点    
            "descNormalFontSize" => 30,     // 描述的普通字体大小
            "descNameFontSize" => 36,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
        
            "extraSpace" => 60,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 12,       // 小的文字的大小差异
            "pageTextXoffset" => 60, // 见多少页的X轴偏移
            "pageTextYoffset" => -15,  // 见多少页的Y轴偏移
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "titleTextYDistance" => 30,     // 绘制族谱标题的y轴偏移
        
            "mergePageOneXOffset" => 30,    // 合并图片时第一页X轴偏移
            "mergePageOneYOffset" => 56,    // 合并图片时第一页Y轴偏移
            "mergePageTwoYOffset" => 56,    // 合并图片时第二页Y轴偏移
        
            "allblankImgPath" => '/allblank_hd.png',
            "deschalfImgPath" => '/deschalf_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
        
        
            'descMaxLen' => 1416,            // 描述部分文字的最大长度
            'descTextXStep' => 69,          // 描述文字X轴上每一个的位移距离
            'descLevelTextXOffset' => 9,    // 描述第几世文字的X轴偏移量
            'descLevelTextYOffset' => 120,   // 描述第几世文字的Y轴偏移量
            'descNameTextXOffset' => 12,     // 描述姓名文字的X轴偏移量
            'descNameTextYOffset' => -42,   // 描述姓名文字的Y轴偏移量
            'descNormalTextXOffset' => 18,   // 描述普通文字的X轴偏移量
            'descTextFontYDistance' => 16,   // 描述文字的Y轴上的距离
            'descTopTextFontSize' => 18,     // 顶部的描述文字的大小
            'descTopTextFontDistance' => 6,  // 顶部的描述文字的间距
            'descTopTextYOffset' => -222,    // 顶部的描述文字的y轴偏移
        
            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离
        
            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距
        
            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从右往左的配置文件
     */
    public static function getHDR2LConfig()
    {
        return [
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 210,             // 人名处的高度
            "siblingSeparation" => 78,      // 兄弟之间的间隔
            "subtreeSeparation" => 78,      // 隔代之间的间隔
            "spouseSeparation" => 78,       // 配偶之间的间隔
            // "spouseXOffset" => -168,       // 画配偶时的偏移
            // "spouseLineXOffset" => -78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 120,        // 世代之间的间隔
            "fontSize" => 36,               // 人名字体大小
            "fontXOffset" => -75,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => 18,                // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 996,             // 每页的宽度
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,             
            "copyYOffset" => 0,
            "lineWidth" => 1,
            "pageNumLeftX" => 1185,          // 左边页码的位置X
            "pageNumRightX" => 1290,         // 右边页码的位置X
            "pageNumY" => 1140,              // 页码的位置Y
            "pageFontSize" => 30,           // 页码的字体大小   
            "titleX" =>  1185,
            "titleY" => 240,
            "titleFontSize" => 90,           // 标题文字的大小
            "pageFontMargin" => 18,           // 页码文字之间的距离
            "levelTextX" => 1029,             // 第几世文字的x位置   
            "levelTextY" => 102,              // 第几世文字的y位置
            "levelTextDistance" => 324,       // 第几世文字的距离
            "levelTextFontSize" => 24,         //辈分的字体大小
            "descStartX" => 996,            // 描述部分正文的X起点       
            "descStartY" => 342,            // 描述部分正文的Y起点    
            "descNormalFontSize" => 30,     // 描述的普通字体大小
            "descNameFontSize" => 36,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
        
            "extraSpace" => 60,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 12,       // 小的文字的大小差异
            "pageTextXoffset" => -45, // 见多少页的X轴偏移
            "pageTextYoffset" => -15,  // 见多少页的Y轴偏移
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "titleTextYDistance" => 30,     // 绘制族谱标题的y轴偏移
        
            "mergePageOneXOffset" => 30,    // 合并图片时第一页X轴偏移
            "mergePageOneYOffset" => 56,    // 合并图片时第一页Y轴偏移
            "mergePageTwoYOffset" => 56,    // 合并图片时第二页Y轴偏移
        
            "allblankImgPath" => '/allblank_hd.png',
            "deschalfImgPath" => '/deschalf_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
        
            'descMaxLen' => 1416,            // 描述部分文字的最大长度
            'descTextXStep' => -69,          // 描述文字X轴上每一个的位移距离
            'descLevelTextXOffset' => 3,    // 描述第几世文字的X轴偏移量
            'descLevelTextYOffset' => 120,   // 描述第几世文字的Y轴偏移量
            'descNameTextXOffset' => 0,     // 描述姓名文字的X轴偏移量
            'descNameTextYOffset' => -42,   // 描述姓名文字的Y轴偏移量
            'descNormalTextXOffset' => 6,   // 描述普通文字的X轴偏移量
            'descTextFontYDistance' => 16,   // 描述文字的Y轴上的距离
            'descTopTextFontSize' => 18,     // 顶部的描述文字的大小
            'descTopTextFontDistance' => 6,  // 顶部的描述文字的间距
            'descTopTextYOffset' => -222,    // 顶部的描述文字的y轴偏移
        
            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离
        
            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距
        
        
            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }
}