<?php

/**
 * 测试绘制宗祠
 */

require_once('../vendor/autoload.php');

use FamilyTreePDF\Model\EventData;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuBigEventPaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;

$eventText = '**哈哈**大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿';

$eventData = new EventData('大事件名', $eventText, '2018-09-09');
$eventData2 = new EventData('大事件名', '大事件内容但是大号是滴啊会松动为安徽神雕华盛顿', '2018-09-09');

$events = [];
$events[] = $eventData;
$events[] = $eventData2;

$config = PaintConfig::getL2RConfig();
$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);

$eventPaint = new TraditionalSuBigEventPaint();
$eventPaint->setContext($context);
$eventPaint->setConfig($config);
$eventPaint->input($events);
$eventPaint->paint();