<?php
/**
 * 测试PDFBuilder
 * @Author: jiangpengfei
 * @Date:   2019-01-10
 */

require_once('../vendor/autoload.php');

use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\SysConst;

$builder = new PDFBuilder(SysConst::$LEFT_TO_RIGHT);

$builder->merge('测试家族');
$builder->toPdf();