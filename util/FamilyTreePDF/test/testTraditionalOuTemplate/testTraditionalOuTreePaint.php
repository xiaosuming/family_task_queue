<?php
/**
 * 测试绘制
 */
require_once('../../../../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuTreePaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig;
use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\SysConst;

//$direction = SysConst::$RIGHT_TO_LEFT;
 $direction = SysConst::$LEFT_TO_RIGHT;

$isHD = false;

$isPageNumDisabled = true;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $config = PaintConfig::getHDR2LConfig();
    } else {
        $config = PaintConfig::getR2LConfig();
    }
    
} else {
    if ($isHD) {
        $config = PaintConfig::getHDL2RConfig();
    } else {
        $config = PaintConfig::getL2RConfig();
    }
    
}

$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/p7.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoTreePaint = new TraditionalOuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);
$xinhuoTreePaint->setMottos([
    "器具质而洁", "瓦缶胜金玉", "饮食约而精", "园蔬愈珍馐"
]);
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装

$xinhuoDescPaint = new TraditionalOuDescPaint();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setDirection($direction);
$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->paint();

$xinhuoTreePaint->paint();


$builder = new PDFBuilder($direction, $isHD);

$total = $builder->merge('张氏家族');
$builder->toPdf($total);

$builder->clear();
