<?php
/**
 * 测试绘制
 */
require_once('../../../../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuTreePaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint3;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig;
use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\SysConst;

$direction = SysConst::$RIGHT_TO_LEFT;
// $direction = SysConst::$LEFT_TO_RIGHT;

$isHD = true;

$isPageNumDisabled = false;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $config = PaintConfig::getHDR2LConfig();
    } else {
        $config = PaintConfig::getR2LConfig();
    }
    
} else {
    if ($isHD) {
        $config = PaintConfig::getHDL2RConfig();
    } else {
        $config = PaintConfig::getL2RConfig();
    }
    
}

$options = [
    'isShowRanking' => 1,
    'showAdoption'=>1,

];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/p6.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoTreePaint = new TraditionalOuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);
$xinhuoTreePaint->setMottos([
    "器具质而洁", "瓦缶胜金玉", "饮食约而精", "园蔬愈珍馐"
]);
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装

$xinhuoDescPaint = new TraditionalOuDescPaint3();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setDirection($direction);
$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->input($persons,true);
$xinhuoDescPaint->paint();
$xinhuoTreePaint->paint();


$builder = new PDFBuilder($direction, $isHD);
//$builder = new A4PDFBuilder($direction, $isHD, false);

$total = $builder->merge('张氏家族', '','',2,'');
$builder->toPdf($total);

$builder->clear();
print_r($GLOBALS['branchName']);
