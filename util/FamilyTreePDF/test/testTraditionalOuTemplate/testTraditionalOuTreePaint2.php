<?php
/**
 * 测试绘制
 */
//require_once('../../vendor/autoload.php');
require_once('../../../../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuTreePaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint2;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig;
use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\SysConst;

//$direction = SysConst::$RIGHT_TO_LEFT;
 $direction = SysConst::$LEFT_TO_RIGHT;

$isHD = true;

$isPageNumDisabled = false;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $config = PaintConfig::getHDR2LConfigForDesc2();
    } else {
        $config = PaintConfig::getR2LConfigForDesc2();
    }
    
} else {
    if ($isHD) {
        $config = PaintConfig::getHDL2RConfigForDesc2();
    } else {
        $config = PaintConfig::getL2RConfigForDesc2();
    }
    
}

$options = [
    'isShowRanking' => 1,
    'showAdoption'=>1,
    'moreSpaceBetweenTopAndBottom'=>0,
//    'pageLength'=>1
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/p3.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoTreePaint = new TraditionalOuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);
$xinhuoTreePaint->setMottos([
    "器具质而洁", "瓦缶胜金玉", "饮食约而精", "园蔬愈珍馐"
]);
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装
//print_r($persons);
$xinhuoDescPaint = new TraditionalOuDescPaint2();
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setDirection($direction);
$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->input($persons,1);
$xinhuoDescPaint->paint();

$xinhuoTreePaint->paint();


//$builder = new PDFBuilder($direction, $isHD);
$builder = new A4PDFBuilder($direction, $isHD, false);


$total = $builder->merge('测试家族');
$builder->toPdf($total,100);

$builder->clear();
