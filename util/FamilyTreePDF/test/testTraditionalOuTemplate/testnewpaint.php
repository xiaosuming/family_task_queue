<?php

require_once('../../../../vendor/autoload.php');
use FamilyTreePDF\Paint\NewPaint\NewPaintByLevel;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Util\PDFBuilder;

$direction = SysConst::$RIGHT_TO_LEFT;
// $direction = SysConst::$LEFT_TO_RIGHT;

$isHD = true;
$hd = $isHD;

$isPageNumDisabled = false;
$treePaint = new NewPaintByLevel();
$generation = 5;
$content = file_get_contents(__DIR__ . "/p6.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];


if ($direction == SysConst::$LEFT_TO_RIGHT) {
    if ($isHD) {
        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDL2RConfig();
    } else {
        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getL2RConfig();
    }
} else {
    if ($isHD) {
        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDR2LConfig();
    } else {
        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getR2LConfig();
    }
}

$treePaint->setConfig($config);
$treePaint->setGeneration($generation);
$treePaint->input($persons);
$treePaint->setFamilyName('家族名称');
$treePaint->setGeneration($generation);
$countPDF = $treePaint->paint();

$builder = new PDFBuilder($direction, $hd);
$others['ChildName'] = '子代名称';
$builder->toCover('姓氏', 'c', 'p', 'a');
$builder->toLevelPdf(0, $countPDF);
$builder->clear();