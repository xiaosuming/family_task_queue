<?php
/**
 * 测试绘制所有
 */
require_once('../../../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPaintAll;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;
use FamilyTreePDF\Util\SysConst;


$direction = SysConst::$RIGHT_TO_LEFT;

if ($direction == SysConst::$LEFT_TO_RIGHT) {
    $config = PaintConfig::getHDL2RConfig();
} else {
    $config = PaintConfig::getHDR2LConfig();
}

$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/mqb.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoBasePaintAll = new TraditionalSuPaintAll();
$xinhuoBasePaintAll->setContext($context);
$xinhuoBasePaintAll->setConfig($config);
// 设置绘制方向
$xinhuoBasePaintAll->setDirection($direction);

$xinhuoBasePaintAll->input($persons);
$xinhuoBasePaintAll->paint();

