<?php
require_once('../vendor/autoload.php');

use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\SysConst;


$builder = new PDFBuilder(SysConst::$LEFT_TO_RIGHT);

$builder->clear();