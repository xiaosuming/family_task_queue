<?php

/**
 * 测试绘制宗祠
 */

require_once('../vendor/autoload.php');

use FamilyTreePDF\Model\GraveData;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuGravePaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;

$graveData = new GraveData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈', '上海闵行');

$config = PaintConfig::getL2RConfig();
$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);

$gravePaint = new TraditionalSuGravePaint();
$gravePaint->setContext($context);
$gravePaint->setConfig($config);
$gravePaint->input($graveData);
$gravePaint->paint();