<?php
/**
 * 测试绘制
 */
require_once('../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuDescPaint;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuTreePaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;

$config = PaintConfig::getL2RConfig();
$direction = 0;
$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/person.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoTreePaint = new TraditionalSuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装

$xinhuoDescPaint = new TraditionalSuDescPaint();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoDescPaint->paint();

$xinhuoTreePaint->paint();
