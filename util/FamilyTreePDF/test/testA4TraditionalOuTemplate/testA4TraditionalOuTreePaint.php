<?php
/**
 * 将所有的部分结合起来绘制
 */
require_once('../../vendor/autoload.php');

use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuDescPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuTreePaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Model\EventData;
use FamilyTreePDF\Model\GraveData;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Model\PhotoData;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuBigEventPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuGravePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoOnePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoTwoPaint;


// 绘制方向
// $direction = SysConst::$RIGHT_TO_LEFT;
$direction = SysConst::$LEFT_TO_RIGHT;

$isHD = false;

$isPageNumDisabled = false;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $config = PaintConfig::getHDR2LConfig();
    } else {
        $config = PaintConfig::getR2LConfig();
    }
    
} else {
    if ($isHD) {
        $config = PaintConfig::getHDL2RConfig();
    } else {
        $config = PaintConfig::getL2RConfig();
    }
    
}

$options = [
    'isShowRanking' => 0
];

$context = new PaintContext();
$context->setOptions($options);


$content = file_get_contents(__DIR__ . "/person_sidebar.json");
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$xinhuoTreePaint = new A4TraditionalOuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装

$xinhuoDescPaint = new A4TraditionalOuDescPaint();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($config);
$xinhuoDescPaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->paint();

$xinhuoTreePaint->paint();

// 大事件
$eventText = '**哈哈**大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿';

$eventData = new EventData('大事件名', $eventText, '2018-09-09');
$eventData2 = new EventData('大事件名', '大事件内容但是大号是滴啊会松动为安徽神雕华盛顿', '2018-09-09');

$events = [];
$events[] = $eventData;
$events[] = $eventData2;

$eventPaint = new A4TraditionalOuBigEventPaint();
$eventPaint->setContext($context);
$eventPaint->setConfig($config);
$eventPaint->input($events);
$eventPaint->paint();

// 单张相册
$photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');

$photoOnePaint = new A4TraditionalOuPhotoOnePaint();
$photoOnePaint->setContext($context);
$photoOnePaint->setConfig($config);
$photoOnePaint->input($photoData);
$photoOnePaint->paint();

// 两张相册
$photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');
$photoData2 = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');

$photos = [];
$photos[] = $photoData;
$photos[] = $photoData2;

$photoTwoPaint = new A4TraditionalOuPhotoTwoPaint();
$photoTwoPaint->setContext($context);
$photoTwoPaint->setConfig($config);
$photoTwoPaint->input($photos);
$photoTwoPaint->paint();


// 宗祠
$graveData = new GraveData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈', '上海闵行');
$gravePaint = new A4TraditionalOuGravePaint();
$gravePaint->setContext($context);
$gravePaint->setConfig($config);
$gravePaint->input($graveData);
$gravePaint->paint();


$builder = new A4PDFBuilder($direction, $isHD);

$total = $builder->merge('测试家族');
$builder->toPdf($total);

$builder->clear();
