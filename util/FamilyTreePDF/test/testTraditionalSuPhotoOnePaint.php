<?php
/**
 * 测试单张相册的绘制
 */

require_once('../vendor/autoload.php');

use FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPhotoOnePaint;
use FamilyTreePDF\Model\PhotoData;

$config = PaintConfig::getL2RConfig();
$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);

$photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');

$photoOnePaint = new TraditionalSuPhotoOnePaint();
$photoOnePaint->setContext($context);
$photoOnePaint->setConfig($config);
$photoOnePaint->input($photoData);
$photoOnePaint->paint();