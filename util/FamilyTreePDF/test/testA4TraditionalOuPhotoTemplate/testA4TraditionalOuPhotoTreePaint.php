<?php
/**
 * 将所有的部分结合起来绘制
 */
require_once('../../vendor/autoload.php');

use FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\A4TraditionalOuPhotoDescPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuTreePaint;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Model\EventData;
use FamilyTreePDF\Model\GraveData;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Model\PhotoData;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuBigEventPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuGravePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoOnePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoTwoPaint;
use FamilyTreePDF\Util\DataFilter;


// 绘制方向
$direction = SysConst::$RIGHT_TO_LEFT;
// $direction = SysConst::$LEFT_TO_RIGHT;

//添加是否导出吊线图 全局
$addTree = false;
//end

$isHD = false;

$isPageNumDisabled = false;

if ($direction == SysConst::$RIGHT_TO_LEFT) {
    if ($isHD) {
        $photoDescConfig = FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getHDR2LConfig();
        $config = FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
    } else {
        $photoDescConfig = FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getR2LConfig();
        $config = FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
    }
    
} else {
    if ($isHD) {
        $photoDescConfig = FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getHDL2RConfig();
        $config = FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
    } else {
        $photoDescConfig = FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getL2RConfig();
        $config = FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
    }
    
}

$options = [
    'isShowRanking' => 1
];

$context = new PaintContext();
$context->setOptions($options);


// $content = file_get_contents(__DIR__ . "/../199person.json");
//$content = file_get_contents('person_sidebar.json');
$content = file_get_contents('633.json');
$persons = json_decode($content, true);
$persons = $persons['data']['persons'];

$dataFilter = new DataFilter($persons);
//添加startPersonId 和 isShowStartPersonFathers
$dataFilter->setOptions($options);
$persons = $dataFilter->getPersonSet();

$xinhuoTreePaint = new A4TraditionalOuTreePaint();
$xinhuoTreePaint->setContext($context);
$xinhuoTreePaint->setConfig($config);
$xinhuoTreePaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoTreePaint->input($persons);                  // 这里的input必须先执行。这个部分也可以单独提取出来封装
if($addTree !== true){
    # 去掉吊线图
    $context->setTotalPage(0);
}
$xinhuoDescPaint = new A4TraditionalOuPhotoDescPaint();
$xinhuoDescPaint->setContext($context);
$xinhuoDescPaint->setConfig($photoDescConfig);
$xinhuoDescPaint->setDirection($direction);         // 必须要设置排版方向
$xinhuoDescPaint->setPageNum($isPageNumDisabled);
$xinhuoDescPaint->paint();

// $xinhuoTreePaint->paint();
if($addTree === true){
    $xinhuoTreePaint->paint();
}

// // 大事件
// $eventText = '**哈哈**大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿大事件内容但是大号是滴啊会松动为安徽神雕华盛顿';

// $eventData = new EventData('大事件名', $eventText, '2018-09-09');
// $eventData2 = new EventData('大事件名', '大事件内容但是大号是滴啊会松动为安徽神雕华盛顿', '2018-09-09');

// $events = [];
// $events[] = $eventData;
// $events[] = $eventData2;

// $eventPaint = new A4TraditionalOuBigEventPaint();
// $eventPaint->setContext($context);
// $eventPaint->setConfig($config);
// $eventPaint->input($events);
// $eventPaint->paint();

// // 单张相册
// $photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');

// $photoOnePaint = new A4TraditionalOuPhotoOnePaint();
// $photoOnePaint->setContext($context);
// $photoOnePaint->setConfig($config);
// $photoOnePaint->input($photoData);
// $photoOnePaint->paint();

// // 两张相册
// $photoData = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');
// $photoData2 = new PhotoData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈哈', '上海闵行');

// $photos = [];
// $photos[] = $photoData;
// $photos[] = $photoData2;

// $photoTwoPaint = new A4TraditionalOuPhotoTwoPaint();
// $photoTwoPaint->setContext($context);
// $photoTwoPaint->setConfig($config);
// $photoTwoPaint->input($photos);
// $photoTwoPaint->paint();


// // 宗祠
// $graveData = new GraveData('https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmIIGAHLV9AACamJVbrxQ690.jpg', '江鹏飞', '哈哈哈哈', '上海闵行');
// $gravePaint = new A4TraditionalOuGravePaint();
// $gravePaint->setContext($context);
// $gravePaint->setConfig($config);
// $gravePaint->input($graveData);
// $gravePaint->paint();


$builder = new A4PDFBuilder($direction, $isHD);

$total = $builder->merge('司马氏家族');
$builder->toPdf($total);

$builder->clear();