<?php

namespace FamilyTreePDF\BaseInterface;

use FamilyTreePDF\Paint\PaintContext;

interface PaintInterface {

    function setContext(PaintContext $context);             // 设置绘制时的上下文环境

    function setConfig($config);

    function input($paintData);     // 输入要绘制的绘制数据

    function paint();        // 在canvas上绘图
}