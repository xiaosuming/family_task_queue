<?php

/**
 * 鑫火单张图片的绘制封装
 * @Author: jiangpengfei
 * @Date: 2019-01-04
 */
namespace FamilyTreePDF\Paint\TraditionalSuTemplate;

use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\FileUtil;


class TraditionalSuPhotoOnePaint implements PaintInterface
{

    private $photoData;
    private $config;
    private $totalPage;
    private $logger;

    public function __construct()
    {

    }

    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef();
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function input($photoData)
    {
        $this->photoData = $photoData;
    }

        // 绘制图片
    private function paintPhoto($canvas, $photoUrl, $width, $height, $marginTop)
    {
        $outfile = ResourceManager::$HALFOUTPUT_PATH. '/photo';


        if (PaintUtil::downloadImage($photoUrl, $outfile, $width, $height)) {
            $file['name'] = 'photo';
            $file['tmp_name'] = $outfile;
            $filetype = FileUtil::getFileType($file);
            if ($filetype == FileUtil::FILE_TYPE_PNG) {
                $paintImage = imagecreatefrompng($outfile);
            } else if ($filetype == FileUtil::FILE_TYPE_JPG) {
                $paintImage = imagecreatefromjpeg($outfile);
            } else {
                echo '不支持的图片格式';
                return;
            }

            list($iwidth, $iheight, $type, $attr) = getimagesize($outfile);

            $startX = ($this->config['photoPageWidth'] - $iwidth) / 2;

            imagecopymerge($canvas, $paintImage, $startX, $marginTop, 0, 0, $iwidth, $iheight, 100);

            return $canvas;
        } else {
                // 图片下载出错
            if ($this->logger != null) {
                $this->logger->error("error when download image:{$url}");
            } else {
                echo "图片下载出错";
            }

            return null;
        }
    }
    
        // 绘制photoData对象
    private function paintPhotoData($canvas, $photoData, $width, $height, $marginTop)
    {
        $url = $photoData->url;
        $personNames = $photoData->personNames;
        $desc = $photoData->desc;
        $address = $photoData->address;

        $this->paintPhoto($canvas, $url, $width, $height, $marginTop);
    }

    public function paint()
    {
        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['totalblankImgPath']);
        $this->paintPhotoData($canvas, $this->photoData, $this->config['photoMaxWidth'], $this->config['photoOneMaxHeight'], $this->config['photoOneMarginTop']);
        imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . '/output' . $this->totalPage . '.png');
        imagedestroy($canvas);

        $time = date('h:i:s');
        echo "绘制相册第{$this->totalPage}页, {$time}\n";

        $this->totalPage += 1;
    }
}