<?php
/**
 * 
 */
namespace FamilyTreePDF\Paint\TraditionalSuTemplate;

use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\FontManager;

class TraditionalSuBigEventPaint {

    private $config;
    private $events;
    private $totalPage;
    private $normalTextFont;

    public function __construct()
    {
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);        
    }

    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    public function setContext(PaintContext $context) {
        
        $this->totalPage = &$context->getTotalPageRef();
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function input($events) {
        $this->events = $events;
    }

    public function paint() {
        $options = [
            "paddingLeft" => $this->config['eventPaddingLevel'], // 20
            "paddingRight" => $this->config['eventPaddingRight'], // 20
            "paddingTop" => $this->config['eventPaddingTop'],  // 40
            "paddingBottom" => $this->config['eventPaddingBottom'], // 40
            "lineheight" => $this->config['eventLineheight'],   // 30
            "fontsize" => $this->config['eventFontsize'],      // 14
            "fontDistance" => $this->config['eventFontDistance'], // 字体间距
          ];
        foreach($this->events as $event) {
            $title = $event->expName;
            $content = $event->expContent;
            $time = $event->expTime;

            if ($time == "0001-01-01") {
                $time = "时间不详";
            }

            $all = '# '.$title."\n\n**".$time."**\n\n".$content;
            // 参数: 字体路径(char*), 风格(0,1), 输出的起始索引(int), 输出路径(char*), 内容(char*)
            $pages = md2pic($this->normalTextFont, 1, $this->totalPage, ResourceManager::$HALFOUTPUT_PATH.'/', $all, $this->config['eventPageWidth'], $this->config['eventPageHeigth'],$options);
            
            $time = date('h:i:s');
            echo "绘制大事件".$this->totalPage.", {$time}".PHP_EOL;
            $this->totalPage += $pages;

            
        }
    }

}