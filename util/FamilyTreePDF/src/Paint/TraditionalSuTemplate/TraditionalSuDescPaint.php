<?php

/**
 * 行传的绘制，基本模板
 */
namespace FamilyTreePDF\Paint\TraditionalSuTemplate;

use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\FontManager;
use FamilyTreePDF\Util\Util;

class TraditionalSuDescPaint implements PaintInterface
{

    private $config;
    private $totalPage;
    private $personMap;
    private $descPageMap;
    private $descDataMap;       // 记录某个人处理的descData
    private $minLevel;
    private $maxLevel;
    private $allLevelList;
    private $calculatePage; //计算用的页数变量

    private $direction; // 排版方向,
    private $options;
    private $normalTextFont;    // 正文字体
    private $isPageNumDisabled;

    public function __construct()
    {
        $this->direction = SysConst::$LEFT_TO_RIGHT;
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
        $this->descDataMap = [];
        $this->isPageNumDisabled = false;
    }

    public function setFont($font) {
        $this->normalTextFont = $font;
    }
    
    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }


    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef();
        $this->personMap = &$context->getPersonMapRef();
        $this->descPageMap = &$context->getDescPageMapRef();
        $this->minLevel = &$context->getMinLevelRef();
        $this->maxLevel = &$context->getMaxLevelRef();
        $this->allLevelList = &$context->getAllLevelListRef();

        $this->options = $context->getOptions();
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function input($paintData)
    {

    }

    /**
     * 是否开启页码
     * @param $disabled 是否禁用 true禁用
     */
    public function setPageNum($disabled) 
    {
        $this->isPageNumDisabled = $disabled;
    }

    /**
     * 获取一个人物的排行称呼，如长子，次子，长女等
     * @param $person 人物信息
     */
    private function getRanking($person)
    {
        // 先检查人物是否有ranking
        if ($person['ranking'] > 0) {
            return PaintUtil::convertRanking($person['ranking'], $person['gender']);
        }

        // 没有排行，目前根据加入的顺序来判断
        $siblings = array_merge($person['brother'], $person['sister']);

        sort($siblings);

        foreach ($siblings as $index => $personId) {
            if ($person['id'] == $personId) {
                return PaintUtil::convertRanking($index + 1, $person['gender']);
            }
        }
    }

    /**
     * 绘制页码数字
     */
    private function paintPageNum(&$canvas, &$x, &$y, $pageNum)
    {
        imagettftext($canvas, $this->config["descPageNumFontSize"], 0, $x-2, $y - $this->config["descPageNumFontSize"], 0x000000, $this->normalTextFont, '('.$pageNum.')');
        
        $y += $this->config['descTextFontYDistance'] + $this->config['fontSize'];
        
        if ($y > $this->config['descMaxLen']) {
            // 到达底部并且不是最后一个字，换行
            $x += $this->config['descTextXStep'];
            $y = $this->config['descStartY'];
        }

        if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
            $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                    // 重开一页
            imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
            $time = date('h:i:s');
            echo "绘制描述第{$this->totalPage}页, {$time}\n";
            $this->totalPage++;
            $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['deschalfImgPath']);
            $x = $this->config['descStartX'];
            $y = $this->config['descStartY'];
        }
    }

    /**
     * 绘制人物描述页的文字
     * @param $canvas 画布
     * @param $x    绘制的横坐标
     * @param $y    绘制的纵坐标
     * @param $desc 绘制的名字
     * @param $type 类型，1是世代，2是名字，3是普通描述
     * @param $bold 是否是粗体，默认是
     */
    private function paintDescText(&$canvas, &$x, &$y, $desc, $type, $bold = true, $topDesc = '')
    {
        $maxLen = $this->config['descMaxLen'];
        $xLimit = 0;

        $xStep = $this->config['descTextXStep'];

        switch ($type) {
            case 1:
                $fontSize = $this->config['descLevelFontSize'];
                $xOffset = $this->config['descLevelTextXOffset'];
                $yOffset = $this->config['descLevelTextYOffset'];
                break;
            case 2:
                $fontSize = $this->config['descNameFontSize'];
                $xOffset = $this->config['descNameTextXOffset'];
                $yOffset = $this->config['descNameTextYOffset'];
                break;
            case 3:
                $yOffset = 0;
                $xOffset = $this->config['descNormalTextXOffset'];

                $fontSize = $this->config['descNormalFontSize'];
                break;
        }

        if ($type == 2 && $topDesc != '') {
            // echo "绘制topDesc---------------------------\n";
            // 要绘制顶部的长子之类的
            $len = mb_strlen($topDesc);
            $tempY = $y;
            for ($i = 0; $i < $len; $i++) {
                $c = mb_substr($topDesc, $i, 1);
                imagettftext(
                    $canvas,
                    $this->config['descTopTextFontSize'],
                    0,
                    $x + $xOffset,
                    $tempY + $yOffset + $this->config['descTopTextYOffset'],
                    0x000000,
                    $this->normalTextFont,
                    $c
                );


                $tempY = $tempY + $this->config['descTopTextFontDistance'] + $this->config['descTopTextFontSize'];
            }

        }

        $len = mb_strlen($desc);

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($desc, $i, 1);
            imagettftext(
                $canvas,
                $fontSize,
                0,
                $x + $xOffset,
                $y + $yOffset,
                0x000000,
                $this->normalTextFont,
                $c
            );

            if ($bold) {
                imagettftext(
                    $canvas,
                    $fontSize,
                    0,
                    $x + 1 + $xOffset,
                    $y + $yOffset,
                    0x000000,
                    $this->normalTextFont,
                    $c
                );
            }


            $y = $y + $this->config['descTextFontYDistance'] + $this->config['fontSize'];

            if ($i < $len - 1 && $y > $maxLen) {
                // 到达底部并且不是最后一个字，换行
                $x += $xStep;
                $y = $this->config['descStartY'];

                if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                    $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                    // 重开一页
                    // 先保存当前的
                    imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                    $time = date('h:i:s');
                    echo "绘制描述第{$this->totalPage}页, {$time}\n";
                    $this->totalPage++;
                    $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['deschalfImgPath']);
                    $x = $this->config['descStartX'];
                    $y = $this->config['descStartY'];
                }
            }
        }

        // 写完一次后，必须换行
        if ($type == 1 || $type == 2) {
            $x += $xStep;
            $y = $this->config['descStartY'];
            if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                // 重开一页
                imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                $time = date('h:i:s');
                echo "绘制描述第{$this->totalPage}页, {$time}\n";
                $this->totalPage++;
                $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['deschalfImgPath']);
                $x = $this->config['descStartX'];
                $y = $this->config['descStartY'];
            }
        }
    }

    /**
     * 计算描述的页码，主要目的是计算每个人物所在的页码
     * @param $x    绘制的横坐标
     * @param $y    绘制的纵坐标
     * @param $desc 绘制的名字
     * @param $type 类型，1是世代，2是名字，3是普通描述
     * @param $bold 是否是粗体，默认是
     */
    private function calculateDesc(&$x, &$y, $desc, $type, $bold = true)
    {
        $maxLen = $this->config['descMaxLen'];
        $xLimit = 0;
        $initPage = $this->calculatePage;
        $xStep = $this->config['descTextXStep'];

        switch ($type) {
            case 1:
                $fontSize = $this->config['descLevelFontSize'];
                $xOffset = $this->config['descLevelTextXOffset'];
                $yOffset = $this->config['descLevelTextYOffset'];
                break;
            case 2:
                $fontSize = $this->config['descNameFontSize'];
                $xOffset = $this->config['descNameTextXOffset'];
                $yOffset = $this->config['descNameTextYOffset'];
                break;
            case 3:
                $yOffset = 0;
                $xOffset = $this->config['descNormalTextXOffset'];

                $fontSize = $this->config['descNormalFontSize'];
                break;
        }

        $len = mb_strlen($desc);

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($desc, $i, 1);

            $y = $y + $this->config['descTextFontYDistance'] + $this->config['fontSize'];

            if ($i < $len - 1 && $y > $maxLen) {
                // 到达底部并且不是最后一个字，换行
                $x += $xStep;
                $y = $this->config['descStartY'];

                if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                    $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                    $this->calculatePage++;
                    $x = $this->config['descStartX'];
                    $y = $this->config['descStartY'];
                }
            }
        }

        // 写完一次后，必须换行

        if ($type == 1 || $type == 2) {
            $x += $xStep;
            $y = $this->config['descStartY'];
            if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                // 重开一页
                $this->calculatePage++;
                $x = $this->config['descStartX'];
                $y = $this->config['descStartY'];
            }
        }

        return $initPage;
    }


    public function paint($setLevel = 1, $setDefaultDesc = 0)
    {
        $placeHolderText = '@'; // 页码的占位符
        if ($this->isPageNumDisabled) {
            $placeHolderText = '';
        }

        $calculateX = $this->config['descStartX'];
        $calculateY = $this->config['descStartY'];

        // 先计算一遍
        $this->calculatePage = $this->totalPage + 1;
        $this->RankingTxt = [];
        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {
            $level = $this->allLevelList[$key];
            $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

            $this->calculateDesc($calculateX, $calculateY, $levelText, 1);

            $tmpLevel = array_reverse($level);  // 倒序

            foreach ($tmpLevel as $person) {
                // 生于
                // 出生地
                // 孩子
                // 妻/夫
                $children = array();
                $childrenId = array();
                $spouse = '';
                   
                if($setDefaultDesc == 1){
                    $birthday = "出生日期不详 ";
                    $address = "出生地不详 ";
                }else{
                    $birthday = '';
                    $address = '';
                }
                if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                    $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                }

                if ($person['address'] != '') {
                    $address = "出生地" . $person['address'] . " ";
                }

                if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                    $children[] = "";//无子女
                    $childrenId[] = 0;
                } else if (count($person['son']) == 0) {
                    $total = count($person['daughter']);
                    $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";
                    $childrenId[] = 0;

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                } else if (count($person['daughter']) == 0) {
                    $total = count($person['son']);
                    $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";
                    $childrenId[] = 0;

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                } else {
                    $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子";
                    $childrenId[] = 0;

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }

                    $children[] = "，生".PaintUtil::number2chinese(count($person['daughter']))."女";
                    $childrenId[] = 0;

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                }

                // 配偶
                foreach ($person['spouse'] as $personId) {
                    if ($person['gender'] == 0) {
                        //女
                        $spouse .= "夫" . $this->personMap[$personId]['name'] . " ";
                    } else {
                        $spouse .= "妻" . $this->personMap[$personId]['name'] . " ";
                    }
                }

                // 当前人物的描述已经好了
                $name = $person['name'];
                $person['profileText'] = trim($person['profileText']);
                $personPage = $this->calculateDesc($calculateX, $calculateY, $name, 2);
                $this->descPageMap[$person['id']] = $personPage;


                //添加自己在父亲的排行信息
                if ($this->options['isShowRanking'] == 1) {
                    // 绘制排行
                    if (
                        isset($person['father']) && count($person['father']) > 0
                        && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                    ) {
                        // 父亲是本家人
                        $this->RankingTxt[$person['id']]['name'] = $this->personMap[$person['father'][0]]['name'];
                        $this->RankingTxt[$person['id']]['rank'] = PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        $this->RankingTxtId[$person['id']] = $person['father'][0];
                        $txt = $this->RankingTxt[$person['id']]['name'] . $placeHolderText . $this->RankingTxt[$person['id']]['rank'];
                        $this->calculateDesc($calculateX, $calculateY, $txt, 3);
                    } else if (
                        isset($person['mother']) && count($person['mother']) > 0
                        && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                    ) {
                        // 母亲是本家人
                        $this->RankingTxt[$person['id']]['name'] = $this->personMap[$person['mother'][0]]['name'];
                        $this->RankingTxt[$person['id']]['rank'] = PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        $this->RankingTxtId[$person['id']] = $person['mother'][0];
                        $txt = $this->RankingTxt[$person['id']]['name'] . $placeHolderText . $this->RankingTxt[$person['id']]['rank'];
                        $this->calculateDesc($calculateX, $calculateY, $txt, 3);
                    } else {
                        $this->RankingTxt[$person['id']]= [];
                        $this->RankingTxtId[$person['id']] = 0;
                    }
                }

                if ($person['profileText'] != '') {

                    $descData = PaintUtil::processProfileText($person, $this->personMap);

                    $extraText = '';
                    foreach($descData as $item) {
                        $extraText .= $placeHolderText;
                    }

                    $this->calculateDesc($calculateX, $calculateY, $person['profileText'].$extraText, 3, false);

                    $this->descDataMap[$person['id']] = $descData;
                } else {

                    $this->calculateDesc($calculateX, $calculateY, $birthday, 3, false);
                    $this->calculateDesc($calculateX, $calculateY, $address, 3, false);

                    // 孩子部分特殊计算
                    $childrenLen = count($children);

                    if ($childrenLen == 1) {    // 没有孩子
                        $this->calculateDesc($calculateX, $calculateY, $children[0], 3, false);
                    } else {
                        $isFirst = true;
                        for ($i = 0; $i < $childrenLen; ) {
                            if ($isFirst) {
                                $paintText = $children[$i];

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1] . $placeHolderText;  // 这里的7文字仅仅用作占位计算，无任何意义.页码横写，占一个字符的高度, 改成$placeHolderText
                                }

                                if (isset($children[$i + 2])) {
                                    $paintText .= $children[$i + 2] . $placeHolderText;
                                }

                                // 计算文字占用范围
                                $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);

                                $i = $i + 3;
                                $isFirst = false;
                            } else {
                                $paintText = $children[$i] . $placeHolderText;

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1] . $placeHolderText;
                                }

                                $this->calculateDesc($calculateX, $calculateY, $paintText, 3, false);
                                $i = $i + 2;
                            }
                        }
                    }

                    if ($spouse != '') {
                        $this->calculateDesc($calculateX, $calculateY, $spouse, 3, false);
                    }
                }

                // 一个人写完了，换行
                $calculateX += $this->config['descTextXStep'];;
                $calculateY = $this->config['descStartY'];
                if ($calculateX < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                    $calculateX > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                    // 重开一页
                    $this->calculatePage++;
                    $calculateX = $this->config['descStartX'];
                    $calculateY = $this->config['descStartY'];
                }
            }
        }

        // 实际绘制 ****************************************************************************
        $x = $this->config['descStartX'];
        $y = $this->config['descStartY'];
        $canvas = null;
        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['deschalfImgPath']);

        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {
            $level = $this->allLevelList[$key];
            $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

            $this->paintDescText($canvas, $x, $y, $levelText, 1);

            $tmpLevel = array_reverse($level);

            foreach ($tmpLevel as $person) {
                // 生于
                // 出生地
                // 孩子
                // 妻/夫
                $children = array();
                $childrenId = array();
                $spouse = '';

                   
                if($setDefaultDesc == 1){
                    $birthday = "出生日期不详 ";
                    $address = "出生地不详 ";
                }else{
                    $birthday = '';
                    $address = '';
                }
                if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                    $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                }

                if ($person['address'] != '') {
                    $address = "出生地" . $person['address'] . " ";
                }

                if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                    $children[] = "";//无子女
                    $childrenId[] = 0;
                } else if (count($person['son']) == 0) {
                    $total = count($person['daughter']);
                    $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";
                    $childrenId[] = 0;

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                } else if (count($person['daughter']) == 0) {
                    $total = count($person['son']);
                    $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";
                    $childrenId[] = 0;

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                } else {
                    $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子";
                    $childrenId[] = 0;

                    foreach ($person['son'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }

                    $children[] = "，生".PaintUtil::number2chinese(count($person['daughter']))."女";
                    $childrenId[] = 0;

                    foreach ($person['daughter'] as $personId) {
                        $children[] = $this->personMap[$personId]['name'];
                        $childrenId[] = $personId;
                    }
                }

                // 配偶
                foreach ($person['spouse'] as $personId) {
                    if ($person['gender'] == 0) {
                        //女
                        $spouse .= "夫" . $this->personMap[$personId]['name'] . " ";
                    } else {
                        $spouse .= "妻" . $this->personMap[$personId]['name'] . " ";
                    }
                }

                // 当前人物的描述已经好了
                $name = $person['name'];

                // 排行的绘制
                if (isset($this->options['isShowRanking']) && $this->options['isShowRanking'] == 1) {
                    // 绘制排行
                    if (isset($person['father']) && count($person['father']) > 0
                        && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1) {
                        // 父亲是本家人

                        $fatherName = $this->personMap[$person['father'][0]]['name'];
                        $this->paintDescText($canvas, $x, $y, $name, 2, true, $fatherName . ' ' . PaintUtil::getRanking($person, $this->options['showAdoption']));
                    } else if (isset($person['mother']) && count($person['mother']) > 0
                        && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1) {
                        // 母亲是本家人
                        $motherName = $this->personMap[$person['mother'][0]]['name'];
                        $this->paintDescText($canvas, $x, $y, $name, 2, true, $motherName . ' ' . PaintUtil::getRanking($person, $this->options['showAdoption']));

                    } else {
                        $this->paintDescText($canvas, $x, $y, $name, 2, true);
                    }
                } else {
                    // 不绘制排行
                    $this->paintDescText($canvas, $x, $y, $name, 2, true);
                }

                // 绘制详情页人物排行, 无论是否自己填写过详情都添加在行首. RankingTxt会根据isShowRanking参数生成
                if(isset($this->RankingTxt[$person['id']]) && !empty($this->RankingTxt[$person['id']])){
                    $rankname = $this->RankingTxt[$person['id']]['name'];
                    $this->paintDescText($canvas, $x, $y, $rankname, 3, false);
                    if (!$this->isPageNumDisabled) {
                        $ranknum = $this->descPageMap[$this->RankingTxtId[$person['id']]];
                        $this->paintPageNum($canvas, $x, $y, $ranknum);
                    }
                    $rank = $this->RankingTxt[$person['id']]['rank'];
                    $this->paintDescText($canvas, $x, $y, $rank, 3, false);

                }

                $person['profileText'] = trim($person['profileText']);

                if ($person['profileText'] != '') {
                    $profileText = $person['profileText'];
                    $prev = 0;
                    if (isset($this->descDataMap[$person['id']])) {
                        foreach($this->descDataMap[$person['id']] as $item) {
                            $pageNum = $this->descPageMap[$item['id']];
                            $pos = $item['pos'];
                            $tmpText = mb_substr($profileText, $prev, $pos-$prev);
                            $this->paintDescText($canvas, $x, $y, $tmpText, 3, false);

                            $prev = $pos;
                            if (!$this->isPageNumDisabled) {
                                $this->paintPageNum($canvas, $x, $y, $this->descPageMap[$item['id']]);
                            }
                            
                        }

                        $tmpText = mb_substr($profileText, $prev);
                        if ($tmpText != '') { 
                            $this->paintDescText($canvas, $x, $y, $tmpText, 3, false);
                        }
                    } else {
                        $this->paintDescText($canvas, $x, $y, $profileText, 3, false);
                    }
                } else {

                    $this->paintDescText($canvas, $x, $y, $birthday, 3, false);
                    $this->paintDescText($canvas, $x, $y, $address, 3, false);

                    $childrenLen = count($children);

                    if ($childrenLen == 1) {    // 没有孩子
                        $this->paintDescText($canvas, $x, $y, $children[0], 3, false);
                    } else {
                        $isFirst = true;
                        for ($i = 0; $i < $childrenLen; ) {
                            if ($isFirst) {
                                $paintText = $children[$i];
                                $this->paintDescText($canvas, $x, $y, $paintText, 3, false);

                                if (isset($children[$i + 1])) {
                                    $paintText = $children[$i + 1];
                                    $this->paintDescText($canvas, $x, $y, $paintText, 3, false);

                                    if ($childrenId[$i+1] != 0) {
                                        if (!$this->isPageNumDisabled) {
                                            $pageNum = $this->descPageMap[$childrenId[$i+1]];
                                            $this->paintPageNum($canvas, $x, $y, $pageNum);
                                        }
                                    }
                                }

                                if (isset($children[$i + 2])) {
                                    $paintText = $children[$i + 2];
                                    $this->paintDescText($canvas, $x, $y, $paintText, 3, false);
                                    
                                    if ($childrenId[$i+2] != 0) {
                                        if (!$this->isPageNumDisabled) {
                                            $pageNum = $this->descPageMap[$childrenId[$i + 2]];
                                            $this->paintPageNum($canvas, $x, $y, $pageNum);
                                        }
                                    }
                                }

                                $i = $i + 3;
                                $isFirst = false;
                            } else {
                                $paintText = $children[$i];
                                $this->paintDescText($canvas, $x, $y, $paintText, 3, false);

                                if ($childrenId[$i] != 0) {
                                    if (!$this->isPageNumDisabled) {
                                        $pageNum = $this->descPageMap[$childrenId[$i]];
                                        $this->paintPageNum($canvas, $x, $y, $pageNum);
                                    }
                                }

                                if (isset($children[$i + 1])) {
                                    $paintText = $children[$i + 1];
                                    $this->paintDescText($canvas, $x, $y, $paintText, 3, false);

                                    if ($childrenId[$i+1] != 0) {
                                        if (!$this->isPageNumDisabled) {
                                            $pageNum = $this->descPageMap[$childrenId[$i+1]];
                                            $this->paintPageNum($canvas, $x, $y, $pageNum);
                                        }
                                    }
                                }
                                $i = $i + 2;
                            }
                        }
                    }


                    if ($spouse != '') {
                        $this->paintDescText($canvas, $x, $y, $spouse, 3, false);
                    }
                }

                // 一个人写完了，换行
                $x += $this->config['descTextXStep'];;
                $y = $this->config['descStartY'];
                if ($x < 0 && $this->direction == SysConst::$RIGHT_TO_LEFT ||
                    $x > $this->config['pageWidth'] && $this->direction == SysConst::$LEFT_TO_RIGHT) {
                    // 重开一页
                    imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                    $time = date('h:i:s');
                    echo "绘制描述第{$this->totalPage}页, {$time}\n";
                    $this->totalPage++;
                    $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['deschalfImgPath']);
                    $x = $this->config['descStartX'];
                    $y = $this->config['descStartY'];
                }
            }
        }

        imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
        $time = date('h:i:s');
        echo "绘制描述第{$this->totalPage}页, {$time}\n";
        $this->totalPage++;
    }

}
