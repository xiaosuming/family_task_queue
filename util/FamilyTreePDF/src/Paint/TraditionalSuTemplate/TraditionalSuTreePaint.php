<?php
/**
 * 鑫火基本体例吊线图
 * @Author: jiangpengfei
 * @Date:   2019-01-03
 */

namespace FamilyTreePDF\Paint\TraditionalSuTemplate;

use Dv\Dv;
use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\FontManager;

class TraditionalSuTreePaint implements PaintInterface {

    private $paintDataQueue; // 等待绘制的人物队列
    private $totalPage; // 总页数
    private $descPageMap; // 用来存储描述的一个map, 是personId和desc的页码对应
    private $personMap; // id和人物对应
    private $rootPersonQueue; // 绘制吊线图的根人物队列
    private $allLevelList; // 所有人物的levelList
    private $minLevel; // 最低的level，level越小，辈分越高
    private $maxLevel; // 最高的level, level越大，辈分越小
    private $direction; // 排版方向,

    private $config;
    private $normalTextFont;    // 正文字体

    public function __construct() {
        $this->totalPage = 0;
        $this->paintDataQueue = array();
        $this->direction = SysConst::$LEFT_TO_RIGHT;
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    public function setMottos($mottos) {

    }

    function setContext(PaintContext $context) {
        $this->descPageMap = &$context->getDescPageMapRef();
        $this->totalPage = &$context->getTotalPageRef();        // totalpage传的是引用
        $this->personMap = &$context->getPersonMapRef();

        $this->minLevel = &$context->getMinLevelRef();
        $this->maxLevel = &$context->getMaxLevelRef();

        $this->allLevelList = &$context->getAllLevelListRef();

        $this->minLevel = PHP_INT_MAX;
        $this->maxLevel = PHP_INT_MIN;
    }

    function setConfig($config) {
        $this->config = $config;
    }

   /**
    * 数据适配器
    * @param 所有的人物数组
    */
   private function dataAdaptor($persons)
   {

       $allPersons = array();

       foreach ($persons as $person) {
           $personbj = array();
           $personbj['id'] = $person['id'];
           $personbj['name'] = $person['name'];
           $personbj['image'] = $person['photo'];
           $personbj['level'] = $person['level'];
           $personbj['gender'] = $person['gender'];
           $personbj['type'] = $person['type'];
           $personbj['ranking'] = $person['ranking'] ?? 0;
           $personbj['isMain'] = $person['isMain'] ?? 0;
           
           if (count($person['father']) > 0) {
               $personbj['fathers'] = $person['father'];
           }

           if (count($person['mother']) > 0) {
               $personbj['mothers'] = $person['mother'];
           }

           if (count($person['brother']) > 0) {
               $personbj['brothers'] = $person['brother'];
           }

           if (count($person['sister']) > 0) {
               $personbj['sisters'] = $person['sister'];
           }

           if (count($person['son']) > 0) {
               $personbj['sons'] = $person['son'];
           }

           if (count($person['daughter']) > 0) {
               $personbj['daughters'] = $person['daughter'];
           }

           // 配偶
           if (count($person['spouse']) > 0) {
               $personbj['spouses'] = $person['spouse'];
           }

           $allPersons[] = $personbj;
       }

       return $allPersons;
   }

    /**
     * 向人物数组中添加亲属, (只添加子代)
     * @param $corePerson 核心人物
     * @param $persons    人物数组
     * @param $depth      当前的递归深度
     * @param $limit      递归深度的限制
     */
    private function addRelative($corePerson, &$persons, $depth, $limit)
    {
        if ($depth > $limit) {
            // 如果深度超过,则不再查找
            return;
        }

        // echo "深度".$depth.PHP_EOL;
        // echo "level".$corePerson['level'];

        if (!isset($persons[$corePerson['id']])) {
            $persons[$corePerson['id']] = $corePerson;


            // 添加这个人的配偶
            foreach($corePerson['spouse'] as $spouseId) {
                if (isset($this->personMap[$spouseId])) {
                    $persons[$spouseId] = $this->personMap[$spouseId];
                }
            }

            // echo "儿子数量".count($corePerson['son']);
            // echo "女儿数量".count($corePerson['daughter']);
            if ($depth == $limit && (count($corePerson['son']) > 0 || count($corePerson['daughter']) > 0)) {
                // 最后一代,放到rootPerson队列中
                array_push($this->rootPersonQueue, $corePerson);
            }
        }



        // 添加儿子
        foreach ($corePerson['son'] as $id) {
            if (!isset($persons[$id])) {
                $this->addRelative($this->personMap[$id], $persons, $depth + 1, $limit);
            }
        }

        // 添加女儿
        foreach ($corePerson['daughter'] as $id) {
            if (!isset($persons[$id])) {
                $this->addRelative($this->personMap[$id], $persons, $depth + 1, $limit);
            }
        }

    }

    function input($allPersons) {
        if (count($allPersons) == 0) {
            return;
        }
        
        $page = 0;
        $this->personMap = array();

        // root person,在族谱的绘制中,是作为一页的起点来做的
        $this->rootPersonQueue = array();

        $tmpAllLevelList = [];

        // 构建personMap,id和person对应
        foreach ($allPersons as $person) {
            $this->personMap[$person['id']] = $person;

            $level = $person['level'];

            if ($level < $this->minLevel) {
                $this->minLevel = $level;
                $rootPerson = $person;
            }

            if ($level > $this->maxLevel) {
                $this->maxLevel = $level;
            }

            if (!isset($tmpAllLevelList[$level])) {
                $tmpAllLevelList[$level] = array();
            }

            $tmpAllLevelList[$level][] = $person;
        }

        $this->allLevelList[$this->minLevel] = $tmpAllLevelList[$this->minLevel];

        // 第一代人，默认只有一个起点以及其配偶
        usort($this->allLevelList[$this->minLevel], function($person1, $person2) {
            if ($person1['type'] == 1) {
                return true;
            } else if ($person2['type'] == 1) {
                return false;
            } else {
                // type都是2，根据id排序
                return intval($person1['id']) < intval($person2['id']);
            }
        });

        // 整理tmpAllLevelList，保证每一代人的排序都是正常的
        for ($s = $this->minLevel+1; $s <= $this->maxLevel; $s++) {
            // 根据上一代的顺序确定这一代的顺序
            $this->allLevelList[$s] = PaintUtil::sortPersons($this->allLevelList[$s-1], $tmpAllLevelList[$s]);
        }

        $needPersons = array();

        $this->addRelative($rootPerson, $needPersons, 1, 5);

        $needPersons = array_values($needPersons);

        $needPaintPersons = $this->dataAdaptor($needPersons);

        $dv = new Dv();
        $dv->setConfig($this->config);

        $dv->input($needPaintPersons);
        $dv->setDisplayStyle(2);
        $dv->setMoreSpace(false);
        $levelList = $dv->output();

        $rect = $dv->getRect();
        $paths = PaintUtil::getPath($dv->getPath());
        //释放内存
        $dv->free();

        // 更新总页数
        $this->totalPage += ceil(($rect['maxWidth'] + $this->config['extraSpace']) / $this->config['pageWidth']);

        $startLevel = $rootPerson['level'] - $this->minLevel + 1;
        $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

        $this->paintDataQueue[] = $paintData;

        // 检查rootPersonQueue是否为空
        while (count($this->rootPersonQueue) > 0) {
            $rootPerson = $this->rootPersonQueue[0];
            $rootPerson['father'] = array();
            $rootPerson['mother'] = array();

            $needPersons = array();

            $this->addRelative($rootPerson, $needPersons, 1, 5);

            $needPersons = array_values($needPersons);

            if (count($needPersons) > 0) {

                $needPaintPersons = $this->dataAdaptor($needPersons);
                $dv = new Dv();
                $dv->setConfig($this->config);

                $dv->input($needPaintPersons);
                $dv->setDisplayStyle(2);
                $dv->setMoreSpace(false);
                $levelList = $dv->output();

                $rect = $dv->getRect();
                $paths = PaintUtil::getPath($dv->getPath());
                //释放内存
                $dv->free();

                $this->totalPage += ceil(($rect['maxWidth'] + $this->config['extraSpace']) / $this->config['pageWidth']);
                $startLevel = $rootPerson['level'] - $this->minLevel + 1;
                $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

                $this->paintDataQueue[] = $paintData;
            }
            $this->rootPersonQueue = array_slice($this->rootPersonQueue, 1);
        }
    }

    /**
     * 绘制世代的文字
     * @param $canvas 画布
     * @param $startLevel 开始的辈分
     */
    private function paintLevel($canvas, $startLevel)
    {

        $x = $this->config['levelTextX'];
        $y = $this->config['levelTextY'];
        $text = "第" . PaintUtil::number2chinese($startLevel) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['levelTextFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->config['levelTextFontSize'];
        }

        $y = $this->config['levelTextY'] + $this->config['levelTextDistance'];
        $text = "第" . PaintUtil::number2chinese($startLevel + 1) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['levelTextFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->config['levelTextFontSize'];
        }

        $y = $this->config['levelTextY'] + $this->config['levelTextDistance'] * 2;
        $text = "第" . PaintUtil::number2chinese($startLevel + 2) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['levelTextFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->config['levelTextFontSize'];
        }

        $y = $this->config['levelTextY'] + $this->config['levelTextDistance'] * 3;
        $text = "第" . PaintUtil::number2chinese($startLevel + 3) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['levelTextFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->config['levelTextFontSize'];
        }

        $y = $this->config['levelTextY'] + $this->config['levelTextDistance'] * 4;
        $text = "第" . PaintUtil::number2chinese($startLevel + 4) . "世";
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['levelTextFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['levelTextYDistance'] + $this->config['levelTextFontSize'];
        }
    }


    /**
     * 在一张画布上绘制所有的人物
     * @param $allPersons 所有的人物集合
     * @param $page       页数
     * @param $startLevel 绘制的辈分起点
     */
    private function paintPage($paintData, &$page)
    {
        $innerPage = 0;
        $xoffset = $this->config['xoffset'];
        $yoffset = $this->config['yoffset'];
        $pageWidth = $this->config['pageWidth'];
        $pageHeight = $this->config['pageHeight'];
        $pageCenterWidth = $this->config['pageCenterWidth'];
        $fontSize = $this->config['fontSize'];
        $fontXOffset = $this->config['fontXOffset'];
        $fontYOffset = $this->config['fontYOffset'];
        $copyXOffset = $this->config['copyXOffset'];
        $copyYOffset = $this->config['copyYOffset'];
        $lineWidth = $this->config['lineWidth'];

        $rect = $paintData->rect;
        $levelList = $paintData->levelList;
        $paths = $paintData->path;
        $startLevel = $paintData->startLevel;

        $maxWidth = $rect['maxWidth']+$this->config['extraSpace'];   // 多留20的旁白
        if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
            // 从右往左,将所有坐标做镜像颠倒
            foreach ($levelList as &$level) {
                foreach ($level as &$item) {
                    $item['x'] = $maxWidth - $item['x'];
                }
            }

            foreach ($paths as $key => $path) {
                $paths[$key]['ex'] = $maxWidth - $path['ex'];
                $paths[$key]['sx'] = $maxWidth - $path['sx'];
            }
        }

        $canvas = imagecreatetruecolor($maxWidth, $rect['maxHeight']);

        imagefilledrectangle($canvas, 0, 0, $maxWidth, $rect['maxHeight'], 0xffffff);

        foreach ($levelList as &$level) {
            foreach ($level as &$item) {
                if ($item['name'] != 'virtual') {
                    $x = $item['x'];
                    $y = $item['y'];

                    // imagefilledrectangle($canvas, $x+$xoffset, $y,
                    //                     $item['x']+$xoffset+$item['nodeWidth'], $item['y']+$item['nodeHeight'],
                    //                     0xcccccc
                    //                     );

                    // 绘制人物的名字
                    $color = 0x000000; //black default
                    if(isset($this->personMap[$item['id']]['isMain']) && $this->personMap[$item['id']]['isMain'] == 1){
                       // $color = 0xFF0000; //red
                    }
                    PaintUtil::paintText($canvas, $x, $y, $item['name'], $this->config, $this->normalTextFont, false, true, $color);

                    //吊线图页码
//                    PaintUtil::paintText($canvas,
//                                        $x+$this->config['pageTextXoffset'],
//                                        $y+$this->config['pageTextYoffset'],
//                                        "见".PaintUtil::number2chinese($this->descPageMap[$item['id']])."页",
//                                        $this->config,
//                                        $this->normalTextFont,
//                                        true,
//                                        false);

                }
            }
        }

        // 绘制路径
        foreach ($paths as $path) {
            PaintUtil::imagelineWithWidth($canvas, $path['sx'] + $this->config['xoffset'], 
            $path['sy'] - $this->config['yoffset'], $path['ex'] + $this->config['xoffset'], 
            $path['ey'] - $this->config['yoffset'], $lineWidth);
        }

        // 这里开始分页
        $leftWidth = $maxWidth;
        $leftHeight = $rect['maxHeight'];

        while ($leftWidth > 0) {

            $copyWidth = $pageWidth < $leftWidth ? $pageWidth : $leftWidth;
            $copyHeight = $pageHeight < $leftHeight ? $pageHeight : $leftHeight;

            $dstCanvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['halfblankImgPath']);

            $srcCopyX = ($innerPage * $pageWidth);

            if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                // 裁剪的位置从又向左
                $srcCopyX = $maxWidth - $srcCopyX - $copyWidth;

                // 粘贴的位置右对齐
                $copyXOffset = $pageWidth - $copyWidth + $copyXOffset;
            }

            imagecopy($dstCanvas, $canvas, $copyXOffset, $copyYOffset, $srcCopyX, 0, $copyWidth, $copyHeight);

            $leftWidth = $leftWidth - $copyWidth;

            $this->paintLevel($dstCanvas, $startLevel); // 写上第几世
            imagepng($dstCanvas, ResourceManager::$HALFOUTPUT_PATH."/output{$page}.png");
            if (SysConst::$DEBUG) {
                $time = date('h:i:s');
                echo "绘制吊线图第{$page}页, {$time}" . PHP_EOL;
            }
            // 释放内存
            imagedestroy($dstCanvas);
            $page++;
            $innerPage++;
        }
        imagedestroy($canvas);
    }

    function paint($setLevel = 1) {
        $page = 0;
        $treePage = [];
        foreach ($this->paintDataQueue as $data) {

            $GLOBALS['treePage'][] = $page;
            $treePage[] = $page;

            $data->startLevel = $data->startLevel + $setLevel -1;
            $this->paintPage($data, $page);
        }
    }
}
