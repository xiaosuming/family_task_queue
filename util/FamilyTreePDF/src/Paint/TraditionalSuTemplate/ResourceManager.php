<?php
/**
 * 资源管理类
 */

namespace FamilyTreePDF\Paint\TraditionalSuTemplate;

class ResourceManager {
    public static $FONT = __DIR__.'/../../res/font/1.TTC';
    public static $RESOURCE_PATH = __DIR__.'/../../res/TraditionalSuTemplate';
    public static $HALFOUTPUT_PATH = __DIR__.'/../../output/halfoutput';
    public static $OUTPUT_PATH = __DIR__.'/../../output';
}