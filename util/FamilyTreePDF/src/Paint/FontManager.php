<?php
/**
 * 字体管理器
 * @author: jiangpengfei
 * @date:   2019-06-04
 */

namespace FamilyTreePDF\Paint;

class FontManager{
    
    public static $FONT_DEFAULT = 1;   // 默认字体,楷体
    public static $FONT_FANG = 2;   // 仿宋
    public static $FONT_SONG = 3;    // 宋体
    public static $FONT_MINGLIU = 4;     // 名流

    /**
     * 获取要使用的字体
     */
    public static function getFont($font) {
        switch($font) {
        case self::$FONT_DEFAULT:
            return __DIR__.'/../res/font/SIMKAI.TTF';
        case self::$FONT_FANG:
            return __DIR__.'/../res/font/SIMFANG.TTF';
        case self::$FONT_SONG:
            return __DIR__.'/../res/font/SIMSUN.TTC';
        case self::$FONT_MINGLIU:
            return __DIR__.'/../res/font/1.TTC';
        default:
            return __DIR__.'/../res/font/SIMKAI.TTF';
        }
    }
}