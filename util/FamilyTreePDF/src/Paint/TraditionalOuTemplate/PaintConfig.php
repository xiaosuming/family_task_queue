<?php

namespace FamilyTreePDF\Paint\TraditionalOuTemplate;

class PaintConfig
{

    /**
     * 获取普通清晰度的从左往右的配置文件
     */
    public static function getL2RConfig()
    {
        return [
            "gridWidth" => 60,
            "nodeWidth" => 30,              // 人名处的宽度, 加上separation后是60
            "nodeHeight" => 65,             // 人名处的高度
            "siblingSeparation" => 30,      // 兄弟之间的间隔
            "subtreeSeparation" => 30,      // 隔代之间的间隔
            "spouseSeparation" => 30,       // 配偶之间的间隔
            // "spouseXOffset" => 56,       // 画配偶时的偏移
            // "spouseLineXOffset" => 26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 25,        // 世代之间的间隔
            "fontSize" => 13,               // 人名字体大小
            "fontXOffset" => 5,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => 6,                 // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 360,             // 每页的宽度,除去世代的部分
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 73,
            "lineWidth" => 1,
            "levelImageYOffset" => 20,
            "levelImageWidth" => 60,       // 世代图的宽度
            "levelImageHeight" => 492,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 20,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 8,
            'sideTextVSize' => 5,
            'sideTextHSize' => 5,
            'sideHeight' => 80,
            'sidebarXOffset' => 25,
            'sidebarYOffset' => 15,
            'sidebarAdjust' => 0,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 20,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
                                        // 这个留白里，不需要偏移

            "extraSpace" => 30,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "levelTextX" => 22,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 26,              // 第几世文字的y位置  tree
            "levelTextDistance" => 92,       // 第几世文字的距离   tree
            "levelTextFontSize" => 10,

            "deschalfImgPath" => '/deschalf.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',
            'levelImgPath' => '/level.png',

            "mottoHeight" => 90,              // 家训部分的高度
            "mottoFontSize" => 20,          // 家训的字体大小
            "mottoXOffset" => 20,           // 家训的X轴偏移
            "mottoYOffset" => 30,           // 家训的y轴偏移
            "descStartX" => 0,            // 描述部分正文的X起点       
            "descStartY" => 0,            // 描述部分正文的Y起点   
            "descTextOffsetY" => 8,         // 描述部分的文本的y轴偏移，防止与横线重叠 
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 12,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小
            'descTopTextFontSize' => 10,     // 顶部的描述文字的大小
            'descPadding' => 6,             // 内边距
            'descTextDistance' => 8,       // 文字间距
            'descLevelYOffset' => 140,      // 描述部分的世代的y轴偏移

            'descUnitWidth' => 40,          // 描述部分的最小宽度单元
            'topDescHeight' => 40,
            'nameDescHeight' => 80,
            'descMaxLen' => 400,            // 描述部分文字的最大长度

            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,        // 行高
            'eventFontsize' => 14,          // 字体大小
            'eventFontDistance' => 6,       // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取普通清晰度的从右往左的配置文件
     */
    public static function getR2LConfig()
    {
        return [
            "gridWidth" => 60,
            "nodeWidth" => 30,              // 人名处的宽度
            "nodeHeight" => 65,             // 人名处的高度
            "siblingSeparation" => 30,      // 兄弟之间的间隔
            "subtreeSeparation" => 30,      // 隔代之间的间隔
            "spouseSeparation" => 30,       // 配偶之间的间隔
            // "spouseXOffset" => -56,       // 画配偶时的偏移
            // "spouseLineXOffset" => -26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 25,        // 世代之间的间隔
            "fontSize" => 13,               // 人名字体大小
            "fontXOffset" => -5,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => -6,                // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 360,             // 每页的宽度
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 73,
            "lineWidth" => 1,
            "levelImageYOffset" => 20,
            "levelImageWidth" => 60,       // 世代图的宽度
            "levelImageHeight" => 492,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 20,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 8,
            'sideTextVSize' => 5,
            'sideTextHSize' => 5,
            'sideHeight' => 80,
            'sidebarXOffset' => -25,
            'sidebarYOffset' => 15,
            'sidebarAdjust' => 3,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 20,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
                                        // 这个留白里，不需要偏移

            "extraSpace" => 30,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "levelTextX" => 22,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 26,              // 第几世文字的y位置
            "levelTextDistance" => 92,      // 第几世的文字Y轴偏移
            "levelTextFontSize" => 10,

            "deschalfImgPath" => '/deschalf.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',
            'levelImgPath' => '/level.png',

            "mottoHeight" => 90,              // 家训部分的高度
            "mottoFontSize" => 20,          // 家训的字体大小
            "mottoXOffset" => 20,           // 家训的X轴偏移
            "mottoYOffset" => 30,           // 家训的y轴偏移
            "descStartX" => 360,            // 描述部分正文的X起点       
            "descStartY" => 0,            // 描述部分正文的Y起点    
            "descTextOffsetY" => 8,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 12,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小
            'descTopTextFontSize' => 10,     // 顶部的描述文字的大小
            'descPadding' => 6,             // 内边距
            'descTextDistance' => 8,       // 文字间距
            'descLevelYOffset' => 140,      // 描述部分的世代的y轴偏移

            'descUnitWidth' => 40,          // 描述部分的最小宽度单元
            'topDescHeight' => 40,
            'nameDescHeight' => 80,
            'descMaxLen' => 400,            // 描述部分文字的最大长度

            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,             // 行高
            'eventFontsize' => 14,               // 字体大小
            'eventFontDistance' => 6,            // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从左往右的配置文件
     */
    public static function getHDL2RConfig()
    {
        return [
            "gridWidth" => 180,
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 195,             // 人名处的高度
            "siblingSeparation" => 90,      // 兄弟之间的间隔
            "subtreeSeparation" => 90,      // 隔代之间的间隔
            "spouseSeparation" => 90,       // 配偶之间的间隔
            // "spouseXOffset" => 168,       // 画配偶时的偏移
            // "spouseLineXOffset" => 78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 75,        // 世代之间的间隔
            "fontSize" => 39,               // 人名字体大小
            "fontXOffset" => 15,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => 18,                 // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 1080,             // 每页的宽度,除去世代的部分
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 219,
            "lineWidth" => 1,
            "levelImageYOffset" => 60,
            "levelImageWidth" => 180,       // 世代图的宽度
            "levelImageHeight" => 1476,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 60,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 24,
            'sideTextVSize' => 15,
            'sideTextHSize' => 15,
            'sideHeight' => 240,
            'sidebarXOffset' => 75,
            'sidebarYOffset' => 45,
            'sidebarAdjust' => 0,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 60,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
                                        // 这个留白里，不需要偏移

            "extraSpace" => 90,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 18,       // 小的文字的大小差异
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "levelTextX" => 66,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 78,              // 第几世文字的y位置  tree
            "levelTextDistance" => 276,       // 第几世文字的距离   tree
            "levelTextFontSize" => 30,

            "deschalfImgPath" => '/deschalf_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
            'levelImgPath' => '/level_hd.png',

            "mottoHeight" => 270,              // 家训部分的高度
            "mottoFontSize" => 60,          // 家训的字体大小
            "mottoXOffset" => 60,           // 家训的X轴偏移
            "mottoYOffset" => 90,           // 家训的y轴偏移
            "descStartX" => 0,            // 描述部分正文的X起点       
            "descStartY" => 0,            // 描述部分正文的Y起点    
            "descTextOffsetY" => 24,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 30,     // 描述的普通字体大小,字体过小是画不出来的
            "descNameFontSize" => 36,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
            'descTopTextFontSize' => 30,     // 顶部的描述文字的大小
            'descPadding' => 18,             // 内边距
            'descTextDistance' => 24,       // 文字间距
            'descLevelYOffset' => 420,      // 描述部分的世代的y轴偏移

            'descUnitWidth' => 120,          // 描述部分的最小宽度单元
            'topDescHeight' => 120,
            'nameDescHeight' => 240,
            'descMaxLen' => 1200,            // 描述部分文字的最大长度

            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离

            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距

            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从右往左的配置文件
     */
    public static function getHDR2LConfig()
    {
        return [
            "gridWidth" => 180,
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 195,             // 人名处的高度
            "siblingSeparation" => 90,      // 兄弟之间的间隔
            "subtreeSeparation" => 90,      // 隔代之间的间隔
            "spouseSeparation" => 90,       // 配偶之间的间隔
            // "spouseXOffset" => -168,       // 画配偶时的偏移
            // "spouseLineXOffset" => -78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 75,        // 世代之间的间隔
            "fontSize" => 39,               // 人名字体大小
            "fontXOffset" => -15,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => -18,                // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 1080,             // 每页的宽度
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 219,
            "lineWidth" => 1,
            "levelImageYOffset" => 60,
            "levelImageWidth" => 180,       // 世代图的宽度
            "levelImageHeight" => 1476,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 60,            // 为了在顶部有留白，整棵树的y整体偏移
            "specialXOffset" => 0,

            'sideTextSize' => 24,
            'sideTextVSize' => 15,
            'sideTextHSize' => 15,
            'sideHeight' => 240,
            'sidebarXOffset' => -75,
            'sidebarYOffset' => 45,
            'sidebarAdjust' => 9,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 60,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
                                        // 这个留白里，不需要偏移

            "extraSpace" => 90,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 18,       // 小的文字的大小差异
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "levelTextX" => 66,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 78,              // 第几世文字的y位置
            "levelTextDistance" => 276,      // 第几世的文字Y轴偏移
            "levelTextFontSize" => 30,

            "deschalfImgPath" => '/deschalf_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
            'levelImgPath' => '/level_hd.png',

            "mottoHeight" => 270,              // 家训部分的高度
            "mottoFontSize" => 60,          // 家训的字体大小
            "mottoXOffset" => 60,           // 家训的X轴偏移
            "mottoYOffset" => 90,           // 家训的y轴偏移
            "descStartX" => 1080,            // 描述部分正文的X起点       
            "descStartY" => 0,            // 描述部分正文的Y起点   
            "descTextOffsetY" => 24,         // 描述部分的文本的y轴偏移，防止与横线重叠 
            "descNormalFontSize" => 30,     // 描述的普通字体大小
            "descNameFontSize" => 36,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
            'descTopTextFontSize' => 30,     // 顶部的描述文字的大小
            'descPadding' => 18,             // 内边距
            'descTextDistance' => 24,       // 文字间距
            'descLevelYOffset' => 420,      // 描述部分的世代的y轴偏移


            'descUnitWidth' => 120,          // 描述部分的最小宽度单元
            'topDescHeight' => 120,
            'nameDescHeight' => 240,
            'descMaxLen' => 1200,            // 描述部分文字的最大长度

            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离

            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距


            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取普通清晰度的从左往右的配置文件
     */
    public static function getL2RConfigForDesc2()
    {
        return [
            "gridWidth" => 60,
            "nodeWidth" => 30,              // 人名处的宽度, 加上separation后是60
            "nodeHeight" => 65,             // 人名处的高度
            "siblingSeparation" => 30,      // 兄弟之间的间隔
            "subtreeSeparation" => 30,      // 隔代之间的间隔
            "spouseSeparation" => 30,       // 配偶之间的间隔
            // "spouseXOffset" => 56,       // 画配偶时的偏移
            // "spouseLineXOffset" => 26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 25,        // 世代之间的间隔
            "fontSize" => 13,               // 人名字体大小
            "fontXOffset" => 5,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => 6,                 // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 360,             // 每页的宽度,除去世代的部分
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 73,
            "lineWidth" => 1,
            "levelImageYOffset" => 20,
            "levelImageWidth" => 60,       // 世代图的宽度
            "levelImageHeight" => 492,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 20,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 8,
            'sideTextVSize' => 5,
            'sideTextHSize' => 5,
            'sideHeight' => 80,
            'sidebarXOffset' => 25,
            'sidebarYOffset' => 15,
            'sidebarAdjust' => 0,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 20,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
            // 这个留白里，不需要偏移

            "extraSpace" => 30,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "levelTextX" => 22,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 26,              // 第几世文字的y位置  tree
            "levelTextDistance" => 92,       // 第几世文字的距离   tree
            "levelTextFontSize" => 10,

            "deschalfImgPath" => '/desc2_half.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',
            'levelImgPath' => '/level.png',
            'shortDescHalfImgPath' => '/desc2_half2.png', #上下间距更大的行转图

            "mottoHeight" => 90,              // 家训部分的高度
            "mottoFontSize" => 20,          // 家训的字体大小
            "mottoXOffset" => 20,           // 家训的X轴偏移
            "mottoYOffset" => 30,           // 家训的y轴偏移
            "descStartX" => 0,            // 描述部分正文的X起点
            "descStartY" => 0,            // 描述部分正文的Y起点
            "descTextOffsetY" => 8,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 10,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小
            'descTopTextFontSize' => 15,     // 顶部的描述文字的大小
            'descPadding' => 6,             // 内边距
            'descTextDistance' => 8,       // 文字间距
            'descLevelYOffset' => 280,      // 描述部分的世代的y轴偏移
            'descTextDistanceForName' => 6,   #行传名字的字符间距
            'descLineWithWidthOffsetY' => 40,  #行传竖线距离顶部的距离
            'descParentNameFontSize' => 10,    #行传长辈名字的字体大小
//            'descTopOffsetY' => 15,            #顶部文本偏移量
            'descTopOffsetY' => 8,            #顶部文本偏移量
            'descParentNameTinyFontSize' => 8,  #行传长辈名字的小号字体大小
            'descTinyTextDistanceForName' => 2,   #行传字的字符间距


            ################
            # 新添加
            'topDescHeight2' => 35,
            'parentNameDescHeight2' => 40,  # 三代人的姓名高度
            'nameDescHeight2' => 50,
            'descTextDistance2' => 4,       // 文字间距
            'descParentNameFontSize2' => 10,    #行传长辈名字的字体大小
            'descParentNameTinyFontSize2' => 8,  #行传长辈名字的小号字体大小
            'descNameFontSize2' => 10,       // 描述名字的字体大小
            'descNormalFontSize2' => 10,     // 描述的普通字体大小
            "pageHeight2" => 520,            // 每页的高度
            'descTextDistanceForName2' => 4,   #行传名字的字符间距
            'descTextOffsetY2' => 12 ,
            'descMaxLen2' => 300,            // 描述部分文字的最大长度,原长度240
            ################

            'descUnitWidth' => 45,          // 描述部分的最小宽度单元
            'topDescHeight' => 40,         # 顶部侧边栏信息的高度
            'parentNameDescHeight' => 50,  # 三代人的姓名高度
            'nameDescHeight' => 60,
            'descMaxLen' => 300,            // 描述部分文字的最大长度,原长度240

            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,        // 行高
            'eventFontsize' => 14,          // 字体大小
            'eventFontDistance' => 6,       // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取普通清晰度的从右往左的配置文件
     */
    public static function getR2LConfigForDesc2()
    {
        return [
            "gridWidth" => 60,
            "nodeWidth" => 30,              // 人名处的宽度
            "nodeHeight" => 65,             // 人名处的高度
            "siblingSeparation" => 30,      // 兄弟之间的间隔
            "subtreeSeparation" => 30,      // 隔代之间的间隔
            "spouseSeparation" => 30,       // 配偶之间的间隔
            // "spouseXOffset" => -56,       // 画配偶时的偏移
            // "spouseLineXOffset" => -26,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 35,      // 画配偶连线时的Y偏移
            "levelSeparation" => 25,        // 世代之间的间隔
            "fontSize" => 13,               // 人名字体大小
            "fontXOffset" => -5,             // 绘制人物名字的左偏移量
            "fontYOffset" => 20,            // 绘制人物名字的上偏移
            "xoffset" => -6,                // 粘贴图片时的偏移
            "yoffset" => 100,               // 粘贴图片时的偏移
            "pageWidth" => 360,             // 每页的宽度
            "pageHeight" => 540,            // 每页的高度
            "pageCenterWidth" => 130,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 73,
            "lineWidth" => 1,
            "levelImageYOffset" => 20,
            "levelImageWidth" => 60,       // 世代图的宽度
            "levelImageHeight" => 492,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 20,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 8,
            'sideTextVSize' => 5,
            'sideTextHSize' => 5,
            'sideHeight' => 80,
            'sidebarXOffset' => -25,
            'sidebarYOffset' => 15,
            'sidebarAdjust' => 3,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 20,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
            // 这个留白里，不需要偏移

            "extraSpace" => 30,             // 族谱树裁剪时的额外留白
            "textYDistance" => 6,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 3,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 6,       // 小的文字的大小差异
            "levelTextYDistance" => 6,      // 第几世的文字Y轴偏移
            "levelTextX" => 22,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 26,              // 第几世文字的y位置
            "levelTextDistance" => 92,      // 第几世的文字Y轴偏移
            "levelTextFontSize" => 10,

            "deschalfImgPath" => '/desc2_half.png',
            'halfblankImgPath' => '/halfblank.png',
            'totalblankImgPath' => '/totalblank.png',
            'levelImgPath' => '/level.png',
            'shortDescHalfImgPath' => '/desc2_half2.png', #上下间距更大的行转图

            "mottoHeight" => 90,              // 家训部分的高度
            "mottoFontSize" => 20,          // 家训的字体大小
            "mottoXOffset" => 20,           // 家训的X轴偏移
            "mottoYOffset" => 30,           // 家训的y轴偏移
            "descStartX" => 360,            // 描述部分正文的X起点
            "descStartY" => 0,            // 描述部分正文的Y起点
            "descTextOffsetY" => 8,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 10,     // 描述的普通字体大小
            "descNameFontSize" => 10,       // 描述名字的字体大小
            "descLevelFontSize" => 14,       // 辈分的字体大小
            'descTopTextFontSize' => 15,     // 顶部的描述文字的大小
            'descPadding' => 6,             // 内边距
            'descTextDistance' => 8,       // 文字间距
            'descLevelYOffset' => 280,      // 描述部分的世代的y轴偏移
            'descTextDistanceForName' => 6,   # 行传名字字的字符间距
            'descLineWithWidthOffsetY' => 40,  # 行传竖线距离顶部的距离
            'descParentNameFontSize' => 10,    # 行传长辈名字的字体大小
//            'descTopOffsetY' => 15,            # 顶部文本偏移量
            'descTopOffsetY' => 8,            #顶部文本偏移量
            'descParentNameTinyFontSize' => 8,  # 行传长辈名字的小号字体大小
            'descTinyTextDistanceForName' => 2,   # 行传字的字符间距

            ################
            # 新添加
            'topDescHeight2' => 35,
            'parentNameDescHeight2' => 40,  # 三代人的姓名高度
            'nameDescHeight2' => 50,
            'descTextDistance2' => 4,       // 文字间距
            'descParentNameFontSize2' => 10,    #行传长辈名字的字体大小
            'descParentNameTinyFontSize2' => 8,  #行传长辈名字的小号字体大小
            'descNameFontSize2' => 10,       // 描述名字的字体大小
            'descNormalFontSize2' => 10,     // 描述的普通字体大小
            "pageHeight2" => 520,            // 每页的高度
            'descTextDistanceForName2' => 4,   #行传名字的字符间距
            'descTextOffsetY2' => 12 ,
            'descMaxLen2' => 300,            // 描述部分文字的最大长度,原长度240
            ################

            'descUnitWidth' => 45,          // 描述部分的最小宽度单元
            'topDescHeight' => 40,         # 顶部侧边栏信息的高度
            'parentNameDescHeight' => 50,  # 三代人的姓名高度
            'nameDescHeight' => 60,
            'descMaxLen' => 300,            // 描述部分文字的最大长度

            'eventPageWidth' => 360,        // 大事件的每页宽度
            'eventPageHeigth' => 546,       // 大事件的每页高度
            'eventPaddingLevel' => 20,      // 大事件的左内边距
            'eventPaddingRight' => 20,      // 大事件的右内边距
            'eventPaddingTop' => 40,        // 大事件的上内边距
            'eventPaddingBottom' => 40,    // 大事件的下内边距
            'eventLineheight' => 30,             // 行高
            'eventFontsize' => 14,               // 字体大小
            'eventFontDistance' => 6,            // 字体距离

            'photoPageWidth' => 360,        // 相片的每页宽度
            'photoMaxWidth' => 300,         // 相片的最大宽度
            'photoOneMaxHeight' => 400,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 200,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 60,
            'photoTwoMarginTop1' => 20,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273,      // 第二张相片的上外边距

            'graveStartX' => 20,            // 宗祠部分的起点X
            'graveStartY' => 40,            // 宗祠部分的起点Y
            'graveNameFontSize' => 20,      // 宗祠名字体大小
            'graveAddressFontSize' => 12,   // 宗祠地址字体大小
            'graveDescFontSize' => 12,      // 宗祠描述字体大小
            'graveMaxWidth' => 300,         // 宗祠最大宽度
            'graveMaxHeight' => 546,        // 宗祠最大高度
            'gravePhotoMargin' => 50,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从左往右的配置文件
     */
    public static function getHDL2RConfigForDesc2()
    {
        return [
            "gridWidth" => 180,
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 195,             // 人名处的高度
            "siblingSeparation" => 90,      // 兄弟之间的间隔
            "subtreeSeparation" => 90,      // 隔代之间的间隔
            "spouseSeparation" => 90,       // 配偶之间的间隔
            // "spouseXOffset" => 168,       // 画配偶时的偏移
            // "spouseLineXOffset" => 78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 75,        // 世代之间的间隔
            "fontSize" => 39,               // 人名字体大小
            "fontXOffset" => 15,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => 18,                 // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 1080,             // 每页的宽度,除去世代的部分
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 219,
            "lineWidth" => 1,
            "levelImageYOffset" => 60,
            "levelImageWidth" => 180,       // 世代图的宽度
            "levelImageHeight" => 1476,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 60,            // 为了在顶部有留白，整棵树的y整体偏移

            'sideTextSize' => 24,
            'sideTextVSize' => 15,
            'sideTextHSize' => 15,
            'sideHeight' => 240,
            'sidebarXOffset' => 75,
            'sidebarYOffset' => 45,
            'sidebarAdjust' => 0,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 60,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
            // 这个留白里，不需要偏移

            "extraSpace" => 90,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 18,       // 小的文字的大小差异
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "levelTextX" => 66,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 78,              // 第几世文字的y位置  tree
            "levelTextDistance" => 276,       // 第几世文字的距离   tree
            "levelTextFontSize" => 30,

            "deschalfImgPath" => '/desc2_half_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
            'levelImgPath' => '/level_hd.png',

            "mottoHeight" => 270,              // 家训部分的高度
            "mottoFontSize" => 60,          // 家训的字体大小
            "mottoXOffset" => 60,           // 家训的X轴偏移
            "mottoYOffset" => 90,           // 家训的y轴偏移
            "descStartX" => 0,            // 描述部分正文的X起点
            "descStartY" => 0,            // 描述部分正文的Y起点
            "descTextOffsetY" => 24,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 30,     // 描述的普通字体大小,字体过小是画不出来的
            "descNameFontSize" => 10 * 3,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
            'descTopTextFontSize' => 14 * 3,     // 顶部的描述文字的大小
            'descPadding' => 18,             // 内边距
            'descTextDistance' => 24,       // 文字间距
            'descLevelYOffset' => 840,      // 描述部分的世代的y轴偏移
            'descTextDistanceForName' => 6 * 3,   #行传字的字符间距
            'descLineWithWidthOffsetY' => 40 * 3,  #行传竖线距离顶部的距离
            'descParentNameFontSize' => 10 * 3,    #行传长辈名字的字体大小
//            'descTopOffsetY' => 15 * 3,            #顶部文本偏移量
            'descTopOffsetY' => 8 * 3,            #顶部文本偏移量
            'descParentNameTinyFontSize' => 8 * 3,  #行传长辈名字的小号字体大小
            'descTinyTextDistanceForName' => 2 * 3,   #行传字的字符间距

            ################
            # 新添加
            'shortDescHalfImgPath' => '/desc2_half_hd2.png', #上下间距更大的行转图
            'topDescHeight2' => 35 * 3,
            'parentNameDescHeight2' => 40 * 3,  # 三代人的姓名高度
            'nameDescHeight2' => 50 * 3,
            'descTextDistance2' => 4 * 3,       // 文字间距
            'descParentNameFontSize2' => 10 * 3,    #行传长辈名字的字体大小
            'descParentNameTinyFontSize2' => 8 * 3,  #行传长辈名字的小号字体大小
            'descNameFontSize2' => 10 * 3,       // 描述名字的字体大小
            'descNormalFontSize2' => 10 * 3,     // 描述的普通字体大小
            "pageHeight2" => 520 * 3,            // 每页的高度
            'descTextDistanceForName2' => 4 * 3,   #行传名字的字符间距
            'descTextOffsetY2' => 12 * 3,
            'descMaxLen2' => 300 * 3,            // 描述部分文字的最大长度,原长度240 * 3
            ################

            'descUnitWidth' => 45 *3,          // 描述部分的最小宽度单元
            'topDescHeight' => 40 * 3,         # 顶部侧边栏信息的高度
            'parentNameDescHeight' => 50 * 3,  # 三代人的姓名高度
            'nameDescHeight' => 60 * 3,
            'descMaxLen' => 300 * 3,            // 描述部分文字的最大长度

            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离

            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距

            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }

    /**
     * 获取高清的从右往左的配置文件
     */
    public static function getHDR2LConfigForDesc2()
    {
        return [
            "gridWidth" => 180,
            "nodeWidth" => 90,              // 人名处的宽度
            "nodeHeight" => 195,             // 人名处的高度
            "siblingSeparation" => 90,      // 兄弟之间的间隔
            "subtreeSeparation" => 90,      // 隔代之间的间隔
            "spouseSeparation" => 90,       // 配偶之间的间隔
            // "spouseXOffset" => -168,       // 画配偶时的偏移
            // "spouseLineXOffset" => -78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移
            "levelSeparation" => 75,        // 世代之间的间隔
            "fontSize" => 39,               // 人名字体大小
            "fontXOffset" => -15,             // 绘制人物名字的左偏移量
            "fontYOffset" => 60,            // 绘制人物名字的上偏移
            "xoffset" => -18,                // 粘贴图片时的偏移
            "yoffset" => 300,               // 粘贴图片时的偏移
            "pageWidth" => 1080,             // 每页的宽度
            "pageHeight" => 1620,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 219,
            "lineWidth" => 1,
            "levelImageYOffset" => 60,
            "levelImageWidth" => 180,       // 世代图的宽度
            "levelImageHeight" => 1476,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "treeYOffset" => 60,            // 为了在顶部有留白，整棵树的y整体偏移
            "specialXOffset" => 0,

            'sideTextSize' => 24,
            'sideTextVSize' => 15,
            'sideTextHSize' => 15,
            'sideHeight' => 240,
            'sidebarXOffset' => -75,
            'sidebarYOffset' => 45,
            'sidebarAdjust' => 9,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 60,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
            // 这个留白里，不需要偏移

            "extraSpace" => 90,             // 族谱树裁剪时的额外留白
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离
            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 18,       // 小的文字的大小差异
            "levelTextYDistance" => 18,      // 第几世的文字Y轴偏移
            "levelTextX" => 66,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 78,              // 第几世文字的y位置
            "levelTextDistance" => 276,      // 第几世的文字Y轴偏移
            "levelTextFontSize" => 30,

            "deschalfImgPath" => '/desc2_half_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
            'levelImgPath' => '/level_hd.png',

            "mottoHeight" => 270,              // 家训部分的高度
            "mottoFontSize" => 60,          // 家训的字体大小
            "mottoXOffset" => 60,           // 家训的X轴偏移
            "mottoYOffset" => 90,           // 家训的y轴偏移
            "descStartX" => 1080,            // 描述部分正文的X起点
            "descStartY" => 0,            // 描述部分正文的Y起点
            "descTextOffsetY" => 24,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 30,     // 描述的普通字体大小
            "descNameFontSize" => 10 * 3,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
            'descTopTextFontSize' => 14 * 3,     // 顶部的描述文字的大小
            'descPadding' => 18,             // 内边距
            'descTextDistance' => 24,       // 文字间距
            'descLevelYOffset' => 840,      // 描述部分的世代的y轴偏移
            'descTextDistanceForName' => 6 * 3,   #行传字的字符间距
            'descLineWithWidthOffsetY' => 40 * 3,  #行传竖线距离顶部的距离
            'descParentNameFontSize' => 10 * 3,    #行传长辈名字的字体大小
//            'descTopOffsetY' => 15 * 3,            #顶部文本偏移量
            'descTopOffsetY' => 8 * 3,            #顶部文本偏移量
            'descParentNameTinyFontSize' => 8 * 3,  #行传长辈名字的小号字体大小
            'descTinyTextDistanceForName' => 2 * 3,   #行传字的字符间距

            ################
            # 新添加
            'shortDescHalfImgPath' => '/desc2_half_hd2.png', #上下间距更大的行转图
            'topDescHeight2' => 35 * 3,
            'parentNameDescHeight2' => 40 * 3,  # 三代人的姓名高度
            'nameDescHeight2' => 50 * 3,
            'descTextDistance2' => 4 * 3,       // 文字间距
            'descParentNameFontSize2' => 10 * 3,    #行传长辈名字的字体大小
            'descParentNameTinyFontSize2' => 8 * 3,  #行传长辈名字的小号字体大小
            'descNameFontSize2' => 10 * 3,       // 描述名字的字体大小
            'descNormalFontSize2' => 10 * 3,     // 描述的普通字体大小
            "pageHeight2" => 520 * 3,            // 每页的高度
            'descTextDistanceForName2' => 4 * 3,   #行传名字的字符间距
            'descTextOffsetY2' => 12 * 3,
            'descMaxLen2' => 300 * 3,            // 描述部分文字的最大长度,原长度240 * 3
            ################

            'descUnitWidth' => 45 * 3,          // 描述部分的最小宽度单元
            'topDescHeight' => 40 * 3,         # 顶部侧边栏信息的高度
            'parentNameDescHeight' => 50 * 3,  # 三代人的姓名高度
            'nameDescHeight' => 60 * 3,
            'descMaxLen' => 300 * 3,            // 描述部分文字的最大长度

            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离

            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距


            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }
}
