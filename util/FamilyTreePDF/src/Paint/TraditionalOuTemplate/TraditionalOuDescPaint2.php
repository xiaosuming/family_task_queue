<?php

/**
 * 行传的绘制，基本模板,没有完全调好
 */
namespace FamilyTreePDF\Paint\TraditionalOuTemplate;

use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\FontManager;

class TraditionalOuDescPaint2 implements PaintInterface
{

    private $config;
    private $totalPage;
    private $personMap;
    private $descPageMap;// 人物id所在页码的映射
    private $minLevel;
    private $maxLevel;
    private $allLevelList;
    private $calculatePage; //计算用的页数变量

    private $direction; // 排版方向,
    private $options;

    private $leftTopDesc;   // 当页没画完的文字
    private $leftContent;
    private $leftName;

    private $normalTextFont;    // 正文的字体配置
    private $isPageNumDisabled; // 页码禁用

    private $deschalfImgPath;
    private $descTextDistance;
    private $descParentNameFontSize;
    private $descParentNameTinyFontSize;
    private $parentNameDescHeight;
    private $nameDescHeight;
    private $descNameFontSize;
    private $descNormalFontSize;
    private $pageHeight;
    private $descTextDistanceForName;
    private $topDescHeight;
    private $descTextOffsetY;
    private $descMaxLen;

    public function __construct()
    {
        $this->direction = SysConst::$LEFT_TO_RIGHT;
        $this->leftTopDesc = '';
        $this->leftContent = '';
        $this->leftName = '';
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
        $this->isPageNumDisabled = false;
    }

    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }


    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef();
        $this->personMap = &$context->getPersonMapRef();
        $this->descPageMap = &$context->getDescPageMapRef();
        $this->minLevel = &$context->getMinLevelRef();
        $this->maxLevel = &$context->getMaxLevelRef();
        $this->allLevelList = &$context->getAllLevelListRef();

        $this->options = $context->getOptions();

        $this->moreSpaceBetweenTopAndBottom = $this->options['moreSpaceBetweenTopAndBottom'] ? true:false;
        var_dump([86,$this->options['moreSpaceBetweenTopAndBottom'],$this->moreSpaceBetweenTopAndBottom ]);

        if ($this->moreSpaceBetweenTopAndBottom){
            $this->deschalfImgPath = $this->config['shortDescHalfImgPath'];
            $this->descTextDistance = $this->config['descTextDistance2'];
            $this->descParentNameFontSize = $this->config['descParentNameFontSize2'];
            $this->descParentNameTinyFontSize = $this->config['descParentNameTinyFontSize2'];
            $this->parentNameDescHeight = $this->config['parentNameDescHeight2'];
            $this->nameDescHeight = $this->config['nameDescHeight2'];
            $this->descNameFontSize = $this->config['descNameFontSize2'];
            $this->descNormalFontSize = $this->config['descNormalFontSize2'];
            $this->pageHeight = $this->config['pageHeight2'];
            $this->descTextDistanceForName = $this->config['descTextDistanceForName2'];
            $this->topDescHeight = $this->config['topDescHeight2'];
            $this->descTextOffsetY = $this->config['descTextOffsetY2'];
            $this->descMaxLen = $this->config['descMaxLen2'];

        }else{
            $this->deschalfImgPath = $this->config['deschalfImgPath'];
            $this->descTextDistance = $this->config['descTextDistance'];
            $this->descParentNameFontSize = $this->config['descParentNameFontSize'];
            $this->descParentNameTinyFontSize = $this->config['descParentNameTinyFontSize'];
            $this->parentNameDescHeight = $this->config['parentNameDescHeight'];
            $this->nameDescHeight = $this->config['nameDescHeight'];
            $this->descNameFontSize = $this->config['descNameFontSize'];
            $this->descNormalFontSize = $this->config['descNormalFontSize'];
            $this->pageHeight = $this->config['pageHeight'];
            $this->descTextDistanceForName = $this->config['descTextDistanceForName'];
            $this->topDescHeight = $this->config['topDescHeight'];
            $this->descTextOffsetY = $this->config['descTextOffsetY'];
            $this->descMaxLen = $this->config['descMaxLen'];

        }

    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setFont($font)
    {
        $this->normalTextFont = $font;
    }

    public function setPageNum($disabled) {
        $this->isPageNumDisabled = $disabled;
    }

    public function input($paintData, $orderBy=false){
        if (!$orderBy){
            return null;
        }
        $data = [];
        foreach ($paintData as $k =>$v){
            $data[$v['level']][] = $v;
        }
        $this->allLevelList = $data;
//        foreach ($data as $j){
//            foreach ($j as $i){
//                if (!$i['birthday']){
//                    continue;
//                }
//                print_r([$i['id'], $i['name'], $i['birthday']]);
//            }
//
//        }
    }


    private function getParent($person){
        if (
            isset($person['father']) && count($person['father']) > 0
            && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
        ) {
            // 父亲是本家人
            return $this->personMap[$person['father'][0]];
        } else if (
            isset($person['mother']) && count($person['mother']) > 0
            && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
        ) {
            // 母亲是本家人
            return $this->personMap[$person['mother'][0]];
        } else {
            return [];
        }
    }

    /**
     * 获取一个人物的排行称呼，如长子，次子，长女等
     * @param $person 人物信息
     */
    private function getRanking($person)
    {
        // 先检查人物是否有ranking
        if ($person['ranking'] > 0) {
            return PaintUtil::convertRanking($person['ranking'], $person['gender']);
        }

        // 没有排行，目前根据加入的顺序来判断
        $siblings = array_merge($person['brother'], $person['sister']);

        sort($siblings);

        foreach ($siblings as $index => $personId) {
            if ($person['id'] == $personId) {
                return PaintUtil::convertRanking($index + 1, $person['gender']);
            }
        }
    }

    /**
     * 绘制世代之后的换行
     */
    private function wrapAfterLevel(&$x, &$y)
    {
        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
            $x += $this->config['descUnitWidth'];
            $y = $this->config['descStartY'];
        } else {
            $x -= $this->config['descUnitWidth'];
            $y = $this->config['descStartY'];
        }
    }

    private function wrapAfterContent(&$x, &$y, $width)
    {
        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
            $x += $width;
            $y = $this->config['descStartY'];
        } else {
            $x -= $width;
            $y = $this->config['descStartY'];
        }
    }

    private function updateCanvas(&$canvas, $leftWidth)
    { }

    /**
     * 获取宽度，传入人物的顶部描述，名字，内容。返回适合的宽度
     * 宽度会取三者的最大值，并且是最小宽度(40)的整数倍
     */
    private function getWidth($parent,$grandparent, $greatgrandparent, $name, $content, $boxPadding)
    {

        $descTextDistance = $this->descTextDistance;
        $descTextDistanceForName = $this->descTextDistanceForName;


//        $topWidth = PaintUtil::getTextsWidth(
//            $topDesc,
//            $this->config['descTopTextFontSize'],
//            $descTextDistance,
//            $descTextDistance,
//            $this->config['topDescHeight'] - $boxPadding['top']
//        );
        $parentNameWidth = PaintUtil::getTextsWidth(
            $parent['name'],
            $parent['fontSize'] ?$this->descParentNameFontSize:$this->descParentNameTinyFontSize ,
            $descTextDistanceForName,
            $descTextDistance,
            $this->parentNameDescHeight - $boxPadding['top']
        );

        $grandparentNameWidth = PaintUtil::getTextsWidth(
            $grandparent['name'],
            $grandparent['fontSize'] ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize ,
            $descTextDistanceForName,
            $descTextDistance,
            $this->parentNameDescHeight - $boxPadding['top']
        );

        $greatgrandparentNameWidth = PaintUtil::getTextsWidth(
            $greatgrandparent['name'],
            $greatgrandparent['fontSize'] ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize ,
            $descTextDistanceForName,
            $descTextDistance,
            $this->parentNameDescHeight - $boxPadding['top']
        );

        $nameWidth = PaintUtil::getTextsWidth(
            $name,
            $this->descNameFontSize,
            $descTextDistance,
            $descTextDistance,
            $this->nameDescHeight  - $boxPadding['top']
        );

        $contentWidth = PaintUtil::getTextsWidth(
            $content,
            $this->descNormalFontSize ,
            $descTextDistance,
            $descTextDistance,
            $this->descMaxLen - $boxPadding['top']
        );


        $maxWidth = max($parentNameWidth, $grandparentNameWidth, $greatgrandparentNameWidth, $nameWidth, $contentWidth);

        // echo "maxWidth".$maxWidth;

        // $maxWidth += $descTextDistance * 2;

        $unitWidth = $this->config['descUnitWidth'];

        $width = ceil($maxWidth / $unitWidth) * $unitWidth;
//        var_dump([$topWidth, $nameWidth, $contentWidth,$width,$descTextDistance]);

        return $width;
    }

    public function paint($setLevel = 1, $setDefaultDesc = 0)
    {
        $placeHolderText = '@'; // 页码的占位符
        if ($this->isPageNumDisabled) {
            $placeHolderText = '';
        }

        $personPosPNMap = [];
        $descDataMap = [];
        $pageNum = 0;    #页码标记，用于检测是否需要在顶部绘制
        $isFirstPerson = false;   #检查是否是族谱的第一个人

        $isPainted = false;     // 标记新开的canvas有没有被绘制，防止空页产生
        $maxLen = $this->descMaxLen;
        $descPadding = $this->config['descPadding'];
        $descTextDistance = $this->descTextDistance;

        $boxPadding = ['top' => $descPadding, 'left' => $descPadding, 'bottom' => $descPadding, 'right' => $descPadding];

        $this->calculatePage = $this->totalPage + 1;    // 提前计算的页码
        /********************************提前计算*********************************/
        $calculateX = $this->config['descStartX'];
        $calculateY = $this->config['descStartY'];
        $calculateLeftWidth = $this->config['pageWidth'];

        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {

            $isPainted = true;

            $level = $this->allLevelList[$key];
            $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

            $extraXOffset = 0;
            if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                $extraXOffset = -$this->config['descLevelFontSize'];
            }

            // 计算世代的换行
            $this->wrapAfterLevel($calculateX, $calculateY);

            $calculateLeftWidth -= $this->config['descUnitWidth'];

            // 检测是否还有剩余宽度
            if ($calculateLeftWidth <= 0) {
                // 新写一页
                $this->calculatePage++;
                $isPainted = false;
                $calculateLeftWidth = $this->config['pageWidth'];
                $calculateX = $this->config['descStartX'];
                $calculateY = $this->config['descStartY'];
            }

            $personCount = count($level);

            //剩余字没画完 也继续循环
            while ($personCount > 0 || $this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                // 检查是否还有剩余未画完的文字
                if ($this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                    // 将剩余的文字画完，
                    $topDesc = $this->leftTopDesc;
                    $name = $this->leftName;
                    $paintText = $this->leftContent;

                    $this->leftTopDesc = '';
                    $this->leftContent = '';
                    $this->leftName = '';
                } else {
                    $personCount--;

                    $person = $level[$personCount];
                    if ($person['birthday']){
                        print_r([$person['name'], $person['birthday']]);
                    }
                    // 记录这个人的页数
                    $this->descPageMap[$person['id']] = $this->calculatePage;

                    $children = array();
                    $childrenId = array();
                    $spouse = '';
                    $paintText = "";
                    $noHolderRank = "";
                    //添加自己在父亲的排行信息
                    if ($this->options['isShowRanking'] == 1) {
                        // 绘制排行
                        if (
                            isset($person['father']) && count($person['father']) > 0
                            && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                        ) {
                            // 父亲是本家人
                            $paintText .= $this->personMap[$person['father'][0]]['name'];
                            $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['father'][0]]['id']], $placeHolderText);
                            $posPNMap[$res['pos']] = $res['pn'];
                            $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                            $noHolderRank = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        } else if (
                            isset($person['mother']) && count($person['mother']) > 0
                            && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                        ) {
                            // 母亲是本家人
                            $paintText .= $this->personMap[$person['mother'][0]]['name'];
                            $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['mother'][0]]['id']], $placeHolderText);
                            $posPNMap[$res['pos']] = $res['pn'];
                            $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                            $noHolderRank = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        } else {
                            $paintText .= '';
                        }
                    }
                    
                    if($setDefaultDesc == 1){
                        $birthday = "出生日期不详 ";
                        $address = "出生地不详 ";
                    }else{
                        $birthday = '';
                        $address = '';
                    }
                    if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                        $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                    }

                    if ($person['address'] != '') {
                        $address = "出生地" . $person['address'] . " ";
                    }

                    if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                        $children[] = "";//无子女
                    } else if (count($person['son']) == 0) {
                        $total = count($person['daughter']);
                        $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";

                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }
                    } else if (count($person['daughter']) == 0) {
                        $total = count($person['son']);
                        $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";

                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }
                    } else {
                        $total = count($person['son']);
                        $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子，" . PaintUtil::number2chinese(count($person['daughter'])) . "女";

                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'].$placeHolderText;
                            $childrenId[] = $personId;
                        }
                    }

                    // 配偶
                    foreach ($person['spouse'] as $personId) {
                        if ($person['gender'] == 0) {
                            //女
                            $spouse .= "夫" . $this->personMap[$personId]['name'] . $placeHolderText . ' ';
                        } else {
                            $spouse .= "妻" . $this->personMap[$personId]['name'] . $placeHolderText . ' ';
                        }
                    }

                    // 当前人物的描述已经好了
                    $name = $person['name'];

                    if ($this->options['isShowRanking'] == 1) {
                        // 绘制排行
                        $greatGrandparentName = "";
                        $grandparentName = "";
                        $parentName = "";
                        $parent = $person;
                        foreach (range(0,2) as $i){
                            $parent = $this->getParent($parent);
                            if ($parent === []){
                                break;
                            }
                            if($i === 0){
                                $parentName = $parent['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) ."";
                            }elseif ($i === 1){
                                $grandparentName = $parent['name'] . "之孙";
                            }elseif ($i === 2){
                                $greatGrandparentName = $parent['name'] . "曾孙" ;
                            }
                        }
//                        if (
//                            isset($person['father']) && count($person['father']) > 0
//                            && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
//                        ) {
//                            // 父亲是本家人
//
//                            $topDesc = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']);
//                        } else if (
//                            isset($person['mother']) && count($person['mother']) > 0
//                            && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
//                        ) {
//                            // 母亲是本家人
//                            $topDesc = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']);
//                        } else {
//                            $topDesc = '';
//                        }
//                    } else {
//                        $topDesc = '';
                    }

                    if(empty($birthday) || empty($address)){
                        # 去除中间空格
                        $paintText .= $birthday . $address;
                    }else{
                        $paintText .= $birthday . ' ' . $address;
                    }


                    $childrenLen = count($children);

                    if ($childrenLen == 1) {    // 没有孩子
                        $paintText .= $children[0];
                    } else {
                        $isFirst = true;
                        for ($i = 0; $i < $childrenLen;) {
                            if ($isFirst) {
                                $paintText .= $children[$i];

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1] . " ";
                                }

                                if (isset($children[$i + 2])) {
                                    $paintText .= $children[$i + 2] . " ";
                                }

                                $i = $i + 3;
                                $isFirst = false;
                            } else {
                                $paintText .= $children[$i] . " ";

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1] . " ";
                                }
                                $i = $i + 2;
                            }
                        }
                    }

                    $paintText .= $spouse;

                    $person['profileText'] = trim($person['profileText']);
                    if (isset($person['profileText']) && $person['profileText'] != '') {
                        $profileText = $noHolderRank . $person['profileText'];
                        $person['profileText'] = $profileText;
                        $descData = PaintUtil::processProfileText($person, $this->personMap);

                        $offset = 0;
                        foreach($descData as $item) {
                            $pos = $item['pos'];
                            
                            $t1 = mb_substr($profileText, 0, $pos+$offset);
                            $t2 = mb_substr($profileText, $pos+$offset);
                            $profileText = $t1.$placeHolderText.$t2;
                            $offset++;   
                        }
                        $paintText = $profileText;
                        $this->personMap[$person['id']]['profileText'] = $profileText;
                        $descDataMap[$person['id']] = $descData;
                        
                    }
                } // end if. 是否有剩余的文字未绘制检测完毕

                $beforeLen = mb_strlen($paintText);
                $paintText = ltrim($paintText);      // 去掉首位空格
                $trimedTextLen = $beforeLen - mb_strlen($paintText);
                $name = trim($name);
                $topDesc = '';
//                $topDesc = trim($topDesc);

                // 计算顶部描述
                $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->topDescHeight, $boxPadding, $descTextDistance, $this->config['descTopTextFontSize']);

                if ($availabelCount < mb_strlen($topDesc)) {
                    // 剩余空间不够用
                    $this->leftTopDesc = mb_substr($topDesc, $availabelCount);
                    $topDesc = mb_substr($topDesc, 0, $availabelCount);

                    // echo "剩下了top\n";
                }

                // 计算名字
                $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->nameDescHeight, $boxPadding, $descTextDistance,$this->descNameFontSize);

                if ($availabelCount < mb_strlen($name)) {
                    // 剩余空间不够用
                    $this->leftName = mb_substr($name, $availabelCount);
                    $name = mb_substr($name, 0, $availabelCount);
                }

                // 计算内容
                $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->descMaxLen, $boxPadding, $descTextDistance, $this->descNormalFontSize );
                if ($availabelCount < mb_strlen($paintText)) {
                    // 剩余空间不够用
                    $this->leftContent = mb_substr($paintText, $availabelCount);
                    $paintText = mb_substr($paintText, 0, $availabelCount);
                }


                // 获取宽度, 这是实际会占用的宽度
                $width = $this->getWidth(
                    ['name'=>$parentName,'fontSize'=>(mb_strlen($parentName)<=4 ? true:false)],
                    ['name'=>$grandparentName,'fontSize'=>(mb_strlen($grandparentName)<=4 ? true:false)],
                    ['name'=>$greatGrandparentName,'fontSize'=>(mb_strlen($greatGrandparentName)<=4 ? true:false)],
                    $name, $paintText, $boxPadding
                );

                // 画顶部描述
                $calculateY = $this->config['descStartY'];
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->config['descTopTextFontSize'];
                }

                $calculateY = $calculateY + $this->topDescHeight;


                // 画名字
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->descNameFontSize;
                }

                $calculateY = $calculateY + $this->nameDescHeight;

                // 画内容
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->descNormalFontSize ;
                }

                // 画完一个人要换行
                $this->wrapAfterContent($calculateX, $calculateY, $width);

                $calculateLeftWidth -= $width;

                if ($calculateLeftWidth <= 0) {
                    // 新写一页
                    $this->calculatePage++;
                    $isPainted = false;
                    $calculateLeftWidth = $this->config['pageWidth'];
                    $calculateX = $this->config['descStartX'];
                    $calculateY = $this->config['descStartY'];
                }
            }
        }

        /*******************************实际绘制***************************/
        $x = $this->config['descStartX'];
        $y = $this->config['descStartY'];
        $leftWidth = $this->config['pageWidth'];


        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->deschalfImgPath);
        $isPainted = false;

        for ($key = $this->minLevel; $key <= $this->maxLevel; $key++) {

            $isPainted = true;

            $level = $this->allLevelList[$key];
            $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

            $extraXOffset = 0;
            if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                $extraXOffset = -$this->config['descLevelFontSize'];
            }

            // 绘制世代
            PaintUtil::paintInBox(
                $canvas,
                $x + $extraXOffset,
                $y + $this->config['descLevelYOffset'],
                $this->config['descUnitWidth'],
                $maxLen,
                $levelText,
                $boxPadding,
                $descTextDistance,
                $this->direction,
                $this->normalTextFont,
                $this->config['descLevelFontSize'],
                0x000000,
                true,
                false,
                true
            );

            // 画完世代要换行
            $this->wrapAfterLevel($x, $y);

            // 在这里画一条竖线
            PaintUtil::imagelineWithWidth($canvas, $x, $this->config['descLineWithWidthOffsetY'], $x, $y + $this->pageHeight, 1);

            $leftWidth -= $this->config['descUnitWidth'];   // 世代占一个单元的宽度

            // 检测是否还有剩余宽度
            if ($leftWidth <= 0) {
                // 新写一页
                imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                $time = date('h:i:s');
                echo "绘制描述第{$this->totalPage}页, {$time}\n";
                $this->totalPage++;
                $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->deschalfImgPath);
                $isPainted = false;
                $leftWidth = $this->config['pageWidth'];
                $x = $this->config['descStartX'];
                $y = $this->config['descStartY'];
            }

            $personCount = count($level);

            $textOffset = 0;    // 当前的文字偏移量(leftContent导致的)
            $preTextOffset = 0; // 之前的文字偏移量
            while ($personCount > 0) {
                $isPainted = true;
//                var_dump([$name,$person['id']]);
//                var_dump([$this->leftTopDesc,$this->leftContent,$this->leftName]);
                // 检查是否还有剩余未画完的文字
                if ($this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                    // 将剩余的文字画完，
//                    var_dump([$person['name'],$person['id']]);
//                    var_dump([$this->leftTopDesc,$this->leftContent,$this->leftName,$person]);
                    $topDesc = $this->leftTopDesc;
                    $name = $this->leftName;
                    $paintText = $this->leftContent;

                    $this->leftTopDesc = '';
                    $this->leftContent = '';
                    $this->leftName = '';
                } else {
//                    var_dump([$person['name'],$person['id']]);
                    $posPNMap = [];     // 文字位置和页码的映射
                    $personCount--;

                    $person = $level[$personCount];
                    if (!$isFirstPerson){
                        $theFirstPerson = $person;
                        $isFirstPerson = true;
                    }

                    $children = array();
                    $childrenId = array();
                    $spouse = '';

                    $paintText = "";
                    //添加自己在父亲的排行信息
                    if ($this->options['isShowRanking'] == 1) {
                        // 绘制排行
                        if (
                            isset($person['father']) && count($person['father']) > 0
                            && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                        ) {
                            // 父亲是本家人
                            $paintText .= $this->personMap[$person['father'][0]]['name'];
                            $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['father'][0]]['id']], $placeHolderText);
                            $posPNMap[$res['pos']] = $res['pn'];
                            $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        } else if (
                            isset($person['mother']) && count($person['mother']) > 0
                            && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                        ) {
                            // 母亲是本家人
                            $paintText .= $this->personMap[$person['mother'][0]]['name'];
                            $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['mother'][0]]['id']], $placeHolderText);
                            $posPNMap[$res['pos']] = $res['pn'];
                            $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                        } else {
                            $paintText .= '';
                        }
                    }

                    if($setDefaultDesc == 1){
                        $birthday = "出生日期不详 ";
                        $address = "出生地不详 ";
                    }else{
                        $birthday = '';
                        $address = '';
                    }
                    if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                        $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                    }

                    if ($person['address'] != '') {
                        $address = "出生地" . $person['address'] . " ";
                    }

                    if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                        $children[] = "";//无子女
                    } else if (count($person['son']) == 0) {
                        $total = count($person['daughter']);
                        $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";
                        $childrenId[] = 0;
                        
                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }
                    } else if (count($person['daughter']) == 0) {
                        $total = count($person['son']);
                        $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";
                        $childrenId[] = 0;

                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }
                    } else {
                        $total = count($person['son']);
                        $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子，" . PaintUtil::number2chinese(count($person['daughter'])) . "女";
                        $childrenId[] = 0;

                        foreach ($person['son'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }

                        foreach ($person['daughter'] as $personId) {
                            $children[] = $this->personMap[$personId]['name'];
                            $childrenId[] = $personId;
                        }
                    }

                    // 配偶
                    foreach ($person['spouse'] as $personId) {
                        if ($person['gender'] == 0) {
                            //女
                            $spouse .= "夫" . $this->personMap[$personId]['name'] . " ";
                        } else {
                            $spouse .= "妻" . $this->personMap[$personId]['name'] . " ";
                        }
                    }

                    // 当前人物的描述已经好了
                    $name = $person['name'];

                    if ($this->options['isShowRanking'] == 1) {
                        // 绘制排行
                        $greatGrandparentName = "";
                        $grandparentName = "";
                        $parentName = "";
                        $parent = $person;
                        foreach (range(0,2) as $i){
                            $parent = $this->getParent($parent);
                            if ($parent === []){
                                break;
                            }
                            if($i === 0){
                                $parentName = $parent['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) ."";
                            }elseif ($i === 1){
                                $grandparentName = $parent['name'] . "之孙";
                            }elseif ($i === 2){
//                                $greatGrandparent = $parent;
                                $greatGrandparentName = $parent['name'] . "曾孙" ;
                            }
//                            $parentName .= $parent['name'] . PaintUtil::getRanking($parent, $this->options['showAdoption']) ."\n";
                        }
//                        $topDesc = $parentName;
//                        if (
//                            isset($person['father']) && count($person['father']) > 0
//                            && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
//                        ) {
//                            // 父亲是本家人
//
//                            $topDesc = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption'])."\n";
//                        } else if (
//                            isset($person['mother']) && count($person['mother']) > 0
//                            && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
//                        ) {
//                            // 母亲是本家人
//                            $topDesc = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']);
//                        } else {
//                            $topDesc = '';
//                        }
                    } else {
                        $topDesc = '';
                    }

                    if(empty($birthday) || empty($address)){
                        # 去除中间空格
                        $paintText .= $birthday . $address;
                    }else{
                        $paintText .= $birthday . ' ' . $address;
                    }


                    $childrenLen = count($children);

                    if ($childrenLen == 1) {    // 没有孩子, 0是生几子
                        $paintText .= $children[0];
                    } else {
                        $isFirst = true;
                        for ($i = 0; $i < $childrenLen;) {
                            if ($isFirst) {
                                $paintText .= $children[$i];

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1];
                                    $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i+1]], $placeHolderText);
                                    $paintText .= " ";
                                    $posPNMap[$res['pos']] = $res['pn'];
                                }

                                if (isset($children[$i + 2])) {
                                    $paintText .= $children[$i + 2];
                                    $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i+2]], $placeHolderText);
                                    $paintText .= " ";
                                    $posPNMap[$res['pos']] = $res['pn'];
                                }

                                $i = $i + 3;
                                $isFirst = false;
                            } else {
                                $paintText .= $children[$i];
                                $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i]], $placeHolderText);
                                $paintText .= " ";
                                $posPNMap[$res['pos']] = $res['pn'];

                                if (isset($children[$i + 1])) {
                                    $paintText .= $children[$i + 1];
                                    $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i+1]], $placeHolderText);
                                    $paintText .= " ";
                                    $posPNMap[$res['pos']] = $res['pn'];
                                }
                                $i = $i + 2;
                            }
                        }
                    }

                    $paintText .= $spouse;

                    $person['profileText'] = trim($person['profileText']);
                    if (isset($person['profileText']) && $person['profileText'] != '') {
                        $posPNMap = [];
                        if (isset($descDataMap[$person['id']])) {
                            $descData = $descDataMap[$person['id']];
                            $paintText = $this->personMap[$person['id']]['profileText'];
                            $offset = 0;
                            foreach($descData as $item) {
                                $posPNMap[$item['pos']+$offset] = $this->descPageMap[$item['id']];
                                $offset++;
                            }
                        } else {
                            $posPNMap = [];
                        }
                        
                    }
                } // end if. 是否有剩余的文字未绘制检测完毕

                $beforeLen = mb_strlen($paintText);
                $paintText = ltrim($paintText);      // 去掉首位空格
                $trimedTextLen = $beforeLen - mb_strlen($paintText);
                $textOffset += $trimedTextLen;
                $name = trim($name);
                $topDesc = trim($topDesc);

                // 计算顶部描述，这里未修改，待修改
                $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth,$this->topDescHeight, $boxPadding, $descTextDistance, $this->config['descTopTextFontSize']);

                if ($availabelCount < mb_strlen($topDesc)) {
//                    var_dump([$topDesc,$person]);
                    // 剩余空间不够用
                    $this->leftTopDesc = mb_substr($topDesc, $availabelCount);
                    $topDesc = mb_substr($topDesc, 0, $availabelCount);
                }

                // 计算名字
                $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth, $this->nameDescHeight, $boxPadding, $descTextDistance, $this->descNameFontSize);

                if ($availabelCount < mb_strlen($name)) {
                    // 剩余空间不够用
                    $this->leftName = mb_substr($name, $availabelCount);
                    $name = mb_substr($name, 0, $availabelCount);
                }

                $preTextOffset = $textOffset;

                // 计算内容
                $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth, $this->descMaxLen, $boxPadding, $descTextDistance, $this->descNormalFontSize );
                if ($availabelCount < mb_strlen($paintText)) {
                    // 剩余空间不够用
                    $this->leftContent = mb_substr($paintText, $availabelCount);
                    $paintText = mb_substr($paintText, 0, $availabelCount);
                    $textOffset += $availabelCount;
                } else {
                    $textOffset = 0;
                }


                // 获取宽度, 这是实际会占用的宽度
                $width = $this->getWidth(
                    ['name'=>$parentName,'fontSize'=>(mb_strlen($parentName)<=4 ? true:false)],
                    ['name'=>$grandparentName,'fontSize'=>(mb_strlen($grandparentName)<=4 ? true:false)],
                    ['name'=>$greatGrandparentName,'fontSize'=>(mb_strlen($greatGrandparentName)<=4 ? true:false)],
                    $name, $paintText, $boxPadding
                );
//                var_dump([$width,$person['name']]);
//                var_dump($name);
//                var_dump([$name,$person['id']]);
                // 画顶部描述
                $y = $this->config['descStartY'];
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->config['descTopTextFontSize'];
                }

                $extraXOffsetForParentName = 0;
                $extraXOffsetTinyForParentName = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffsetForParentName = -$this->descParentNameFontSize;
                    $extraXOffsetTinyForParentName = -$this->descParentNameTinyFontSize ;
                }

                // 画每页的顶部
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->descNormalFontSize ;
                }

                # 绘制顶部
                $titleText = $person['title'] ? $person['title']:"";
//                $titleText = $person['title'] ? $person['title']:($theFirstPerson['sideText'] ? $theFirstPerson['sideText']:$theFirstPerson['name'] . "支系");
                PaintUtil::paintInBox(
                    $canvas,
                    $x + (mb_strlen($titleText)<=4 ? $extraXOffsetForParentName:$extraXOffsetTinyForParentName),
                    $this->config['descStartY'] + $this->config['descTopOffsetY'],
                    $width,
                    $this->topDescHeight,
                    $titleText,
                    $boxPadding,
                    mb_strlen($titleText)<=4 ? $this->config['descTinyTextDistanceForName']:$this->config['descTinyTextDistanceForName'],
                    $this->direction,
                    $this->normalTextFont,
                    mb_strlen($titleText)<=4 ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize  ,
                    0x000000,
                    true,
                    true,
                    false,
                    $posPNMap,
                    $preTextOffset
                );
//                $y = $y + $this->topDescHeight;

                # 绘制曾祖父
                PaintUtil::paintInBox(
                    $canvas,
                    $x + (mb_strlen($greatGrandparentName)<=4 ? $extraXOffsetForParentName:$extraXOffsetTinyForParentName),
                    $y + $this->descTextOffsetY + $this->topDescHeight,
                    $width,
                    $this->topDescHeight,
                    $greatGrandparentName,
                    $boxPadding,
                    mb_strlen($greatGrandparentName)<=4 ? $this->config['descTinyTextDistanceForName']:$this->config['descTinyTextDistanceForName'],
                    $this->direction,
                    $this->normalTextFont,
                    mb_strlen($greatGrandparentName)<=4 ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize  ,
                    0x000000,
                    true,
                    true
                );
                $y = $y + $this->parentNameDescHeight;

                # 绘制祖父
                PaintUtil::paintInBox(
                    $canvas,
                    $x + (mb_strlen($grandparentName)<=4 ? $extraXOffsetForParentName:$extraXOffsetTinyForParentName),
                    $y + $this->descTextOffsetY + $this->topDescHeight,
                    $width,
                    $this->topDescHeight,
                    $grandparentName,
                    $boxPadding,
                    mb_strlen($grandparentName)<=4 ? $this->config['descTinyTextDistanceForName']:$this->config['descTinyTextDistanceForName'],
                    $this->direction,
                    $this->normalTextFont,
                    mb_strlen($grandparentName)<=4 ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize ,
                    0x000000,
                    true,
                    true
                );
                $y = $y + $this->parentNameDescHeight;

                # 绘制父亲
                PaintUtil::paintInBox(
                    $canvas,
                    $x + (mb_strlen($parentName)<=4 ? $extraXOffsetForParentName:$extraXOffsetTinyForParentName),
                    $y + $this->descTextOffsetY + $this->topDescHeight,
                    $width,
                    $this->topDescHeight,
                    $parentName,
                    $boxPadding,
                    mb_strlen($parentName)<=4 ? $this->config['descTinyTextDistanceForName']:$this->config['descTinyTextDistanceForName'],
                    $this->direction,
                    $this->normalTextFont,
                    mb_strlen($parentName)<=4 ? $this->descParentNameFontSize:$this->descParentNameTinyFontSize ,
                    0x000000,
                    true,
                    true
                );
                $y = $y +$this->parentNameDescHeight;

                // 画名字
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->descNameFontSize;
                }

                PaintUtil::paintInBox(
                    $canvas,
                    $x + $extraXOffset,
                    $y + $this->descTextOffsetY + $this->topDescHeight,
                    $width,
                    $this->nameDescHeight,
                    $name,
                    $boxPadding,
                    $descTextDistance,
                    $this->direction,
                    $this->normalTextFont,
                    $this->descNameFontSize,
                    0x000000,
                    true,
                    true,
                    true
                );
                $y = $y + $this->parentNameDescHeight;
//                var_dump([$canvas,
//                    $x + $extraXOffset,
//                    $y + $this->descTextOffsetY + $this->topDescHeight,
//                    $width,
//                    $this->nameDescHeight,
//                    $name,
//                    $boxPadding,
//                    $descTextDistance,
//                    $this->direction,
//                    $this->normalTextFont,
//                    $this->descNameFontSize,
//                    0x000000,
//                    true,
//                    true,
//                    true]);
                // 画内容
                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->descNormalFontSize;
                }

                PaintUtil::paintInBox(
                    $canvas,
                    $x + $extraXOffset,
                    $y + $this->descTextOffsetY + $this->parentNameDescHeight,
                    $width,
                    $this->descMaxLen,
                    $paintText,
                    $boxPadding,
                    $descTextDistance,
                    $this->direction,
                    $this->normalTextFont,
                    $this->descNormalFontSize,
                    0x000000,
                    true,
                    false,
                    false,
                    $posPNMap,
                    $preTextOffset
                );

                // 画完一个人要换行
                $this->wrapAfterContent($x, $y, $width);
                PaintUtil::imagelineWithWidth($canvas, $x, $this->config['descLineWithWidthOffsetY'], $x, $y + $this->pageHeight, 1);

                $leftWidth -= $width;

//                if ($pageNum !== $this->totalPage){
                    // 画每页的顶部
//                    $extraXOffset = 0;
//                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
//                        $extraXOffset = -$this->descNormalFontSize ;
//                    }
//
//                    PaintUtil::paintInBox(
//                        $canvas,
//                        $this->config['descStartX'] + $extraXOffset,
//                        $this->config['descStartY'] + $this->config['descTopOffsetY'] ,
//                        $this->config['pageWidth'],
//                        $this->topDescHeight,
//                        $theFirstPerson['sideText'] ? $theFirstPerson['sideText']:$theFirstPerson['name'] . "支系",
//                        $boxPadding,
//                        $descTextDistance,
//                        $this->direction,
//                        $this->normalTextFont,
//                        $this->config['descTopTextFontSize'],
//                        0x000000,
//                        true,
//                        true,
//                        false,
//                        $posPNMap,
//                        $preTextOffset
//                    );
//                    $pageNum = $this->totalPage;
//                }

                if ($leftWidth <= 0) {

                    // 新写一页
                    imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                    $time = date('h:i:s');
                    echo "绘制描述第{$this->totalPage}页, {$time}\n";
                    $this->totalPage++;
                    $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->deschalfImgPath);
                    $isPainted = false;
                    $leftWidth = $this->config['pageWidth'];
                    $x = $this->config['descStartX'];
                    $y = $this->config['descStartY'];
                }
            } // end while
        }

        if ($isPainted) {
            imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
            $time = date('h:i:s');
            echo "绘制描述第{$this->totalPage}页, {$time}\n";
            $this->totalPage++;
        }
    }
}
