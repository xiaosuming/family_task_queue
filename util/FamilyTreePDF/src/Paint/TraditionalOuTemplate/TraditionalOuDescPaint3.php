<?php

/**
 * 行传的绘制，基本模板
 */

namespace FamilyTreePDF\Paint\TraditionalOuTemplate;

use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\FontManager;

class TraditionalOuDescPaint3 implements PaintInterface
{

    private $config;
    private $totalPage;
    private $personMap;
    private $descPageMap;// 人物id所在页码的映射
    private $minLevel;
    private $maxLevel;
    private $allLevelList;
    private $calculatePage; //计算用的页数变量

    private $direction; // 排版方向,
    private $options;

    private $leftTopDesc;   // 当页没画完的文字
    private $leftContent;
    private $leftName;

    private $normalTextFont;    // 正文的字体配置
    private $isPageNumDisabled; // 页码禁用
    private $branchLevelData;  # 支派各世代的数据
//    private $personIdDict;

    public function __construct()
    {
        $this->direction = SysConst::$LEFT_TO_RIGHT;
        $this->leftTopDesc = '';
        $this->leftContent = '';
        $this->leftName = '';
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
        $this->isPageNumDisabled = false;
    }

    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }


    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef();
        $this->personMap = &$context->getPersonMapRef();
        $this->descPageMap = &$context->getDescPageMapRef();
        $this->minLevel = &$context->getMinLevelRef();
        $this->maxLevel = &$context->getMaxLevelRef();
        $this->allLevelList = &$context->getAllLevelListRef();

        $this->options = $context->getOptions();
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setFont($font)
    {
        $this->normalTextFont = $font;
    }

    public function setPageNum($disabled)
    {
        $this->isPageNumDisabled = $disabled;
    }

    public function input($paintData, $orderBy = false)
    {
        if (!$orderBy) {
            return null;
        }
        $data = [];
        $levels = [];
        $levelData = [];
        $personIdDict = [];
        foreach ($paintData as $k => $v) {
            if ($v['zan']) {
                $data[$v['branchName']][$v['level']][] = [
                    'name' => $v['zanType'],
                    'yueZanText' => $v['zan'] ? true : false,
                    'profileText'=>$v['zan'],
                    'branchName' => $v['branchName'],
                    'id' => $v['id'],
                    'familyIndex' => $v['familyIndex'],
//                    'level' => $v['level']
                ];
            }

            $data[$v['branchName']][$v['level']][] = array_merge($v, ['yueZanText' => null]);
            $levels[$v['branchName']][] = $v['level'];
//            $personIdDict[$v['id']] = $v;
        }
        foreach ($levels as $k => $v) {
            $levelData[$k]['maxLevel'] = max($v);
            $levelData[$k]['minLevel'] = min($v);
            $levelData[$k]['levels'] = $v;
        }
        $this->allLevelList = $data;
        $this->branchLevelData = $levelData;
//        $this->personIdDict = $personIdDict;
    }


    /**
     * 获取一个人物的排行称呼，如长子，次子，长女等
     * @param $person 人物信息
     */
    private function getRanking($person)
    {
        // 先检查人物是否有ranking
        if ($person['ranking'] > 0) {
            return PaintUtil::convertRanking($person['ranking'], $person['gender']);
        }

        // 没有排行，目前根据加入的顺序来判断
        $siblings = array_merge($person['brother'], $person['sister']);

        sort($siblings);

        foreach ($siblings as $index => $personId) {
            if ($person['id'] == $personId) {
                return PaintUtil::convertRanking($index + 1, $person['gender']);
            }
        }
    }

    /**
     * 绘制世代之后的换行
     */
    private function wrapAfterLevel(&$x, &$y)
    {
        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
            $x += $this->config['descUnitWidth'];
            $y = $this->config['descStartY'];
        } else {
            $x -= $this->config['descUnitWidth'];
            $y = $this->config['descStartY'];
        }
    }

    private function wrapAfterContent(&$x, &$y, $width)
    {
        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
            $x += $width;
            $y = $this->config['descStartY'];
        } else {
            $x -= $width;
            $y = $this->config['descStartY'];
        }
    }

    private function updateCanvas(&$canvas, $leftWidth)
    {
    }

    /**
     * 获取宽度，传入人物的顶部描述，名字，内容。返回适合的宽度
     * 宽度会取三者的最大值，并且是最小宽度(40)的整数倍
     */
    private function getWidth($topDesc, $name, $content, $boxPadding)
    {

        $descTextDistance = $this->config['descTextDistance'];

        $topWidth = PaintUtil::getTextsWidthOu3(
            $topDesc,
            $this->config['descTopTextFontSize'],
            $descTextDistance,
            $descTextDistance,
            $this->config['topDescHeight'] - $boxPadding['top']
        );

        $nameWidth = PaintUtil::getTextsWidthOu3(
            $name,
            $this->config['descNameFontSize'],
            $descTextDistance,
            $descTextDistance,
            $this->config['nameDescHeight'] - $boxPadding['top']
        );

        $contentWidth = PaintUtil::getTextsWidthOu3(
            $content,
            $this->config['descNormalFontSize'],
            $descTextDistance,
            $descTextDistance,
            $this->config['descMaxLen'] - $boxPadding['top']
        );


        $maxWidth = max($topWidth, $nameWidth, $contentWidth);
//        print_r([203, $contentWidth, $maxWidth]);

        // echo "maxWidth".$maxWidth;

        // $maxWidth += $descTextDistance * 2;

        $unitWidth = $this->config['descUnitWidth'];

        $width = ceil($maxWidth / $unitWidth) * $unitWidth;

        return $width;
    }

    public function paint($setLevel = 1, $setDefaultDesc = 0)
    {
        $placeHolderText = ''; // 页码的占位符
        if ($this->isPageNumDisabled) {
            $placeHolderText = '';
        }

        $personPosPNMap = [];
        $descDataMap = [];
        $GLOBALS['branchName'] = [];  # 用于存储支派信息

        $isPainted = false;     // 标记新开的canvas有没有被绘制，防止空页产生
        $maxLen = $this->config['descMaxLen'];
        $descPadding = $this->config['descPadding'];
        $descTextDistance = $this->config['descTextDistance'];

        $boxPadding = ['top' => $descPadding, 'left' => $descPadding, 'bottom' => $descPadding, 'right' => $descPadding];

        $this->calculatePage = $this->totalPage + 1;    // 提前计算的页码
        /********************************提前计算*********************************/
        $calculateX = $this->config['descStartX'];
        $calculateY = $this->config['descStartY'];
        $calculateLeftWidth = $this->config['pageWidth'];

        foreach ($this->allLevelList as $branchName => $peopleInfo) {
            for ($key = $this->branchLevelData[$branchName]['minLevel']; $key <= $this->branchLevelData[$branchName]['maxLevel']; $key++) {
                if (!in_array($key, $this->branchLevelData[$branchName]['levels'])) {
                    continue;
                }
                $isPainted = true;
                $level = $peopleInfo[$key];
                $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->config['descLevelFontSize'];
                }

                if (in_array($key, $this->branchLevelData[$branchName]['levels'])) {
                    // 计算世代的换行
                    $this->wrapAfterLevel($calculateX, $calculateY);
                }


                $calculateLeftWidth -= $this->config['descUnitWidth'];

                // 检测是否还有剩余宽度
                if ($calculateLeftWidth <= 0) {
                    // 新写一页
                    $this->calculatePage++;
                    $isPainted = false;
                    $calculateLeftWidth = $this->config['pageWidth'];
                    $calculateX = $this->config['descStartX'];
                    $calculateY = $this->config['descStartY'];
                }

                $personCount = $level ? count($level) : 0;

                //剩余字没画完 也继续循环
                $textOffset = 0;
                $isLastPerson = false;
                while ($personCount > 0 || $this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                    // 检查是否还有剩余未画完的文字
                    if ($this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                        // 将剩余的文字画完，
                        if ($personCount === 1 && $isLastPerson){
                            $personCount = 0;
                            $isLastPerson = false;
                        }

                        $topDesc = $this->leftTopDesc;
                        $name = $this->leftName;
                        $paintText = $this->leftContent;

                        $this->leftTopDesc = '';
                        $this->leftContent = '';
                        $this->leftName = '';
                    } else {
                        $personCount--;

                        $person = $level[$personCount];
                        // 记录这个人的页数
                        $this->descPageMap[$person['id']] = $this->calculatePage;

                        $children = array();
                        $childrenId = array();
                        $spouse = '';
                        $paintText = "";
                        $noHolderRank = "";
                        if (!$person['yueZanText']) {
                            //添加自己在父亲的排行信息
                            if ($this->options['isShowRanking'] == 1) {
                                // 绘制排行
                                if (
                                    isset($person['father']) && count($person['father']) > 0
                                    && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                                ) {
                                    // 父亲是本家人
//                                    $paintText .= $this->personMap[$person['father'][0]]['name'];
//                                $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['father'][0]]['id']], $placeHolderText);
//                                $posPNMap[$res['pos']] = $res['pn'];
//                                    $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
//                                    $noHolderRank = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
//                                    $noHolderRank = PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';

                                } else if (
                                    isset($person['mother']) && count($person['mother']) > 0
                                    && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                                ) {
                                    // 母亲是本家人
//                                    $paintText .= $this->personMap[$person['mother'][0]]['name'];
//                                $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['mother'][0]]['id']], $placeHolderText);
//                                $posPNMap[$res['pos']] = $res['pn'];
//                                    $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
//                                    $noHolderRank = PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
//                                    $noHolderRank = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
//                                    $noHolderRank = PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';

                                } else {
                                    $paintText .= '';
                                }
                            }

                            if ($setDefaultDesc == 1) {
                                $birthday = "出生日期不详 ";
                                $address = "出生地不详 ";
                            } else {
                                $birthday = '';
                                $address = '';
                            }
                            if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                                $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                            }

                            if ($person['address'] != '') {
                                $address = "出生地" . $person['address'] . " ";
                            }

                            if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                                $children[] = "";//无子女
                            } else if (count($person['son']) == 0) {
                                $total = count($person['daughter']);
                                $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }
                            } else if (count($person['daughter']) == 0) {
                                $total = count($person['son']);
                                $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }
                            } else {
                                $total = count($person['son']);
                                $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子，" . PaintUtil::number2chinese(count($person['daughter'])) . "女";

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'] . $placeHolderText;
                                    $childrenId[] = $personId;
                                }
                            }

                            // 配偶
                            foreach ($person['spouse'] as $personId) {
                                if ($person['gender'] == 0) {
                                    //女
                                    $spouse .= "夫" . $this->personMap[$personId]['name'] . $placeHolderText . ' ';
                                } else {
                                    $spouse .= "妻" . $this->personMap[$personId]['name'] . $placeHolderText . ' ';
                                }
                            }

                            // 当前人物的描述已经好了
                            $name = $person['name'];

                            if ($this->options['isShowRanking'] == 1) {
                                // 绘制排行
                                if (
                                    isset($person['father']) && count($person['father']) > 0
                                    && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                                ) {
                                    // 父亲是本家人

                                    $topDesc = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']);
                                } else if (
                                    isset($person['mother']) && count($person['mother']) > 0
                                    && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                                ) {
                                    // 母亲是本家人
                                    $topDesc = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption']);
                                } else {
                                    $topDesc = '';
                                }
                            } else {
                                $topDesc = '';
                            }

                            if (empty($birthday) || empty($address)) {
                                # 去除中间空格
                                $paintText .= $birthday . $address;
                            } else {
                                $paintText .= $birthday . ' ' . $address;
                            }


                            $childrenLen = count($children);

                            if ($childrenLen == 1) {    // 没有孩子
                                $paintText .= $children[0];
                            } else {
                                $isFirst = true;
                                for ($i = 0; $i < $childrenLen;) {
                                    if ($isFirst) {
                                        $paintText .= $children[$i];

                                        if (isset($children[$i + 1])) {
                                            $paintText .= $children[$i + 1] . " ";
                                        }

                                        if (isset($children[$i + 2])) {
                                            $paintText .= $children[$i + 2] . " ";
                                        }

                                        $i = $i + 3;
                                        $isFirst = false;
                                    } else {
                                        $paintText .= $children[$i] . " ";

                                        if (isset($children[$i + 1])) {
                                            $paintText .= $children[$i + 1] . " ";
                                        }
                                        $i = $i + 2;
                                    }
                                }
                            }

                            $paintText .= $spouse;

                            $person['profileText'] = trim($person['profileText']);
                            if (isset($person['profileText']) && $person['profileText'] != '') {
                                $profileText = $noHolderRank . $person['profileText'];
                                $person['profileText'] = $profileText;
//                                $descData = PaintUtil::processProfileText($person, $this->personMap);
//
//                                $offset = 0;
//                                foreach ($descData as $item) {
//                                    $pos = $item['pos'];
//
//                                    $t1 = mb_substr($profileText, 0, $pos + $offset);
//                                    $t2 = mb_substr($profileText, $pos + $offset);
//                                    $profileText = $t1 . $placeHolderText . $t2;
//                                    $offset++;
//                                }
//                                $paintText = $profileText;
                                $paintText = $profileText . ' ' . $paintText;
                                ###########################################
                                # 临时修改
//                                $paintText = $person['profileText'];
//                                $paintText = $person['profileText'] . ' ' . $paintText;
                                ###########################################
                                $this->personMap[$person['id']]['profileText'] = $profileText;
//                                $descDataMap[$person['id']] = $descData;
                            }else{
                                $paintText = $person['profileText'];
                            }
                        } // end if. 是否有剩余的文字未绘制检测完毕
                    }

                    if (!$person['yueZanText']) {
                        $beforeLen = mb_strlen($paintText);
                        $paintText = ltrim($paintText);      // 去掉首位空格
                        $trimedTextLen = $beforeLen - mb_strlen($paintText);
                        $textOffset += $trimedTextLen;
                        $name = trim($name);
                        $topDesc = trim($topDesc);
                    } else {
                        $name = $person['name'];
                        $topDesc = '';
                        $beforeLen = mb_strlen($paintText);
                        $paintText = ltrim($paintText);      // 去掉首位空格
                        $trimedTextLen = $beforeLen - mb_strlen($paintText);
                        $textOffset += $trimedTextLen;
                        $name = trim($name);
                        $topDesc = trim($topDesc);
//                        $paintText = $paintText ? $paintText : ltrim($person['yueZanText']);
//                        $beforeLen = mb_strlen($paintText);
//                        $trimedTextLen = $beforeLen - mb_strlen($paintText);
//                        $textOffset += $trimedTextLen;
                    }

                    // 计算顶部描述
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->config['topDescHeight'], $boxPadding, $descTextDistance, $this->config['descTopTextFontSize']);

                    if ($availabelCount < mb_strlen($topDesc)) {
                        // 剩余空间不够用
                        $this->leftTopDesc = mb_substr($topDesc, $availabelCount);
                        $topDesc = mb_substr($topDesc, 0, $availabelCount);

                        // echo "剩下了top\n";
                    }

                    // 计算名字
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->config['nameDescHeight'], $boxPadding, $descTextDistance, $this->config['descNameFontSize']);

                    if ($availabelCount < mb_strlen($name)) {
                        // 剩余空间不够用
                        $this->leftName = mb_substr($name, $availabelCount);
                        $name = mb_substr($name, 0, $availabelCount);
                    }

                    // 计算内容
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($calculateLeftWidth, $this->config['descMaxLen'], $boxPadding, $descTextDistance, $this->config['descNormalFontSize']);
                    if ($availabelCount < mb_strlen($paintText)) {
                        if ($personCount === 0 && !$isLastPerson){
                            $personCount += 1;
                            $isLastPerson = true;
                        }
                        // 剩余空间不够用
                        $this->leftContent = mb_substr($paintText, $availabelCount);
                        $paintText = mb_substr($paintText, 0, $availabelCount);
                    }


                    // 获取宽度, 这是实际会占用的宽度
                    $width = $this->getWidth($topDesc, $name, $paintText, $boxPadding);

                    // 画顶部描述
                    $calculateY = $this->config['descStartY'];
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descTopTextFontSize'];
                    }

                    $calculateY = $calculateY + $this->config['topDescHeight'];


                    // 画名字
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descNameFontSize'];
                    }

                    $calculateY = $calculateY + $this->config['nameDescHeight'];

                    // 画内容
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descNormalFontSize'];
                    }

                    // 画完一个人要换行
                    $this->wrapAfterContent($calculateX, $calculateY, $width);

                    $calculateLeftWidth -= $width;

                    if ($calculateLeftWidth <= 0) {
                        // 新写一页
                        $this->calculatePage++;
                        $isPainted = false;
                        $calculateLeftWidth = $this->config['pageWidth'];
                        $calculateX = $this->config['descStartX'];
                        $calculateY = $this->config['descStartY'];
                    }
                }
            }
        }

        /*******************************实际绘制***************************/
        $x = $this->config['descStartX'];
        $y = $this->config['descStartY'];
        $leftWidth = $this->config['pageWidth'];


        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->config['deschalfImgPath']);
        $isPainted = false;
        foreach ($this->allLevelList as $branchName => $peopleInfo) {
            for ($key = $this->branchLevelData[$branchName]['minLevel']; $key <= $this->branchLevelData[$branchName]['maxLevel']; $key++) {
                if (!in_array($key, $this->branchLevelData[$branchName]['levels'])) {
                    continue;
                }


                $isPainted = true;
                $level = $peopleInfo[$key];
//                print_r([593, $level]);
                $levelText = "第" . PaintUtil::number2chinese($key - $this->minLevel + $setLevel) . "世";

                $extraXOffset = 0;
                if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                    $extraXOffset = -$this->config['descLevelFontSize'];
                }

                // 绘制世代
                PaintUtil::paintInBox(
                    $canvas,
                    $x + $extraXOffset,
                    $y + $this->config['descLevelYOffset'],
                    $this->config['descUnitWidth'],
                    $maxLen,
                    $levelText,
                    $boxPadding,
                    $descTextDistance,
                    $this->direction,
                    $this->normalTextFont,
                    $this->config['descLevelFontSize'],
                    0x000000,
                    true,
                    false,
                    true
                );

                // 画完世代要换行
                $this->wrapAfterLevel($x, $y);


                // 在这里画一条竖线
                PaintUtil::imagelineWithWidth($canvas, $x, $y, $x, $y + $this->config['pageHeight'], 1);

                $leftWidth -= $this->config['descUnitWidth'];   // 世代占一个单元的宽度

                // 检测是否还有剩余宽度
                if ($leftWidth <= 0) {
                    // 新写一页
                    imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
                    $GLOBALS['branchName'][$this->totalPage + 1] = $person['branchName'];
                    $time = date('h:i:s');
                    echo "绘制描述第{$this->totalPage}页, {$time}\n";
                    $this->totalPage++;
                    $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->config['deschalfImgPath']);
                    $isPainted = false;
                    $leftWidth = $this->config['pageWidth'];
                    $x = $this->config['descStartX'];
                    $y = $this->config['descStartY'];
                }

                $personCount = count($level);

                $textOffset = 0;    // 当前的文字偏移量(leftContent导致的)
                $preTextOffset = 0; // 之前的文字偏移量
                $isLastPerson = false;
                while ($personCount > 0) {
                    $isPainted = true;
                    // 检查是否还有剩余未画完的文字
                    if ($this->leftTopDesc != '' || $this->leftContent != '' || $this->leftName != '') {
                        // 将剩余的文字画完，

                        if ($personCount === 1 && $isLastPerson){
                            $personCount = 0;
                            $isLastPerson = false;
                        }

                        $topDesc = $this->leftTopDesc;
                        $name = $this->leftName;
                        $paintText = $this->leftContent;
                        var_dump($paintText);
                        $this->leftTopDesc = '';
                        $this->leftContent = '';
                        $this->leftName = '';
                    } else {
                        $posPNMap = [];     // 文字位置和页码的映射
                        $personCount--;

                        $person = $level[$personCount];
//                        var_dump($person['name']);

                        $children = array();
                        $childrenId = array();
                        $spouse = '';

                        if (!$person['yueZanText']) {
                            $paintText = "";
                            //添加自己在父亲的排行信息
                            if ($this->options['isShowRanking'] == 1) {
                                // 绘制排行
                                if (
                                    isset($person['father']) && count($person['father']) > 0
                                    && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                                ) {
                                    // 父亲是本家人
//                                    $paintText .= $this->personMap[$person['father'][0]]['name'];
//                                $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['father'][0]]['id']], $placeHolderText);
//                                $posPNMap[$res['pos']] = $res['pn'];
//                                    $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                                } else if (
                                    isset($person['mother']) && count($person['mother']) > 0
                                    && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                                ) {
                                    // 母亲是本家人
//                                    $paintText .= $this->personMap[$person['mother'][0]]['name'];
//                                $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$this->personMap[$person['mother'][0]]['id']], $placeHolderText);
//                                $posPNMap[$res['pos']] = $res['pn'];
//                                    $paintText .= PaintUtil::getRanking($person, $this->options['showAdoption']) . ' ';
                                } else {
                                    $paintText .= '';
                                }
                            }

                            if ($setDefaultDesc == 1) {
                                $birthday = "出生日期不详 ";
                                $address = "出生地不详 ";
                            } else {
                                $birthday = '';
                                $address = '';
                            }
                            if ($person['birthday'] != null && $person['birthday'] != '0001-01-01') {
                                $birthday = "出生日期" . PaintUtil::dateToChinese($person['birthday']) . " ";
                            }

                            if ($person['address'] != '') {
                                $address = "出生地" . $person['address'] . " ";
                            }

                            if (count($person['son']) == 0 && count($person['daughter']) == 0) {
                                $children[] = "";//无子女
                            } else if (count($person['son']) == 0) {
                                $total = count($person['daughter']);
                                $children[] = "生" . PaintUtil::number2chinese($total) . "女 ";
                                $childrenId[] = 0;

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }
                            } else if (count($person['daughter']) == 0) {
                                $total = count($person['son']);
                                $children[] = "生" . PaintUtil::number2chinese($total) . "子 ";
                                $childrenId[] = 0;

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }
                            } else {
                                $total = count($person['son']);
                                $children[] = "生" . PaintUtil::number2chinese(count($person['son'])) . "子，" . PaintUtil::number2chinese(count($person['daughter'])) . "女";
                                $childrenId[] = 0;

                                foreach ($person['son'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }

                                foreach ($person['daughter'] as $personId) {
                                    $children[] = $this->personMap[$personId]['name'];
                                    $childrenId[] = $personId;
                                }
                            }

                            // 配偶
                            foreach ($person['spouse'] as $personId) {
                                if ($person['gender'] == 0) {
                                    //女
                                    $spouse .= "夫" . $this->personMap[$personId]['name'] . " ";
                                } else {
                                    $spouse .= "妻" . $this->personMap[$personId]['name'] . " ";
                                }
                            }

                            // 当前人物的描述已经好了
                            $name = $person['name'];

                            if ($this->options['isShowRanking'] == 1) {
                                // 绘制排行
                                if (
                                    isset($person['father']) && count($person['father']) > 0
                                    && isset($this->personMap[$person['father'][0]]) && $this->personMap[$person['father'][0]]['type'] == 1
                                ) {
                                    // 父亲是本家人
                                    $showZhiZi = $person['gender'] == 0 ?  $this->personMap[$person['father'][0]]['daughter']:$this->personMap[$person['father'][0]]['son'];
                                    $showZhiZi = count($showZhiZi) == 1;
                                    $topDesc = $this->personMap[$person['father'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption'], $showZhiZi);
                                } else if (
                                    isset($person['mother']) && count($person['mother']) > 0
                                    && isset($this->personMap[$person['mother'][0]]) && $this->personMap[$person['mother'][0]]['type'] == 1
                                ) {
                                    // 母亲是本家人
                                    $showZhiZi = $person['gender'] == 0 ?  $this->personMap[$person['mother'][0]]['daughter']:$this->personMap[$person['mother'][0]]['son'];
                                    $showZhiZi = count($showZhiZi) == 1;
                                    $topDesc = $this->personMap[$person['mother'][0]]['name'] . PaintUtil::getRanking($person, $this->options['showAdoption'] , $showZhiZi);
                                } else {
                                    $topDesc = '';
                                }
                            } else {
                                $topDesc = '';
                            }

                            if (empty($birthday) || empty($address)) {
                                # 去除中间空格
                                $paintText .= $birthday . $address;
                            } else {
                                $paintText .= $birthday . ' ' . $address;
                            }


                            $childrenLen = count($children);

                            if ($childrenLen == 1) {    // 没有孩子, 0是生几子
                                $paintText .= $children[0];
                            } else {
                                $isFirst = true;
                                for ($i = 0; $i < $childrenLen;) {
                                    if ($isFirst) {
                                        $paintText .= $children[$i];

                                        if (isset($children[$i + 1])) {
                                            $paintText .= $children[$i + 1];
//                                        $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i + 1]], $placeHolderText);
                                            $paintText .= " ";
//                                        $posPNMap[$res['pos']] = $res['pn'];
                                        }

                                        if (isset($children[$i + 2])) {
                                            $paintText .= $children[$i + 2];
//                                        $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i + 2]], $placeHolderText);
                                            $paintText .= " ";
//                                        $posPNMap[$res['pos']] = $res['pn'];
                                        }

                                        $i = $i + 3;
                                        $isFirst = false;
                                    } else {
                                        $paintText .= $children[$i];
                                        $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i]], $placeHolderText);
                                        $paintText .= " ";
                                        $posPNMap[$res['pos']] = $res['pn'];

                                        if (isset($children[$i + 1])) {
                                            $paintText .= $children[$i + 1];
                                            $res = PaintUtil::concatPageNum($paintText, $this->descPageMap[$childrenId[$i + 1]], $placeHolderText);
                                            $paintText .= " ";
                                            $posPNMap[$res['pos']] = $res['pn'];
                                        }
                                        $i = $i + 2;
                                    }
                                }
                            }

                            $paintText .= $spouse;

//                            $person['profileText'] = trim($person['profileText']);
//                            if (isset($person['profileText']) && $person['profileText'] != '') {
//                                $profileText = $noHolderRank . $person['profileText'];
//                                $person['profileText'] = $profileText;
//                                $paintText = $profileText . ' '. $paintText;
//                                $this->personMap[$person['id']]['profileText'] = $profileText;
//                            } else {
//                                    $posPNMap = [];
//                            }

                            $person['profileText'] = trim($person['profileText']);
                            if (isset($person['profileText']) && $person['profileText'] != '') {
                                $paintText = $person['profileText'] . ' ' . $paintText;
//
//                                $posPNMap = [];
//                                if (isset($descDataMap[$person['id']])) {
//                                    $descData = $descDataMap[$person['id']];
//                                    $paintText = $this->personMap[$person['id']]['profileText'].  ' ' . $paintText;;
//                                    $offset = 0;
//                                    foreach ($descData as $item) {
//                                        $posPNMap[$item['pos'] + $offset] = $this->descPageMap[$item['id']];
//                                        $offset++;
//                                    }
//                                } else {
//                                    $posPNMap = [];
//                                }

                            }
                        }else{
                            $paintText = $person['profileText'];
                        }
                        $paintText = $person['profileText'];
                    } // end if. 是否有剩余的文字未绘制检测完毕

                    if (!$person['yueZanText']) {
                        $beforeLen = mb_strlen($paintText);
                        $paintText = ltrim($paintText);      // 去掉首位空格
                        $trimedTextLen = $beforeLen - mb_strlen($paintText);
                        $textOffset += $trimedTextLen;
                        $name = trim($name);
                        $topDesc = trim($topDesc);
                    } else {
                        $name = $person['name'];
                        $topDesc = '';
                        $beforeLen = mb_strlen($paintText);
                        $paintText = ltrim($paintText);      // 去掉首位空格
                        $trimedTextLen = $beforeLen - mb_strlen($paintText);
                        $textOffset += $trimedTextLen;
                        $name = trim($name);
                        $topDesc = trim($topDesc);
                    }


                    // 计算顶部描述
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth, $this->config['topDescHeight'], $boxPadding, $descTextDistance, $this->config['descTopTextFontSize']);

                    if ($availabelCount < mb_strlen($topDesc)) {
                        // 剩余空间不够用
                        $this->leftTopDesc = mb_substr($topDesc, $availabelCount);
                        $topDesc = mb_substr($topDesc, 0, $availabelCount);
                    }

                    // 计算名字
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth, $this->config['nameDescHeight'], $boxPadding, $descTextDistance, $this->config['descNameFontSize']);

                    if ($availabelCount < mb_strlen($name)) {
                        // 剩余空间不够用
                        $this->leftName = mb_substr($name, $availabelCount);
                        $name = mb_substr($name, 0, $availabelCount);
                    }

                    $preTextOffset = $textOffset;

                    // 计算内容
                    $availabelCount = PaintUtil::getAvailableTextCountInBox($leftWidth, $this->config['descMaxLen'], $boxPadding, $descTextDistance, $this->config['descNormalFontSize']);
                    if ($availabelCount < mb_strlen($paintText)) {
                        if ($personCount === 0 && !$isLastPerson){
                            $personCount += 1;
                            $isLastPerson = true;
                        }
                        // 剩余空间不够用
                        $this->leftContent = mb_substr($paintText, $availabelCount);
                        $paintText = mb_substr($paintText, 0, $availabelCount);
                        $textOffset += $availabelCount;
                    } else {
                        $textOffset = 0;
                    }


                    // 获取宽度, 这是实际会占用的宽度
                    $width = $this->getWidth($topDesc, $name, $paintText, $boxPadding);
//                    var_dump(927);
//                    print_r([926, $availabelCount, mb_strlen($paintText), $width]);
                    // 画顶部描述
                    $y = $this->config['descStartY'];
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descTopTextFontSize'];
                    }
//                    var_dump($name);
                    PaintUtil::paintInBox(
                        $canvas,
                        $x + $extraXOffset,
                        $y + $this->config['descTextOffsetY'],
                        $width,
                        $this->config['topDescHeight'],
                        $topDesc,
                        $boxPadding,
                        $descTextDistance,
                        $this->direction,
                        $this->normalTextFont,
                        $this->config['descTopTextFontSize'],
                        0x000000,
                        true,
                        true
                    );
                    $y = $y + $this->config['topDescHeight'];
//                    print_r([901, $person['name'], $topDesc, $branchName]);

                    // 画名字
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descNameFontSize'];
                    }

                    PaintUtil::paintInBox(
                        $canvas,
                        $x + $extraXOffset,
                        $y + $this->config['descTextOffsetY'],
                        $width,
                        $this->config['nameDescHeight'],
                        $name,
                        $boxPadding,
                        $descTextDistance,
                        $this->direction,
                        $this->normalTextFont,
                        $this->config['descNameFontSize'],
                        0x000000,
                        true,
                        true,
                        true
                    );
                    $y = $y + $this->config['nameDescHeight'];

                    // 画内容
                    $extraXOffset = 0;
                    if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                        $extraXOffset = -$this->config['descNormalFontSize'];
                    }

                    PaintUtil::paintInBox(
                        $canvas,
                        $x + $extraXOffset,
                        $y + $this->config['descTextOffsetY'],
                        $width,
                        $this->config['descMaxLen'],
                        $paintText,
                        $boxPadding,
                        $descTextDistance,
                        $this->direction,
                        $this->normalTextFont,
                        $this->config['descNormalFontSize'],
                        0x000000,
                        true,
                        false,
                        false,
                        $posPNMap,
                        $preTextOffset
                    );

                    // 画完一个人要换行
                    $this->wrapAfterContent($x, $y, $width);
                    PaintUtil::imagelineWithWidth($canvas, $x, $y, $x, $y + $this->config['pageHeight'], 1);

                    $leftWidth -= $width;

                    if ($leftWidth <= 0) {
//                        print_r([602, $person['name']]);
                        // 新写一页
                        imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
//                        var_dump($this->totalPage);
//                        sleep(10);
                        $time = date('h:i:s');
                        echo "绘制描述第{$this->totalPage}页, {$time}\n";
                        $GLOBALS['branchName'][$this->totalPage + 1] = $person['branchName'];
                        $this->totalPage++;
                        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->config['deschalfImgPath']);
                        $isPainted = false;
                        $leftWidth = $this->config['pageWidth'];
                        $x = $this->config['descStartX'];
                        $y = $this->config['descStartY'];
                    }
                } // end while
            }

            // 新写一页
            imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
//            var_dump($this->totalPage);
            $time = date('h:i:s');
            echo "绘制描述第{$this->totalPage}页, {$time}\n";
            $GLOBALS['branchName'][$this->totalPage + 1] = $person['branchName'];
            $this->totalPage++;
            $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . '/' . $this->config['deschalfImgPath']);
            $isPainted = false;
            $leftWidth = $this->config['pageWidth'];
            $x = $this->config['descStartX'];
            $y = $this->config['descStartY'];
        }

        if ($isPainted) {
            imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH . "/output" . $this->totalPage . ".png");
            $time = date('h:i:s');
            echo "绘制描述第{$this->totalPage}页, {$time}\n";
            $GLOBALS['branchName'][$this->totalPage + 1] = $person['branchName'];
            $this->totalPage++;
        }
    }
}
