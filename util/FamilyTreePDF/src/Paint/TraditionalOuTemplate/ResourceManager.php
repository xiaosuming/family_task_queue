<?php
/**
 * 资源管理类
 */

namespace FamilyTreePDF\Paint\TraditionalOuTemplate;

class ResourceManager {
    public static $FONT = __DIR__.'/../../res/font/1.TTC';
    public static $RESOURCE_PATH = __DIR__.'/../../res/TraditionalOuTemplate';
    public static $HALFOUTPUT_PATH = __DIR__.'/../../output/halfoutput';
    public static $OUTPUT_PATH = __DIR__.'/../../output';
}
