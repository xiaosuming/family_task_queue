<?php

/**
 * 新定义的导出样式
 * @author yuanxin
 * @date 2021/1/7
 *
 *
 */

namespace FamilyTreePDF\Paint\NewPaint;
use FamilyTreePDF\Paint\FontManager;

class NewPaintByLevel
{
    private $allPersons;
    private $config;
    private $normalTextFont;
    private $familyName;
    private $generation;//一页展示的世代数量
    private $pages;
    private $black = 0x000000;
    private $white = 0xffffff;
    private $red = 0xFF0000;


    public function __construct()
    {
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    public function setGeneration($generation) {
        $this->generation = $generation;
    }

    public function setMottos($mottos) {
    }


    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function input($allPersons)
    {
        $this->allPersons = $allPersons;
    }

    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }



    public function paint(): int
    {
        $persons = $this->allPersons;
        $maxLevel = -99999;
        $minLevel = $persons[0]['level'];
        $page = 0;
        $personsPageMapperY = array();
        foreach($persons as $key => $person){
            if($person['level']>$maxLevel){
                $maxLevel = $person['level'];
            }
//            每一页横坐标
            $persons[$key]['pageX']= ($person['level']-$minLevel) % ($this->generation - 1);
//            第几大页
            $persons[$key]['page'] = floor(($person['level']-$minLevel)/($this->generation-1));
        }

        $totalLongPage  = ceil(($maxLevel  -  $minLevel)/($this->generation-1));
        $startLevel = 1;
        $pagePersonNum = ceil(((($this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight']) * 1.333) - ($this->config['marginTop'] + $this->config['marginBottom'])) / ($this->config['nodeHeight'] + $this->config['subtreeSeparation']));
        $lastLevels =  array();
        $lastPersonY =  array();
        $this->pages = array();

//
        for ($i = 0;$i<$totalLongPage;$i++){
            $lastLevels[$i] = $minLevel;
            $personsPageNum[$i]=array();
            $personsPageMapperY[$i] = array();
            for ($j = 0;$j<$this->generation;$j++){
                $personsPageMapperY[$i][$j] = 0;
            }
            $lastPersonY[$i] = $pagePersonNum;
            $this->pages[$i] = 0;
        }
        foreach ($persons  as $key => $person) {

            /******************************************************处理每页最后一世*************************************************************/
            if ($person['pageX'] == 0 && $person['level'] > $minLevel ) {

                $currentY =  $personsPageMapperY[$person['page']-1][$this->generation - 1];
                if ($currentY == 0){
                    $currentY = $lastPersonY[$person['page']-1];
                    $lastPersonY[$person['page']-1] = max($currentY,$lastPersonY[$person['page']-1]);
                    $personsPageMapperY[$person['page']-1][$this->generation - 1] = $lastPersonY[$person['page']-1];
                }elseif($currentY >= $pagePersonNum){
                    $lastPersonY[$person['page']-1] = 0;
                    $this->pages[$person['page']-1]++;
                    for ($j = 0;$j<$this->generation;$j++){
                        $personsPageMapperY[$person['page']-1][$j] = 0;
                    }
                }else{
                    $currentY = $personsPageMapperY[$person['page']-1][$this->generation - 1] ;
                    $lastPersonY[$person['page']-1] = max($currentY,$lastPersonY[$person['page']-1]);
                    $personsPageMapperY[$person['page']-1][$this->generation - 1] = $lastPersonY[$person['page']-1];
                }
                $personsPageMapperY[$person['page']-1][$this->generation - 1] += 1;
                $lastLevels[$person['page']-1] = $this->generation-1;
                //                  去除没有孩子的下一页的节点
                if(count($person['son'])==0 && count($person['daughter'])==0 ){
                    $lastLevels[$person['page']-1] = $this->generation-1;
                    continue;
                }
            }
            /******************************************************处理每页最后一世*************************************************************/

            /******************************************************添加线段*************************************************************/
            //    下一个添加的大于上一个辈分,向下添加
            if ($person['pageX'] > $lastLevels[$person['page']]) {
                $persons[$key]['pageY'] = $lastPersonY[$person['page']] ;
                $personsPageMapperY[$person['page']][$person['pageX']] = $lastPersonY[$person['page']];
            }else {
                /******************************************************折叠*************************************************************/
//                if (count($person['son'])>0 || count($person['daughter'])>0 ){
//                    $persons[$key]['pageY'] = $lastPersonY[$person['page']] + 1;
//                }else{
//                    $persons[$key]['pageY'] = $personsPageMapperY[$person['page']][$person['pageX']]  + 1 ;
//                }
//                $lastPersonY[$person['page']] = min($persons[$key]['pageY'],$lastPersonY[$person['page']]);
                /******************************************************折叠*************************************************************/
                /******************************************************不需要折叠*************************************************************/
                $persons[$key]['pageY'] = $lastPersonY[$person['page']] + 1;
                $lastPersonY[$person['page']] = $persons[$key]['pageY'];
                /******************************************************不需要折叠*************************************************************/
                /******************************************************翻页*************************************************************/
                if ($persons[$key]['pageY'] >= $pagePersonNum){
                    $this->pages[$person['page']]++;
                    $personsPageMapperY[$person['page']] = array();
                    for ($j = 0;$j<$this->generation;$j++){
                        $personsPageMapperY[$person['page']][$j] = 0;
                    }
                    $persons[$key]['pageY'] = 0;
                    $lastPersonY[$person['page']] = $persons[$key]['pageY'];
                }
                /******************************************************翻页*************************************************************/
                $personsPageMapperY[$person['page']][$person['pageX']] = $persons[$key]['pageY'];
            }

            $lastLevels[$person['page']] = $person['pageX'];
            $persons[$key]['pageNum'] = $this->pages[$person['page']];

            /******************************************************添加线段*************************************************************/
            /******************************************************写人名*************************************************************/
            /******************************************************写人名*************************************************************/
        }
        for($i = 0;$i<$totalLongPage;$i++){
            $paintPersons = array();
            foreach($persons  as $key  => $person) {
                if(($person['level'] - $minLevel) < $this->generation * ($i+1) - $i) {
                    array_push($paintPersons,$person);
                    if(($person['level'] - $minLevel) < ($this->generation * ($i+1) - $i - 1)) {
                        unset($persons[$key]);
//                  去除没有孩子的下一页的节点
                    }elseif(count($person['son'])==0 && count($person['daughter'])==0 ){
                        unset($persons[$key]);
                    }
                }
            }
            $page =  $this->newPaint($paintPersons,$page,$startLevel);
            $startLevel = $startLevel + $this->generation-1;
        }
        return $page;

        /************************************************************************************************* */
    }
    /**
     * 绘制世代的文字
     * 暂时能顶99世
     * @param $canvas
     * @param $startLevel
     * @param $level
     */
    private function paintLevel($canvas, $startLevel,$level)
    {

        $fontSize = $this->config['levelTextFontSize'];

        for($j = 0; $j < $level ;$j++){
            $text = "第" . ($startLevel+$j) . "世";
            $bbox = imageftbbox($this->config['levelTextFontSize'] ,0,$this->normalTextFont,$text);
            imagefilledrectangle(
                $canvas,
                $j * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['marginLeft'] - $this->config['levelPadding'],
                $this->config['titleHeight'] - $bbox[1]/2,
                $j * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['nodeWidth'] + $this->config['marginLeft'] + $this->config['levelPadding'],
                $this->config['marginTop'] + $bbox[3]/2,
                $this->black
            );

            $bbox2 = imageftbbox($this->config['levelTextFontSize'],0,$this->normalTextFont,$text);
            //              计算字体大小 修正
            if ($bbox[2] - $bbox[0] - $this->config['nodeWidth'] > 0){
                $fontSize = $this->config['nodeWidth'] * $this->config['fontSize'] /    ($bbox[2] - $bbox[0]) ;
                $bbox2 = imageftbbox($fontSize,0,$this->normalTextFont,$text);
            }
            imagettftext($canvas,
                $fontSize,
                0,
                $j * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['marginLeft'] + ($this->config['nodeWidth'] - $bbox2[2] + $bbox2[1])/2,
                $this->config['marginTop']  +  $bbox[7]/2,
                $this->white,
                $this->normalTextFont,
                $text
            );

        }
    }

    /**
     *  绘制族谱名字
     * @param $canvas
     *
     */
    private function paintFamilyName($canvas){
        $fontSize = $this->config['familyNameFontSize'];
        $bbox =  imageftbbox($this->config['familyNameFontSize'], 0, $this->normalTextFont,$this->familyName);
        if ($bbox[2] - $bbox[0] - imagesx($canvas) > 0){
            $fontSize = imagesx($canvas) * $this->config['familyNameFontSize'] / ($bbox[2] - $bbox[0]) ;
            $bbox = imageftbbox($fontSize,0,$this->normalTextFont,$this->familyName);
        }
        imagettftext($canvas,
            $fontSize,
            0,
            (imagesx($canvas) - $bbox[2] + $bbox[0])/2,
            ($this->config['titleHeight'] - $bbox[7] + $bbox[1])/2,
            0x000000,
            $this->normalTextFont,
            $this->familyName);
    }



    /**
     * 绘制一整页
     * @param $persons
     * @param $page
     * @param $startLevel
     * @return int
     */

    private function newPaint($persons,$page,$startLevel): int
    {
        if ($persons != null) {


            $rootLevel = $persons[0]['level'];
            $lastLevel = $rootLevel;
            $lastPersonY = $this->config['marginTop'];
            $mapperY = array();
            $mapperLineY = array();
            $mapperY[0] = 0;
            $mapperLineY[0] = 0;
            $personChild = array();
            $personChild[0] = 0;
            /******************************************************底部框架*************************************************************/
            $canvas = $this->newCanvas();
            $A4_HEIGHT = imagesy($canvas);
            /******************************************************底部框架*************************************************************/
            /******************************************************家族名*************************************************************/
            $this->paintFamilyName($canvas);
            /******************************************************家族名*************************************************************/
            /******************************************************世代*************************************************************/
            $this->paintLevel($canvas,$startLevel,$this->generation);
            /******************************************************世代*************************************************************/
            /******************************************************page*************************************************************/
            $this->paintFamilyPage($canvas,$page);
            /******************************************************page*************************************************************/
            foreach ($persons  as  $person) {
                if (isset($personChild[$person['level'] - $rootLevel + 1])){
                    $personChild[$person['level'] - $rootLevel + 1] += count($person['son'])+ count($person['daughter']);
                }else{
                    $personChild[$person['level'] - $rootLevel + 1] = count($person['son'])+ count($person['daughter']);
                }
                /******************************************************添加线段*************************************************************/
                $person['x'] = ($person['level'] - $rootLevel) * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['marginLeft'];
                //    下一个添加的大于上一个辈分,向下添加
                if ($person['level'] > $lastLevel) {
                    $person['y'] = $lastPersonY ;
                    imagefilledrectangle($canvas,
                        $person['x'] - $this->config['siblingSeparation'] + 2,
                        $person['y'] -  $this->config['nodeHeight']/2 - 1,
                        $person['x'] - 2,
                        $person['y'] -  $this->config['nodeHeight']/2 + 1,
                        $this->red);
                    $mapperY[$person['level'] - $rootLevel] = $lastPersonY;
                    $mapperLineY[$person['level'] - $rootLevel] = $lastPersonY;
                }else {
                    /******************************************************折叠*************************************************************/
//                    if (count($person['son'])>0 || count($person['daughter'])>0 ){
//                        $person['y'] = $lastPersonY + $this->config['nodeHeight'] + $this->config['subtreeSeparation'];
//                    }else{
//                        $person['y'] = $mapperY[$person['level'] - $rootLevel] + $this->config['nodeHeight'] + $this->config['subtreeSeparation'];
//                    }
//                    $lastPersonY = max($person['y'],$lastPersonY);
                    /******************************************************折叠*************************************************************/
                    /******************************************************不需要折叠*************************************************************/
                    $person['y'] = $lastPersonY + $this->config['nodeHeight'] + $this->config['subtreeSeparation'];
                    $lastPersonY = $person['y'];
                    /******************************************************不需要折叠*************************************************************/
                    /******************************************************翻页*************************************************************/
                    if ($person['y'] - $A4_HEIGHT > 0){
//                        补齐上一页的线
                        for ($i = $rootLevel ; $i <= $person['level'];$i++){
                            if ($personChild[$i - $rootLevel] > 0){
//                                直线
                                imagefilledrectangle($canvas,
                                    ($i - $rootLevel) * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['marginLeft'] - $this->config['siblingSeparation'] / 2 + 1,
                                    $mapperLineY[$i - $rootLevel] - $this->config['nodeHeight'] / 2 - 1,
                                    ($i - $rootLevel) * ($this->config['nodeWidth']+$this->config['siblingSeparation']) + $this->config['marginLeft'] - $this->config['siblingSeparation'] / 2 + 3,
                                    $person['y'] - $this->config['nodeHeight'] / 2 + 1,
                                    $this->red);
                            }
                        }

                        var_dump("paint page ".$page);
                        imagepng($canvas, ResourceManager::$OUTPUT_PATH.'/'.$page.'.png');
                        imagedestroy($canvas);
                        $page++;
                        foreach ($mapperY as $key => $item) {
                            $mapperY[$key] = $this->config['marginTop'] + $this->config['nodeHeight'] + $this->config['subtreeSeparation'];
                            $mapperLineY[$key] = $this->config['marginTop'] + $this->config['nodeHeight'];
                        }
                        $canvas = $this->newCanvas();
                        $this->paintLevel($canvas,$startLevel,$this->generation);
                        $this->paintFamilyName($canvas);
                        $this->paintFamilyPage($canvas,$page);
                        $person['y'] = $this->config['marginTop'] + $this->config['nodeHeight'] + $this->config['subtreeSeparation'];
                        $lastPersonY = $person['y'];
                    }
                    /******************************************************翻页*************************************************************/
                    if (($person['level'] - $rootLevel) > 0) {
                        imagefilledrectangle($canvas,
                            $person['x'] - $this->config['siblingSeparation'] / 2 + 2,
                            $person['y'] - $this->config['nodeHeight'] / 2 - 1,
                            $person['x'] - 2,
                            $person['y'] - $this->config['nodeHeight'] / 2 + 1,
                            $this->red);

                        imagefilledrectangle($canvas,
                            $person['x'] - $this->config['siblingSeparation'] / 2 + 1,
                            $mapperLineY[$person['level'] - $rootLevel] - $this->config['nodeHeight'] / 2 - 1,
                            $person['x'] - $this->config['siblingSeparation'] / 2 + 3,
                            $person['y'] - $this->config['nodeHeight'] /  + 2,
                            $this->red);
                    }
                    $mapperY[$person['level'] - $rootLevel] = $person['y'];
                    $mapperLineY[$person['level'] - $rootLevel] = $person['y'];
                }
                $lastLevel = $person['level'];
                /******************************************************添加线段*************************************************************/
                /******************************************************写人名*************************************************************/
                $fontSize = $this->config['fontSize'];
                $bbox = imageftbbox($fontSize,0,$this->normalTextFont,$person['name']);
                //              计算字体大小 修正
                if ($bbox[2] - $bbox[0] - $this->config['nodeWidth'] > 0){
                    $fontSize = $this->config['nodeWidth'] * $this->config['fontSize'] / ($bbox[2] - $bbox[0]) ;
                    $bbox = imageftbbox($fontSize,0,$this->normalTextFont,$person['name']);
                }
                imagettftext(
                    $canvas,
                    $fontSize,
                    0,
                    $person['x'] + ($this->config['nodeWidth'] - $bbox[2] + $bbox[0])/2,
                    $person['y'] - ($this->config['nodeHeight']   -  ($bbox[1] - $bbox[5]))/2,
                    $this->black,
                    $this->normalTextFont,
                    $person['name']
                );
                $x = $person['x'] + ($this->config['nodeWidth'] - $bbox[2] + $bbox[0])/2 + $bbox[2]-$bbox[0] + 10 ;
                if (($person['level'] - $rootLevel == $this->generation -1) && (count($person['son'])>0 || count($person['daughter'])>0 )) {
                    $personPage = $person['pageNum'];
                    for ($k = 0; $k < $person['page']; $k++) {
                        $personPage += $this->pages[$k];
                    }
                    $pageFontSize = $this->config['fontSize']/1.6;
                    imagettftext(
                        $canvas,
                        $pageFontSize,
                        0,
                        $x,
                        $person['y'] - ($this->config['nodeHeight'] - ($bbox[1] - $bbox[5])) / 2,
                        $this->red,
                        $this->normalTextFont,
                        $personPage
                    );
                }
                /******************************************************写人名*************************************************************/
                /******************************************************侧边栏*************************************************************/
                if ($person['sideText']){
                    $sideTextSize = $this->config['sideTextSize'];
                    $bbox = imageftbbox($sideTextSize,0,$this->normalTextFont,$person['sideText']);
                    //              计算字体大小 修正
                    if ($bbox[2] - $bbox[0] - $this->config['nodeWidth'] > 0){
                        $sideTextSize = $this->config['nodeWidth'] * $this->config['sideTextSize'] / ($bbox[2] - $bbox[0]) ;
                        $bbox = imageftbbox($sideTextSize,0,$this->normalTextFont,$person['sideText']);
                    }
                    imagettftext(
                        $canvas,
                        $sideTextSize,
                        0,
                        $person['x']  + ($this->config['nodeWidth'] - $bbox[2] + $bbox[0])/2 ,
                        $person['y'] + $this->config['sideTextSeparation'] + ($bbox[1] - $bbox[5])/2 ,
                        $this->red,
                        $this->normalTextFont,
                        $person['sideText']
                    );
                }
                /******************************************************侧边栏*************************************************************/
                $personChild[$person['level'] - $rootLevel] -= 1;
            }
            var_dump("paint page ".$page);
            imagepng($canvas, ResourceManager::$OUTPUT_PATH.'/'.$page.'.png');
            imagedestroy($canvas);
            $page++;
        }
        return $page;
    }

    /**
     *
     * 获取一块新的A4画布
     * @return resource
     */
    private function newCanvas()
    {
        $personNum = ceil(((($this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight']) * 1.333) - ($this->config['marginTop'] + $this->config['marginBottom'])) / ($this->config['nodeHeight'] + $this->config['subtreeSeparation']));
        $canvas = imagecreatetruecolor(
            $this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight'],
            $this->config['marginBottom'] + $this->config['marginTop'] + $this->config['nodeHeight'] * $personNum + $this->config['subtreeSeparation'] * ($personNum - 0.5)
        );
        imagefilledrectangle($canvas,
            0,
            0,
            $this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight'],
            $this->config['marginBottom'] + $this->config['marginTop'] + $this->config['nodeHeight'] * $personNum + $this->config['subtreeSeparation'] * ($personNum - 0.5),
            0xffffff);
        return $canvas;
    }


    /**
     *  绘制族谱page
     * @param $canvas
     * @param $page
     */
    private function paintFamilyPage($canvas,$page){
        $fontSize = $this->config['familyPageFontSize'];
        $bbox =  imageftbbox($this->config['familyPageFontSize'], 0, $this->normalTextFont,$page+1);
        if ($bbox[2] - $bbox[0] - imagesx($canvas) > 0){
            $fontSize = imagesx($canvas) * $this->config['familyPageFontSize'] / ($bbox[2] - $bbox[0]) ;
            $bbox = imageftbbox($fontSize,0,$this->normalTextFont,$page);
        }
        imagettftext($canvas,
            $fontSize,
            0,
            imagesx($canvas) - $bbox[2] - $bbox[0] - $this->config['marginRight'],
            ($this->config['titleHeight'] - $bbox[7] + $bbox[1]) / 2,
            0x000000,
            $this->normalTextFont,
            $page+1);
    }
}


