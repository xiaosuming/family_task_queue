<?php


namespace FamilyTreePDF\Paint\NewPaint;



use Dv\Dv;
use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\Level7PaintUtil;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Paint\TraditionalOuTemplate\ResourceManager;
use FamilyTreePDF\Util\SysConst;
use Mpdf\Tag\P;
use FamilyTreePDF\Paint\FontManager;

class A4Traditional7Level
{

    private $paintDataQueue; // 等待绘制的人物队列
    private $totalPage; // 总页数
    private $personMap; // id和人物对应



    private $rootPersonQueue; // 绘制吊线图的根人物队列
    private $allLevelList; // 所有人物的levelList
    private $minLevel; // 最低的level，level越小，辈分越高
    private $minLevelList;
    private $maxLevel; // 最高的level, level越大，辈分越小
    private $maxLevelList;
    private $direction; // 排版方向,
    private $lastPageCanvas; // 上一次页面画布
    private $lastOffsetX; // 上一次偏移量
    private $needPaintLevelFirst;
    private $levelPaintMap; // 用来记录哪些世代的level已经绘制了
    private $mottos; // 家训

    private $dvImageXMap; // 用来记录dv绘图在totalWidth中的X位置
    private $dvImageSplitLineMap; // 用来记录第几张dv图的x处有切割线

    private $dvPaintImageIndex; // 当前正在第几次使用dv绘图
    private $dvImageWidthMap; // 记录每次生成的Dv图片的宽度

    private $normalTextFont;    // 正文字体

    private $config;

    public function __construct()
    {
        $this->totalPage = 0;
        $this->paintDataQueue = array();
        $this->direction = SysConst::$LEFT_TO_RIGHT;
        $this->lastPageCanvas = null;
        $this->lastOffsetX = 0;
        $this->needPaintLevelFirst = true;
        $this->levelPaintMap = [];
        $this->dvImageXMap = [];
        $this->dvImageSplitLineMap = [];
        $this->personMap =  array();
        $this->allLevelList = array();


        $this->dvPaintImageIndex = 0;
        $this->mottos = [
            '   ',
        ];
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    /**
     * 设置排版方向，可用的值有LEFT_TO_RIGHT和RIGHT_TO_LEFT
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef(); // totalpage传的是引用
        $this->personMap = &$context->getPersonMapRef();

        $this->minLevel = &$context->getMinLevelRef();
        $this->maxLevel = &$context->getMaxLevelRef();

        $this->allLevelList = &$context->getAllLevelListRef();

        $this->minLevel = PHP_INT_MAX;
        $this->maxLevel = PHP_INT_MIN;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    /**
     * 数据适配器
     * @param 所有的人物数组
     */
    private function dataAdaptor($persons)
    {

        $allPersons = array();

        foreach ($persons as $person) {
            $personbj = array();
            $personbj['id'] = $person['id'];
            $personbj['name'] = $person['name'];
            $personbj['image'] = $person['photo'];
            $personbj['level'] = $person['level'];
            $personbj['gender'] = $person['gender'];
            $personbj['type'] = $person['type'];
            $personbj['ranking'] = $person['ranking'] ?? 0;

            // 这里有一个sidebarWidth，是为了保存侧边栏的宽度
            $personbj['sidebarWidth'] = $person['sidebarWidth'] ?? 0;

            if (count($person['father']) > 0) {
                $personbj['fathers'] = $person['father'];
            }

            if (count($person['mother']) > 0) {
                $personbj['mothers'] = $person['mother'];
            }

//            if (count($person['brother']) > 0) {
//                $personbj['brothers'] = $person['brother'];
//            }
//
//            if (count($person['sister']) > 0) {
//                $personbj['sisters'] = $person['sister'];
//            }

            if (count($person['son']) > 0) {
                $personbj['sons'] = $person['son'];
            }

            if (count($person['daughter']) > 0) {
                $personbj['daughters'] = $person['daughter'];
            }

            // 配偶
            if (count($person['spouse']) > 0) {
                $personbj['spouses'] = $person['spouse'];
            }

            $allPersons[] = $personbj;
        }

        return $allPersons;
    }

    /**
     * 向人物数组中添加亲属, (只添加子代)
     * @param $corePerson 核心人物
     * @param $persons    人物数组
     * @param $depth      当前的递归深度
     * @param $limit      递归深度的限制
     */
    private function addRelative($corePerson, &$persons, $depth, $limit)
    {
        if ($depth > $limit) {
            // 如果深度超过,则不再查找
            return;
        }

        $gridWidth = $this->config['gridWidth'];

        //echo "深度x".$depth.PHP_EOL;
        //echo "level".$corePerson['level'];

        if (!isset($persons[$corePerson['id']])) {

            // 这里修改成根据人物的侧边栏信息，进行绘制
            $width = 0;
            if ($corePerson['sideText'] == '') {
                $width = 0;
            } else {
                $width = Level7PaintUtil::getTextsWidth(
                    $corePerson['sideText'],
                    $this->config['sideTextSize'],
                    $this->config['sideTextVSize'],
                    $this->config['sideTextHSize'],
                    $this->config['sideHeight']
                );
            }

            // 这里要检查该人物是不是处于最右边（在绘制的吊线图中，如果不是最右边，则没有这个留白宽度）
            $spaceWidth = $this->config['sidebarSpace']; // 本来就有的留白宽度

            if ($width - $spaceWidth > 0) {
                $nums = floor(($width - $spaceWidth) / $gridWidth) + 1;
            } else {
                $nums = 0;
            }

            // echo "个数" . $nums . PHP_EOL;

            // 更新person的sidebarWidth
            $corePerson['sidebarWidth'] = $nums * $gridWidth;

            $persons[$corePerson['id']] = $corePerson;

            // 添加这个人的配偶
            foreach ($corePerson['spouse'] as $spouseId) {
                if (isset($this->personMap[$spouseId])) {
                    $spousePerson = $this->personMap[$spouseId];
                    // 设置侧边栏宽度
                    $width = 0;
                    if ($spousePerson['sideText'] == '') {
                        $width = 0;
                    } else {
                        $width = Level7PaintUtil::getTextsWidth(
                            $spousePerson['sideText'],
                            $this->config['sideTextSize'],
                            $this->config['sideTextVSize'],
                            $this->config['sideTextHSize'],
                            $this->config['sideHeight']
                        );
                    }

                    $spaceWidth = $this->config['sidebarSpace']; // 本来就有的留白宽度

                    $num = 0;
                    if ($width - $spaceWidth > 0) {
                        $nums = floor(($width - $spaceWidth) / $gridWidth) + 1;
                    } else {
                        $nums = 0;
                    }

                    // 更新person的sidebarWidth
                    $spousePerson['sidebarWidth'] = $num * $gridWidth;
                    $persons[$spouseId] = $spousePerson;
                }
            }

            // echo "儿子数量".count($corePerson['son']);
            // echo "女儿数量".count($corePerson['daughter']);
            if ($depth == $limit && (count($corePerson['son']) > 0 || count($corePerson['daughter']) > 0)) {
                // 最后一代,放到rootPerson队列中
                array_push($this->rootPersonQueue, $corePerson);
                //echo $corePerson['name']."\n";
            }
        }

        $son =array();
        // 添加儿子
        foreach ($corePerson['son'] as $id) {
            array_push($son,$this->personMap[$id]);

        }
        usort($son,function ($person1, $person2) {
            if ( $person1["ranking"]== $person2["ranking"]) return 0;
            return ( $person1["ranking"]< $person2["ranking"])?-1:1;
        });

        //print_r($son);

        foreach ($son as $id1){
            if (!isset($persons[$id1['id']])) {
                $this->addRelative($this->personMap[$id1['id']], $persons, $depth + 1, $limit);
            }
        }



        // 添加女儿
        foreach ($corePerson['daughter'] as $id) {
            if (!isset($persons[$id])) {
                $this->addRelative($this->personMap[$id], $persons, $depth + 1, $limit);
            }
        }
    }






    /**
     * A4体例顶部会绘制家训，家训是一个数组，会循环写在顶部
     * @param $mottos 家训
     */
    public function setMottos(array $mottos)
    {
        if (count($mottos) == 0) {
            $this->mottos = ['   '];
        } else {
            $this->mottos = $mottos;
        }

    }



    public function preInput($persons){

        $branchPersonMap=array();

        $minLevel = PHP_INT_MAX;
//        if (count($persons) == 0) {
//            return;
//        }


        $personMap[] = array();
//$rootPerson[] = array();
        foreach ($persons as $key => $person) {
            $branchName = $person['branchName'];
            //echo $branchName."\n";
            $personMap[$person['id']] = $person;
            $branchPersonMap[$branchName][$person['id']] = $person;

            $level = $person['level'];
            if(!isset( $minLevelList[$branchName])){
                $minLevelList[$branchName]=$minLevel;
            }

            if ($level < $minLevel) {
                if($level < $minLevelList[$branchName]){
                    $minLevelList[$branchName] = $level;
                    $rootPerson [$branchName]= $person;
                }
            }
        }


        foreach ( $rootPerson as $k1=>$v2) {
            $person1 = $v2;

            if(count($person1['father']) == 1){
                //echo $person1['father'][0]."\n";
                $father1= $personMap[$person1['father'][0]];
                $son1 =array("0"=>$person1['id']);

                $father1['son']=$son1;
                $fatherbn=$father1['branchName'];

                $son2 =array();
                $person1['son']=$son2;
                $branchPersonMap[$fatherbn][$v2['id']] = $person1;

                $father1['branchName']=$person1['branchName'];
                $father1['father']=array();

                $branchPersonMap[$k1][$father1['id']] = $father1;


            }
            if (count($person1['father']) == 0){
                //添加一个虚拟的父亲节点
                $father1= $personMap[$person1['id']];
                $son1 =array("0"=>$person1['id']);
                $father1['son']=$son1;
                $father1['father']=array();
                $father1['level']=$father1['level']-1;
                $father1['ranking']=-1;
                $father1['id']=$father1['id']-1;

                $branchPersonMap[$k1][$father1['id']] = $father1;

                $person= $personMap[$person1['id']];
                $father =array("0"=>$father1['id']);
                $person['father']=$father;

                $branchPersonMap[$k1][$person['id']] = $person;


            }
        }
        return $branchPersonMap;
    }


    public function input($allPersons, $startPage=0)
    {
        if (count($allPersons) == 0) {
            return;
        }
        $startPage -=1;
        $dvIndex = 0; // 第几次用dv画图
        $totalWidth = 0; // 所有的吊线图加一起的宽度，用来计算页数


        $this->personMap = array();

        $this->dvImageXMap =array();

        // root person,在族谱的绘制中,是作为一页的起点来做的
        $this->rootPersonQueue = array();
        $rootPerson;

        $tmpAllLevelList = [];

        // 构建personMap,id和person对应  $tmpAllLevelList每世的list
        foreach ($allPersons as $key => $person) {

            $this->personMap[$person['id']] = $person;


            $level = $person['level'];

            if ($level < $this->minLevel) {
                $this->minLevel = $level;
                $rootPerson = $person;
            }

            if ($level > $this->maxLevel) {
                $this->maxLevel = $level;
            }

            if (!isset($tmpAllLevelList[$level])) {
                $tmpAllLevelList[$level] = array();
            }

            $tmpAllLevelList[$level][] = $person;

        }


        $this->allLevelList[$this->minLevel] = $tmpAllLevelList[$this->minLevel];

//        print_r($tmpAllLevelList);
//        foreach($tmpAllLevelList as $k => $v){
//            echo $k."\n";
//            foreach ($v as $k2=>$v2){
//                echo $v2['name']."  ";
//            }
//            echo "\n";
//        }
        // 第一代人，默认只有一个起点以及其配偶
        usort($this->allLevelList[$this->minLevel], function($person1, $person2) {
            if ($person1['type'] == 1) {
                return true;
            } else if ($person2['type'] == 1) {
                return false;
            } else {
                // type都是2，根据id排序
                return intval($person1['id']) < intval($person2['id']);
            }
        });

        // 整理tmpAllLevelList，保证每一代人的排序都是正常的    rank从大到小
        for ($s = $this->minLevel+1; $s <= $this->maxLevel; $s++) {
            // 根据上一代的顺序确定这一代的顺序
            $this->allLevelList[$s] = Level7PaintUtil::sortPersons($this->allLevelList[$s-1], $tmpAllLevelList[$s]);
        }


//        foreach($this->allLevelList as $k => $v){
//            foreach ($v as $k2=>$v2){
//                echo $v2['name']."  ";
//            }
//            echo "\n";
//        }
//        echo $this->allLevelList[-1][0]['name']."\n";
//        echo $this->allLevelList[-1][1]['name']."\n";
//        echo $this->allLevelList[-1][2]['name']."\n";

        $needPersons = array();

        $this->addRelative($rootPerson, $needPersons, 1, 6);

        $needPersons = array_values($needPersons); //1-5世父子关系优先

//        foreach($needPersons as $k => $v){
//            echo $v['name']."\n";
//        }


        $needPaintPersons = $this->dataAdaptor($needPersons);

//        foreach($needPaintPersons as $k => $v){
//            echo $v['name']."  ";
//        }

        $dv = new Dv();
        $dv->setConfig($this->config);

        $dv->input($needPaintPersons);
        $dv->setDisplayStyle(2);
        $dv->setMoreSpace(false);
        $levelList = $dv->output();

        $rect = $dv->getRect();
        $paths = Level7PaintUtil::getPath($dv->getPath());
        //释放内存
        $dv->free();


//        foreach($levelList as $k1 => $v1){
//            foreach ($v1 as $kk => $vv) {
//                echo $vv['name'] . "\n";
////                print_r($vv);
//            }
//        }


        // 这里除了totalWidth,还要考虑世代图的宽度
        //$this->dvImageXMap[$dvIndex] = $this->config['gridWidth'];
        //$dvIndex++;

        $ImagePerson =array(1,$levelList[1][0]['id']) ;
        //print_r($ImagePerson);
        array_push( $this->dvImageXMap,$ImagePerson);



        // TODO: 更新总页数,这里的页数计算不正确
        $totalWidth += $rect['maxWidth'] + $this->config['extraSpace'];


        $startLevel = $rootPerson['level'] - $this->minLevel + 1;
        $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

        $this->paintDataQueue[] = $paintData;


        // 检查rootPersonQueue是否为空
        while (count($this->rootPersonQueue) > 0) {
            // echo count($this->rootPersonQueue)."\n";
            // print_r(array_slice($this->rootPersonQueue,0));

            $rootPerson = $this->rootPersonQueue[0];
            $rootPerson['father'] = array();
            $rootPerson['mother'] = array();

            $needPersons = array();

            $this->addRelative($rootPerson, $needPersons, 1, 6);

            $needPersons = array_values($needPersons);


//        foreach($needPersons as $k => $v){
//            echo $v['name'];
//        }
//        echo "\n";

            if (count($needPersons) > 0) {

                $needPaintPersons = $this->dataAdaptor($needPersons);
                $dv = new Dv();
                $dv->setConfig($this->config);

                $dv->input($needPaintPersons);
                $dv->setDisplayStyle(2);
                $dv->setMoreSpace(false);
                $levelList = $dv->output();

                $rect = $dv->getRect();
                $paths = PaintUtil::getPath($dv->getPath());
                //释放内存
                $dv->free();

                $startLevel = $rootPerson['level'] - $this->minLevel + 1;
                // 这里除了totalWidth,还要考虑世代图的宽度
                // echo "$startLevel 第二次totalWidth".$totalWidth.PHP_EOL;
                //$this->dvImageXMap[$dvIndex] = $totalWidth + ((($startLevel - 1) / 5) + 1) * $this->config['gridWidth'];
                $ImagePage = $totalWidth + ((($startLevel - 1) / 5) + 1) * $this->config['gridWidth']+$this->config['gridWidth'];

                //TODO:branch
                //  $ImagePage +=  $this->config['gridWidth'];

                if(($ImagePage % $this->config['pageWidth'])==0){
                    $ImagePage = ceil($ImagePage / $this->config['pageWidth'])+  $startPage +1;
                }else{
                    $ImagePage = ceil($ImagePage / $this->config['pageWidth'])+  $startPage ;
                }

                //$dvIndex++;
                $ImagePerson =array($ImagePage,$levelList[1][0]['id']) ;
                //print_r($ImagePerson);
                array_push( $this->dvImageXMap,$ImagePerson);



                // TODO: 这里的页数计算不正确
                $totalWidth += $rect['maxWidth'] + $this->config['extraSpace'];

                $paintData = new PaintData($levelList, $rect, $paths, $startLevel);

                $this->paintDataQueue[] = $paintData;
            }
            $this->rootPersonQueue = array_slice($this->rootPersonQueue, 1);
        }

        $paintLevel = ceil(($this->maxLevel - $this->minLevel) / 6);
        //TODO:branch个数

        $totalWidth += $paintLevel * $this->config['gridWidth'];
        //$totalWidth +=  $this->config['gridWidth'];

        $this->totalPage = ceil($totalWidth / $this->config['pageWidth']);

        // echo "总宽度 $totalWidth\n";
        // 找出每张图的切割线位置
        for ($lineIndex = 1; $lineIndex * $this->config['pageWidth'] < $totalWidth; $lineIndex++) {
            $totalX = $lineIndex * $this->config['pageWidth'];

            // echo "总偏移 $totalX\n";

            $tempX = 0;
            $dvImageIndex = 0;
            $loopToLast = false;
            foreach ($this->dvImageXMap as $dvImageIndex => $x) {
                // echo $dvImageIndex;
                if ($x < $totalX) {
                    $tempX = $totalX - $x;
                } else {
                    // $tempX是切割位置
                    $this->dvImageSplitLineMap[$dvImageIndex - 1][] = $tempX;
                    $loopToLast = false;
                    break;
                }
                $loopToLast = true;
            }

            if ($loopToLast) {
                $this->dvImageSplitLineMap[$dvImageIndex][] = $tempX;
            }
        }
        // var_dump($this->dvImageXMap);

        // var_dump($this->dvImageSplitLineMap);   // 切割线的映射图
    }

    /**
     * 绘制世代的文字
     * @param $canvas 画布
     * @param $startLevel 开始的辈分
     */
    private function paintLevel($canvas, $startLevel)
    {
        //echo $startLevel."\n";
        $x = $this->config['levelTextX'];
        $TextFontSize=$this->config['levelTextFontSize'];
        for ($j = 0; $j < 5; $j++){
            $level=$startLevel +$j;
            $text = "第" . PaintUtil::number2chinese($level) . "世";
            if( $level <= 10){
                $y = $this->config['levelTextY']+ $this->config['levelTextDistance'] *$j;
                $TextYDistance = $this->config['levelTextYDistance'];
            }else if($level % 10 == 0 || $level <= 20 ) {
                $y = $this->config['level2TextY']+ $this->config['levelTextDistance'] *$j;
                $TextYDistance = $this->config['level2TextYDistance'];
            }else {
                $y = $this->config['level3TextY']+ $this->config['levelTextDistance'] *$j;
                $TextYDistance = $this->config['level3TextYDistance'];
            }
            $len = mb_strlen($text);
            for ($i = 0; $i < $len; $i++) {
                $c = mb_substr($text, $i, 1);
                imagettftext($canvas, $TextFontSize, 0, $x, $y, 0xffffff, $this->normalTextFont, $c);
                imagettftext($canvas, $TextFontSize, 0, $x+1, $y, 0xffffff, $this->normalTextFont, $c);
                $y = $y + $TextYDistance  + $TextFontSize;
            }
        }

    }

    /**
     * 绘制世代图片, 世代图片是往上面加的
     */
    private function paintLevelImage($canvas, $x, $y, $startLevel)
    {
        $levelCanvas = \imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['levelImgPath']);
        $this->paintLevel($levelCanvas, $startLevel);

        // 将这个levelCanvas绘制到canvas上面
        \imagecopy($canvas, $levelCanvas, $x, $y, 0, 0, $this->config['levelImageWidth'], $this->config['levelImageHeight']);
    }

    /**
     * 绘制世代图片, 世代图片是往上面加的
     */
    private function paintBranchName($canvas,$x, $text)
    {
        //$levelCanvas = \imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['levelImgPath']);
        $len = mb_strlen($text);
        $x +=$this->config['branchFontXOffset'];
        $y=$this->config['branchFontYOffset'];
        for ($i =0 ; $i<$len; $i++){
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $this->config['branchFontSize'], 0, $x, $y, 0x000000, $this->normalTextFont, $c);
            imagettftext($canvas, $this->config['branchFontSize'], 0, $x+1, $y, 0x000000, $this->normalTextFont, $c);
            $y = $y + $this->config['branchFontSize'] + $this->config['branchTextYDistance'];

        }
        // 将这个levelCanvas绘制到canvas上面
    }


    /**
     * 在页首绘制格言
     * @param $canvas 画布
     * @param $text   文字
     */
    private function paintMotto($canvas, $text)
    {
        // 计算文字的总宽度
        $textNum = mb_strlen($text);
        $usedWidth = $textNum * $this->config['mottoFontSize'] + ($textNum - 1) * 4;

        $xOffset = ($this->config['pageWidth'] - $usedWidth) / 2;
        $yOffset = ($this->config['mottoHeight'] - $this->config['mottoFontSize']) / 2;

        imagettftext($canvas, $this->config['mottoFontSize'], 0, $xOffset, $yOffset, 0x000000, $this->normalTextFont, $text);
    }

    /**
     * 绘制侧边栏的文字
     */
    public function paintSidebar($canvas, $x, $y, $text ,$limit=false)
    {
        $fontSize = $this->config['sideTextSize'];
        $distanceY = $this->config['sideTextVSize'];
        $distanceX = $this->config['sideTextHSize'];
        $sideHeight = $this->config['sideHeight'];
        if($limit){
            $text= mb_substr($text, 0, 5);
        }

        $color = 0x000000;

        $initX = $x;
        $initY = $y;

        $len = mb_strlen($text);

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);

            imagettftext(
                $canvas,
                $fontSize,
                0,
                $x,
                $y,
                $color,
                $this->normalTextFont,
                $c
            );

            $y = $y + $distanceY + $fontSize;

            if ($y + $fontSize > $initY + $sideHeight) {
                // 换行
                $y = $initY;

                if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                    $x = $x + $fontSize + $distanceX;
                } else {
                    $x = $x - $fontSize - $distanceX;
                }

                // 注意，这里有的世代没有切割线，所以需要判断$this->dvImageSplitLineMap[$this->dvPaintImageIndex]是否存在
                if (isset($this->dvImageSplitLineMap[$this->dvPaintImageIndex])) {
                    foreach ($this->dvImageSplitLineMap[$this->dvPaintImageIndex] as $lineX) {

                        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                            // 从左往右
                            if ($x < $lineX && $x + $fontSize > $lineX) {
                                // 字在线上
                                $x = $lineX + 2;
                            } else {
                                break;
                            }
                        } else {
                            // 将切割线做镜像颠倒
                            $lineX = $this->dvImageWidthMap[$this->dvPaintImageIndex] - $lineX;

                            // 从右往左
                            // 这里的10是因为从右向左有bug加上的
                            if ($x < $lineX && $x + $fontSize + $this->config['sidebarAdjust'] > $lineX) {
                                $x = $lineX - $fontSize - $this->config['sidebarAdjust'];
                            } else {
                                break;
                            }

                        }
                    }
                }
            }
        }
    }

    /**
     * 在一张画布上绘制所有的人物
     * 这里有一个lastPageCanvas，表示上一页还没有用完，在当前页继续用
     * @param $allPersons 所有的人物集合
     * @param $page       页数
     */
    private function paintPage($paintData, &$page ,$branchName ,$time)
    {
        $innerPage = 0;
        //$innerPage = &$page;
        $xoffset = $this->config['xoffset'];
        $yoffset = $this->config['yoffset'];
        $pageWidth = $this->config['pageWidth'];
        $pageHeight = $this->config['pageHeight'];
        $pageCenterWidth = $this->config['pageCenterWidth'];
        $fontSize = $this->config['fontSize'];
        $fontYOffset = $this->config['fontYOffset'];
        $copyXOffset = $this->config['copyXOffset'];
        $copyYOffset = $this->config['copyYOffset'];
        $lineWidth = $this->config['lineWidth'];

        $rect = $paintData->rect;
        $levelList = $paintData->levelList;
        $paths = $paintData->path;
        $startLevel = $paintData->startLevel;

        // $maxWidth是吊线图的宽度
        $maxWidth = $rect['maxWidth'] + $this->config['extraSpace'];

        // 记录当前图片的宽度
        $this->dvImageWidthMap[$this->dvPaintImageIndex] = $maxWidth;

        if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
            // 从右往左,将所有坐标做镜像颠倒
            foreach ($levelList as &$level) {
                foreach ($level as &$item) {
                    //print_r($item);
                    $item['x'] = $maxWidth - $item['x'];
                }
            }

            foreach ($paths as $key => $path) {
                $paths[$key]['ex'] = $maxWidth - $path['ex'];
                $paths[$key]['sx'] = $maxWidth - $path['sx'];
            }
        }

        $canvas = imagecreatetruecolor($maxWidth, $rect['maxHeight']);

        //imagefilledrectangle($canvas, 0, 0, $maxWidth, $rect['maxHeight'], 0x00ff00);
        imagefilledrectangle($canvas, 0, 0, $maxWidth, $rect['maxHeight'], 0xffffff);

        // 绘制姓名栏
        foreach ($levelList as $k => &$level) {
            if($k != 1){
                foreach ($level as &$item) {
                    if ($item['name'] != 'virtual') {
                        $x = $item['x'];
                        $y = $item['y'] + $this->config["treeYOffset"];

                        if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                            $x = $x - $this->config['fontSize'] - $this->config['fontSize'] / 2;
                        }
                        Level7PaintUtil::paintText($canvas, $x, $y + $this->config['nameTextYOffset'], $item['name'], $this->config, $this->normalTextFont);

                        //sidebar
                        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                            $sidebarInitX = $x + $this->config['sidebarXOffset'] + $this->config['fontXOffset'] + $this->config['xoffset'];
                        } else {
                            $sidebarInitX = $x + $this->config['sidebarXOffset'] + $this->config['fontXOffset'] + $this->config['xoffset'];
                        }
                        $sidebarInitY = $y + $this->config['sidebarYOffset'] + $this->config['fontYOffset'] - $this->config['yoffset'];
                        $sideText = $this->personMap[$item['id']]['sideText'];

                        $this->paintSidebar(
                            $canvas,
                            $sidebarInitX,
                            $sidebarInitY,
                            $sideText
                        );

                        //originName
                        //TODO:左边侧边栏修改字段名
                        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                            $sidebarRInitX = $x + $this->config['sidebarRXOffset'] + $this->config['fontXOffset'] + $this->config['xoffset'];
                        } else {
                            $sidebarRInitX = $x + $this->config['sidebarRXOffset'] + $this->config['fontXOffset'] + $this->config['xoffset'];
                        }
                        $originName = $this->personMap[$item['id']]['originName'];
                        $this->paintSidebar(
                            $canvas,
                            $sidebarRInitX,
                            $sidebarInitY,
                            $originName,
                            true
                        );


                        //第一行打印 最上面的之子栏
                        if ($k == 2) {
                            $father = $this->personMap[$item['id']]['father'];
                            if (isset($father[0])) {
                                $fatherName = $this->personMap[$father[0]]['name'];
                                $fatherflag = $this->personMap[$father[0]]['ranking'];


                                $fatherRank = $this->personMap[$item['id']]['ranking'];
                                $fatherGender = $this->personMap[$item['id']]['gender'];
                                $fatherSon = $this->personMap[$father[0]]['son'];
                                //$fatherDau = $this->personMap[$item['id']]['daughter'];
                                if (sizeof($fatherSon) > 1) {
                                    $rank = Level7PaintUtil::convertRanking($fatherRank, $fatherGender, 0);
                                    Level7PaintUtil::paintFatherText($canvas, $x + 23, $y + $this->config['fatherNameTextYOffset'], $fatherName, $this->config, $this->normalTextFont);
                                    Level7PaintUtil::paintFatherText($canvas, $x - 8, $y + $this->config['fatherNameTextYOffset'], $rank, $this->config, $this->normalTextFont);
                                }
                                if(sizeof($fatherSon) == 1  && $time != 0) {
                                    $rank = '之子';
                                    Level7PaintUtil::paintFatherText($canvas, $x + 23, $y + $this->config['fatherNameTextYOffset'], $fatherName, $this->config, $this->normalTextFont);
                                    Level7PaintUtil::paintFatherText($canvas, $x - 8, $y + $this->config['fatherNameTextYOffset'], $rank, $this->config, $this->normalTextFont);
                                }

                                if (sizeof($fatherSon) == 1 && $time== 0 && $fatherflag != -1){
                                    $rank = Level7PaintUtil::convertRanking($fatherRank, $fatherGender, 0);
                                    Level7PaintUtil::paintFatherText($canvas, $x + 23, $y + $this->config['fatherNameTextYOffset'], $fatherName, $this->config, $this->normalTextFont);
                                    Level7PaintUtil::paintFatherText($canvas, $x - 8, $y + $this->config['fatherNameTextYOffset'], $rank, $this->config, $this->normalTextFont);
                                }

                                if (sizeof($fatherSon) == 1 && $time== 0 && $fatherflag == -1){
                                    $title=$this->personMap[$item['id']]['sideText'];
                                    $title= mb_substr($title, 0, 5);
                                    $title1= mb_substr($title, 0, 2);
                                    $title2= mb_substr($title, 2, 5);
                                    Level7PaintUtil::paintFatherText($canvas, $x + 23, $y + $this->config['fatherNameTextYOffset'], $title1, $this->config, $this->normalTextFont);
                                    Level7PaintUtil::paintFatherText($canvas, $x - 8, $y + $this->config['fatherNameTextYOffset'], $title2, $this->config, $this->normalTextFont);

                                }

                            }
                        }


                        if($k == 6) { //打印最后一栏 页面标签
                            $dvflage = 0;
                            $dvpage = 0;
                            foreach ($this->dvImageXMap as $k1 => $v) {
                                if ($item['id'] == $v[1]) {
                                    $dvflage = 1;
                                    $dvpage = Level7PaintUtil::pageToChinese($v[0]);
                                    $dvpage = $dvpage . "页";
                                }
                            }
                            if ($dvflage == 1) {
                                Level7PaintUtil::paintPageText($canvas, $x, $y + $this->config['nameTextYOffset'] + 160, $dvpage, $this->config, $this->normalTextFont);
                                //y -shang   x - zuo
                            }
                        }
                    }
                }
            }
        }

        // 绘制路径
        foreach ($paths as $path) {
            if($path['sy'] <=600){
                Level7PaintUtil::imagelineWithWidth(
                    $canvas,
                    $path['sx'] + $this->config['xoffset'],
                    100,
                    $path['sx'] + $this->config['xoffset'],
                    145,
                    $lineWidth
                );
                Level7PaintUtil::imageCricleWithWidth(
                    $canvas,
                    $path['sx'] + $this->config['xoffset']+ floor( $this->config['lineWidth']/2),
                    150,
                    10,
                    2
                );//画圆圈 y -shang
            }else if($path['sy'] == $path['ey']){
                Level7PaintUtil::imagelineWithWidth(
                    $canvas,
                    $path['sx'] + $this->config['xoffset'],
                    $path['sy'] - $this->config['yoffset'] + $this->config["treeYOffset"]+$this->config["lineYOffset"],
                    $path['ex'] + $this->config['xoffset'],
                    $path['ey'] - $this->config['yoffset'] + $this->config["treeYOffset"]+$this->config["lineYOffset"],
                    $lineWidth
                );
                Level7PaintUtil::imageCricleWithWidth(
                    $canvas,
                    $path['ex']+ $this->config['xoffset']+ floor( $this->config['lineWidth']/2),
                    $path['ey'] -360,
                    10,
                    2
                );//画圆圈 y -shang
                //imagearc($canvas, $path['ex']+ $this->config['xoffset'] , $path['ey'] -360, 10, 10, 0, 360, 0x000000);//画圆圈 y -shang
            }else{
                Level7PaintUtil::imagelineWithWidth(
                    $canvas,
                    $path['sx'] + $this->config['xoffset'],
                    $path['sy'] - $this->config['yoffset'] + $this->config["treeYOffset"],
                    $path['ex'] + $this->config['xoffset'],
                    $path['ey'] - $this->config['yoffset'] + $this->config["treeYOffset"]+$this->config["lineYOffset"],
                    $lineWidth
                );
                Level7PaintUtil::imageCricleWithWidth(
                    $canvas,
                    $path['ex']+ $this->config['xoffset']+ floor( $this->config['lineWidth']/2),
                    $path['ey'] -360,
                    10,
                    2
                );//画圆圈 y -shang
                //imagearc($canvas, $path['ex']+ $this->config['xoffset'] , $path['ey'] -360, 10, 10, 0, 360, 0x000000);//画圆圈 y -shang
            }
            //echo $path['sy']."\n";

        }

        // imagepng($canvas, $this->dvPaintImageIndex.'test.png');

        // 这里开始分页, $leftWidth代表的是5代的吊线图的剩余未绘制宽度
        $leftWidth = $maxWidth;
        $leftHeight = $rect['maxHeight'];

        // $copyWidth 代表要粘贴的吊线图的宽度
        $copyWidth = 0;

        //
        $isCopyAllImageWidth = false;   // 这个值代表是否是粘贴了所有的吊线图宽度, 用来处理5代的吊线图宽度比页面小的情况

        while ($leftWidth > 0) {

            if ($this->lastOffsetX == 0) {
                $copyWidth = $pageWidth < $leftWidth ? $pageWidth : $leftWidth;
            } else {
                $copyWidth = $pageWidth - $this->lastOffsetX < $leftWidth ? $pageWidth - $this->lastOffsetX : $leftWidth;
            }

            $isCopyAllImageWidth = $copyWidth != $pageWidth;

            $copyHeight = $pageHeight < $leftHeight ? $pageHeight : $leftHeight;

            $levelOffsetX = 0; // 记录因为画世代造成的偏移

            if ($this->lastPageCanvas == null) {
                $dstCanvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['totalblankImgPath']);
                // 需要在一页的开头绘制世代
                if ($this->needPaintLevelFirst && !isset($this->levelPaintMap[$startLevel])) {

                    $levelImageXOffset = 0;
                    if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                        $levelImageXOffset = $copyXOffset;
                    } else {
                        $levelImageXOffset = $pageWidth - $copyXOffset - $this->config['gridWidth'];
                    }
                    //echo $startLevel."\n";
                    if( $startLevel == 1){
                        //TODO:branchname;
                        $this->paintBranchName($dstCanvas, $levelImageXOffset ,$branchName);
                    }
                    $this->paintLevelImage($dstCanvas, $levelImageXOffset-72, $copyYOffset - $this->config['levelImageYOffset'], $startLevel);
                    $this->needPaintLevelFirst = false;
                    $levelOffsetX = $this->config['levelImageWidth']*2;
                    $this->levelPaintMap[$startLevel] = true;

                    // 因为在一页开头画了level图，所以copyWidth要缩减
                    // 这里有一个注意点，如果copyWidth取得是吊线图的总宽度，那么这里应该取copyWidth和pageWidth-$this->config['gridWidth]中的较小值，来代表即将粘贴的吊线图的总宽度

                    if (!$isCopyAllImageWidth) {
                        $copyWidth = $copyWidth - $this->config['gridWidth']*2;
                    }
                }

                // 在顶部绘制格言
                // $mottoLen = count($this->mottos);
                //$mottoIndex = $page % $mottoLen;
                //$this->paintMotto($dstCanvas, $this->mottos[$mottoIndex]);

            } else {
                $dstCanvas = $this->lastPageCanvas;
                //上一页遗留的canvas清空
                $this->lastPageCanvas = null;
            }

            $imageCopyXOffset = 0;
            if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                // $imageCopyXOffset 用来记录当前吊线图应该粘贴的位置
                $imageCopyXOffset = $copyXOffset + $levelOffsetX + $this->lastOffsetX;
            } else {
                $imageCopyXOffset = $pageWidth - $copyWidth - ($copyXOffset + $levelOffsetX + $this->lastOffsetX);

            }

            // srcCopyX计算从源图中截取的位置
            $srcCopyX = $maxWidth - $leftWidth;

            if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
                // 裁剪的位置从右向左
                $srcCopyX = $maxWidth - $srcCopyX - $copyWidth;
            }

            // 将绘制好的吊线图粘贴到族谱上
            imagecopy($dstCanvas, $canvas, $imageCopyXOffset, $copyYOffset, $srcCopyX, 0, $copyWidth, $copyHeight);

            $leftWidth = $leftWidth - $copyWidth;

            if ($leftWidth == 0) {
                // 5世的最后一页
                $this->lastOffsetX = $copyXOffset + $this->lastOffsetX + $copyWidth + $levelOffsetX;

                if ($this->lastOffsetX < $pageWidth) {
                    // 画完5世后的偏移量，还剩下一些空间, 并且下一次绘制世代数会增长
                    if ($paintData->nextLevel > $startLevel && !isset($this->levelPaintMap[$startLevel + 5]) && $startLevel + 5 < ($this->maxLevel - $this->minLevel + 1)) {
                        // 检查下一个5世有没有画, 并且下一个5世是存在的
                        $levelImageXOffset = 0;
                        if ($this->direction == SysConst::$LEFT_TO_RIGHT) {
                            $levelImageXOffset = $this->lastOffsetX;
                        } else {
                            $levelImageXOffset = $pageWidth - $copyXOffset - $this->lastOffsetX - $this->config['gridWidth'];
                        }
                        $this->paintLevelImage($dstCanvas, $levelImageXOffset, $copyYOffset - $this->config['levelImageYOffset'], $startLevel + 5);
                        $this->lastOffsetX += $this->config['levelImageWidth']; // level图的宽度加上
                        $this->levelPaintMap[$startLevel + 5] = true;
                    }

                    if ($this->lastOffsetX < $pageWidth) {
                        $this->lastPageCanvas = $dstCanvas;
                    } else {
                        // 一页已经用完，换页了
                        // TODO: 这里的代码和下面的代码有重复
                        imagefilledrectangle($dstCanvas, 0, 100, 1080, 102, 0x000000);
                        imagepng($dstCanvas, ResourceManager::$HALFOUTPUT_PATH . "/output{$page}.png");
                        if (SysConst::$DEBUG) {
                            $time = date('h:i:s');
                            echo "1绘制吊线图第{$page}页, {$time}" . PHP_EOL;
                        }
                        // 释放内存
                        imagedestroy($dstCanvas);
                        $page++;
                        $innerPage++;
                        // 如果偏移量是一页的宽度，那么就是正常的模式
                        $this->lastPageCanvas = null;
                        $this->lastOffsetX = 0;
                    }
                } else {
                    $this->needPaintLevelFirst = true; // 标记需要在一页的开头画世代图
                    imagefilledrectangle($dstCanvas, 0, 100, 1080, 102, 0x000000);
                    imagepng($dstCanvas, ResourceManager::$HALFOUTPUT_PATH . "/output{$page}.png");
                    if (SysConst::$DEBUG) {
                        $time = date('h:i:s');
                        echo "2绘制吊线图第{$page}页, {$time}" . PHP_EOL;
                    }
                    // 释放内存
                    imagedestroy($dstCanvas);
                    $page++;
                    $innerPage++;
                    // 如果偏移量是一页的宽度，那么就是正常的模式
                    $this->lastPageCanvas = null;
                    $this->lastOffsetX = 0;
                }
            } else {
                $this->lastOffsetX = 0; // 置为0
                imagefilledrectangle($dstCanvas, 0, 100, 1080, 102, 0x000000);
                imagepng($dstCanvas, ResourceManager::$HALFOUTPUT_PATH . "/output{$page}.png");
                if (SysConst::$DEBUG) {
                    $time = date('h:i:s');
                    echo "3绘制吊线图第{$page}页, {$time}" . PHP_EOL;
                }
                // 释放内存
                imagedestroy($dstCanvas);
                $page++;
                $innerPage++;
            }
        }

        imagedestroy($canvas);
    }

    public function paint($setLevel = 1 , $page=0 , $branchName)
    {
        //$page = 0;
        $treePage = [];
        //print_r($this->paintDataQueue[1]);
//        foreach ($this->paintDataQueue as $index => $data) {
//            //print_r($this->paintDataQueue[1]);
//            $da = $data->startLevel;
//            echo $da ."startLevel\n";
//           // echo $data['nextLevel']."nextLevel\n";
//        }

//print_r($this->dvImageXMap);
        $time=0;
        foreach ($this->paintDataQueue as $index => $data) {
            $data->startLevel = $data->startLevel + $setLevel -1;

            if ($index + 1 < count($this->paintDataQueue)) {
                $nextLevel = $this->paintDataQueue[$index+1]->startLevel + $setLevel - 1;

                $data->nextLevel = $nextLevel;
            }
            //echo $data->startLevel ."startLevel\n";
            //echo $data->nextLevel ."nextLevel\n";


            $GLOBALS['treePage'][] = $page;
            $treePage[] = $page;

            $this->paintPage($data, $page ,$branchName,$time);
            $time++;
            $this->dvPaintImageIndex++;
        }

        if ($this->lastPageCanvas != null) {
            // 检查是否还有lastPageCanvas
            imagefilledrectangle($this->lastPageCanvas, 0, 100, 1080, 102, 0x000000);
            imagepng($this->lastPageCanvas, ResourceManager::$HALFOUTPUT_PATH . "/output{$page}.png");
            if (SysConst::$DEBUG) {
                $time = date('h:i:s');
                echo "4绘制吊线图第{$page}页, {$time}" . PHP_EOL;
            }
            // 释放内存
            imagedestroy($this->lastPageCanvas);
            $page++;
        }
//        return $treePage;
        echo $this->totalPage."\n";
        return $this->totalPage;
    }
}
