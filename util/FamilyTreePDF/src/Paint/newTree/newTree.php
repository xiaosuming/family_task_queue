<?php


namespace FamilyTreePDF\Paint\newTree;


use FamilyTreePDF\Paint\FontManager;

class newTree
{
    private $allPersons;
    private $config;
    private $normalTextFont;
    private $familyName;
    private $generation;//一页展示的世代数量

    public function __construct()
    {
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    public function setGeneration($generation) {
        $this->generation = $generation;
    }

    public function setMottos($mottos) {
    }


    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function input($allPersons)
    {
        $this->allPersons = $allPersons;
    }

    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }

    public function paint(){
        $persons = $this->allPersons;
        $maxLevel = -99999;
        $minLevel = $persons[0]['level'];
        $page = 0;
        $personsPageMapperY = array();
        foreach($persons as $key => $person){
            if($person['level']>$maxLevel){
                $maxLevel = $person['level'];
            }
//            每一页横坐标
            $persons[$key]['pageX']= ($person['level']-$minLevel) % ($this->generation - 1);
//            第几大页
            $persons[$key]['page'] = floor(($person['level']-$minLevel)/($this->generation-1));
        }

        $totalLongPage  = ceil(($maxLevel  -  $minLevel)/($this->generation-1));
        $startLevel = 1;
        $pagePersonNum = ceil(((($this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight']) * 1.333) - ($this->config['marginTop'] + $this->config['marginBottom'])) / ($this->config['nodeHeight'] + $this->config['subtreeSeparation']));
        $lastLevels =  array();
        $lastPersonY =  array();
        $this->pages = array();
    }

    /**
     *
     * 获取一块新的A4画布
     * @return resource
     */
    private function newCanvas()
    {
        $personNum = ceil(((($this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight']) * 1.333) - ($this->config['marginTop'] + $this->config['marginBottom'])) / ($this->config['nodeHeight'] + $this->config['subtreeSeparation']));
        $canvas = imagecreatetruecolor(
            $this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight'],
            $this->config['marginBottom'] + $this->config['marginTop'] + $this->config['nodeHeight'] * $personNum + $this->config['subtreeSeparation'] * ($personNum - 0.5)
        );
        imagefilledrectangle($canvas,
            0,
            0,
            $this->generation * ($this->config['nodeWidth'] + $this->config['siblingSeparation']) + $this->config['marginLeft'] + $this->config['marginRight'],
            $this->config['marginBottom'] + $this->config['marginTop'] + $this->config['nodeHeight'] * $personNum + $this->config['subtreeSeparation'] * ($personNum - 0.5),
            0xffffff);
        return $canvas;
    }
}