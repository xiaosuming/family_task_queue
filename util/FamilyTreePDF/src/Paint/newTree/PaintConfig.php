<?php
/**
 *   10*30的族谱导出
 * 
 */
namespace FamilyTreePDF\Paint\NewPaint;

class PaintConfig
{

    /**
     * 获取普通清晰度的从左往右的配置文件
     */
    public static function getL2RConfig(): array
    {
        return [
            "nodeWidth" => 180,              // 人名处的宽度 2字
            "nodeHeight" => 60,             // 人名处的高度
            "letterSpace" => 60,            //人名处的字间距
            "fontSize" => 50,            // 人名字体大小
            "siblingSeparation" => 60,      // 隔代之间的间隔
            "subtreeSeparation" => 60,      // 兄弟之间的间隔
            "sideTextSeparation" => 25,      // 侧边栏距离底部的距离
            "sideTextSize" => 30,      // 侧边栏字体大小
            "familyNameFontSize" => 105, // 标题的字体大小
            "familyPageFontSize" => 90,// 页码字体大小
            "marginLeft" => 105, // marginLeft
            "marginRight" => 105 - 75, // marginRight - 隔代之间的间隔

            "titleHeight" => 400, //家族名高度
            "generationHeight" => 100, //辈份高度

            "marginTop" => 500, // 家族名高度 + 辈份高度
            "marginBottom" => 90, // marginBottom
            "paddingLeft" => 90, // paddingLeft
            "paddingRight" => 90, // paddingRight
            "paddingTop" => 90, // paddingTop
            "paddingBottom" => 90, // paddingBottom

            "levelTextX" => 0,             // 第几世文字的x位置
            "levelTextY" => 100,              // 第几世文字的y位置
            "levelWidth" => 110,             // 第几世文字背景宽度
            "levelHeight" => 300,              // 第几世文字背景高度
            "levelTextDistance" => 420,       // 第几世文字的距离  = 人高度 加上 世代间隔
            "levelTextFontSize" => 50,         //辈分的字体大小
            "levelNodeWidth" => 200,         // 第几世文字背景宽度
            "levelPadding" => 20,        // 世代之间的fix


            "pageBarWidth" => 150, //页码板块宽度


        ];
    }

    /**
     * 获取普通清晰度的从右往左的配置文件
     */
    public static function getR2LConfig(): array
    {
        return [
            "nodeWidth" => 180,              // 人名处的宽度 2字
            "nodeHeight" => 60,             // 人名处的高度
            "letterSpace" => 60,            //人名处的字间距
            "fontSize" => 50,            // 人名字体大小
            "siblingSeparation" => 60,      // 隔代之间的间隔
            "subtreeSeparation" => 60,      // 兄弟之间的间隔
            "sideTextSeparation" => 25,      // 侧边栏距离底部的距离
            "sideTextSize" => 30,      // 侧边栏字体大小
            "familyNameFontSize" => 105, // 标题的字体大小
            "familyPageFontSize" => 90,// 页码字体大小

            "marginLeft" => 105, // marginLeft
            "marginRight" => 105 - 75, // marginRight - 隔代之间的间隔

            "titleHeight" => 400, //家族名高度
            "generationHeight" => 100, //辈份高度

            "marginTop" => 500, // 家族名高度 + 辈份高度
            "marginBottom" => 90, // marginBottom
            "paddingLeft" => 90, // paddingLeft
            "paddingRight" => 90, // paddingRight
            "paddingTop" => 90, // paddingTop
            "paddingBottom" => 90, // paddingBottom

            "levelTextX" => 0,             // 第几世文字的x位置
            "levelTextY" => 100,              // 第几世文字的y位置
            "levelWidth" => 110,             // 第几世文字背景宽度
            "levelHeight" => 300,              // 第几世文字背景高度
            "levelTextDistance" => 420,       // 第几世文字的距离  = 人高度 加上 世代间隔
            "levelTextFontSize" => 50,         //辈分的字体大小
            "levelNodeWidth" => 200,         // 第几世文字背景宽度
            "levelPadding" => 20,        // 世代之间的fix

            "pageBarWidth" => 150, //页码板块宽度

        ];
    }

    /**
     * 获取高清的从左往右的配置文件
     */
    public static function getHDL2RConfig(): array
    {
        return [
            "nodeWidth" => 180,              // 人名处的宽度 2字
            "nodeHeight" => 60,             // 人名处的高度
            "letterSpace" => 60,            //人名处的字间距
            "fontSize" => 50,            // 人名字体大小
            "siblingSeparation" => 60,      // 隔代之间的间隔
            "subtreeSeparation" => 60,      // 兄弟之间的间隔
            "sideTextSeparation" => 25,      // 侧边栏距离底部的距离
            "sideTextSize" => 30,      // 侧边栏字体大小
            "familyNameFontSize" => 105, // 标题的字体大小
            "familyPageFontSize" => 70,// 页码字体大小

            "marginLeft" => 105, // marginLeft
            "marginRight" => 105 , // marginRight - 隔代之间的间隔

            "titleHeight" => 400, //家族名高度
            "generationHeight" => 100, //辈份高度

            "marginTop" => 500, // 家族名高度 + 辈份高度
            "marginBottom" => 90, // marginBottom
            "paddingLeft" => 90, // paddingLeft
            "paddingRight" => 90, // paddingRight
            "paddingTop" => 90, // paddingTop
            "paddingBottom" => 90, // paddingBottom

            "levelTextX" => 0,             // 第几世文字的x位置
            "levelTextY" => 100,              // 第几世文字的y位置
            "levelWidth" => 110,             // 第几世文字背景宽度
            "levelHeight" => 300,              // 第几世文字背景高度
            "levelTextDistance" => 420,       // 第几世文字的距离  = 人高度 加上 世代间隔
            "levelTextFontSize" => 50,         //辈分的字体大小
            "levelNodeWidth" => 200,         // 第几世文字背景宽度
            "levelPadding" => 20,        // 世代之间的fix

            "pageBarWidth" => 150, //页码板块宽度


        ];
    }

    /**
     * 获取高清的从右往左的配置文件
     */
    public static function getHDR2LConfig(): array
    {
        return [
            "nodeWidth" => 180,              // 人名处的宽度 2字
            "nodeHeight" => 60,             // 人名处的高度
            "letterSpace" => 60,            //人名处的字间距
            "fontSize" => 50,            // 人名字体大小
            "siblingSeparation" => 60,      // 隔代之间的间隔
            "subtreeSeparation" => 60,      // 兄弟之间的间隔
            "sideTextSeparation" => 25,      // 侧边栏距离底部的距离
            "sideTextSize" => 30,      // 侧边栏字体大小
            "familyNameFontSize" => 105, // 标题的字体大小
            "familyPageFontSize" => 90,// 页码字体大小
            "marginLeft" => 105, // marginLeft
            "marginRight" => 105 - 75, // marginRight - 隔代之间的间隔

            "titleHeight" => 400, //家族名高度
            "generationHeight" => 100, //辈份高度

            "marginTop" => 500, // 家族名高度 + 辈份高度
            "marginBottom" => 90, // marginBottom
            "paddingLeft" => 90, // paddingLeft
            "paddingRight" => 90, // paddingRight
            "paddingTop" => 90, // paddingTop
            "paddingBottom" => 90, // paddingBottom

            "levelTextX" => 0,             // 第几世文字的x位置
            "levelTextY" => 100,              // 第几世文字的y位置
            "levelWidth" => 110,             // 第几世文字背景宽度
            "levelHeight" => 300,              // 第几世文字背景高度
            "levelTextDistance" => 420,       // 第几世文字的距离  = 人高度 加上 世代间隔
            "levelTextFontSize" => 50,         //辈分的字体大小
            "levelNodeWidth" => 200,         // 第几世文字背景宽度
            "levelPadding" => 20,        // 世代之间的fix


            "pageBarWidth" => 150, //页码板块宽度


        ];
    }
}