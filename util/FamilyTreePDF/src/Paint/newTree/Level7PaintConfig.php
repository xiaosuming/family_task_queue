<?php


namespace FamilyTreePDF\Paint\newTree;


class Level7PaintConfig
{
    public static function getHDR2LConfig()
    {
        return [
            "gridWidth" => 72,             //=2（node+separatrion）=levelImageWidth   世代图的宽度
            "nodeWidth" => 48,              // 人名处的宽度
            "nodeHeight" => 130,             // 人名处的高度
            "siblingSeparation" => 24,      // 兄弟之间的间隔
            "subtreeSeparation" => 24,      // 隔代之间的间隔
            "spouseSeparation" => 24,       // 配偶之间的间隔
            // "spouseXOffset" => -168,       // 画配偶时的偏移
            // "spouseLineXOffset" => -78,      // 画配偶连线时的X偏移
            // "spouseLineYOffset" => 105,      // 画配偶连线时的Y偏移

            "levelSeparation" => 170,        // 世代之间的间隔
            "lineYOffset" =>50,              // 吊线图横线y轴距离终点距



            //之子2字
            "font4Size" => 22,               // 人名字体大小
            "fontX4Offset" => 0,             // 绘制人物名字的左偏移量
            "fontY4Offset" => 20,            // 绘制人物名字的上偏移
            "text4YDistance" => 8,           // 绘制姓名文字时y轴上的距离   字间距

            //之子3字
            "font5Size" => 18,               // 人名字体大小
            "fontX5Offset" => 0,             // 绘制人物名字的左偏移量
            "fontY5Offset" => 10,            // 绘制人物名字的上偏移
            "text5YDistance" => 6,           // 绘制姓名文字时y轴上的距离   字间距


            //2字
            "fontSize" => 32,               // 人名字体大小
            "fontXOffset" => 0,             // 绘制人物名字的左偏移量
            "fontYOffset" => 50,            // 绘制人物名字的上偏移
            "textYDistance" => 18,           // 绘制姓名文字时y轴上的距离   字间距
            //3字  + 右
            "font3Size" => 26,               // 人名字体大小
            "fontX3Offset" => 5,             // 绘制人物名字的左偏移量
            "fontY3Offset" => 45,            // 绘制人物名字的上偏移
            "text3YDistance" => 5,           // 绘制姓名文字时y轴上的距离   字间距


            "xoffset" => -5,                // 粘贴图片时的偏移
            "yoffset" => 220,               // 粘贴图片时的偏移
            "pageWidth" => 1080,             // 每页的宽度
            "pageHeight" => 1920,            // 每页的高度
            "pageCenterWidth" => 390,       // 每页的中间宽度
            "copyXOffset" => 0,
            "copyYOffset" => 0,           //画布原点距离
            "lineWidth" => 2,
            "levelImageYOffset" => -100,  //- XIA
            "levelImageWidth" => 72,       // 世代图的宽度
            "levelImageHeight" => 1476,     // 世代图的高度
            "nameTextYOffset" => 0,         // 人物姓名的偏移
            "fatherNameTextYOffset" => -125,         // 人物姓名的偏移

            "treeYOffset" => -230,            // 为了在顶部有留白，整棵树的y整体偏移
            "specialXOffset" => 0,


            "pageFontSize" => 14,               // 页面标签字体大小
            "pageFontXOffset" => 20,             // 绘制页面标签的左偏移量
            "pageFontYOffset" => 70,            // 绘制页面标签的上偏移
            "pageTextYDistance" => 4,           // 绘制页面标签时y轴上的距离   字间距


            "branchFontSize" => 50,               // 页面标签字体大小
            "branchFontXOffset" => 0,             // 绘制页面标签的左偏移量
            "branchFontYOffset" => 170,            // 绘制页面标签的上偏移
            "branchTextYDistance" => 26,           // 绘制页面标签时y轴上的距离   字间距


            'sideTextSize' => 18,
            'sideTextVSize' => 4,
            'sideTextHSize' => 4,
            'sideHeight' => 150,
            'sidebarXOffset' => 0,     //-左 +右

            'sideRHeight' => 100,
            'sidebarRXOffset' => 30,     //-左 +右


            'sidebarYOffset' => 75,
            'sidebarAdjust' => 2,       // 从右向左有bug，所以加上这个
            'sidebarSpace' => 54,       // 侧边栏的可用宽度，这个宽度是一个人物边上本来就有一定的留白，如果有少量侧边栏文字则可以画在
            // 这个留白里，不需要偏移

            "extraSpace" => 24,             // 族谱树裁剪时的额外留白       第一行两张图之间间隔


            "smallFontDistance" => 9,       // 小的文字的Y轴距离
            "smallFontSizeDiff" => 18,       // 小的文字的大小差异


            "levelTextYDistance" => 18,      // 1  第几世的文字Y轴偏移   字间距
            "levelTextX" => 12,             // 第几世文字的x位置   Tree部分
            "levelTextY" => 94,              // 第几世文字的y位置
            "levelTextFontSize" => 36,


            "level2TextYDistance" => 10,      // 2  第几世的文字Y轴偏移   字间距
            "level2TextY" => 80,              // 第几世文字的y位置

            "level3TextYDistance" =>2,        // 3  第几世的文字Y轴偏移   字间距
            "level3TextY" => 76,              // 第几世文字的y位置

            "levelTextDistance" => 306,      // 第几世的文字Y轴偏移


            "deschalfImgPath" => '/deschalf_hd.png',
            'halfblankImgPath' => '/halfblank_hd.png',
            'totalblankImgPath' => '/totalblank_hd.png',
            'levelImgPath' => '/level_hd-90.png',

            "mottoHeight" => 270,              // 家训部分的高度
            "mottoFontSize" => 60,          // 家训的字体大小
            "mottoXOffset" => 60,           // 家训的X轴偏移
            "mottoYOffset" => 90,           // 家训的y轴偏移


            "descStartX" => 1080,            // 描述部分正文的X起点
            "descStartY" => 0,            // 描述部分正文的Y起点
            "descTextOffsetY" => 24,         // 描述部分的文本的y轴偏移，防止与横线重叠
            "descNormalFontSize" => 30,     // 描述的普通字体大小
            "descNameFontSize" => 36,       // 描述名字的字体大小
            "descLevelFontSize" => 42,       // 辈分的字体大小
            'descTopTextFontSize' => 30,     // 顶部的描述文字的大小
            'descPadding' => 18,             // 内边距
            'descTextDistance' => 24,       // 文字间距
            'descLevelYOffset' => 420,      // 描述部分的世代的y轴偏移


            'descUnitWidth' => 120,          // 描述部分的最小宽度单元
            'topDescHeight' => 120,
            'nameDescHeight' => 240,
            'descMaxLen' => 1200,            // 描述部分文字的最大长度

            'eventPageWidth' => 1080,        // 大事件的每页宽度
            'eventPageHeigth' => 1638,       // 大事件的每页高度
            'eventPaddingLevel' => 60,      // 大事件的左内边距
            'eventPaddingRight' => 60,      // 大事件的右内边距
            'eventPaddingTop' => 120,        // 大事件的上内边距
            'eventPaddingBottom' =>  120,    // 大事件的下内边距
            'eventLineheight' => 60,             // 行高
            'eventFontsize' => 26,               // 字体大小
            'eventFontDistance' => 18,            // 字体距离

            'photoPageWidth' => 1080,        // 相片的每页宽度
            'photoMaxWidth' => 900,         // 相片的最大宽度
            'photoOneMaxHeight' => 1200,     // 一张相片的最大高度
            'photoTwoMaxHeight' => 600,     // 两张相片排列时的最大高度
            'photoOneMarginTop' => 120,
            'photoTwoMarginTop1' => 60,      // 第一张相片的上外边距
            'photoTwoMarginTop2' => 273*3,      // 第二张相片的上外边距


            'graveStartX' => 60,            // 宗祠部分的起点X
            'graveStartY' => 120,            // 宗祠部分的起点Y
            'graveNameFontSize' => 60,      // 宗祠名字体大小
            'graveAddressFontSize' => 36,   // 宗祠地址字体大小
            'graveDescFontSize' => 36,      // 宗祠描述字体大小
            'graveMaxWidth' => 900,         // 宗祠最大宽度
            'graveMaxHeight' => 1638,        // 宗祠最大高度
            'gravePhotoMargin' => 150,       // 宗祠相片向上的距离
        ];
    }
}