<?php
/**
 * 绘制的上下文，在所有部分的绘制中，有一些需要共享的变量（比如config, page等)
 * 这些变量都存放在这个context中
 * @Author: jiangpengfei
 * @Date:   2019-01-03
 */

 namespace FamilyTreePDF\Paint;


 class PaintContext {
     private $totalPage;            // 当前的总页数
     private $descPageMap;          // 人物id和页码在行传中对应映射
     private $canvas;               // 画布
     private $allLevelList;         // 所有人物的辈分列表
     private $personMap;            // 人物id和人物的映射
     private $minLevel;
     private $maxLevel;          
     private $options;             // 一些用户的配置选项   

     public function __construct()
     {
          $this->totalPage = 0;
     }

     /**
      * Get the value of totalPage
      */ 
     public function &getTotalPageRef()
     {
          return $this->totalPage;
     }

     /**
      * Set the value of totalPage
      *
      * @return  self
      */ 
     public function setTotalPage($totalPage)
     {
          $this->totalPage = $totalPage;

          return $this;
     }

     /**
      * Get the value of descPageMap
      */ 
     public function &getDescPageMapRef()
     {
          return $this->descPageMap;
     }

     /**
      * Set the value of descPageMap
      *
      * @return  self
      */ 
     public function setDescPageMap($descPageMap)
     {
          $this->descPageMap = $descPageMap;

          return $this;
     }

     /**
      * Get the value of canvas
      */ 
     public function getCanvas()
     {
          return $this->canvas;
     }

     /**
      * Set the value of canvas
      *
      * @return  self
      */ 
     public function setCanvas($canvas)
     {
          $this->canvas = $canvas;

          return $this;
     }

     /**
      * Get the value of allLevelList
      */ 
     public function &getAllLevelListRef()
     {
          return $this->allLevelList;
     }

     /**
      * Set the value of allLevelList
      *
      * @return  self
      */ 
     public function setAllLevelList($allLevelList)
     {
          $this->allLevelList = $allLevelList;

          return $this;
     }

     /**
      * Get the value of personMap
      */ 
     public function &getPersonMapRef()
     {
          return $this->personMap;
     }

     /**
      * Set the value of personMap
      *
      * @return  self
      */ 
     public function setPersonMap($personMap)
     {
          $this->personMap = $personMap;

          return $this;
     }

     /**
      * Get the value of minLevel
      */ 
     public function &getMinLevelRef()
     {
          return $this->minLevel;
     }

     /**
      * Set the value of minLevel
      *
      * @return  self
      */ 
     public function setMinLevel($minLevel)
     {
          $this->minLevel = $minLevel;

          return $this;
     }

     /**
      * Get the value of maxLevel
      */ 
     public function &getMaxLevelRef()
     {
          return $this->maxLevel;
     }

     /**
      * Set the value of maxLevel
      *
      * @return  self
      */ 
     public function setMaxLevel($maxLevel)
     {
          $this->maxLevel = $maxLevel;

          return $this;
     }

     /**
      * Get the value of options
      */ 
     public function getOptions()
     {
          return $this->options;
     }

     /**
      * Set the value of options
      *
      * @return  self
      */ 
     public function setOptions($options)
     {
          $this->options = $options;

          return $this;
     }
 }