<?php

/**
 * 负责封装绘制中共用的一些方法，比如绘制文字等等
 */
namespace FamilyTreePDF\Paint;

use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Util\Util;


class PaintUtil
{
    public static function paintText($canvas, $x, $y, $text, $config, $font, $shouldSmall = false, $bold = true, $color = 0x000000)
    {
        $fontSize = $config['fontSize'];
        $distance = $config['textYDistance'];
        //$color = 0x000000;

        if ($shouldSmall) {
            $fontSize = $fontSize - $config['smallFontSizeDiff'];
            $distance = $config['smallFontDistance'];
        }

        $len = mb_strlen($text);

        if (!$shouldSmall) {    // 名字只显示4个字
            if ($len > 4) {
                $text = mb_substr($text, 0, 4);
                $len = 4;
            }
        }

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);

            imagettftext(
                $canvas,
                $fontSize,
                0,
                $x + $config['fontXOffset'] + $config['xoffset'],
                $y + $config['fontYOffset'] - $config['yoffset'],
                $color,
                $font,
                $c
            );

            if ($bold) {
                imagettftext(
                    $canvas,
                    $fontSize,
                    0,
                    $x + 1 + $config['fontXOffset'] + $config['xoffset'],
                    $y + $config['fontYOffset'] - $config['yoffset'],
                    $color,
                    $font,
                    $c
                );
            }

            $y = $y + $distance + $fontSize;
        }
    }

    /**
     * 绘制有宽度的线
     * @param $canvas 画布
     * @param $sx     起点的横坐标
     * @param $sy     起点的纵坐标
     * @param $ex     终点的横坐标
     * @param $ey     终点的纵坐标
     * @param $width  宽度
     */
    public static function imagelineWithWidth($canvas, $sx, $sy, $ex, $ey, $width)
    {
        if ($width == 1) {
            imageline($canvas, $sx, $sy, $ex, $ey, 0x000000);
        } else {
            if ($sx == $ex) {
                // 纵向
                imagefilledrectangle($canvas, $sx, $sy, $ex + $width, $ey, 0x000000);
            } else {
                // 横向
                imagefilledrectangle($canvas, $sx, $sy, $ex, $ey + $width, 0x000000);
            }
        }
    }

    // 下载图片，可以通过width和height指定大小
    public static function downloadImage($url, $outfile, $width, $height)
    {

        if (($pos = strpos($url, "?")) > 0) {
            $url = substr($url, 0, $pos);
            $url = $url . "?imageView2/2/w/$width/h/$height";
        } else {
            $url = $url . "?imageView2/2/w/$width/h/$height";
        }

        $ch = curl_init();

        $fp = fopen($outfile, 'w');

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        fclose($fp);
        curl_close($ch);

        return true;
    }


    /**
     * 获取要绘制的线的路径
     * @param 从dv库中输出的pathStr
     */
    public static function getPath($pathStr)
    {
        $paths = array();

        $cs = explode(' ', $pathStr);

        for ($i = 1; $i < count($cs); $i = $i + 6) {
            $path = array();
            $path['sx'] = $cs[$i + 1];
            $path['sy'] = $cs[$i + 2];
            $path['ex'] = $cs[$i + 4];
            $path['ey'] = $cs[$i + 5];

            $paths[] = $path;
        }

        return $paths;
    }


    /**
     * 将排行转换成文字
     */
    public static function convertRanking($ranking, $gender,$isAdoption, $showAdoption=false, $showZhiZi=false)
    {
        if ($showAdoption){
            switch ($ranking) {
                case 1:
                    if ($showZhiZi){
                        if ($gender == 0) {
                            return $isAdoption == 1 ?'嗣女':'之女';
                        } else {
                            return $isAdoption == 1 ?'嗣子':'之子';
                        }
                    }else{
                        if ($gender == 0) {
                            return $isAdoption == 1 ?'嗣女':'长女';
                        } else {
                            return $isAdoption == 1 ?'嗣子':'长子';
                        }
                    }
                    break;
                case 2:
                    if ($gender == 0) {
                        return $isAdoption == 1 ?'嗣女':'次女';
                    } else {
                        return $isAdoption == 1 ?'嗣子':'次子';
                    }
                    break;

                default:
                    if ($gender == 0) {
                        return $isAdoption == 1 ?'嗣女':PaintUtil::number2chinese($ranking) . '女';
                    } else {
                        return $isAdoption == 1 ?'嗣子':PaintUtil::number2chinese($ranking) . '子';
                    }
            }
        }else{
            switch ($ranking) {
                case 1:
                    if ($showZhiZi){
                        if ($gender == 0) {
                            return '之女';
                        } else {
                            return '之子';
                        }
                    }else{
                        if ($gender == 0) {
                            return '长女';
                        } else {
                            return '长子';
                        }
                    }
                    break;
                case 2:
                    if ($gender == 0) {
                        return '次女';
                    } else {
                        return '次子';
                    }
                    break;

                default:
                    if ($gender == 0) {
                        return PaintUtil::number2chinese($ranking) . '女';
                    } else {
                        return PaintUtil::number2chinese($ranking) . '子';
                    }
            }
        }

    }

//    /**
//     * 将排行转换成文字
//     */
//    public static function convertRanking($ranking, $gender, $showAdoption=0)
//    {
//        switch ($ranking) {
//            case 1:
//                if ($gender == 0) {
//                    return '长女';
//                } else {
//                    return '长子';
//                }
//                break;
//            case 2:
//                if ($gender == 0) {
//                    return '次女';
//                } else {
//                    return '次子';
//                }
//                break;
//
//            default:
//                if ($gender == 0) {
//                    return PaintUtil::number2chinese($ranking) . '女';
//                } else {
//                    return PaintUtil::number2chinese($ranking) . '子';
//                }
//        }
//    }

    /**
     * number2chinese description
     *
     * · 个，十，百，千，万，十万，百万，千万，亿，十亿，百亿，千亿，万亿，十万亿，
     *   百万亿，千万亿，兆；此函数亿乘以亿为兆
     *
     * · 以「十」开头，如十五，十万，十亿等。两位数以上，在数字中部出现，则用「一十几」，
     *   如一百一十，一千零一十，一万零一十等
     *
     * · 「二」和「两」的问题。两亿，两万，两千，两百，都可以，但是20只能是二十，
     *   200用二百也更好。22,2222,2222是「二十二亿两千二百二十二万两千二百二十二」
     *
     * · 关于「零」和「〇」的问题，数字中一律用「零」，只有页码、年代等编号中数的空位
     *   才能用「〇」。数位中间无论多少个0，都读成一个「零」。2014是「两千零一十四」，
     *   20014是「二十万零一十四」，201400是「二十万零一千四百」
     *
     * 参考：https://jingyan.baidu.com/article/636f38bb3cfc88d6b946104b.html
     *
     * @param  minx  $number
     * @param  boolean $isRmb
     * @return string
     */
    public static function number2chinese($number, $isRmb = false)
    {
        // 判断正确数字
        if (!preg_match('/^-?\d+(\.\d+)?$/', $number)) {
            throw new \Exception('number2chinese() wrong number', 1);
        }
        list($integer, $decimal) = explode('.', $number . '.0');
        // 检测是否为负数
        $symbol = '';
        if (substr($integer, 0, 1) == '-') {
            $symbol = '负';
            $integer = substr($integer, 1);
        }
        if (preg_match('/^-?\d+$/', $number)) {
            $decimal = null;
        }
        $integer = ltrim($integer, '0');
        // 准备参数
        $numArr = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九', '.' => '点'];
        $descArr = ['', '十', '百', '千', '万', '十', '百', '千', '亿', '十', '百', '千', '万亿', '十', '百', '千', '兆', '十', '百', '千'];
        if ($isRmb) {
            $number = substr(sprintf("%.5f", $number), 0, -1);
            $numArr = ['', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖', '.' => '点'];
            $descArr = ['', '拾', '佰', '仟', '万', '拾', '佰', '仟', '亿', '拾', '佰', '仟', '万亿', '拾', '佰', '仟', '兆', '拾', '佰', '仟'];
            $rmbDescArr = ['角', '分', '厘', '毫'];
        }
        // 整数部分拼接
        $integerRes = '';
        $count = strlen($integer);
        if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else if ($count == 0) {
            $integerRes = '零';
        } else {
            for ($i = 0; $i < $count; $i++) {
                $n = $integer[$i]; // 位上的数
                $j = $count - $i - 1; // 单位数组 $descArr 的第几位
                // 零零的读法
                $isLing = $i > 1 // 去除首位
                    && $n !== '0' // 本位数字不是零
                    && $integer[$i - 1] === '0'; // 上一位是零
                $cnZero = $isLing ? '零' : '';
                $cnNum = $numArr[$n];
                // 单位读法
                $isEmptyDanwei = ($n == '0' && $j % 4 != 0) // 是零且一断位上
                    || substr($integer, $i - 3, 4) === '0000'; // 四个连续0
                $descMark = isset($cnDesc) ? $cnDesc : '';
                $cnDesc = $isEmptyDanwei ? '' : $descArr[$j];
                // 第一位是一十
                if ($i == 0 && $cnNum == '一' && $cnDesc == '十') {
                    $cnNum = '';
                }

                // 二两的读法
                $isChangeEr = $n > 1 && $cnNum == '二' // 去除首位
                    && !in_array($cnDesc, ['', '十', '百']) // 不读两\两十\两百
                    && $descMark !== '十'; // 不读十两
                if ($isChangeEr) {
                    $cnNum = '两';
                }

                $integerRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 小数部分拼接
        $decimalRes = '';
        $count = strlen($decimal);
        if ($decimal === null) {
            $decimalRes = $isRmb ? '整' : '';
        } else if ($decimal === '0') {
            $decimalRes = '零';
        } else if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else {
            for ($i = 0; $i < $count; $i++) {
                if ($isRmb && $i > count($rmbDescArr) - 1) {
                    break;
                }

                $n = $decimal[$i];
                $cnZero = $n === '0' ? '零' : '';
                $cnNum = $numArr[$n];
                $cnDesc = $isRmb ? $rmbDescArr[$i] : '';
                $decimalRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 拼接结果
        $res = $symbol . ($isRmb ?
            $integerRes . ($decimalRes === '零' ? '元整' : "元$decimalRes") : $integerRes . ($decimalRes === '' ? '' : "点$decimalRes"));
        return $res;
    }

    /**
     * 将日期转换为中文
     * @param $date
     * @return string 日期的中文名称
     */
    public static function dateToChinese($date)
    {

        if ($date == null || $date == '') {
            return '';
        }

        $arr = explode('-', $date);
        $year = $arr[0];
        $month = $arr[1];
        $day = $arr[2];

        $yearStr = '';
        for ($i = 0; $i < strlen($year); $i++) {
            $yearStr .= PaintUtil::number2chinese($year[$i]);
        }
        $yearStr .= '年';

        $monthStr = PaintUtil::number2chinese($month) . '月';
        $dayStr = PaintUtil::number2chinese($day) . '月';


        return $yearStr . $monthStr . $dayStr;
    }

    /**
     * 根据字体的宽度，间距，一行的高度，获取最小的宽度
     */
    public static function getTextsWidth($texts, $fontSize, $vSize, $hSize, $height)
    {
        $lineTextNum = floor(($height + $vSize) / ($fontSize + $vSize));   // 一行有多少个字

        // if ($fontSize == 30) {
        //     echo "一行的字数$lineTextNum\n";
        //     echo "字数".mb_strlen($texts).PHP_EOL;
        //     echo "文字".$texts.PHP_EOL;
        // }

        $lineNum = ceil(mb_strlen($texts) / $lineTextNum);    // 多少行

        $width = $lineNum * $fontSize;

        if ($lineNum > 1) {
            $width += ($lineNum - 1) * $hSize;
        }

        // echo "宽度 $width \n";

        return $width;
    }

    /**
     * 使用generator在text中迭代字符
     */
    public static function getInText($text)
    {
        $strlen = mb_strlen($text);
        for ($i = 0; $i < $strlen; $i++) {
            $c = mb_substr($text, $i, 1);
            yield $c;
        }
    }

    /**
     * 获取在一个盒子中能画下的最多的字数
     */
    public static function getAvailableTextCountInBox($width, $height, $padding, $textDistance, $fontSize)
    {
        $width = $width - $padding['left'] - $padding['right'];
        $height = $height - $padding['top'] - $padding['bottom'];

        $rows = floor(($height + $textDistance) / ($fontSize + $textDistance));
        $cols = floor(($width + $textDistance) / ($fontSize + $textDistance));

        return $rows * $cols;
    }

    /**
     * 在一个盒子中绘制文字.
     * 这个方法不会考虑换页的问题，所以换页问题需要在调用这个方法外面去解决
     * @param $canvas 要绘制的画布
     * @param $x      文字的起始x
     * @param $y      文字的起始y
     * @param $width  盒子的宽度
     * @param $height 盒子的高度
     * @param $text   要绘制的文字
     * @param $padding 内边距 [top, left, right, bottom]
     * @param $textDistance 文字间的间距
     * @param $direction 绘制的方向
     * @param $font   字体
     * @param $fontSize 字体大小
     * @param $color  字体颜色
     * @param $needCenter 文字是否需要居中
     * @param $needVerticalCenter 文字是否需要垂直居中
     * @param $needBold 文字是否需要加粗
     * @param $posPageMap pos->pagenum  文字位置对应的页码
     * @param $textOffset 文字偏移量 pos - textOffset -> pagenum
     */
    public static function paintInBox($canvas, $x, $y, $width, $height, $text, $padding, $textDistance, $direction, $font, $fontSize, $color, $needCenter = false, $needVerticalCenter = false, $needBold = false, $posPageMap = [], $textOffset = 0)
    {
        $initX = $x;
        $initY = $y;

        $startX = 0;
        $startY = 0;

        if ($needCenter) {
            $usedWidth = PaintUtil::getTextsWidth($text, $fontSize, $textDistance, $textDistance, $height - $padding['top'] - $padding['bottom']);
        }

        if ($needVerticalCenter) {
            $lineTextNum = floor(($height - $padding['top'] - $padding['bottom'] + $textDistance) / ($fontSize + $textDistance));   // 一行有多少个字

            $actualTextNum = mb_strlen($text);

            // 实际是否有这么多字
            if ($actualTextNum >= $lineTextNum) {
                $usedHeight = $lineTextNum * $fontSize + ($lineTextNum - 1) * $textDistance;
            } else {
                // 没这么多字，实际高度是多少
                $usedHeight = $actualTextNum * $fontSize + ($actualTextNum - 1) * $textDistance;
            }
        }

        if ($direction == SysConst::$LEFT_TO_RIGHT) {
            if ($needCenter) {
                $startX = $x + ($width - $usedWidth) / 2;
            } else {
                $startX = $x + $padding['left'];
            }

            if ($needVerticalCenter) {
                $startY = $y + ($height - $usedHeight) / 2;
            } else {
                $startY = $y + $padding['top'];
            }
        } else {
            if ($needCenter) {
                $startX = $x - ($width - $usedWidth) / 2;
            } else {
                $startX = $x - $padding['right'];
            }

            if ($needVerticalCenter) {
                $startY = $y + ($height - $usedHeight) / 2;
            } else {
                $startY = $y + $padding['top'];
            }
        }

        $x = $startX;
        $y = $startY;


        $textGenerator = PaintUtil::getInText($text);

        $pos = $textOffset;

        foreach ($textGenerator as $c) {
            
            if ($c == '@' && isset($posPageMap[$pos])) {
                // 特殊的页码，使用页码的方式绘制
                $pn = $posPageMap[$pos];
                imagettftext($canvas, floor($fontSize / 1.5), 0, $x - ($fontSize/3), $y, $color, $font, '('.$pn.')');
            } else {
                imagettftext($canvas, $fontSize, 0, $x, $y, $color, $font, $c);
            }

            if ($needBold) {
                // 粗体
                imagettftext(
                    $canvas,
                    $fontSize,
                    0,
                    $x + 1,
                    $y,
                    $color,
                    $font,
                    $c
                );
            }

            $y = $y + $fontSize + $textDistance;

            if ($y + $fontSize + $padding['bottom'] > $initY + $height) {
                // 换行

                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    $x = $x + $fontSize + $textDistance;
                } else {
                    $x = $x - $fontSize - $textDistance;
                }

                $y = $startY;
            }

            $pos++;

        }   // end foreach
    }

    /**
     * 根据人物来排序
     * 父辈作为一级排序依据
     * 同一父辈排行作为二级排序依据
     * @param $parentPersons 父辈的人物
     * @param $levelPersons 要排序的一代人
     * @return array 排序好的人物数组
     */
    public static function sortPersons($parentPersons, $levelPersons)
    {
        $rankMap = [];
        $personMap = [];
        $max = count($parentPersons);
        foreach ($parentPersons as $person) {
            $rankMap[$person['id']] = $max;
            $personMap[$person['id']] = $person;
            $max--;
        }

        foreach ($levelPersons as $person) {
            $personMap[$person['id']] = $person;
        }

        usort($levelPersons, function ($person1, $person2) use ($rankMap, $personMap) {
            $parent1Id = 0;
            $parent2Id = 0;

            if ($person1['type'] != '1') {
                // 是配偶
                $tmpPerson1 = $personMap[$person1['spouse'][0]];
            } else {
                $tmpPerson1 = $person1;
            }

            if (count($tmpPerson1['father']) > 0 && isset($personMap[$tmpPerson1['father'][0]]) && $personMap[$tmpPerson1['father'][0]]['type'] == '1') {
                $parent1Id = $tmpPerson1['father'][0];
            } else {
                $parent1Id = $tmpPerson1['mother'][0];
            }

            if ($person2['type'] != '1') {
                // 是配偶
                $tmpPerson2 = $personMap[$person2['spouse'][0]];
            } else {
                $tmpPerson2 = $person2;
            }

            if (count($tmpPerson2['father']) > 0 && isset($personMap[$tmpPerson2['father'][0]]) && $personMap[$tmpPerson2['father'][0]]['type'] == '1') {
                $parent2Id = $tmpPerson2['father'][0];
            } else {
                $parent2Id = $tmpPerson2['mother'][0];
            }

            if ($parent1Id != $parent2Id) {
                return intval($rankMap[$parent1Id], 10) < intval($rankMap[$parent2Id], 10);
            } else {
                $person1Rank = $tmpPerson1['ranking'];
                $person2Rank = $tmpPerson2['ranking'];

                if ($person1Rank != $person2Rank) {
                    // 同一个父辈，比较兄弟之间的ranking
                    return intval($person1Rank, 10) < intval($person2Rank, 10);
                } else {
                    // ranking一样，检查是没设置，还是两个配偶之间的对比
                    if ($person1['type'] == '1' && $person2['type'] == '1') {
                        // 根据id对比，id小的先创建，默认为排行较大
                        return $person1['id'] < $person2['id'];
                    } else {
                        // 检查是否是互相为配偶
                        if (in_array($person1['id'], $person2['spouse'])) {
                            // 互相为配偶
                            if ($person1['type'] == 1) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return $tmpPerson1['id'] < $tmpPerson2['id'];
                        }
                    }
                }
            }
        });

        return $levelPersons;
    }

    /**
     * 处理人物的详细信息，返回其中的人名以及对应的字符串位置
     * @param $person 要处理的人物
     * @param $personMap 人物映射
     * @return array 包含人物id和pos的数组
     */
    public static function processProfileText($person, $personMap)
    {
        $profileText = trim($person['profileText']);

        $descData = [];

        // 1. 查询这个人物的父亲(type为1)，配偶（type为2的情况)，子女(type为1)
        // 2. 根据查询到的信息进行文字匹配， 匹配到则用一个字来帮助页码占位

        if ($person['type'] == '1') {
            foreach ($person['father'] as $fatherId) {
                //if(isset($personMap[$fatherId])){// 防止数据找不到导致报错
                    $fatherName = $personMap[$fatherId]['name'];

                    $stFatherName = Util::getAllST($fatherName);
    
                    foreach ($stFatherName as $fatherName) {
                        if (is_int($pos = mb_strpos($profileText, $fatherName))) {
                            // 找到了，记录位置
                            $descData[] = [
                                'id' => $fatherId,
                                'pos' => $pos + mb_strlen($fatherName)
                            ];
    
                            break;
                        }
                    }
                //}
            }
        } else {
            foreach ($person['spouse'] as $spouseId) {
                $spouseName = $personMap[$spouseId]['name'];

                $stSpouseName = Util::getAllST($spouseName);

                foreach ($stSpouseName as $spouseName) {
                    if (is_int($pos = mb_strpos($profileText, $spouseName))) {
                        $descData[] = [
                            'id' => $spouseId,
                            'pos' => $pos + mb_strlen($spouseName)
                        ];
                        break;
                    }
                }
            }
        }


        foreach ($person['son'] as $sonId) {
            $sonName = $personMap[$sonId]['name'];

            $stSonName = Util::getAllST($sonName);

            foreach ($stSonName as $sonName) {
                if (is_int($pos = mb_strpos($profileText, $sonName))) {
                    $descData[] = [
                        'id' => $sonId,
                        'pos' => $pos + mb_strlen($sonName)
                    ];
                    break;
                }
            }
        }

        foreach ($person['daughter'] as $daughterId) {
            $daughterName = $personMap[$daughterId]['name'];

            $stDaughterName = Util::getAllST($daughterName);

            foreach ($stDaughterName as $daughterName) {
                if (is_int($pos = mb_strpos($profileText, $daughterName))) {
                    $descData[] = [
                        'id' => $daughterId,
                        'pos' => $pos + mb_strlen($daughterName)
                    ];
                    break;
                }
            }
        }

        // 对descData进行排序，排序的依据是pos的大小
        usort($descData, function ($a, $b) {
            return $a['pos'] > $b['pos'];
        });

        return $descData;
    }

    /**
     * 将正文连接页码，这里的主要目的是解决页码需要用特殊的绘制方式解决的问题
     */
    public static function concatPageNum(&$text, $pageNum, $placeHolderText) {
        $pos = mb_strlen($text);
        $text = $text.$placeHolderText;
        
        return [
            'pos' => $pos,
            'pn' => $pageNum
        ];
    }

        /**
     * 获取一个人物的排行称呼，如长子，次子，长女等
     * @param $person 人物信息
     */
    public static function getRanking($person, $showAdoption=false, $showZhiZi=false)
    {
        // 没有排行，目前根据加入的顺序来判断
        $siblings = array_merge($person['brother'], $person['sister']);
        // sort($siblings);

        // 先检查人物是否有ranking
        if ($person['ranking'] > 0) {
            // 如果子女并没有全部录入, 那么不能用这种方式判断是否为幼子/幼女
            // if(count($siblings) == $person['ranking']){
            //     if($person['gender'] == 0){
            //         return '幼女';
            //     }else{
            //         return '幼子';
            //     }
            // }
            return self::convertRanking($person['ranking'], $person['gender'],$person['isAdoption'],$showAdoption, $showZhiZi);
        }
        if($person['gender'] == 0){
            return $showAdoption ? ($person['isAdoption']==1 ? '嗣女':'女儿'):'女儿';
        }else{
            return $showAdoption ? ($person['isAdoption']==1 ? '嗣子':'儿子'):'儿子';
        }
    }

    /**
     * 根据字体的宽度，间距，一行的高度，获取最小的宽度
     */
    public static function getTextsWidthOu3($texts, $fontSize, $vSize, $hSize, $height)
    {
        $lineTextNum = floor(($height + $vSize) / ($fontSize + $vSize));   // 一行有多少个字

        // if ($fontSize == 30) {
        //     echo "一行的字数$lineTextNum\n";
        //     echo "字数".mb_strlen($texts).PHP_EOL;
        //     echo "文字".$texts.PHP_EOL;
        // }

        $lineNum = ceil(mb_strlen($texts) / $lineTextNum);    // 多少行

        $width = $lineNum * $fontSize;

        if ($lineNum > 1) {
            if ($lineNum % 2 === 0){
                $width += ($lineNum - 1) * $hSize;
            }else{
                $width += ($lineNum) * $hSize;
            }

        }

        // echo "宽度 $width \n";
//        print_r([$texts, $fontSize, $vSize, $hSize, $height,mb_strlen($texts),$width]);
        return $width;
    }
}
