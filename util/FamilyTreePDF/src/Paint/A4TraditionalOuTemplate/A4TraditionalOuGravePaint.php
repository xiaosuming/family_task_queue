<?php

/**
 * 宗祠绘制
 */

namespace FamilyTreePDF\Paint\A4TraditionalOuTemplate;

use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\FileUtil;
use FamilyTreePDF\Paint\FontManager;

class A4TraditionalOuGravePaint
{
    private $config;
    private $totalPage;
    private $graveData;
    private $font;

    public function __construct()
    {
        $this->font = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    public function setFont($font) {
        $this->font = $font;
    }

    public function setContext(PaintContext $context)
    {
        $this->totalPage = &$context->getTotalPageRef();
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function input($graveData)
    {
        $this->graveData = $graveData;
    }

    // 绘制图片
    private function paintPhoto($canvas, $photoUrl, $width, $height, $marginTop)
    {
        $outfile = ResourceManager::$HALFOUTPUT_PATH . '/photo';


        if (PaintUtil::downloadImage($photoUrl, $outfile, $width, $height)) {
            $file['name'] = 'photo';
            $file['tmp_name'] = $outfile;
            $filetype = FileUtil::getFileType($file);
            if ($filetype == FileUtil::FILE_TYPE_PNG) {
                $paintImage = imagecreatefrompng($outfile);
            } else if ($filetype == FileUtil::FILE_TYPE_JPG) {
                $paintImage = imagecreatefromjpeg($outfile);
            } else {
                echo '不支持的图片格式';
                return;
            }

            list($iwidth, $iheight, $type, $attr) = getimagesize($outfile);

            $startX = ($this->config['photoPageWidth'] - $iwidth) / 2;

            imagecopymerge($canvas, $paintImage, $startX, $marginTop, 0, 0, $iwidth, $iheight, 100);

            return $canvas;
        } else {
                        // 图片下载出错
            if ($this->logger != null) {
                $this->logger->error("error when download image:{$url}");
            } else {
                echo "图片下载出错";
            }

            return null;
        }
    }

    /**
     * 绘制普通的段落文本
     */
    private function paintNormalText($canvas, $text, $fontSize, $startX, $startY, $width)
    {
        $len = mb_strlen($text);
        $x = $startX;
        $y = $startY;
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($text, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->font, $c);
            $x = $x + $fontSize + intval($fontSize / 2);

            if ($x > $width + $startX) {
                $x = $startX;
                $y = $y + $fontSize + intval($fontSize / 2);
            }
        }

        return $y + $fontSize + intval($fontSize / 2);
    }

    public function paint()
    {
        $canvas = imagecreatefrompng(ResourceManager::$RESOURCE_PATH . $this->config['totalblankImgPath']);
        $personName = $this->graveData->personName;
        $address = $this->graveData->address;
        $desc = $this->graveData->desc;
        $photo = $this->graveData->photo;

        $startX = $this->config['graveStartX'];
        $startY = $this->config['graveStartY'];

        // 绘制宗祠标题
        $startY = $this->paintNormalText($canvas, $personName, $this->config['graveNameFontSize'], $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠地址
        $startY = $this->paintNormalText($canvas, '地址:' . $address, $this->config['graveAddressFontSize'], $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠描述
        $startY = $this->paintNormalText($canvas, '描述:' . $desc, $this->config['graveDescFontSize'], $startX, $startY, $this->config['graveMaxWidth']);

        // 绘制宗祠图片
        if ($photo != null) {
            $width = $this->config['graveMaxWidth'];
            $height = $this->config['graveMaxHeight'] - $startY - $this->config['gravePhotoMargin'];
            $tmp = $this->paintPhoto($canvas, $photo, $width, $height, $startY + $this->config['gravePhotoMargin']);

            if ($tmp != null) {
                $canvas = $tmp;
            } else {
                echo "绘制宗祠图片出错";
            }

        }

        imagepng($canvas, ResourceManager::$HALFOUTPUT_PATH.'/output' . $this->totalPage . '.png');
        imagedestroy($canvas);

        echo "绘制宗祠第{$this->totalPage}页\n";
        $this->totalPage += 1;
    }

}
