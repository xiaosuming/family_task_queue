<?php

/**
 * 将所有人物画在一张吊线图上
 */

namespace FamilyTreePDF\Paint\A4TraditionalOuTemplate;

use Dv\Dv;
use FamilyTreePDF\BaseInterface\PaintInterface;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Paint\FontManager;

class A4TraditionalOuPaintAll implements PaintInterface
{
    private $allPersons;
    private $config;
    private $direction;
    private $normalTextFont;

    public function __construct()
    {
        $this->normalTextFont = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    public function setFont($font) {
        $this->normalTextFont = $font;
    }

    public function setContext(PaintContext $context)
    {
        
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function input($allPersons)
    {
        $this->allPersons = $allPersons;
    }

    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * 数据适配器
     * @param 所有的人物数组
     */
    private function dataAdaptor($persons)
    {

        $allPersons = array();

        foreach ($persons as $person) {
            $personbj = array();
            $personbj['id'] = $person['id'];
            $personbj['name'] = $person['name'];
            $personbj['image'] = $person['photo'];
            $personbj['level'] = $person['level'];
            $personbj['gender'] = $person['gender'];
            $personbj['type'] = $person['type'];
            $personbj['ranking'] = $person['ranking'] ?? 0;

            if (count($person['father']) > 0) {
                $personbj['fathers'] = $person['father'];
            }

            if (count($person['mother']) > 0) {
                $personbj['mothers'] = $person['mother'];
            }

            if (count($person['brother']) > 0) {
                $personbj['brothers'] = $person['brother'];
            }

            if (count($person['sister']) > 0) {
                $personbj['sisters'] = $person['sister'];
            }

            if (count($person['son']) > 0) {
                $personbj['sons'] = $person['son'];
            }

            if (count($person['daughter']) > 0) {
                $personbj['daughters'] = $person['daughter'];
            }

           // 配偶
            if (count($person['spouse']) > 0) {
                $personbj['spouses'] = $person['spouse'];
            }

            $allPersons[] = $personbj;
        }

        return $allPersons;
    }

    public function paint()
    {
        $allPersons = $this->dataAdaptor($this->allPersons);

        $innerPage = 0;
        $xoffset = $this->config['xoffset'];
        $yoffset = $this->config['yoffset'];
        $pageWidth = $this->config['pageWidth'];
        $pageHeight = $this->config['pageHeight'];
        $pageCenterWidth = $this->config['pageCenterWidth'];
        $fontSize = $this->config['fontSize'];
        $fontXOffset = $this->config['fontXOffset'];
        $fontYOffset = $this->config['fontYOffset'];
        $copyXOffset = $this->config['copyXOffset'];
        $copyYOffset = $this->config['copyYOffset'];
        $lineWidth = $this->config['lineWidth'];

        $dv = new Dv();
        $dv->setConfig($this->config);

        $dv->input($allPersons);
        $dv->setDisplayStyle(2);
        $dv->setMoreSpace(false);
        $levelList = $dv->output();

        $rect = $dv->getRect();
        $paths = PaintUtil::getPath($dv->getPath());

        $canvas = imagecreatetruecolor($rect['maxWidth'], $rect['maxHeight']);

        $maxWidth = $rect['maxWidth'];
        if ($this->direction == SysConst::$RIGHT_TO_LEFT) {
            // 从右往左,将所有坐标做镜像颠倒
            foreach ($levelList as &$level) {
                foreach ($level as &$item) {
                    $item['x'] = $maxWidth - $item['x'];
                }
            }

            foreach ($paths as $key => $path) {
                $paths[$key]['ex'] = $maxWidth - $path['ex'];
                $paths[$key]['sx'] = $maxWidth - $path['sx'];
            }
        }

        $font = $this->normalTextFont;

        imagefilledrectangle($canvas, 0, 0, $rect['maxWidth'], $rect['maxHeight'], 0xffffff);

        foreach ($levelList as &$level) {
            foreach ($level as &$item) {
                if ($item['name'] != 'virtual') {
                    $x = $item['x'];
                    $y = $item['y'];

                    PaintUtil::paintText($canvas, $x, $y, $item['name'], $this->config, $this->normalTextFont);
                }
            }
        }

        // 绘制路径
        foreach ($paths as $path) {
            PaintUtil::imagelineWithWidth($canvas, $path['sx'] + $this->config['xoffset'], 
            $path['sy'] - $this->config['yoffset'], $path['ex'] + $this->config['xoffset'], 
            $path['ey'] - $this->config['yoffset'], $lineWidth);
        }

        imagepng($canvas, ResourceManager::$OUTPUT_PATH.'/all.png');
        imagedestroy($canvas);

        //释放内存
        $dv->free();
    }
}