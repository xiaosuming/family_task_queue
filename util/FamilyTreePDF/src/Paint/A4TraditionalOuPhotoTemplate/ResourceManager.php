<?php
/**
 * 资源管理类
 */

namespace FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate;

class ResourceManager {
    public static $FONT = __DIR__.'/../../res/font/1.TTC';
    public static $NAME_FONT = __DIR__ . '/../../res/font/sourcehanserif_semibold.ttf';
    public static $RESOURCE_PATH = __DIR__.'/../../res/A4TraditionalOuPhotoTemplate';
    public static $HALFOUTPUT_PATH = __DIR__.'/../../output/halfoutput';
    public static $OUTPUT_PATH = __DIR__.'/../../output';
    public static $HEADPHOTO_PATH = __DIR__.'/../../res/DefaultHeadPhoto';
}