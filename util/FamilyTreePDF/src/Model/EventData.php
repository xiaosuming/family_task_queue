<?php
/**
 * 大事件要绘制的数据
 */

namespace FamilyTreePDF\Model;

class EventData {
    public $expName;
    public $expContent;
    public $expTime;

    public function __construct($expName, $expContent, $expTime)
    {
        $this->expName = $expName;
        $this->expContent = $expContent;
        $this->expTime = $expTime;
    }
}