<?php
/**
 * 将半页图片合成pdf时需要的一些配置
 */

namespace FamilyTreePDF\Util;

class A4PDFBuilderConfig
{
    /**
     * 获取A4的普通配置
     */
    public static function getA4NormalConfig($direction)
    {

        if ($direction == SysConst::$LEFT_TO_RIGHT) {
            // 从左往右
            return [
                'odd' => [
                    'pageWidth' => 360,         // 一页图片的宽度
                    'pageHeight' => 560,        // 一页图片的高度
                    'pageXOffset' => 8,        // 一页的X轴偏移量
                    'pageYOffset' => 8,        // 一页的Y轴偏移量
                    'titleX' => 378,            // 标题的X轴偏移
                    'titleY' => 225,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 18,      // 标题的大小 最大大小
                    'titleTextYDistance' => 10, // 标题文字的间隔
                    'titleMaxLength' => 200,    // 标题字域最大宽度

                    'pageNumX' => 384,          // 页码的x
                    'pageNumY' => 450,          // 页码的y
                    'pageFontSize' => 8,       // 页码文字的大小
                    'pageFontMargin' => 6,      // 页码文字的上外边距

                    'ChildNameNumX' => 384,          // 支系谱名的x
                    'ChildNameNumY' => 42,          // 支系谱名的y
                    'ChildNameFontSize' => 8,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 6,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_l2r.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384,
                    'branchNameY' => 120,
                    'branchNameFontSize' => 10,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6,      // 支派名页码文字的上外边距
                ],
                'even' => [
                    'pageWidth' => 360,         // 一页图片的宽度
                    'pageHeight' => 560,        // 一页图片的高度
                    'pageXOffset' => 43,        // 一页的X轴偏移量
                    'pageYOffset' => 8,        // 一页的Y轴偏移量
                    'titleX' => 10,            // 标题的X轴偏移
                    'titleY' => 225,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 18,      // 标题的大小 最大大小
                    'titleTextYDistance' => 10, // 标题文字的间隔
                    'titleMaxLength' => 200,    // 标题字域最大宽度

                    'pageNumX' => 15,          // 页码的x
                    'pageNumY' => 450,          // 页码的y
                    'pageFontSize' => 8,       // 页码文字的大小
                    'pageFontMargin' => 6,      // 页码文字的上外边距

                    'ChildNameNumX' => 15,          // 支系谱名的x
                    'ChildNameNumY' => 42,          // 支系谱名的y
                    'ChildNameFontSize' => 8,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 6,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_r2l.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 15,
                    'branchNameY' => 70,
                    'branchNameFontSize' => 10,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6,      // 支派名页码文字的上外边距
                ]
            ];
        } else {
            // 从右往左
            return [
                'odd' => [
                    'pageWidth' => 360,         // 一页图片的宽度
                    'pageHeight' => 560,        // 一页图片的高度
                    'pageXOffset' => 43,        // 一页的X轴偏移量
                    'pageYOffset' => 8,        // 一页的Y轴偏移量
                    'titleX' => 10,            // 标题的X轴偏移
                    'titleY' => 225,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 18,      // 标题的大小 最大大小
                    'titleTextYDistance' => 10, // 标题文字的间隔
                    'titleMaxLength' => 200,    // 标题字域最大宽度

                    'pageNumX' => 15,          // 页码的x
                    'pageNumY' => 450,          // 页码的y
                    'pageFontSize' => 8,       // 页码文字的大小
                    'pageFontMargin' => 6,      // 页码文字的上外边距

                    'ChildNameNumX' => 15,          // 支系谱名的x
                    'ChildNameNumY' => 42,          // 支系谱名的y
                    'ChildNameFontSize' => 8,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 6,      // 支系谱名文字的上外边距
                    
                    'backgroundImgPath' => '/a4_r2l.png',    // 从右往左，奇数页

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 15,
                    'branchNameY' => 70,
                    'branchNameFontSize' => 10,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6,      // 支派名页码文字的上外边距
                ],
                'even' => [
                    'pageWidth' => 360,         // 一页图片的宽度
                    'pageHeight' => 560,        // 一页图片的高度
                    'pageXOffset' => 8,        // 一页的X轴偏移量
                    'pageYOffset' => 8,        // 一页的Y轴偏移量
                    'titleX' => 378,            // 标题的X轴偏移
                    'titleY' => 225,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 18,      // 标题的大小 最大大小
                    'titleTextYDistance' => 10, // 标题文字的间隔
                    'titleMaxLength' => 200,    // 标题字域最大宽度

                    'pageNumX' => 384,          // 页码的x
                    'pageNumY' => 450,          // 页码的y
                    'pageFontSize' => 8,       // 页码文字的大小
                    'pageFontMargin' => 6,      // 页码文字的上外边距

                    'ChildNameNumX' => 384,          // 支系谱名的x
                    'ChildNameNumY' => 42,          // 支系谱名的y
                    'ChildNameFontSize' => 8,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 6,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_l2r.png',    // 从右往左，偶数页

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384,
                    'branchNameY' => 70,
                    'branchNameFontSize' => 10,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6,      // 支派名页码文字的上外边距
                ]
            ];
        }
    }

    /**
     * 获取A4的高清配置
     */
    public static function getA4HDConfig($direction)
    {
        if ($direction == SysConst::$LEFT_TO_RIGHT) {
            return [
                'odd' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 24,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 1134,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值 705
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 1152,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 1152,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y 3*y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_l2r_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ],
                'even' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 129,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 30,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 45,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 45,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_r2l_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值


                    'branchNameX' => 15 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ]
            ];
        } else {
            return [
                'odd' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 129,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 30,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 45,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 45,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_r2l_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 15 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ],
                'even' => [

                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 24,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 1134,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 1152,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 1152,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_l2r_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ]
            ];
        }
    }

    /**
     * 获取A4的高清配置
     */
    public static function getLevel7A4HDConfig($direction)
    {
        if ($direction == SysConst::$LEFT_TO_RIGHT) {
            return [
                'odd' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 24,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 1134,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值 705
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 1152,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 1152,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y 3*y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_lr_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ],
                'even' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 129,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 30,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 45,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 45,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_rl_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值


                    'branchNameX' => 15 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ]
            ];
        } else {
            return [
                'odd' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 129,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 30,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 45,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 45,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_rl_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 15 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ],
                'even' => [

                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 24,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量
                    'titleX' => 1134,            // 标题的X轴偏移
                    'titleY' => 675,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 54,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 1152,          // 页码的x
                    'pageNumY' => 1350,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 18,      // 页码文字的上外边距

                    'ChildNameNumX' => 1152,          // 支系谱名的x
                    'ChildNameNumY' => 126,          // 支系谱名的y
                    'ChildNameFontSize' => 24,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'backgroundImgPath' => '/a4_lr_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ]
            ];
        }
    }


}
