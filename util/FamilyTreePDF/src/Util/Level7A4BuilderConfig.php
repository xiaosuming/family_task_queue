<?php


namespace FamilyTreePDF\Util;


class Level7A4BuilderConfig
{
    public static function getLevel7A4HDConfig($direction)
    {
//        if ($direction == SysConst::$LEFT_TO_RIGHT) {
            return [
                'even' => [
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 24,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量

                    'titleX' => 1124,            // 标题的X轴偏移   + →
                    'titleY' => 110,             // 标题的Y轴偏移 默认值 705
                    'titleFontSize' => 60,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 1150,          // 页码的x
                    'pageNumY' => 1300,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 8,      // 页码文字的上外边距

                    'ChildNameNumX' => 1145,          // 支系谱名的x
                    'ChildNameNumY' => 630,          // 支系谱名的y 3*y 126
                    'ChildNameFontSize' => 32,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'TangHaoNumX' => 1130,          // 堂号的x
                    'TangHaoNumY' => 1506,          // 堂号的y 3*y 126
                    'TangHaoFontSize' => 60,       // 堂号文字的大小
                    'TangHaoFontMargin' => 18,      // 堂号文字的上外边距

                    'backgroundImgPath' => '/a4_lr_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值

                    'branchNameX' => 384 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ],
                'odd' => [ //- ←
                    'pageWidth' => 1080,         // 一页图片的宽度
                    'pageHeight' => 1680,        // 一页图片的高度
                    'pageXOffset' => 129,        // 一页的X轴偏移量
                    'pageYOffset' => 24,        // 一页的Y轴偏移量

                    'titleX' => 32,            // 标题的X轴偏移
                    'titleY' => 110,             // 标题的Y轴偏移 默认值
                    'titleFontSize' => 60,      // 标题的大小 最大大小
                    'titleTextYDistance' => 30, // 标题文字的间隔
                    'titleMaxLength' => 600,    // 标题字域最大宽度

                    'pageNumX' => 54,          // 页码的x
                    'pageNumY' => 1300,          // 页码的y
                    'pageFontSize' => 24,       // 页码文字的大小
                    'pageFontMargin' => 8,      // 页码文字的上外边距

                    'ChildNameNumX' => 45,          // 支系谱名的x
                    'ChildNameNumY' => 630,          // 支系谱名的y
                    'ChildNameFontSize' =>32,       // 支系谱名文字的大小
                    'ChildNameFontMargin' => 18,      // 支系谱名文字的上外边距

                    'TangHaoNumX' => 32,          // 堂号的x
                    'TangHaoNumY' => 1506,          // 堂号的y
                    'TangHaoFontSize' =>60,       // 堂号文字的大小
                    'TangHaoFontMargin' => 18,      // 堂号文字的上外边距

                    'backgroundImgPath' => '/a4_rl_hd.png',    // A4从左往右

                    'defaultPageLength' => 300, //默认分页页数值


                    'branchNameX' => 15 * 3,
                    'branchNameY' => 70 * 3,
                    'branchNameFontSize' => 10 * 3,       // 支派名页码文字的大小
                    'branchNameFontMargin' => 6 * 3,      // 支派名页码文字的上外边距
                ]
            ];
        }
}