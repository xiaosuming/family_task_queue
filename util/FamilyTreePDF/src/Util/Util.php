<?php

namespace FamilyTreePDF\Util;

class Util {
    /**
     * 初始化目录。需要halfoutput,mergeoutput,pdfoutput,pdfoutput/tmp这四个文件夹
     */
    public static function initDir() {
        if (!is_dir(__DIR__."/../output")) {
            mkdir(__DIR__."/../output", 0777);
        }
        
        if (!is_dir(__DIR__."/../output/halfoutput")) {
            mkdir(__DIR__."/../output/halfoutput", 0777);
        }

        if (!is_dir(__DIR__."/../output/mergeoutput")) {
            mkdir(__DIR__."/../output/mergeoutput", 0777);
        }

        if (!is_dir(__DIR__."/../output/pdfoutput")) {
            mkdir(__DIR__."/../output/pdfoutput", 0777);
        }

        if (!is_dir(__DIR__."/../output/pdfoutput/tmp")) {
            mkdir(__DIR__."/../output/pdfoutput/tmp", 0777);
        }
    }

    /**
     * 获取一个词语所有的简繁体组合
     * 用户输入人物姓名的时候，常常出现前文出现繁体，后文又是简体，导致在做字符串匹配过程中遇到困难
     * 这个函数会返回一个词语所有的简繁体组合，做遍历匹配
     * 数组是按照最有可能的搭配为优先级返回出来的
     * ** 暂时只做了全简体，和全繁体。其他的组合会根据具体需求进行调整。**
     * @param string $text 要检索的字符串
     * @return array 组合数组
     */
    public static function getAllST($text) {
        $s = opencc_open(__DIR__.'/opencc/t2s.json');
        $simpleText = opencc_convert($text, $s);
        opencc_close($s);

        $t = opencc_open(__DIR__.'/opencc/s2t.json');
        $traditionalText = opencc_convert($text, $t);
        opencc_close($t);

        $result = [];
        $result[] = $simpleText;

        if ($traditionalText != $simpleText) {
            $result[] = $traditionalText;
        }

        
        return $result;
    }
}
