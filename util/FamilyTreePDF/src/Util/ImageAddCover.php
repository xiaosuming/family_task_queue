<?php
/**
 * 图片处理类:为图片添加封面（家族封面 + 家族姓氏）
 * 
 * 
 */

namespace FamilyTreePDF\Util;


class ImageAddCover{

	function __construct(){
		
		//outpath
		$this->outpath =  __DIR__.'/../output/cover.png';
		//字体路径
		$this->fontpath = __DIR__.'/../res/font/';
		//背景图路径
		$this->backpath =  __DIR__.'/../res/background/startcover/';
		
	}

	public function getCoverImage($params){

		$backimg = $this->checkBackImg($params);
		//$filename = 'cover.png';
		//新文件存放地址
		$newImg = $this->outpath;
		$img = imagecreatefromstring(file_get_contents($backimg['coverImg']));

		//旋转角度 
		$circleSize = 0;
		//字体文件 旁门正道粗体
		// $font = $this->fontpath.'pangmenzdct.ttf';
		$font = $this->fontpath.'SIMKAI.TTF';
		
		switch ($backimg['coverId']) {//封面ID

			case '1'://蓝底 单姓
				
		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 1918; 
		        //上边距 
		        $top = 990; 
				break;

			case '2'://蓝底 复姓

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 1918; 
		        //上边距 
		        $top = 920; 

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 400 , $black, $font, $surname_2);

		        $params['surname'] = '';

				break;

			case '3'://白底 单姓

		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 0, 0, 0); 
		        //字体大小 
		        $fontSize = 420;
		        //左边距 
		        $left = 982; 
		        //上边距 
		        $top = 1150; 
		        //拼音左右加横线（矩形框）
		        imagefilledrectangle($img, 450, 2700, 750, 2705, $black);
		        imagefilledrectangle($img, 1730, 2700, 2030, 2705, $black);
		        //拼音字体 苹方
		        $Pfont =$this->fontpath.'pingfang.ttf'; 
		        //拼音字体大小
		        $PfontSize = 70;
		        $pinyin = strtoupper($params['pinyin'].' SHI ZONG PU'); //zhang氏宗谱
		        $fontBox = imagettfbbox($PfontSize, 0, $Pfont, $pinyin);
		        //拼音水平居中
		        imagettftext($img, $PfontSize, 0, ceil(($backimg['Width'] - $fontBox[2])/ 2), 2730, $black, $Pfont, $pinyin);

				break;

			case '4'://白底 复姓

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 0, 0, 0); 
		        //字体大小 
		        $fontSize = 320;
		        //左边距 
		        $left = 982; 
		        //上边距 
		        $top = 950;

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 450 , $black, $font, $surname_2);

		        $params['surname'] = '';

		        //拼音左右加横线（矩形框）
		        imagefilledrectangle($img, 450, 2700, 750, 2705, $black);
		        imagefilledrectangle($img, 1730, 2700, 2030, 2705, $black);
		        //拼音字体 苹方
		        $Pfont = $this->fontpath.'pingfang.ttf'; 
		        //拼音字体大小
		        $PfontSize = 60;
		        $pinyin = strtoupper($params['pinyin'].' SHI ZONG PU'); //zhang氏宗谱
		        $fontBox = imagettfbbox($PfontSize, 0, $Pfont, $pinyin);
		        //拼音水平居中
		        imagettftext($img, $PfontSize, 0, ceil(($backimg['Width'] - $fontBox[2])/ 2), 2730, $black, $Pfont, $pinyin);

				break;

			case '5'://蓝底 单姓 从右到左 -RToL-
				
		        //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 170; 
		        //上边距 
		        $top = 990; 
				break;

			case '6'://蓝底 复姓 从右到左 -RToL-

			    //字体颜色(RGB) 
		        $black = imagecolorallocate($img, 255, 255, 255); 
		        //字体大小 
		        $fontSize = 290; 
		        //左边距 
		        $left = 170; 
		        //上边距 
		        $top = 920; 

		        //文字拆分成两个
		        $surname_1 =  mb_substr($params['surname'], 0, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

		        $surname_2 =  mb_substr($params['surname'], 1, 1);
		        imagefttext($img, $fontSize, $circleSize, $left, $top + 400 , $black, $font, $surname_2);

		        $params['surname'] = '';

				break;

				case '7'://红底
	
					//字体颜色(RGB)  金色
					$black = imagecolorallocate($img, 235,199,113); 
					
					//绘制族谱名
					if(mb_strlen($params['surname']) == 1){//单姓
						//字体大小 
						$fontSize = 220; 
						//左边距 
						$left = 1360; 
						//上边距 
						$top =1340; 
						//字体间隔
						$topWriting = 325;
		
						imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $params['surname']);

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting, $black, $font, '氏');

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting*2, $black, $font, '族');

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting*3, $black, $font, '谱');

					}else{//复姓
						//字体大小 
						$fontSize = 180; 
						//左边距 
						$left = 1390; 
						//上边距 
						$top = 1280; 
						//字体间隔
						$topWriting = 270;
		
						$surname_1 =  mb_substr($params['surname'], 0, 1);
						imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $surname_1);

						$surname_2 =  mb_substr($params['surname'], 1, 1);
						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting, $black, $font, $surname_2);

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting*2, $black, $font, '氏');

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting*3, $black, $font, '族');

						imagefttext($img, $fontSize, $circleSize, $left, $top+$topWriting*4, $black, $font, '谱');

					}
	
					$params['surname'] = '';

					//绘制支系谱名
					if(isset($params['ChildName'])){
						//字体大小 
						$childFontSize = 90; 
						//左边距 
						$childLeft = 2040; 
						//上边距 
						$childTop = 400; 
						//字体间隔
						$childTopWriting = 140;

						for($i = 0; $i < mb_strlen($params['ChildName']); $i++){
							$text = mb_substr($params['ChildName'], $i, 1);
							imagefttext($img, $childFontSize, $circleSize, $childLeft, $childTop+$childTopWriting*$i, $black, $font,$text);
						}
					}
	
					break;
			
			default:
				break;
		}

		//姓氏添加
		if($params['surname'] != ''){
		    imagefttext($img, $fontSize, $circleSize, $left, $top, $black, $font, $params['surname']);
		}
		switch ($backimg['Type']) {
		  case 1://gif
		    //header("Content-type: image/gif");
		    imagegif($img, $newImg); 
		    break; 
		  case 2://jpg
		    //header("Content-type: image/jpg"); $quality 默认75
		    imagejpeg($img, $newImg);
		    break; 
		  case 3://png
		    //header("Content-type: image/png");
		    imagepng($img, $newImg);
		    break; 
		  default: 
		    break; 
		 } 
		 //销毁照片 
		 imagedestroy($img);
		 return true;

	}

	private function checkBackImg($params){
		
		$res = [
			'coverImg' => $this->backpath . 'backgroud-blue.png',
			'coverId' => 1,
	    ];//默认蓝底 单姓
		$surnameLength = mb_strlen($params['surname']);

		switch ($params['id']){
			case 1: //蓝底
			    if($surnameLength == 1){ //单姓
			    	if(isset($params['direction']) && $params['direction'] == 1 ){ // 从右到左 -RTOL-
			    		$res['coverImg'] = $this->backpath . 'backgroud-RToL-blue.png';
			    	    $res['coverId'] = 5;
			    	}else{	 // 从左到右 默认 		    		
			    	    $res['coverImg'] = $this->backpath . 'backgroud-blue.png';
			    	    $res['coverId'] = 1;
			    	}
			    }elseif($surnameLength == 2){//复姓
			    	if(isset($params['direction']) && $params['direction'] == 1 ){ // 从右到左 -RTOL-
			    		$res['coverImg'] = $this->backpath . 'backgroud-two-RToL-blue.png';
			    	    $res['coverId'] = 6;
			    	}else{	 // 从左到右 默认 		    		
			    	    $res['coverImg'] = $this->backpath . 'backgroud-two-blue.png';
			    	    $res['coverId'] = 2;
			    	}
			    }
			    break;
			case 2://白底
			    if($surnameLength == 1){ //单姓
			    	$res['coverImg'] = $this->backpath . 'backgroud-white.png';
			    	$res['coverId'] = 3;
			    }elseif($surnameLength == 2){//复姓
			    	$res['coverImg'] = $this->backpath . 'backgroud-two-white.png';
			    	$res['coverId'] = 4;
				}
			break;
			case 3://红底  不判断是一个字还是两个字，都手动生成
			    $res['coverImg'] = $this->backpath . 'backgroud-red.png';
			    $res['coverId'] = 7;
			default: 
		    break;
		}
		list($res['Width'], $res['Height'], $res['Type']) = getimagesize($res['coverImg']);
		switch ($res['Type']) {
			case 1://gif 
		    $res['suffix'] = 'gif';
		    break; 
		  case 2://jpg 
		    $res['suffix'] = 'jpg';
		    break; 
		  case 3://png 
		    $res['suffix'] = 'png';
		    break; 
		  default: 
		    break; 
		}
		return $res;

	}

}

;?>
