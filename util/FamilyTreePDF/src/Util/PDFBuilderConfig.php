<?php
/**
 * 将半页图片合成pdf时需要的一些配置
 */

namespace FamilyTreePDF\Util;

class PDFBuilderConfig {
    
    /**
     * 获取普通清晰度的配置(A3)
     */
    public static function getNormalConfig() {
        return [
            'pageWidth' => 360,     // 半页图片的宽度
            'pageHeight' => 546,    // 半页图片的高度
            'pageOneXOffset' => 10, // 第一页的X轴偏移量
            'pageTwoXOffset' => 460, // 第二页的X轴偏移量
            'pageOneYOffset' => 20,
            'pageTwoYOffset' => 20,
            'titleX' => 400,            // 标题的X轴偏移 380 已废弃
            'titleY' => 60,             // 标题的Y轴偏移 默认值
            'titleFontSize' => 30,      // 标题的大小 最大大小
            'titleTextYDistance' => 10, // 标题文字的间隔 已废弃
            'titleMaxLength' => 180,    // 标题字域最大宽度

            'pageNumLeftX' => 395,      // 左边页码的x
            'pageNumRightX' => 430,     // 右边页码的x
            'pageNumY' => 390,          // 页码的y
            'pageFontSize' => 10,       // 页码文字的大小
            'pageFontMargin' => 5,      // 页码文字的上外边距

            'ChildNameNumLeftX' => 395,      // 左边支系谱名的x
            'ChildNameNumRightX' => 430,     // 右边支系谱名的x
            'ChildNameNumY' => 280,          // 支系谱名的y
            'ChildNameFontSize' => 10,       // 支系谱名文字的大小
            'ChildNameFontMargin' => 4,      // 支系谱名文字的上外边距

            'allblankImgPath' => '/allblank.png',
            'allblankImgPath2' => '/allblank2.png',

            'defaultPageLength' => 300, //默认分页页数值
        ];
    }

    /**
     * 获取高清配置(A3)        
     */
    public static function getHDConfig() {
        return [
            'pageWidth' => 1080,     // 半页图片的宽度
            'pageHeight' => 1638,    // 半页图片的高度
            'pageOneXOffset' => 30, // 第一页的X轴偏移量
            'pageTwoXOffset' => 1401, // 第二页的X轴偏移量
            'pageOneYOffset' => 56,
            'pageTwoYOffset' => 56,
            'titleX' => 1200,            // 标题的X轴偏移 380 已废弃
            'titleY' => 180,             // 标题的Y轴偏移 默认值
            'titleFontSize' => 90,      // 标题的大小 最大大小
            'titleTextYDistance' => 30, // 标题文字的间隔 已废弃
            'titleMaxLength' => 540,    // 标题字域最大宽度

            'pageNumLeftX' => 1185,      // 左边页码的x
            'pageNumRightX' => 1290,     // 右边页码的x
            'pageNumY' => 1170,          // 页码的y
            'pageFontSize' => 30,       // 页码文字的大小
            'pageFontMargin' => 15,      // 页码文字的上外边距
            
            'ChildNameNumLeftX' => 1185,      // 左边支系谱名的x
            'ChildNameNumRightX' => 1290,     // 右边支系谱名的x
            'ChildNameNumY' => 840,          // 支系谱名的y
            'ChildNameFontSize' => 30,       // 支系谱名文字的大小
            'ChildNameFontMargin' => 12,      // 支系谱名文字的上外边距

            'allblankImgPath' => '/allblank_hd.png',
            'allblankImgPath2' => '/allblank2.png',
            'defaultPageLength' => 300, //默认分页页数值
        ];
    }

    /**
     * 获取高清配置(A3)
     */
    public static function getHDConfig2() {
        return [
            'pageWidth' => 1080,     // 半页图片的宽度
            'pageHeight' => 1638,    // 半页图片的高度
            'pageOneXOffset' => 30, // 第一页的X轴偏移量
            'pageTwoXOffset' => 1401, // 第二页的X轴偏移量
            'pageOneYOffset' => 56,
            'pageTwoYOffset' => 56,
            'titleX' => 1200,            // 标题的X轴偏移 380 已废弃
            'titleY' => 180,             // 标题的Y轴偏移 默认值
            'titleFontSize' => 90,      // 标题的大小 最大大小
            'titleTextYDistance' => 30, // 标题文字的间隔 已废弃
            'titleMaxLength' => 540,    // 标题字域最大宽度

            'hallNameFontSize' => 90,      // 堂号的大小 最大大小
            'hallNameMaxLength'=>450, //堂号字域最大宽度
            'hallNameY'=>1110, //堂号的Y轴偏移 默认值

            'pageNumLeftX' => 1185-30,      // 左边页码的x
            'pageNumRightX' => 1290+45,     // 右边页码的x
            'pageNumY' => 1100,          // 页码的y
            'pageFontSize' => 20,       // 页码文字的大小
            'pageFontMargin' => 10,      // 页码文字的上外边距

            'ChildNameNumLeftX' => 1185-30,      // 左边支系谱名的x
            'ChildNameNumRightX' => 1290+15,     // 右边支系谱名的x
            'ChildNameNumY' => 720,          // 支系谱名的y
            'ChildNameFontSize' => 40,       // 支系谱名文字的大小
            'ChildNameFontMargin' => 12,      // 支系谱名文字的上外边距

            'allblankImgPath' => '/allblank_hd.png',
            'allblankImgPath2' => '/allblank2_hd.png',
            'topDistance'=>100,
            'bottomDistance'=>100,


            "branchFamilyNameFontSize" => 28.947368421053,
            'branchFamilyNameNumLeftX' => 1185-30,      // 左边familyName的x
            'branchFamilyNameNumRightX' => 1315,     // 右边familyName的x
            'branchFamilyNameNumY' => 700,          // familyName的y
            'branchFamilyNameMargin' => 10,      // familyName文字的上外边距

            'defaultPageLength' => 300, //默认分页页数值
        ];
    }

    /**
     * 获取高清配置(A3)
     */
    public static function getConfig2() {
        return [
            'pageWidth' => 1080/3,     // 半页图片的宽度
            'pageHeight' => 1638/3,    // 半页图片的高度
            'pageOneXOffset' => 30/3, // 第一页的X轴偏移量
            'pageTwoXOffset' => 1401/3, // 第二页的X轴偏移量
            'pageOneYOffset' => 56/3,
            'pageTwoYOffset' => 56/3,
            'titleX' => 1200/3,            // 标题的X轴偏移 380 已废弃
            'titleY' => 180/3,             // 标题的Y轴偏移 默认值
            'titleFontSize' => 90/3,      // 标题的大小 最大大小
            'titleTextYDistance' => 30/3, // 标题文字的间隔 已废弃
            'titleMaxLength' => 540/3,    // 标题字域最大宽度

            'hallNameFontSize' => 90/3,      // 堂号的大小 最大大小
            'hallNameMaxLength'=>450/3, //堂号字域最大宽度
            'hallNameY'=>1110/3, //堂号的Y轴偏移 默认值

            'pageNumLeftX' => 1155/3,      // 左边页码的x
            'pageNumRightX' => 1335/3,     // 右边页码的x
            'pageNumY' => 1100/3,          // 页码的y
            'pageFontSize' => 20/3,       // 页码文字的大小
            'pageFontMargin' => 10/3,      // 页码文字的上外边距

            'ChildNameNumLeftX' => 1155/3,      // 左边支系谱名的x
            'ChildNameNumRightX' => 1305/3,     // 右边支系谱名的x
            'ChildNameNumY' => 720/3,          // 支系谱名的y
            'ChildNameFontSize' => 40/3,       // 支系谱名文字的大小
            'ChildNameFontMargin' => 12/3,      // 支系谱名文字的上外边距

            'allblankImgPath' => '/allblank.png',
            'allblankImgPath2' => '/allblank2.png',
            'topDistance'=>100/3,
            'bottomDistance'=>100/3,


            "branchFamilyNameFontSize" => 28.947368421053/3,
            'branchFamilyNameNumLeftX' => 1155/3,      // 左边familyName的x
            'branchFamilyNameNumRightX' => 1315/3,     // 右边familyName的x
            'branchFamilyNameNumY' => 700/3,          // familyName的y
            'branchFamilyNameMargin' => 10/3,      // familyName文字的上外边距

            'defaultPageLength' => 300/3, //默认分页页数值
        ];
    }
}
