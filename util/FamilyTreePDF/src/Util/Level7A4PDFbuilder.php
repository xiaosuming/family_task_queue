<?php


namespace FamilyTreePDF\Util;

use FamilyTreePDF\Paint\Level7PaintUtil;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Util\Level7A4BuilderConfig;
use FamilyTreePDF\Util\ImageAddCover;
use FamilyTreePDF\Util\Pinyin;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

class Level7A4PDFbuilder
{

    private $direction;
    private $config;
    private $font;
    private $titleFont;
    private $resourcePath;
    private $outPath;
    private $isHD;
    private $moreSpaceBetweenTopAndBottom;
    private $treePage;

    /**
     * @param $direction 布局方向
     * @param $isHD 是否高清
     */
    public function __construct($direction, $isHD = false, $moreSpaceBetweenTopAndBottom = false, $treePage = [])
    {
        $this->direction = $direction;
        $this->font = __DIR__ . '/../res/font/1.TTC';
        $this->lishufont = __DIR__ . '/../res/font/lishu.TTF';
        $this->titleFont = __DIR__.'/../res/font/SIMKAI.TTF';
//        $this->titleFont = __DIR__.'/../res/font/pangmenzhengdao.ttf';
        $this->resourcePath = __DIR__ . '/../res/background';
        $this->outPath = __DIR__ . '/../output';

        $this->startcover = __DIR__ . '/../output/cover.png';
        $this->endcover = '';

//        if ($isHD) {
//            $this->config = A4PDFBuilderConfig::getA4HDConfig($direction);
//        } else {
//            $this->config = A4PDFBuilderConfig::getA4NormalConfig($direction);
//        }

        $this->config = Level7A4BuilderConfig::getLevel7A4HDConfig($direction);

        $this->isHD = $isHD;
        $this->defaultPageLength =  $this->config['even']['defaultPageLength'];
        $this->moreSpaceBetweenTopAndBottom = $moreSpaceBetweenTopAndBottom;
        $this->treePage = $treePage;
    }

    /**
     * 绘制A4族谱的标题
     * @param $canvas 画布
     * @param $title  标题文字
     */
    private function paintA4Title($canvas, $title, $pageIndex)
    {
        $pageConfig = [];
        if ($pageIndex % 2 == 0) {
            $pageConfig = $this->config['even'];
        } else {
            $pageConfig = $this->config['odd'];
        }
        if ($this->moreSpaceBetweenTopAndBottom){
            $backgroundImgPath = substr($pageConfig['backgroundImgPath'],0,-4) . "2.png";
        }else{
            $backgroundImgPath = $pageConfig['backgroundImgPath'];
        }

        $backImgPath = $this->resourcePath . $backgroundImgPath ;

//        $res = $this->caculateText($title, $pageConfig, $backImgPath);
//        if($res == false){
//            return false;
//        }
//        $fontSize = $res['fontSize'];
//        $titleTextYDistance = $res['TextYDistance'];
//        $titleY = $res['Y'];
//        $titleX = $res['X'];

        $titleX = $pageConfig['titleX'];
        $titleY = $pageConfig['titleY'];
        $fontSize = $pageConfig['titleFontSize'];
        $titleTextYDistance = $pageConfig['titleTextYDistance'];

        $x = $titleX;
        $y = $titleY;
        $len = mb_strlen($title);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($title, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            imagettftext($canvas, $fontSize, 0, $x+1, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $titleTextYDistance + $fontSize;
        }
    }

    /**
     * 计算文字的数量，返回字体大小 字体间隔 Y偏移量 X偏移量
     * @param $text 文字
     * @param $maxLength  字域最宽
     * @return arr
     */
    private function caculateText($text, $pageConfig, $backImgPath)
    {
        $textLength = mb_strlen($text); //字体字数

        if($textLength == 0){

            return false;
        }
        $maxFont = $pageConfig['titleFontSize']; //最大字体
        $maxLength = $pageConfig['titleMaxLength']; //字域最宽
        $res = [
            'fontSize' => '',
            'TextYDistance' => '',
            'Y' => '',
            'X' => '',
        ];
        $caculateFont = floor(($maxLength/$textLength)*(3/4));
        $res['fontSize'] = ($caculateFont > $maxFont) ? $maxFont : $caculateFont;//字体大小

        list($Width, $Height, $Type) = getimagesize($backImgPath);
        $res['X'] = $pageConfig['titleX'] + ($maxFont - $res['fontSize'])/2;//X偏移

        if($caculateFont > $maxFont){

            $res['TextYDistance'] = $pageConfig['titleTextYDistance'];//字体间隔
            $test = $textLength * $res['fontSize'] + ($textLength-1) * $res['TextYDistance'];
            $res['Y'] = ceil(($Height - $test)/2) + $pageConfig['titleY'] * 0.08;//Y偏移量
        }else{
            $res['TextYDistance'] = floor($res['fontSize']/3);//字体间隔
            $res['Y'] = $pageConfig['titleY'] * 0.95;
        }

        return $res;
    }

    /**
     * 在A4上绘制支系谱名
     * @param $canvas 画布
     * @param $ChildName   支系谱名
     */
    private function paintChildNameOnA4($canvas, $page, $ChildName)
    {
        $config = [];

        if ($page % 2 == 0) {
            $config = $this->config['even'];
        } else {
            $config = $this->config['odd'];
        }

        $ChildNameNumX = $config['ChildNameNumX'];
        $ChildNameNumY = $config['ChildNameNumY'];
        $ChildNameFontSize = $config['ChildNameFontSize'];
        $ChildNameFontMargin = $config['ChildNameFontMargin'];

        $fontSize = $ChildNameFontSize;
        $ChildNameText = $ChildName;

        $x = $ChildNameNumX;
        $y = $ChildNameNumY;
        $len = mb_strlen($ChildNameText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($ChildNameText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $ChildNameFontMargin + $fontSize;
        }
    }


    /**
     * 在A4上绘制支系谱名
     * @param $canvas 画布
     * @param $ChildName   支系谱名
     */
    private function paintTangOnA4($canvas, $page, $TangHao)
    {
        $config = [];

        if ($page % 2 == 0) {
            $config = $this->config['even'];
        } else {
            $config = $this->config['odd'];
        }

        $TangHaoNumX = $config['TangHaoNumX'];
        $TangHaoNumY = $config['TangHaoNumY'];
        $TangHaoFontSize = $config['TangHaoFontSize'];
        $TangHaoFontMargin = $config['TangHaoFontMargin'];

        $fontSize = $TangHaoFontSize;
        $TangHaoText = $TangHao;

        $x = $TangHaoNumX;
        $y = $TangHaoNumY;
        $len = mb_strlen($TangHaoText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($TangHaoText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $TangHaoFontMargin + $fontSize;
        }
    }


    /**
     * 在A4上绘制页码
     * @param $canvas 画布
     * @param $page   页码
     */
    private function paintPageOnA4($canvas, $page)
    {
        $config = [];

        if ($page % 2 == 0) {
            $config = $this->config['even'];
        } else {
            $config = $this->config['odd'];
        }

        $pageNumX = $config['pageNumX'];
        $pageNumY = $config['pageNumY'];
        $pageFontSize = $config['pageFontSize'];
        $pageFontMargin = $config['pageFontMargin'];

        $fontSize = $pageFontSize;
//        $pageText = PaintUtil::number2chinese($page) . "页";
        $pageText = Level7PaintUtil::pageToChinese($page) . "页";

        $x = $pageNumX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }
    }

    /**
     * 合并半页的图片
     * @param 族谱的标题
     */
    public function merge($titleText, $branchPage = '',$TangHao)
    {
        $return=0;
        foreach($branchPage as $k1=>$v1){
            $ChildName=$k1;
            $startpage=$v1[0];
            $endpage=$v1[1];
            $page = $startpage;
            $treePage = $this->treePage;
//            for ($i=0;$i<=$endpage-$startpage;$i++){
                //$page +=$i;
//        sort($treePage);
            var_dump($treePage);
            while ($page<=$endpage) {
                $outputPageIndex = $page ;
                $pageConfig = [];
                if ($outputPageIndex % 2 == 0) {
                    $pageConfig = $this->config['even'];
                } else {
                    $pageConfig = $this->config['odd'];
                }
                if (SysConst::$DEBUG) {
                    $time = date('h:i:s');
                    echo "合并{$outputPageIndex}页, {$time}" . PHP_EOL;
                }
                if ($this->moreSpaceBetweenTopAndBottom){
                    $backgroundImgPath = in_array($page,$treePage) ? $pageConfig['backgroundImgPath']:substr($pageConfig['backgroundImgPath'],0,-4) . "2.png";
                }else{
                    $backgroundImgPath = $pageConfig['backgroundImgPath'];
                }

                $dstCanvas = imagecreatefrompng($this->resourcePath . $backgroundImgPath);
                $this->paintA4Title($dstCanvas, $titleText, $outputPageIndex);
                $this->paintPageOnA4($dstCanvas, $outputPageIndex);
                if (array_key_exists('branchName', $GLOBALS)){
                    $this->paintBranchNameOnA4($dstCanvas, $outputPageIndex);
                }

                //$ChildName = $this->getChildName($outputPageIndex);
                if($ChildName != ''){
                    $this->paintChildNameOnA4($dstCanvas, $outputPageIndex, $ChildName);
                }
                if($TangHao != ''){
                    $this->paintTangOnA4($dstCanvas, $outputPageIndex, $TangHao);
                }


                $filepath = $this->outPath . "/halfoutput/output{$page}.png";
                if (file_exists($filepath)) {
                    $size = getimagesize($filepath);
                    $width = $size[0];
                    $height = $size[1];

                    if ($this->moreSpaceBetweenTopAndBottom){
                        $pageYOffset = $this->isHD ? $pageConfig['pageYOffset'] + 60:$pageConfig['pageYOffset'] + 20;
                    }else{
                        $pageYOffset = $pageConfig['pageYOffset'];
                    }

                    $srcCanvas = imagecreatefrompng($filepath);
                    imagecopy($dstCanvas, $srcCanvas, $pageConfig['pageXOffset'], $pageYOffset, 0, 0, $width, $height);
                    imagedestroy($srcCanvas);
                } else {
                    break;
                }
                imagepng($dstCanvas, $this->outPath . "/mergeoutput/output{$outputPageIndex}.png");
                imagedestroy($dstCanvas);
                $page += 1;
            }
            $return=$page;
        }
        return $return;
    }

    /**
     *  生成封面图
     *  判断参数是否合法
     * @param $surname 姓氏
     * @param $coverId 封面id 1 蓝色 2 白色 3 红色
     * @param $pinyin 姓氏的拼音
     * @return bool true/false
     *  组装参数
     */
    public function toCover($surname, $coverId = '1', $pinyin = '', $arr = [])
    {

        if(mb_strlen($surname) == 0 || mb_strlen($surname) > 2){
            return false;
        }
        if(isset($pinyin)){
            if(strlen($pinyin) > 12){
                return false;
            }
        }else{
            $py = new Pinyin();
            $pinyin = $py->getpy($surname, true);
            if(strlen($pinyin) == 0){
                return false;
            }
        }
        $params = [
            'id' => $coverId,
            'surname' => $surname,
            'pinyin' => $pinyin,
            'direction' => $this->direction // 布局： 0 从左到右(默认) ，1 从右到左
        ];

        switch ($coverId) {
            case '1':
                $this->endcover = $this->resourcePath . '/endcover/endcover-blue.png';
                break;
            case '2':
                $this->endcover = $this->resourcePath . '/endcover/endcover-white.png';
                break;
            case '3':
                $this->endcover = $this->resourcePath . '/endcover/endcover-red.png';
                if(isset($arr['ChildName'])){
                    $params['ChildName'] = $arr['ChildName'];
                }
            default:
                break;
        }


        $ImageAddCover = new ImageAddCover();
        return $ImageAddCover->getCoverImage($params);
        //生成的封面图路径  __DIR__.'/../output/cover.png' = $this->outPath . '/cover.png';
    }

    /**
     * 给pdf分页
     * @param $TotalPage 总页数
     * @param $PageLength 每份PDF的页数
     * @return $countPDF int
     */
    public function toPdf($TotalPage, $PageLength = 0)
    {

        if($PageLength < 1){
            // if($PageLength < $this->defaultPageLength){
            $PageLength = $this->defaultPageLength;
        }
        $countPDF = ceil($TotalPage/$PageLength);
        for($i = 0; $i< $countPDF; $i ++){
            $this->cutPdf($i, $PageLength );
        }
        //清除多余的pdf
        $otherNum = $countPDF;
        while (true) {

            $preview_filepath = $this->outPath . "/pdfoutput/preview_".$otherNum.".pdf";
            var_dump($preview_filepath);
            $print_filepath = $this->outPath . "/pdfoutput/print_".$otherNum.".pdf";

            if (file_exists($preview_filepath) || file_exists($print_filepath)) {
                if(file_exists($preview_filepath)){
                    unlink($preview_filepath);
                }
                if(file_exists($print_filepath)){
                    unlink($print_filepath);
                }
            } else {
                break;
            }
            $otherNum++;
        }
        return $countPDF;

    }

    /**
     * 将merge后的图片转换为pdf
     */
    public function cutPdf($current = 0, $PageLength = 100)
    {
        $startPage = $current * $PageLength + 1;
        /**************生产预览版***************/
        $mpdf = new Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $html = '';

        //添加首页封面图
        if(file_exists($this->startcover)){
            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
        }
        //end

        $page = $startPage;
        while ($page < $startPage+$PageLength) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $html .= "<img style='width:100%' src='" . $filepath . "'>";
            $page++;
        }

        //添加尾页封面图
        if(file_exists($this->endcover)){
            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
        }
        //end

        $mpdf->WriteHTML($html);
        $mpdf->Output($this->outPath . "/pdfoutput/preview_".$current.".pdf", Destination::FILE);

        /**************生成打印版***************/

        $mpdf = new Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $html = '';

        //添加首页封面图
        if(file_exists($this->startcover)){
            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
        }
        //end

        $page = $startPage;
        while ($page < $startPage+$PageLength) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $html .= "<img style='width:100%' src='" . $filepath . "'>";
            $page++;
        }

        //添加尾页封面图
        if(file_exists($this->endcover)){
            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
        }
        //end

        $mpdf->WriteHTML($html);
        $mpdf->Output($this->outPath . "/pdfoutput/print_".$current.".pdf", Destination::FILE);
    }

    /**
     * 清除中间文件
     */
    public function clear()
    {
        $page = 1;
        while (true) {

            $filepath = $this->outPath . "/halfoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = $this->outPath . "/pdfoutput/tmp/tmp" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        //清除封面文件
        if(file_exists($this->startcover)){
            unlink($this->startcover);
        }
        //end
    }

    /**
     * 在A4上绘制页码
     * @param $canvas 画布
     * @param $page   页码
     */
    private function paintBranchNameOnA4($canvas, $page)
    {
        $config = [];

        if ($page % 2 == 0) {
            $config = $this->config['even'];
        } else {
            $config = $this->config['odd'];
        }

        $branchNameX = $config['branchNameX'];
        $branchNameY = $config['branchNameY'];
        $branchNameFontSize = $config['branchNameFontSize'];
        $branchNameFontMargin = $config['branchNameFontMargin'];
//        var_dump();

        $fontSize = $branchNameFontSize;
        if (!array_key_exists($page, $GLOBALS['branchName'])){
            return;
        }

        $branchNameText = $GLOBALS['branchName'][$page];
        print_r([533, $branchNameText, $page]);
        $x = $branchNameX;
        $y = $branchNameY;
        $len = mb_strlen($branchNameText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($branchNameText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $branchNameFontMargin + $fontSize;
        }
    }


    /**
     * 清除一些全局变量，例如支派名（branchName）
     */
    public function destoryGlobals(){
        if (array_key_exists('branchName', $GLOBALS)){
            unset($GLOBALS['branchName']);
        }
    }
}