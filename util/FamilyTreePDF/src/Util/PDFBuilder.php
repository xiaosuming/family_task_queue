<?php

/**
 * 负责将半页半页的族谱组装成pdf
 * @Author: jiangpengfei
 * @Date: 2019-01-10
 */

namespace FamilyTreePDF\Util;

use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Paint\PaintUtil;
use FamilyTreePDF\Util\PDFBuilderConfig;
use FamilyTreePDF\Util\ImageAddCover;
use FamilyTreePDF\Util\Pinyin;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

class PDFBuilder
{

    private $direction;
    private $config;
    private $font;
    private $titleFont;
    private $resourcePath;
    private $outPath;
    private $isHD;

    /**
     * @param $direction 布局方向
     * @param $isHD 是否高清
     */
    public function __construct($direction, $isHD = false)
    {
        $this->direction = $direction;
        $this->font = __DIR__ . '/../res/font/1.TTC';
        $this->lishufont = __DIR__ . '/../res/font/lishu.TTF';
        // $this->titleFont = __DIR__.'/../res/font/pangmenzhengdao.ttf';
        $this->titleFont = __DIR__.'/../res/font/SIMKAI.TTF';
        $this->resourcePath = __DIR__ . '/../res/background';
        $this->outPath = __DIR__ . '/../output';

        $this->startcover = __DIR__ . '/../output/cover.png';
        $this->endcover = '';
        $this->isHD = $isHD;
        if ($isHD) {
            $this->config = PDFBuilderConfig::getHDConfig();
        } else {
            $this->config = PDFBuilderConfig::getNormalConfig();
        }
        $this->defaultPageLength =  $this->config['defaultPageLength'];
    }

    /**
     * 绘制族谱的标题
     * @param $canvas 画布
     * @param $title  标题文字
     */
    private function paintTitle($canvas, $title)
    {
        $maxLength = $this->config['titleMaxLength'];
        $backImgPath = $this->resourcePath .$this->config['allblankImgPath'];
        $res = $this->caculateText($title, $this->config, $backImgPath, $this->titleFont);
        if($res == false){
            return false;
        }
        $fontSize = $res['fontSize'];
        $titleTextYDistance = $res['TextYDistance'];
        $titleY = $res['Y'];
        $titleX = $res['X'];

        $x = $titleX;
        $y = $titleY;
        $len = mb_strlen($title);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($title, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->titleFont, $c);
            $y = $y + $titleTextYDistance + $fontSize;
        }
    }

    /**
     * 计算文字的数量，返回字体大小 字体间隔 Y偏移量 X偏移量
     * @param $text 文字
     * @param $pageConfig
     * @param $backImgPath
     * @param $font
     * @return arr
     */
    private function caculateText($text, $pageConfig, $backImgPath, $font)
    {
        $textLength = mb_strlen($text); //字体字数

        if($textLength == 0){

            return false;
        }
        $res = [
            'fontSize' => '',
            'TextYDistance' => '',
            'Y' => '',
            'X' => '',
        ];
        $maxFont = $pageConfig['titleFontSize']; //最大字体
        $maxLength = $pageConfig['titleMaxLength']; //字域最宽
        $caculateFont = floor(($maxLength/$textLength)*(3/4));
        $res['fontSize'] = ($caculateFont > $maxFont) ? $maxFont : $caculateFont;//字体大小
        $res['TextYDistance'] = floor($res['fontSize']/3);//字体间隔

        list($Width, $Height, $Type) = getimagesize($backImgPath);
        $fontBox = imagettfbbox($res['fontSize'], 0, $font, mb_substr($text, 0, 1));
        $res['X'] = ceil(($Width - $fontBox[2] + $fontBox[0])/2);//X偏移

        if($caculateFont < $maxFont){
            $res['Y'] = $pageConfig['titleY'];
            if($textLength < 10){
                $res['Y'] = $res['Y'] + (10 - $textLength) * 0.03 * $pageConfig['titleY'];//加入一定校准
            }
        }else{
            $textCountLength = ($textLength-1) * $res['fontSize'] + ($textLength - 1) * $res['TextYDistance'];
            $res['Y'] = ceil(($maxLength - $textCountLength)/2) + $pageConfig['titleY'];//Y偏移量

        }

        return $res;
    }

    /**
     * 绘制页码
     * @param $canvas 画布
     * @param $page   页数
     */
    private function paintChildName($canvas, $page, $ChildName)
    {
        $config = $this->config;
        $ChildNameNumLeftX = $config['ChildNameNumLeftX'];
        $ChildNameNumRightX = $config['ChildNameNumRightX'];
        $ChildNameNumY = $config['ChildNameNumY'];
        $ChildNameFontSize = $config['ChildNameFontSize'];
        $ChildNameFontMargin = $config['ChildNameFontMargin'];

        $fontSize = $ChildNameFontSize;

        /*if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        } else {
            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        }*/
        $pageText = $ChildName;
        $x = $ChildNameNumLeftX;
        $y = $ChildNameNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $ChildNameFontMargin + $fontSize;
        }

        /*if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        } else {
            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        }*/
        $x = $ChildNameNumRightX;
        $y = $ChildNameNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $ChildNameFontMargin + $fontSize;
        }
    }

    /**
     * 绘制页码
     * @param $canvas 画布
     * @param $page   页数
     */
    private function paintPage($canvas, $page)
    {
        $config = $this->config;
        $pageNumLeftX = $config['pageNumLeftX'];
        $pageNumRightX = $config['pageNumRightX'];
        $pageNumY = $config['pageNumY'];
        $pageFontSize = $config['pageFontSize'];
        $pageFontMargin = $config['pageFontMargin'];

        $fontSize = $pageFontSize;

        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        } else {
            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        }
        $x = $pageNumLeftX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }

        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        } else {
            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        }
        $x = $pageNumRightX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }
    }

    /**
     * 合并半页的图片
     * @param 族谱的标题
     * @param string $ChildName
     * @param string $branchFamilyName
     * @param int $blankImg
     * @return int
     */
    public function merge($titleText, $ChildName = '',$branchFamilyName='',$blankImg  = 1,$hallName='')
    {

        $pageWidth = $this->config['pageWidth'];
        $page = 0;
        while (true) {
            $outputPageIndex = ($page / 2 + 1);
            if (SysConst::$DEBUG) {
                $time = date('h:i:s');
                echo "合并{$outputPageIndex}页, {$time}" . PHP_EOL;
            }

            switch ($blankImg){
                case 1:$blankImgRes = $this->config['allblankImgPath'];
                    $dstCanvas = imagecreatefrompng($this->resourcePath . $blankImgRes);
                    $this->paintTitle($dstCanvas, $titleText);
                    $this->paintPage($dstCanvas, $outputPageIndex);
                    if($ChildName != ''){
                        $this->paintChildName($dstCanvas, $outputPageIndex, $ChildName);
                    }
                    $this->paintBranchFamilyName($dstCanvas,$branchFamilyName);

                    break;
                case 2:
                    if ($this->isHD) {
                        $this->config = PDFBuilderConfig::getHDConfig2();
                    } else {
                        $this->config = PDFBuilderConfig::getConfig2();
                    }
                    $blankImgRes = $this->config['allblankImgPath2'];
                    $dstCanvas = imagecreatefrompng($this->resourcePath . $blankImgRes);
                    $this->paintMiddleText($dstCanvas,$titleText,$ChildName,$hallName);
//                    $this->paintBranchFamilyName2($dstCanvas,$branchFamilyName);
                    if (array_key_exists('branchName', $GLOBALS)){
                        $this->paintBranchFamilyName2($dstCanvas, $outputPageIndex, $branchFamilyName);
                    }
                    $this->paintPage2($dstCanvas, $outputPageIndex);
                    break;

                default: $blankImgRes = $this->config['allblankImgPath'];
                    $dstCanvas = imagecreatefrompng($this->resourcePath . $blankImgRes);
                    if($ChildName != ''){
                        $this->paintChildName($dstCanvas, $outputPageIndex, $ChildName);
                    }
                    $this->paintTitle($dstCanvas, $titleText);
                    $this->paintPage($dstCanvas, $outputPageIndex);
                    $this->paintBranchFamilyName($dstCanvas,$branchFamilyName);


            }



            if ($this->direction == 1) {
                // 从右往左，顺序颠倒
                $filepath = $this->outPath . "/halfoutput/output" . ($page + 1) . ".png";
            } else {
                $filepath = $this->outPath . "/halfoutput/output{$page}.png";
            }
            if (file_exists($filepath)) {
                // 复制第一页
                $size = getimagesize($filepath);
                $width = $size[0];
                $height = $size[1];


                $srcCanvas = imagecreatefrompng($filepath);
                imagecopy($dstCanvas, $srcCanvas, $this->config['pageOneXOffset'], $this->config['pageOneYOffset'], 0, 0, $width, $height);
                imagedestroy($srcCanvas);
            } else {
                imagepng($dstCanvas, $this->outPath . "/mergeoutput/output{$outputPageIndex}.png");
            }

            if ($this->direction == 1) {
                // 从右往左，顺序颠倒
                $filepath = $this->outPath . "/halfoutput/output" . ($page) . ".png";
            } else {
                $filepath = $this->outPath . "/halfoutput/output" . ($page + 1) . ".png";
            }
            if (file_exists($filepath)) {
                // 复制第二页
                $size = getimagesize($filepath);
                $width = $size[0];
                $height = $size[1];

                $srcCanvas = imagecreatefrompng($filepath);
                imagecopy($dstCanvas, $srcCanvas, $this->config['pageTwoXOffset'], $this->config['pageTwoYOffset'], 0, 0, $width, $height);
                imagedestroy($srcCanvas);
            } else {
                imagepng($dstCanvas, $this->outPath . "/mergeoutput/output{$outputPageIndex}.png");
                break;
            }

            imagepng($dstCanvas, $this->outPath . "/mergeoutput/output{$outputPageIndex}.png");
            imagedestroy($dstCanvas);
            $page += 2;
        }
        return $page;
    }

    /**
     *  生成封面图
     *  判断参数是否合法
     * @param $surname 姓氏
     * @param string $coverId 封面id 1 蓝色 2 白色 3 红色
     * @param string $pinyin 姓氏的拼音
     * @param array $arr
     * @return bool true/false
     *  组装参数
     */
    public function toCover($surname, $coverId = '1', $pinyin = '', $arr = [])
    {

        if(mb_strlen($surname) == 0 || mb_strlen($surname) > 2){
            return false;
        }
        if(isset($pinyin)){
            if(strlen($pinyin) > 12){
                return false;
            }
        }else{
            $py = new Pinyin();
            $pinyin = $py->getpy($surname, true);
            if(strlen($pinyin) == 0){
                return false;
            }
        }
        $params = [
            'id' => $coverId,
            'surname' => $surname,
            'pinyin' => $pinyin,
            'direction' => $this->direction // 布局： 0 从左到右 ，1 从右到左
            ];

        switch ($coverId) {
            case '1':
                $this->endcover = $this->resourcePath . '/endcover/endcover-blue.png';
                break;
            case '2':
                $this->endcover = $this->resourcePath . '/endcover/endcover-white.png';
                break;
            case '3':
                $this->endcover = $this->resourcePath . '/endcover/endcover-red.png';
                if(isset($arr['ChildName'])){
                    $params['ChildName'] = $arr['ChildName'];
                }
            default:
                break;
        }


        $ImageAddCover = new ImageAddCover();
        return $ImageAddCover->getCoverImage($params);
        //生成的封面图路径  __DIR__.'/../output/cover.png' = $this->outPath . '/cover.png';
    }

    /**
     * 给pdf分页
     * @param int $TotalPage 总页数
     * @param int $PageLength 每份PDF的页数
     * @return false|float $countPDF int
     */
    public function  toPdf($TotalPage, $PageLength = 0)
    {
        if($PageLength < 1){
        // if($PageLength < $this->defaultPageLength){
            $PageLength = $this->defaultPageLength;
        }
        $countPDF = ceil($TotalPage/$PageLength);
        for($i = 0; $i< $countPDF; $i ++){
            $this->cutPdf($i, $PageLength );
        }
        //清除多余的pdf
        $otherNum = $countPDF;
        while (true) {

            $preview_filepath = $this->outPath . "/pdfoutput/preview_".$otherNum.".pdf";
            $print_filepath = $this->outPath . "/pdfoutput/print_".$otherNum.".pdf";

            if (file_exists($preview_filepath) || file_exists($print_filepath)) {
                if(file_exists($preview_filepath)){
                    unlink($preview_filepath);
                }
                if(file_exists($print_filepath)){
                    unlink($print_filepath);
                }
            } else {
                break;
            }
            $otherNum++;
        }

        return $countPDF;

    }

    /**
     * 将merge后的图片转换为pdf
     * @param int $current
     * @param int $PageLength
     * @throws \Mpdf\MpdfException
     */
    public function cutPdf($current = 0, $PageLength = 100)
    {
        $PageLength = ceil($PageLength/2); //A3是两页在一起，所以需要除2取上
        $startPage = $current * ($PageLength) + 1;
        /**************生产预览版***************/
        $mpdf = new Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $html = '';

        //添加首页封面图
        if(file_exists($this->startcover)){
            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
        }
        //end

        $page = $startPage;
        while ($page < $startPage+$PageLength) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $html .= "<img style='width:100%' src='" . $filepath . "'>";
            $page++;
        }

        //添加尾页封面图
        if(file_exists($this->endcover)){
            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
        }
        //end

        $mpdf->WriteHTML($html);
        $mpdf->Output($this->outPath . "/pdfoutput/preview_".$current.".pdf", Destination::FILE);

        /**************生产打印版***************/

        $mpdf = new Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $html = '';

        //添加首页封面图
        if(file_exists($this->startcover)){
            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
        }
        //end

        $page = $startPage;
        while ($page < $startPage+$PageLength) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (!file_exists($filepath)) {
                break;
            }

            $source = imagecreatefrompng($filepath);
            $rotate = imagerotate($source, 90, 0);

            $tmpFile = $this->outPath . "/pdfoutput/tmp/tmp" . $page . ".png";
            imagepng($rotate, $tmpFile);

            $html .= "<img style='width:100%' src='" . $tmpFile . "'>";
            $page++;
        }

        //添加尾页封面图
        if(file_exists($this->endcover)){
            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
        }
        //end

        $mpdf->WriteHTML($html);
        $mpdf->Output($this->outPath . "/pdfoutput/print_".$current.".pdf", Destination::FILE);
    }

    /**
     * 清除中间文件
     */
    public function clear()
    {
        $page = 0;
        while (true) {

            $filepath = $this->outPath . "/halfoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = $this->outPath . "/mergeoutput/output" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        $page = 1;
        while (true) {

            $filepath = $this->outPath . "/pdfoutput/tmp/tmp" . $page . ".png";
            if (file_exists($filepath)) {
                unlink($filepath);
            } else {
                break;
            }
            $page++;
        }

        //清除封面文件
        if(file_exists($this->startcover)){
            unlink($this->startcover);
        }
        //end

    }

    /**
     * 直接生成Pdf,不需要页数
     * @param startPage  开始页数
     * @param totalPages  总页数
     */
    public function toLevelPdf($startPage = 0,$totalPages)
    {

        /**************生产预览版***************/
        $mpdf = new Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $html = '';
        //添加首页封面图
        if(file_exists($this->startcover)){
            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
        }
        //end

        $page = $startPage;
        while ($page < $totalPages) {

            $filepath = $this->outPath.'/' . $page . ".png";

            if (!file_exists($filepath)) {
                break;
            }

            $html .= "<img style='width:100%' src='" . $filepath . "'>";
            $page++;


        }
        //添加尾页封面图
        if(file_exists($this->endcover)){
            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
        }
        //end
        $mpdf->WriteHTML($html);
        $mpdf->Output($this->outPath . "/pdfoutput/preview_".$startPage.".pdf", Destination::FILE);
        $mpdf->Output($this->outPath . "/pdfoutput/print_".$startPage.".pdf", Destination::FILE);

//        /**************生产打印版***************/
//
//        $mpdf = new Mpdf([
//            'margin_left' => 0,
//            'margin_right' => 0,
//            'margin_top' => 0,
//            'margin_bottom' => 0,
//            'margin_header' => 0,
//            'margin_footer' => 0,
//        ]);
//
//        $html = '';
//
//        //添加首页封面图
//        if(file_exists($this->startcover)){
//            $html .= "<img style='width:100%' src='" . $this->startcover . "'>";
//        }
//        //end
//
//        $page = $startPage;
//        while ($page <=  $totalPages) {
//            $filepath = $this->outPath.'/' . $page . ".png";
//            if (!file_exists($filepath)) {
//                break;
//            }
//            $source = imagecreatefrompng($filepath);
//            $rotate = imagerotate($source, 90, 0);
//            $tmpFile = $this->outPath . "/pdfoutput/tmp". $page . ".png";
//            imagepng($rotate, $tmpFile);
//
//            $html .= "<img style='width:100%' src='" . $tmpFile . "'>";
//            $page++;
//
//        }
//        //添加尾页封面图
//        if(file_exists($this->endcover)){
//            $html .= "<img style='width:100%' src='" . $this->endcover . "'>";
//        }
//        //end
//
//        $mpdf->WriteHTML($html);
//        $mpdf->Output($this->outPath . "/pdfoutput/print_".$startPage.".pdf", Destination::FILE);




    }

    /**
     * 绘制BranchFamilyName
     * @param $canvas
     * @param $branchFamilyName
     */
    private function paintBranchFamilyName($canvas, $branchFamilyName)
    {
        $config = $this->config;
        $pageNumLeftX = $config['pageNumLeftX'];
        $pageNumRightX = $config['pageNumRightX'];
        $pageNumY = $config['pageNumY'];
        $pageFontSize = $config['pageFontSize'];
        $pageFontMargin = $config['pageFontMargin'];

        $fontSize = $pageFontSize;

        $bbox = imageftbbox($fontSize,0,$this->font,$branchFamilyName);
        $fontHeight = $bbox[2] - $bbox[0];

        $pageText = $branchFamilyName;

        $x = $pageNumLeftX;
        $y = $pageNumY - $fontHeight - 50;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }


        $x = $pageNumRightX;
        $y = $pageNumY - $fontHeight - 50;

        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }
    }

    /**
     * 绘制中缝
     * @param $canvas "画布"
     * @param $title "标题文字"
     * @param $childName
     * @param $hallName
     */
    private function paintMiddleText($canvas, $title ,$childName, $hallName ){
        $this->paintTitle($canvas,$title);
        if ($hallName!=''){
            $this->paintHallName($canvas,$hallName);
        }
    }

    /**
     * 绘制族谱的标题
     * @param $canvas "画布"
     * @param $hallName
     * @return false
     */
    private function paintHallName($canvas, $hallName)
    {
        $backImgPath = $this->resourcePath .$this->config['allblankImgPath'];

        $textLength = mb_strlen($hallName); //字体字数

        $maxFont = $this->config['hallNameFontSize']; //最大字体
        $maxLength = $this->config['hallNameMaxLength']; //字域最宽
        $caculateFont = floor(($maxLength/$textLength)*(3/4));
        $res['fontSize'] = ($caculateFont > $maxFont) ? $maxFont : $caculateFont;//字体大小
        $res['TextYDistance'] = floor($res['fontSize']/3);//字体间隔
        list($Width, $Height, $Type) = getimagesize($backImgPath);
        $fontBox = imagettfbbox($res['fontSize'], 0, $this->titleFont, mb_substr($hallName, 0, 1));
        $res['X'] = ceil(($Width - $fontBox[2] + $fontBox[0])/2);//X偏移
        $fontBox = imagettfbbox($res['fontSize'],0,$this->titleFont,$hallName);
        $res['Y'] = imagesy($canvas) - $fontBox[2] + $fontBox[0]-$this->config['bottomDistance'];

        $fontSize = $res['fontSize'];
        $titleTextYDistance = $res['TextYDistance'];
        $titleY = $res['Y'];
        $titleX = $res['X'];

        $x = $titleX;
        $y = $titleY;
        $len = mb_strlen($hallName);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($hallName, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y , 0x000000, $this->titleFont, $c);
            $y = $y + $titleTextYDistance + $fontSize;
        }
    }


    /**
     * 绘制页码
     * @param $canvas "画布"
     * @param $page   "页数"
     */
    private function paintPage2($canvas, $page)
    {

        $config = $this->config;
        $pageNumLeftX = $config['pageNumLeftX'];
        $pageNumRightX = $config['pageNumRightX'];
        $pageNumY = $config['pageNumY'];
        $pageFontSize = $config['pageFontSize'];
        $pageFontMargin = $config['pageFontMargin'];

        $fontSize = $pageFontSize;
//        if (array_key_exists('branchName', $GLOBALS)){
//            $branchNames = $GLOBALS['branchName'];
//            $descStartPage = min(array_keys($branchNames));
//            $tempPage = $tempPage < $descStartPage ? $tempPage : $tempPage - $descStartPage + 15;
//            var_dump($page);
//        }
        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = "页";
            $tempPage = $page*2;
            while ($tempPage>0){
                $pageText = PaintUtil::number2chinese($tempPage%10).$pageText;

                $tempPage = floor($tempPage/10);
            }
//            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        } else {
            $pageText = "页";
            $tempPage = $page*2-1;
            while ($tempPage>0){
                $pageText = PaintUtil::number2chinese($tempPage%10).$pageText;
                $tempPage = floor($tempPage/10);
            }
//            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        }
        $x = $pageNumLeftX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y + $pageFontMargin + $fontSize;
        }

        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            $pageText = "页";
            $tempPage = $page * 2 - 1;

            while ($tempPage>0){
                $pageText = PaintUtil::number2chinese($tempPage%10).$pageText;
                $tempPage = floor($tempPage/10);

            }
//            $pageText = PaintUtil::number2chinese($page * 2 - 1) . "页";
        } else {
            $pageText = "页";
            $tempPage = $page * 2;
            while ($tempPage>0){
                $pageText = PaintUtil::number2chinese($tempPage%10).$pageText;
                $tempPage = floor($tempPage/10);
            }
//            $pageText = PaintUtil::number2chinese($page * 2) . "页";
        }
        $x = $pageNumRightX;
        $y = $pageNumY;
        $len = mb_strlen($pageText);
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);

            $y = $y + $pageFontMargin + $fontSize;
        }
    }

    /**
     * 绘制BranchFamilyName
     * @param $canvas
     * @param $branchFamilyName
     */
    private function paintBranchFamilyName2($canvas, $page, $branchFamilyName='')
    {
        $config = $this->config;
        $pageNumLeftX = $config['branchFamilyNameNumLeftX'];
        $pageNumRightX = $config['branchFamilyNameNumRightX'];
        $pageNumY = $config['branchFamilyNameNumY'];
        $pageFontSize = $config['branchFamilyNameFontSize'];
        $pageFontMargin = $config['branchFamilyNameMargin'];

        $fontSize = $pageFontSize;
//        print_r([880, $page]);
        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            if (!array_key_exists($page *  2, $GLOBALS['branchName'])){
                $pageText = $branchFamilyName;
            }else {
                $pageText = $GLOBALS['branchName'][$page * 2];
//                print_r([863,$page * 2, $pageText]);
            }
        } else {
            if (!array_key_exists($page *  2 - 1, $GLOBALS['branchName'])){
                $pageText = $branchFamilyName;
            }else{
                $pageText = $GLOBALS['branchName'][$page * 2 - 1];
//                print_r([870,$page * 2 - 1, $pageText]);
            }

        }
//        print_r([$page, $pageText]);
        $len = mb_strlen($pageText);

        $bbox = imageftbbox($fontSize,0,$this->font, $pageText);
        $fontHeight = ($bbox[2] - $bbox[0])+($len-1)*$pageFontMargin;


        if ($pageNumY+$fontHeight>$this->config['pageNumY']){
            $fontSize = ($this->config['pageNumY']-$pageNumY)*$fontSize/$fontHeight;

        }

        $y = $pageNumY;

        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            $x = $pageNumLeftX;
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y  + $fontSize + $pageFontMargin;

        }

        $fontSize = $pageFontSize;
        if ($this->direction == 1) {
            // 从右向左， 位置颠倒
            if (!array_key_exists($page *  2 - 1, $GLOBALS['branchName'])){
                $pageText = $branchFamilyName;
            }else {
                if(array_key_exists($page *  2, $GLOBALS['branchName'])){
                    $pageText = $GLOBALS['branchName'][$page * 2-1];
                }else{
                    $pageText = $GLOBALS['branchName'][$page * 2 - 1];
                }
//                print_r([904,$page * 2 - 1, $pageText]);
            }
        } else {
            if (!array_key_exists($page *  2, $GLOBALS['branchName'])){
                $pageText = $branchFamilyName;
            }else{
                $pageText = $GLOBALS['branchName'][$page * 2];
//                print_r([907,$page * 2 - 1, $pageText]);
            }

        }
        $len = mb_strlen($pageText);

        $bbox = imageftbbox($fontSize,0,$this->font, $pageText);
        $fontHeight = ($bbox[2] - $bbox[0])+($len-1)*$pageFontMargin;


        if ($pageNumY+$fontHeight>$this->config['pageNumY']){

            $fontSize = ($this->config['pageNumY']-$pageNumY)*$fontSize/$fontHeight;


        }
        $y = $pageNumY;


        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($pageText, $i, 1);
            $x = $pageNumRightX;
            imagettftext($canvas, $fontSize, 0, $x, $y, 0x000000, $this->lishufont, $c);
            $y = $y  + $fontSize + $pageFontMargin;

        }


    }

    /**
     * 清除一些全局变量，例如支派名（branchName）
     */
    public function destoryGlobals(){
        if (array_key_exists('branchName', $GLOBALS)){
            unset($GLOBALS['branchName']);
        }
    }
}
