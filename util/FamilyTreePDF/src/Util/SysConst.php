<?php
/**
 * 一些常量放在这里
 * @Author: jiangpengfei
 * @Date:   2019-01-10
 */

namespace FamilyTreePDF\Util;

class SysConst {
    public static $DEBUG = true;
    public static $LEFT_TO_RIGHT = 0;
    public static $RIGHT_TO_LEFT = 1;
}