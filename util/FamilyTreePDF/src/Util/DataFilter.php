<?php
/**
 * 数据过滤器类，主要负责从所有的人物集合中获取需要的分支
 * @Author: jiangpengfei
 * @Date: 2018-09-25
 */

namespace FamilyTreePDF\Util;

class DataFilter {

    private $personSet;         // 人物的集合
    private $personMap;         // id和人物的对应
    private $startPersonId;     // 分支的起始点, 有这个值的时候，是用户选择导出部分分支
    private $startPersonIdArray;  // 在用户选择全部导出时，会有个起始点的数组
    private $isShowDaughterOffspring;   // 是否显示外戚
    private $isShowStartPersonFathers;   // 是否显示分支的起始点的上游

    public function __construct(array $persons)
    {
        $this->startPersonId = 0;
        $this->startPersonIdArray = [];
        $this->personSet = $persons;
        
        $minLevel = 99999;
        foreach($persons as $person) {
            $this->personMap[$person['id']] = $person;
            if ($person['level'] < $minLevel) {
                // 辈分更高
                $this->startPersonIdArray = [];
                if($person['type'] == 1){//去除配偶
                    $this->startPersonIdArray[] = $person['id'];
                }
                $minLevel = $person['level'];
            } else if ($person['level'] == $minLevel) {
                // 辈分相等
                if($person['type'] == 1){//去除配偶
                    $this->startPersonIdArray[] = $person['id'];
                }
                $minLevel = $person['level'];
            }
        }
    }

    /**
     * 设置配置项，根据配置项进行数据的过滤
     * @param array $options ["isShowDaughterOffspring" => 0]
     */
    public function setOptions($options) {
        $this->isShowDaughterOffspring = $options['isShowDaughterOffspring'] ?? 1;
        $this->isShowStartPersonFathers = $options['isShowStartPersonFathers'] ?? 0;
    }

    public function setStartPersonId(int $startPersonId)
    {
        $this->startPersonId = $startPersonId;
    }

    private function pushValidPerson($corePerson, &$validPersonSet) {

        // 不显示配偶
        $corePerson['spouse'] = [];

        // 检查是否显示外戚
        if ($this->isShowDaughterOffspring == 0 && $corePerson['gender'] == 0) {
            // 不显示外戚
            $corePerson['son'] = [];
            $corePerson['daughter'] = [];
        }
        array_push($validPersonSet, $corePerson);


        $sonIds = $corePerson['son'];
        foreach($sonIds as $sonId) {
            if (isset($this->personMap[$sonId])) {
                $this->pushValidPerson($this->personMap[$sonId], $validPersonSet);
            }
        }

        $daughterIds = $corePerson['daughter'];
        foreach($daughterIds as $daughterId) {
            if (isset($this->personMap[$daughterId])) {
                $this->pushValidPerson($this->personMap[$daughterId], $validPersonSet);
            }
        }
      
    }

    public function getPersonSet() : array {

        if ($this->startPersonId != 0) {
            // 一个分支的导出
            $startPerson = $this->personMap[$this->startPersonId];
            $validPersonSet = array();
            
            if($this->isShowStartPersonFathers == 1){
                $startPerson['isMain'] = 1;
                $this->personMap[$this->startPersonId]['isMain'] = 1;
                $this->personMap[$this->startPersonId]['ranking'] = -1;
                $this->linkFathersToPerson($startPerson, $validPersonSet);
            }else{
                $startPerson['father'] = [];
                $startPerson['mother'] = [];
            }
            $startPerson['sister'] = [];
            $startPerson['brother'] = [];

            $this->pushValidPerson($startPerson, $validPersonSet);

            return $validPersonSet;
        } else {
            // 全部导出
            $tmp = [];
            foreach($this->startPersonIdArray as $startPersonId) {
                $startPerson = $this->personMap[$startPersonId];

                $validPersonSet = array();

                $this->pushValidPerson($startPerson, $validPersonSet);
                $tmp = array_merge($tmp, $validPersonSet);

                //return $validPersonSet;
            }
            return $tmp;            
        }
    }

    private function linkFathersToPerson($corePerson, &$validPersonSet){
        //过滤兄弟姐妹的儿女
        $tmpBrother = array_merge($this->personMap[$corePerson['id']]['brother'], $this->personMap[$corePerson['id']]['sister']);
        foreach($tmpBrother as $brotherId){
            if($brotherId == $corePerson['id']){
                continue;
            }
            $tmpPerson = $this->personMap[$brotherId];

            $tmpPerson['son'] = [];
            $tmpPerson['daughter'] = [];
            $tmpPerson['spouse'] = [];

            array_push($validPersonSet, $tmpPerson);
        }

        $parents = array_merge($this->personMap[$corePerson['id']]['father'], $this->personMap[$corePerson['id']]['mother']);
        //对父母的其他儿女进行过滤
        foreach($parents as $parentId){
            if($this->personMap[$parentId]['type'] == 1){                
                //父母过滤 需要递归
                $this->personMap[$parentId]['isMain'] = 1;
                //$this->personMap[$parentId]['ranking'] = -1;

                $tmp = $this->personMap[$parentId];
                array_push($validPersonSet, $tmp);
                $this->linkFathersToPerson($tmp, $validPersonSet);
            }else{
                array_push($validPersonSet, $this->personMap[$parentId]);
            }

        }

    }


    /*
     *
     * 按支派名导出，数据预处理
     * $branchPersonMap【支派名】【id】
     *
     */
    public function preInput($persons){

        $branchPersonMap=array();

        $minLevel = PHP_INT_MAX;

        $personMap[] = array();

        foreach ($persons as $key => $person) {
            $branchName = $person['branchName'];
            //echo $branchName."\n";
            $personMap[$person['id']] = $person;
            $branchPersonMap[$branchName][$person['id']] = $person;

            $level = $person['level'];
            if(!isset( $minLevelList[$branchName])){
                $minLevelList[$branchName]=$minLevel;
            }

            if ($level < $minLevel) {
                if($level < $minLevelList[$branchName]){
                    $minLevelList[$branchName] = $level;
                    $rootPerson [$branchName]= $person;
                }
            }
        }


        foreach ( $rootPerson as $k1=>$v2) {
            $person1 = $v2;

            if(count($person1['father']) == 1){
                //echo $person1['father'][0]."\n";
                $father1= $personMap[$person1['father'][0]];
                $son1 =array("0"=>$person1['id']);

                $father1['son']=$son1;
                $fatherbn=$father1['branchName'];

                $son2 =array();
                $person1['son']=$son2;
                $branchPersonMap[$fatherbn][$v2['id']] = $person1;

                $father1['branchName']=$person1['branchName'];
                $father1['father']=array();

                $branchPersonMap[$k1][$father1['id']] = $father1;


            }
            if (count($person1['father']) == 0){
                //添加一个虚拟的父亲节点
                $father1= $personMap[$person1['id']];
                $son1 =array("0"=>$person1['id']);
                $father1['son']=$son1;
                $father1['father']=array();
                $father1['level']=$father1['level']-1;
                $father1['ranking']=-1;
                $father1['id']=$father1['id']-1;

                $branchPersonMap[$k1][$father1['id']] = $father1;

                $person= $personMap[$person1['id']];
                $father =array("0"=>$father1['id']);
                $person['father']=$father;

                $branchPersonMap[$k1][$person['id']] = $person;


            }
        }
        return $branchPersonMap;
    }






}