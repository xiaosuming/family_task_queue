<?php
/**
 * 大事件绘制的工厂模式
 * Author: jiangpengfei
 * Date:   2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuBigEventPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuBigEventPaint;

class BigEventPaintFactory
{

    public static function getBigEventPaint($type, $direction, $isHD)
    {
        $bigEventPaint = null;
        switch ($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $bigEventPaint = new TraditionalSuBigEventPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $bigEventPaint = new A4TraditionalOuBigEventPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            default:
                $bigEventPaint = new TraditionalSuBigEventPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
        }

        $bigEventPaint->setConfig($config);
        return $bigEventPaint;
    }
}
