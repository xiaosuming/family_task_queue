<?php
/**
 * 吊线图的工厂模式
 * Author: jiangpengfei
 * Date:  2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuTreePaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuTreePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuTreePaint;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\NewPaint\NewPaintByLevelWithIndentation;
use FamilyTreePDF\Paint\NewPaint\NewPaintByLevel;


class TreePaintFactory
{

    /**
     * 获取tree type来获取吊线图绘制的实例
     * @param $type
     * @param $direction
     * @param $isHD
     * @return A4TraditionalOuTreePaint|NewPaintByLevel|NewPaintByLevelWithIndentation|TraditionalOuTreePaint|TraditionalSuTreePaint
     */
    public static function getTreePaint($type, $direction, $isHD)
    {
        $treePaint = null;
        $config = null;

        switch ($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $treePaint = new TraditionalSuTreePaint();

                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
            case StyleConst::$PAINT_STYLE_TRADITIONAL_OU:
                $treePaint = new TraditionalOuTreePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $treePaint = new A4TraditionalOuTreePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
                /**版本3 层级样式 */
            case StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL:
                $treePaint = new NewPaintByLevel();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getR2LConfig();
                    }
                }
                break;
                /**版本3 层级样式 */
            case StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL_WithIndentation:
                $treePaint = new NewPaintByLevelWithIndentation();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\NewPaint\PaintConfig::getR2LConfig();
                    }
                }
                break;


            default:
                $treePaint = new TraditionalSuTreePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
        }

        $treePaint->setConfig($config);

        return $treePaint;
    }
}
