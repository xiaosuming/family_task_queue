<?php
/**
 * 描述绘制工厂
 * Author: jiangpengfei
 * Date:   2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuDescPaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint2;
use FamilyTreePDF\Paint\TraditionalOuTemplate\TraditionalOuDescPaint3;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuDescPaint;
use FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\A4TraditionalOuPhotoDescPaint;
use FamilyTreePDF\Util\SysConst;

class DescPaintFactory
{

    public static function getDescPaint($type, $direction, $isHD)
    {
        $descPaint = null;
        $config = null;

        switch ($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $descPaint = new TraditionalSuDescPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_OU:
                $descPaint = new TraditionalOuDescPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $descPaint = new A4TraditionalOuDescPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU_PHOTO:
                $descPaint = new A4TraditionalOuPhotoDescPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuPhotoTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;


            case StyleConst::$PAINT_STYLE_TRADITIONAL_OU2:
                $descPaint = new TraditionalOuDescPaint2();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDL2RConfigForDesc2();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getL2RConfigForDesc2();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDR2LConfigForDesc2();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getR2LConfigForDesc2();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_OU3:
                $descPaint = new TraditionalOuDescPaint3();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            default:
                $descPaint = new TraditionalSuDescPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
        }

        $descPaint->setConfig($config);
        return $descPaint;
    }
}
