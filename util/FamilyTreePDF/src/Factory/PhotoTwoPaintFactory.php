<?php
/**
 * 两张图片的相册工厂模式
 * Author: jiangpengfei
 * Date:   2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPhotoTwoPaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoTwoPaint;
use FamilyTreePDF\Util\SysConst;

class PhotoTwoPaintFactory {

    /**
     * 获取相册2绘制
     */
    public static function getPhotoTwoPaint($type, $direction, $isHD) {
        $photoTwoPaint = null;
        $config = null;

        switch($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $photoTwoPaint = new TraditionalSuPhotoTwoPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $photoTwoPaint = new A4TraditionalOuPhotoTwoPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;


            default:
                $photoTwoPaint = new TraditionalSuPhotoTwoPaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
        }

        $photoTwoPaint->setConfig($config);
        return $photoTwoPaint;
    }
}