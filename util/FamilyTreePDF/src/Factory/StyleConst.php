<?php
/**
 * 模板绘制的风格
 * @author: jiangpengfei
 * @date:   2019-05-28
 */

namespace FamilyTreePDF\Factory;

class StyleConst {
    public static $PAINT_STYLE_TRADITIONAL_SU = 1;
    public static $PAINT_STYLE_TRADITIONAL_OU = 2;
    public static $PAINT_STYLE_TRADITIONAL_A4_OU = 3;
    public static $PAINT_STYLE_TRADITIONAL_A4_OU_PHOTO = 4;
    public static $PAINT_STYLE_TRADITIONAL_OU2 = 5;   #新增显示三代的样式
    public static $PAINT_STYLE_TRADITIONAL_LEVEL = 6;
    public static $PAINT_STYLE_TRADITIONAL_OU3 = 7;   #新增曰赞与支派的样式
    public static $PAINT_STYLE_TRADITIONAL_LEVEL_WithIndentation = 8;

    public static $PAINT_STYLE_TRADITIONAL_LEVEL7 = 9;  #新增branch支派的样式

    public static $PAINT_PAGESIZE_A3 = 1;
    public static $PAINT_PAGESIZE_A4 = 2;
}
