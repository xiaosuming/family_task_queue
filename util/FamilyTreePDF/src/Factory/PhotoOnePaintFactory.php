<?php
/**
 * 单张相册工厂模式
 * Author: jiangpengfei
 * Date:   2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuPhotoOnePaint;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuPhotoOnePaint;
use FamilyTreePDF\Util\SysConst;

class PhotoOnePaintFactory
{
    public static function getPhotoOnePaint($type, $direction, $isHD)
    {
        $photoOnePaint = null;
        $config = null;

        switch ($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $photoOnePaint = new TraditionalSuPhotoOnePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;

            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $photoOnePaint = new A4TraditionalOuPhotoOnePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
            default:
                $photoOnePaint = new TraditionalSuPhotoOnePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
        }

        $photoOnePaint->setConfig($config);
        return $photoOnePaint;
    }
}
