<?php
/**
 * 宗祠绘制工厂模式
 * Author: jiangpengfei
 * Date:    2019-01-21
 */

namespace FamilyTreePDF\Factory;

use FamilyTreePDF\Paint\TraditionalSuTemplate\TraditionalSuGravePaint;
use FamilyTreePDF\Util\SysConst;
use FamilyTreePDF\Paint\A4TraditionalOuTemplate\A4TraditionalOuGravePaint;

class GravePaintFactory
{

    public static function getGravePaint($type, $direction, $isHD)
    {
        $gravePaint = null;
        $config = null;

        switch ($type) {
            case StyleConst::$PAINT_STYLE_TRADITIONAL_SU:
                $gravePaint = new TraditionalSuGravePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
            case StyleConst::$PAINT_STYLE_TRADITIONAL_A4_OU:
                $gravePaint = new A4TraditionalOuGravePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\A4TraditionalOuTemplate\PaintConfig::getR2LConfig();
                    }
                }
                break;
            default:
                $gravePaint = new TraditionalSuGravePaint();
                if ($direction == SysConst::$LEFT_TO_RIGHT) {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDL2RConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getL2RConfig();
                    }
                } else {
                    if ($isHD) {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getHDR2LConfig();
                    } else {
                        $config = \FamilyTreePDF\Paint\TraditionalSuTemplate\PaintConfig::getR2LConfig();
                    }
                }
        }

        $gravePaint->setConfig($config);

        return $gravePaint;
    }
}
