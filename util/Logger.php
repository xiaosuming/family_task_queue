<?php
namespace Util;

use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;

class Logger {

    private static $logger = null;

    private function __construct() {

    }

    public static function getInstance() {

        if (self::$logger == null) {
            $client = new \Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@www.xinhuotech.com:8080/2');
            self::$logger = new \Monolog\Logger("logger");         //用来记录全局的异常警告日志
            $handler = new \Monolog\Handler\RavenHandler($client);
            $handler->setFormatter(new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
            self::$logger->pushHandler($handler);
        }

        return self::$logger;
    }
}