<?php

namespace Util;

class Util
{

    //生成随机码
    public static function generateRandomCode($len)
    {
        $code = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
        $str = "";
        for ($i = 0; $i < $len; $i++) {
            $str = $str . substr($code, rand(0, strlen($code)), 1);
        }
        return $str;
    }

    /**
     * 获取当前时间
     */
    public static function getCurrentTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * 获取当前日期
     */
    public static function getCurrentDate()
    {
        return date("Y-m-d");
    }

    /**
     * 获取请求ip
     */

    public static function getRequestIp()
    {
        return $_SERVER["REMOTE_ADDR"];
    }

    /**
     * 输出结果的函数
     * @param success 是否成功 true是成功，false是失败
     */
    public static function printResult($errorCode, $data)
    {
        $result['errorCode'] = $errorCode;
        if (is_array($data)) {
            $result['msg'] = "";
            $result['data'] = $data;
        } else {
            $result['msg'] = $data;
            $result['data'] = $data;
        }
        echo json_encode($result);
    }

    /**
     * 格式化日期,yy-mm-dd hh:ii:ss 转换成yy-mm-dd
     */
    public static function dateFormat($date)
    {
        $date = date_create($date);
        $newDate = date_format($date, "Y-m-d");
        return $newDate;
    }

    /**
     * 日期比较
     * @param $date1 日期一
     * @param $date2 日期二
     * @return 负数代表日期一早于日期二，0代表日期一等于日期二，正数代表日期一迟于日期二
     */
    public static function dateCompare($date1, $date2)
    {
        $unixDateTime1 = strtotime($date1);
        $unixDateTime2 = strtotime($date2);
        return $unixDateTime1 - $unixDateTime2;
    }

    /**
     * 获取当前时间
     */
    public static function getMicroTime()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    public static function exceptionFormat($e)
    {
        $line = $e->getLine();
        $file = $e->getFile();
        $message = $e->getMessage();
        $trace = json_encode($e->getTrace());
        return "$file($line):$message\n$trace\n";
    }

    public static function generateRegisterEmail($auth)
    {
        return "欢迎注册爱族群,这里是激活链接，http://{$_SERVER['HTTP_HOST']}/auth/{$auth}，30分钟内有效";
    }

    public static function generateEmailBindingEmail($auth)
    {
        return "欢迎,绑定邮箱点击下面链接，http://{$_SERVER['HTTP_HOST']}/authemailbind/{$auth}，30分钟内有效";
    }

    /**
     * 生成验证码
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateVcode($num)
    {
        $res = "";
        while (($num--) > 0) {
            $res .= rand(0, 9);
        }
        return $res;
    }

    /**
     * 生成短信验证序号
     * @param $num 验证码位数
     * @return string 验证码
     */
    public static function generateOrder($num)
    {
        $res = "";
        while (($num--) > 0) {
            $res .= rand(0, 9);
        }
        return $res;
    }

    /**
     * 生成二维码
     * @param $string 二维码存储的信息
     * @return string 二维码的存储地址
     */
    public static function generateQRCode($string)
    {
        $tmpName = Util::generateRandomCode(18); //18为随机数字
        //$path = "/tmp/$tmpName.png";
        $path = "d:/$tmpName.png"; //二维码的缓存地址
        //第一个参数是二维码的内容，
        //第二个参数是文件路径，
        //第三个参数可以是'L',M','Q','H',ECC level，应该是二维码的容错率
        //第三个参数是点的大小,
        //第五个参数是边框的大小,
        \PHPQRCode\QRcode::png($string, $path, 'H', 4, 2);
        //添加公司logo位于二维码中央
        $QR = $path;
        $logo = "d:/logo.png"; //放logo图片的地址
        if ($logo !== false) {
            $QR = imagecreatefromstring(file_get_contents($QR));
            $logo = imagecreatefromstring(file_get_contents($logo));
            $QR_width = imagesx($QR); //二维码图片宽度
            $QR_height = imagesy($QR); //二维码图片高度
            $logo_width = imagesx($logo); //logo图片宽度
            $logo_height = imagesy($logo); //logo图片高度
            $logo_qr_width = $QR_width / 3.5; //3.5 控制logo与二维码的比例
            $scale = $logo_width / $logo_qr_width;
            $logo_qr_height = $logo_height / $scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            //重新组合图片并调整大小
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }
        imagepng($QR, $path);
        imagedestroy($QR);

        $qrModule = $GLOBALS['QR_MODULE'];
        //使用curl传到远程服务器
        $data = array(
            'id_token' => $GLOBALS['userId'] . '|' . $GLOBALS['token'] . '|' . $GLOBALS['usingDevice'],
            'file' => new \CURLFile($path),
            'action' => 'file_action',
            'sub_action' => 'addFile',
            'module' => $qrModule,
        );

        //上传到图片服务器
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.izuqun.com/php/index.php");
        curl_setopt($ch, CURLOPT_POST, true); //post方式上传
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //成功返回true，输出内容
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //提交的数据
        $curl_result = curl_exec($ch); //执行提交
        curl_close($ch); //关闭
        if ($res = json_decode($curl_result, true)) {
            if ($res['errorCode'] == 0) {
                //获取到上传的地址
                $location = $res['data']['location'];
                if ($location != "") {
                    return $location;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * number2chinese description
     *
     * · 个，十，百，千，万，十万，百万，千万，亿，十亿，百亿，千亿，万亿，十万亿，
     *   百万亿，千万亿，兆；此函数亿乘以亿为兆
     *
     * · 以「十」开头，如十五，十万，十亿等。两位数以上，在数字中部出现，则用「一十几」，
     *   如一百一十，一千零一十，一万零一十等
     *
     * · 「二」和「两」的问题。两亿，两万，两千，两百，都可以，但是20只能是二十，
     *   200用二百也更好。22,2222,2222是「二十二亿两千二百二十二万两千二百二十二」
     *
     * · 关于「零」和「〇」的问题，数字中一律用「零」，只有页码、年代等编号中数的空位
     *   才能用「〇」。数位中间无论多少个0，都读成一个「零」。2014是「两千零一十四」，
     *   20014是「二十万零一十四」，201400是「二十万零一千四百」
     *
     * 参考：https://jingyan.baidu.com/article/636f38bb3cfc88d6b946104b.html
     *
     * @param  minx  $number
     * @param  boolean $isRmb
     * @return string
     */
    public static function number2chinese($number, $isRmb = false)
    {
        // 判断正确数字
        if (!preg_match('/^-?\d+(\.\d+)?$/', $number)) {
            throw new Exception('number2chinese() wrong number', 1);
        }
        list($integer, $decimal) = explode('.', $number . '.0');
        // 检测是否为负数
        $symbol = '';
        if (substr($integer, 0, 1) == '-') {
            $symbol = '负';
            $integer = substr($integer, 1);
        }
        if (preg_match('/^-?\d+$/', $number)) {
            $decimal = null;
        }
        $integer = ltrim($integer, '0');
        // 准备参数
        $numArr = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九', '.' => '点'];
        $descArr = ['', '十', '百', '千', '万', '十', '百', '千', '亿', '十', '百', '千', '万亿', '十', '百', '千', '兆', '十', '百', '千'];
        if ($isRmb) {
            $number = substr(sprintf("%.5f", $number), 0, -1);
            $numArr = ['', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖', '.' => '点'];
            $descArr = ['', '拾', '佰', '仟', '万', '拾', '佰', '仟', '亿', '拾', '佰', '仟', '万亿', '拾', '佰', '仟', '兆', '拾', '佰', '仟'];
            $rmbDescArr = ['角', '分', '厘', '毫'];
        }
        // 整数部分拼接
        $integerRes = '';
        $count = strlen($integer);
        if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else if ($count == 0) {
            $integerRes = '零';
        } else {
            for ($i = 0; $i < $count; $i++) {
                $n = $integer[$i]; // 位上的数
                $j = $count - $i - 1; // 单位数组 $descArr 的第几位
                // 零零的读法
                $isLing = $i > 1// 去除首位
                 && $n !== '0' // 本位数字不是零
                 && $integer[$i - 1] === '0'; // 上一位是零
                $cnZero = $isLing ? '零' : '';
                $cnNum = $numArr[$n];
                // 单位读法
                $isEmptyDanwei = ($n == '0' && $j % 4 != 0) // 是零且一断位上
                 || substr($integer, $i - 3, 4) === '0000'; // 四个连续0
                $descMark = isset($cnDesc) ? $cnDesc : '';
                $cnDesc = $isEmptyDanwei ? '' : $descArr[$j];
                // 第一位是一十
                if ($i == 0 && $cnNum == '一' && $cnDesc == '十') {
                    $cnNum = '';
                }

                // 二两的读法
                $isChangeEr = $n > 1 && $cnNum == '二' // 去除首位
                 && !in_array($cnDesc, ['', '十', '百']) // 不读两\两十\两百
                 && $descMark !== '十'; // 不读十两
                if ($isChangeEr) {
                    $cnNum = '两';
                }

                $integerRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 小数部分拼接
        $decimalRes = '';
        $count = strlen($decimal);
        if ($decimal === null) {
            $decimalRes = $isRmb ? '整' : '';
        } else if ($decimal === '0') {
            $decimalRes = '零';
        } else if ($count > max(array_keys($descArr))) {
            throw new Exception('number2chinese() number too large.', 1);
        } else {
            for ($i = 0; $i < $count; $i++) {
                if ($isRmb && $i > count($rmbDescArr) - 1) {
                    break;
                }

                $n = $decimal[$i];
                $cnZero = $n === '0' ? '零' : '';
                $cnNum = $numArr[$n];
                $cnDesc = $isRmb ? $rmbDescArr[$i] : '';
                $decimalRes .= $cnZero . $cnNum . $cnDesc;
            }
        }
        // 拼接结果
        $res = $symbol . ($isRmb ?
            $integerRes . ($decimalRes === '零' ? '元整' : "元$decimalRes") :
            $integerRes . ($decimalRes === '' ? '' : "点$decimalRes"));
        return $res;
    }


    /**
     * 将数组转换成以逗号分割的字符串
     */
    public static function arrayToString($array)
    {
        if (!is_array($array)) {
            $array = json_decode($array, true);
        }

        $str = '';
        $isFirst = true;
        foreach ($array as $item) {
            if ($isFirst) {
                $str .= $item;
                $isFirst = false;
            } else {
                $str .= ",$item";
            }
        }

        return $str;
    }

/**
 *  遍历数组,删除数组中某个值
 */
    public static function array_remove($array,$v){ 

        foreach($array as $key=>$value){
        
                if($value == $v){ 
                        unset($array[$key]); 
        
                } 
        }
        
        return $array;
        
        }
}
