<?php
/**
 * 腾讯云通讯的集成封装
 * author: jiangpengfei
 * date: 2018-02-27
 */
    namespace ThirdParty;

    use ThirdParty\TimRestAPI;
    use ThirdParty\TimUtil;
    use Util\TempStorage;

    class TencentIM implements IInstantMessage{

        private $timApi;
        private $tempStorage;
        private $REDIS_KEY = "tim_user_sig";

        public function __construct(){

            $identifier = $GLOBALS['tencent_im_identifier'];
            $appid = $GLOBALS['tencent_im_appid'];

            $this->timApi = new TimRestAPI();
            $this->timApi->init($appid,$identifier);
            $userSig = $this->getUserSig();
            if( $userSig ){
                $this->timApi->set_user_sig($userSig);
            }else{
                $private_pem_path = __DIR__."/signature/private_key";
                
                if(!file_exists($private_pem_path))
                {
                    echo "私钥文件不存在\n";
                    $GLOBALS['logger']->error('私钥文件不存在');
                    return;
                }

                if(TimUtil::is_64bit()){
                    if(PATH_SEPARATOR==':'){
                        $signature = __DIR__."/signature/linux-signature64";
                    }else{
                        $signature = __DIR__."\\signature\\windows-signature64.exe";
                    }
                    
                }else{
                    if(PATH_SEPARATOR==':')
                    {
                        $signature = __DIR__."/signature/linux-signature32";
                    }else{
                        $signature = __DIR__."\\signature\\windows-signature32.exe";
                    }
                }

                $ret = $this->timApi->generate_user_sig($identifier, '36000', $private_pem_path, $signature);
                if($ret == null || strstr($ret[0], "failed")){
                    echo "获取usrsig失败\n";
                    $GLOBALS['logger']->error('获取usrsig失败');
                    return -1;
                }else{
                    //获取usersig成功,$ret[0]就是sig值，保存起来，有效期是36000
                    $this->saveUserSig($ret[0]);
                }
            }
        }

        /**
         * 向redis中保存user sig
         * @return bool 保存成功或失败
         */
        private function saveUserSig($userSig){
            if($this->tempStorage === null){
                $this->tempStorage = new TempStorage();
            }

            return $this->tempStorage->setTemp($this->REDIS_KEY,$userSig,35950);
        }

        /**
         * 从redis中取出user sig
         * @return mix token或者false(false代表不存在)
         */
        private function getUserSig(){
            if($this->tempStorage === null){
                $this->tempStorage = new TempStorage();
            }

            return $this->tempStorage->get($this->REDIS_KEY);
        }

        /**
         * 创建用户
         * @param $username 用户名
         * @param $password 无用
         * @return bool true代表成功，false代表失败
         */
        public function createUser($username,$password,$face_url = null, $nickname = ''){
            $identifier = $username;
            $nick = $nickname;
            if($face_url === null){
                $face_url = "https://image.izuqun.com/fastdfs/M00/00/05/dD4ZgFmmH_-AIYTLAAAHLtDFCbg683.jpg";
            }
            $result = $this->timApi->account_import($identifier, $nick, $face_url);

            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('IM用户创建失败', $result);
                return false;
            }
        }

        /**
         * 创建聊天室
         * @param name 聊天室名称
         * @param description 聊天室描述
         * @param maxusers 聊天室成员最大数
         * @param owner 聊天室的管理员
         * @param members 聊天室的成员 
         * @return mix 创建成功为聊天室Id，创建失败为false
         */
        public function createChatRoom(string $name,string $description,int $maxusers,string $owner,array $members){

            $group_type = 'ChatRoom';
            $group_name = $name;
            $owner_id = $owner;
            $info_set['introduction'] = $description;
            $info_set['max_member_num'] = $maxusers;

            foreach($members as $member){
                $account['Member_Account'] = $member;
                $mem_list[] = $account;
            }

            $result = $this->timApi->group_create_group2($group_type, $group_name, $owner_id, $info_set, $mem_list);
            if($result['ActionStatus'] === 'OK'){
                return $result['GroupId'];
            }else{
                $GLOBALS['logger']->error('创建聊天室失败', $result);
                return false;
            }
        }

        /**
         * 删除聊天室
         * @param $chatroomId 聊天室id
         * @return bool 成功返回true，失败返回false
         */
        public function deleteChatRoom($chatroomId){
            $result = $this->timApi->group_destroy_group($chatroomId);

            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('删除聊天室失败', $result);
                return false;
            }
        }

        /**
         * 添加聊天室成员
         * @param $chatroom 聊天室
         * @param $username 用户名
         * @return bool false失败,true成功
         */
        public function addUserToChatRoom($chatroom,$username){
            $result = $this->timApi->group_add_group_member($chatroom, $username, 0);

            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('添加聊天室成员失败', $result);
                return false;
            }
        }

        /**
         * 发送消息给单个用户
         * @param $sendUsers 发送的用户数组
         * @param $msgJson   消息内容,字符串类型
         * @param $ext       消息的扩展字段,是一个map
         * @return bool true代表成功，false代表失败
         */
        public function sendTextMessageToUser($sendUsers,$msgJson,$ext = null){
            $account_list = $sendUsers;
            $content['msg'] = $msgJson;

            if($ext != null)
                $content['ext'] = $ext;

            $result = $this->timApi->openim_batch_sendmsg($account_list, json_encode($content));

            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('发送消息给单个用户失败', $result);
                return false;
            }
        }

        /**
         * 创建群组
         * @param groupName 群组名称
         * @param description 聊天室描述
         * @param maxusers 聊天室成员最大数
         * @param owner 聊天室的管理员
         * @param members 聊天室的成员 
         * @param isPublic 是否公开
         * @param needPermit 加入群是否需要群主或者群管理员审批，默认是false
         * @param allowInvites 是否允许群成员邀请别人加入此群。 true：允许群成员邀请人加入此群，false：只有群主或者管理员才可以往群里加人。
         * @return mix 创建成功为聊天室Id，创建失败为false
         */
        public function createChatGroup(string $groupName,string $description,int $maxusers,string $owner,array $members,bool $isPublic,bool $needPermit,bool $allowInvites
        ){
            $group_type = 'Private';
            $group_name = $groupName;
            $owner_id = $owner;
            $info_set['introduction'] = $description;
            $info_set['max_member_num'] = $maxusers;
            foreach($members as $member){
                $account['Member_Account'] = $member;
                $mem_list[] = $account;
            }

            $result = $this->timApi->group_create_group2($group_type, $group_name, $owner_id, $info_set, $mem_list);
            if($result['ActionStatus'] === 'OK'){
                return $result['GroupId'];
            }else{
                $GLOBALS['logger']->error('创建群组失败', $result);
                return false;
            }
        }

        /**
         * 加入黑名单
         * @param $ownerUser 
         * @param $blockUsers
         * @return bool 成功true,失败false
         */
        public function blockUser($ownerUser,$blockUsers){
            $result =  $this->timApi->black_list_add($ownerUser,$blockUsers);

            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('加入黑名单失败', $result);
                return false;
            }     
        }

        /**
         * 从黑名单中删除用户
         * @param $ownerUser
         * @param $blockUser
         * @return bool 成功true失败false
         */
        public function deleteBlockUser($ownerUser,$blockUser){
            $result =  $this->timApi->black_list_delete($ownerUser,$blockUser);
            if($result['ActionStatus'] === 'OK'){
                return true;
            }else{
                $GLOBALS['logger']->error('从黑名单中删除用户失败', $result);
                return false;
            }    
        }

    }