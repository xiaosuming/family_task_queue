<?php
namespace ThirdParty;

use Util\TempStorage;
use ThirdParty\AipOcr;

/**
 * OCR服务的客户端封装
 * 这里会做一个基本的调度.在某个服务的额度到达上限后,进行服务切换
 */
class OCRClient {

    private $baiduCount;
    private $storage;
    private $BAIDU_LIMIT = 50000;

    /**
     * 构造函数
     */
    public function __construct() {
        $this->storage = new TempStorage();
    }

    /**
     * 进行识别
     */
    public function ocr($imageUrl) {

        $ocrInstance = null;
        $ocrResult = '';    // ocr的结果
        $ocrType = 0;

        $day = date('Ymd');
        $baiduKey = 'baidu_ocr_'.$day;

        $baiduUsed = $this->storage->get($baiduKey);
        if (!$baiduUsed) {
            // 不存在, 则设置为0,并设置一天的有效期
            $this->storage->setTemp($baiduKey,0, 24*3600);
            $baiduUsed = 0;
        }

        if ($baiduUsed < $this->BAIDU_LIMIT) {
            // https://cloud.baidu.com/doc/OCR/OCR-PHP-SDK.html#.E9.80.9A.E7.94.A8.E6.96.87.E5.AD.97.E8.AF.86.E5.88.AB
            // 在免费的额度内,则直接使用百度的ocr
            $ocrInstance = new AipOcr($GLOBALS['BAIDU_APP_ID'], $GLOBALS['BAIDU_API_KEY'], $GLOBALS['BAIDU_SECRET_KEY']);
                        
            // 如果有可选参数
            $options = array();
            $options["language_type"] = "CHN_ENG";
            $options["detect_direction"] = "true";
            $options["detect_language"] = "true";
            $options["probability"] = "true";

            // 带参数调用通用文字识别, 图片参数为远程url图片
            $ocrResult = $ocrInstance->basicGeneralUrl($imageUrl, $options);

            $ocrResult = json_encode($ocrResult,JSON_UNESCAPED_UNICODE);

            // 将百度云的调用次数+1
            $this->storage->increment($baiduKey);
            $ocrType = 1;
        } else {
            // 使用我们自己的ocr
            // 这里下载照片，传给ocr-server，然后取回结果，使用updateOCRResult更新ocr的结果
            $content = file_get_contents($imageUrl);
            $post = [
                'file' => $content,
            ];
            
            $ch = curl_init('http://116.62.25.128:8080');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            
            // execute!
            $ocrResult = curl_exec($ch);
            // close the connection, release resources used
            curl_close($ch);
        }

        return [$ocrResult, $ocrType];
    }
}