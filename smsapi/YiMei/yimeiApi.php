<?php
/**
 * 亿美发送短信接口
 */

//namespace smsapi\YiMei;

//use smsapi\YiMei\MagicCrypt;

class yimeiApi{

    const YM_SMS_ADDR = "http://bjksmtn.b2m.cn:80";  /*接口地址*/
    const YM_SMS_SEND_URI = "/inter/sendSingleSMS";  /*发送单条短信接口*/
    const YM_SMS_APPID = "EUCP-EMY-SMS0-JEYLO";  /*APPID*/
    const YM_SMS_AESPWD = "7706601673463920";  /*密钥*/
    const YM_SMS_SEND_BATCH_URI = "/inter/sendBatchSMS";  /*发送批次短信接口*/
    const YM_SMS_SEND_BATCHONLY_SMS_URI =  "/inter/sendBatchOnlySMS";  /*发送批次[不支持自定义smsid]短信接口*/
    const YM_SMS_SEND_PERSONALITY_SMS_URI = "/inter/sendPersonalitySMS";  /*发送个性短信接口*/
    const YM_SMS_GETREPORT_URI = "/inter/getReport";  /*获取状态报告接口*/
    const YM_SMS_GETMO_URI = "/inter/getMo";  /*获取上行接口*/
    const YM_SMS_GETBALANCE_URI = "/inter/getBalance";  /*获取余额接口*/

    public function __construct()
    {
        //var_dump(1234);
    }
        
    /**
     * 亿美 发送单条短信
     */
        public function yimeiSend($phone, $msg_str, $order){
        $item = new stdClass();
        $item->mobile   = $phone;
        $item->content  = '【薪火信息】短信验证码为：'.$msg_str.'，5分钟内有效，短信验证序号'.$order;
        
        $item->requestTime = getMillisecond();
        $item->requestValidPeriod = 120; //生效时间
        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果
        
        $url = self::YM_SMS_ADDR.self::YM_SMS_SEND_URI;
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    private function http_request($url, $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, true);
        $header[] = "appId: ".self::YM_SMS_APPID;
        if (function_exists('gzdecode'))   $header[] = "gzip: on";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        
        curl_setopt($curl, CURLOPT_HEADER, true);
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($curl);
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        curl_close($curl);
        
        $header = substr($res, 0, $headerSize);
        
        $outobj = new stdClass();
        $lines = explode("\r\n",$header);
        foreach($lines as $line)
        {
            $items = explode(": ",$line);
            if(isset($items[0]) and !empty($items[0]) and 
            isset($items[1]) and !empty($items[1])){
                $tmp = $items[0];
                $outobj->$tmp = $items[1];
            }
        }
        
        $outobj->ciphertext = substr($res, $headerSize);

        return $outobj;
    }

}