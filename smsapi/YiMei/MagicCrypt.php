<?php

// namespace smsapi\YiMei;

class MagicCrypt {
    private $iv = "";//密钥偏移量IV，可自定义
    private $encryptKey = 7706601673463920;

    public function __construct(){
    }

    //加密
    public function encrypt($encryptStr) {

        if(version_compare(PHP_VERSION,'7.0.0','ge')){

        return $this->encryptPhp7($encryptStr);//加密结果

        }

        $localIV = $this->iv;
        $encryptKey = $this->encryptKey;
        
        if ( function_exists('gzdecode'))   $encryptStr = gzencode($encryptStr);
 
        //Open module
        $module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, $localIV);
 
        mcrypt_generic_init($module, $encryptKey, $localIV);
 
        //Padding
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $pad = $block - (strlen($encryptStr) % $block); //Compute how many characters need to pad
        $encryptStr .= str_repeat(chr($pad), $pad); // After pad, the str length must be equal to block or its integer multiples
 
        //encrypt
        $encrypted = mcrypt_generic($module, $encryptStr);
 
        //Close
        mcrypt_generic_deinit($module);
        mcrypt_module_close($module);
 
        return $encrypted;
    }

    //PHP7使用这个加密
    public  function  encryptPhp7($encryptStr)
    {
        $localIV = $this->iv;
        $encryptKey = $this->encryptKey;

        if ( function_exists('gzdecode'))   $encryptStr = gzencode($encryptStr);
        return openssl_encrypt($encryptStr, 'AES-128-ECB',$encryptKey,OPENSSL_RAW_DATA,$localIV);
    }

    //解密
    public function decrypt($encryptStr) {

        if(version_compare(PHP_VERSION,'7.0.0','ge')){
            return $this->decryptPhp7($encryptStr);//加密结果
        }

        $localIV = $this->iv;
        $encryptKey = $this->encryptKey;
 
        //Open module
        $module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, $localIV);
 
        mcrypt_generic_init($module, $encryptKey, $localIV);
 
        $encryptedData = mdecrypt_generic($module, $encryptStr);
        
        if (function_exists('gzdecode'))   $encryptedData = gzdecode($encryptedData);
 
        return $encryptedData;
    }

//PHP7使用这个解密
    public  function  decryptPhp7($encryptStr)
    {
        $localIV = $this->iv;
        $encryptKey = $this->encryptKey;
        $encryptedData = openssl_decrypt($encryptStr,'AES-128-ECB',$encryptKey,OPENSSL_RAW_DATA,$localIV);
        if (function_exists('gzdecode'))   $encryptedData = gzdecode($encryptedData);
        return $encryptedData;
    }




}