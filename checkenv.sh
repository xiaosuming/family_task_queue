#!/bin/bash
## 检查环境是否正确

# 检查php扩展

phpext=`php -m`

declare -a exts=("bcmath" "mbstring" "curl" "pdo" "pdo_mysql" "mysqli" "redis" "gd" "libxml" "md2pic" "php_dv" 'imagick')

for value in ${exts[@]}
do
    if test `expr match "$phpext" ".*${value}.*"` -eq 0;then
        echo -e "\e[31m[error]\e[0m: missing php library ${value}";
    else
        echo -e "\e[32m[pass]\e[0m: php library ${value}";
    fi
done

# 检查so库

declare -a so=("libssl1.0" "libgd-dev" "libcurl4-openssl-dev" "libjpeg-dev" "libpng-dev" "libfreetype6-dev")

for value in ${so[@]}
do
    dpkg -s "$value" >/dev/null 2>&1 && {
        echo -e "\e[32m[pass]:\e[0m $value is installed."
    } || {
        echo -e "\e[31m[error]:\e[0m $value is not installed."
    }
done

# 检查读写环境, util下的FamilyTree需要可写(在docker环境下运行，会创建daemon用户组的目录，但是却不可写)。vendor下面需要可写
# 权限问题可参考:https://yq.aliyun.com/articles/53990

chmod a+w -R util/FamilyTree
chmod a+w -R util/FamilyTreePDF
chmod a+w -R vendor/
