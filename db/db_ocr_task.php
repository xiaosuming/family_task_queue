<?php
/**
 * 负责推送ocr任务
 */
    namespace DB;

    use DB\CDBManager;
    use DB\RedisConnect;
    use Util\TaskQueue\Consumer;

    class CDBOCRTask
    {
        public $redis = null;
        public $pdo = null;

        public $TABLE = 'gener_ocr_task';

        public function __construct()
        {
            $this->redis = RedisConnect::getInstance();

        }

        public function init() {
            if (!$this->pdo) {
                if (!isset($GLOBALS['pdo'])) {
                    $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                    if ($this->pdo)
                        $GLOBALS['pdo'] = $this->pdo;
                } else {
                    $this->pdo = $GLOBALS['pdo'];
                }
            }
    
            return true;
        }

        /**
         * 获取OCR的任务队列
         * @return string key值
         */
        private function getOCRKey(){
            return 'ocr_task';
        }

        /**
         * 向数据库中增加ocr任务记录
         */
        public function addOCRTask($userId, $photo)
        {
            if (!$this->init()) {
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
            }
            $sql = "INSERT INTO $this->TABLE (userId, photo, create_by, create_time)
                    VALUES
                    ('$userId', '$photo', '$userId', now())";
            $recordId = $this->pdo->insert($sql);

            if ($recordId > 0) {
                // 添加到ocr任务队列
                $this->setOCRTask($recordId, $photo);
            }

            return $recordId;
        }

        /**
         * 获取OCR任务
         */
        public function getOCRTask($callback) {
            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_ocr_task'], $callback);
        }

        /**
         * 更新ocr的结果
         * 
         * @param $type ocr的服务,0是自己的ocr服务,1是百度云的ocr服务
         */
        public function updateOCRResult($ocrId, $result, $type = 0) {
            if (!$this->init()) {
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
            }

            $sql = "UPDATE $this->TABLE SET result = '$result', finish='1', type = '$type' WHERE id = '$ocrId' ";
            return $this->pdo->update($sql);
        }

        /**
         * 获取OCR的结果
         */
        public function getOCRResult($ocrId) {
            if (!$this->init()) {
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
            }

            $sql = "SELECT id, photo, userId, result, finish, type, create_by, create_time FROM $this->TABLE WHERE id = '$ocrId' ";
            return $this->pdo->uniqueResult($sql);
        }

    }