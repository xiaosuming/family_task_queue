<?php
namespace DB;

use DB\RedisConnect;
use Util\TaskQueue\Consumer;

class MailDB{
    private $redis;
    private $mailDB;

    public function __construct(){
        $this->mailDB = $GLOBALS['redis_mail'];
    }

    /**
     * 从队列中返回当前需要处理的邮件
     * @return string 需要处理的邮件 
     */
    public function popMail($callback){
        $consumer = new Consumer();
        $consumer->consume($GLOBALS['redis_mail'], $callback);
    }
    

};