<?php
/**
 * mysql类中使用了pdo的长连接扩展，因此不需要实现连接池
 * php是运行完会销毁所有资源，因此不能实现连接池，使用swoole可以实现php数据库连接池
 * 
 */

namespace DB;

use \PDO;
use \Redis;

class CDBManager 
{

    public $version = '';
    public $querynum = 0;
    public $link = null;
    public $querys=array();
    
    public $dbhost=null;
    public $dbuser=null;
    public $dbpw=null;
    public $dbname=null;
    public $pconnect=null;
    public $halt=null;
    public $dbcharset2=null;
    public $redis = null;

    /**
     * 构造函数
     * @param $dbhost 数据库地址
     * @param $dbuser 用户名
     * @param $dbpw   密码
     * @param $dbname 数据库名
     * @param $pconnect 是否采用长连接
     * @param dbcharset2 数据库编码
     */
    function __construct($dbhost, $dbuser, $dbpw, $dbname = '', $pconnect = true, $dbcharset2 = 'utf8mb4')
    {
        $this->dbhost=$dbhost;
        $this->dbuser=$dbuser;
        $this->dbpw=$dbpw;
        $this->dbname=$dbname;
        $this->pconnect=$pconnect;
        $this->dbcharset2=$dbcharset2;
    }

    /**
     * 建立连接
     * @param $dbhost 数据库地址
     * @param $dbuser 数据库用户
     * @param $dbpw   数据库密码
     * @param $dbname 数据库名
     * @param $pconnect 是否长连接
     * return Object PDO对象
     */
    public static function createLink($dbhost, $dbuser, $dbpw, $dbname, $pconnect)
    {
        //这里的array(PDO::ATTR_PERSISTENT=>$pconnect)使用了mysql的长连接
        return new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpw, array(PDO::ATTR_PERSISTENT=>$pconnect));
    }


    public function connect() 
    {
        $this->link = self::createLink($this->dbhost, $this->dbuser, $this->dbpw, $this->dbname, $this->pconnect);
        if ($this->link)
        {
            $this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
            $this->link->exec("set names ".$this->dbcharset2);   
        }
    }

    /**
     * 连接到redis数据库
     * 
     *
     */
    public function connectRedis(){
        if($this->redis == null){
            $this->redis = new Redis();
            $this->redis->pconnect($GLOBALS['redis_host'], $GLOBALS['redis_port']);
            $this->redis->auth($GLOBALS['redis_pass']);
        }
    }

    /**
     * 设置缓存
     * @param $key 键
     * @param $value 值
     */
    public function setCache($key,$value){
        $this->connectRedis();
        $this->redis->multi();
        $this->redis->select(1);        //使用数据库1
        $this->redis->set($key,$value);
        $this->redis->exec();
    }

    /**
     * 获取缓存
     * @param $key 键
     * @return string 键对应的值
     */
    public function getCache($key){
        $this->connectRedis();
        $this->redis->multi();
        $this->redis->select(1);        //使用数据库1
        $this->redis->get($key);
        $result = $this->redis->exec();

        return $result[1];
    }

    /**
     * 删除缓存
     * @param $key 键
     * @return 删除结果
     */
    public function delCache($key){
        $this->connectRedis();
        $this->redis->multi();
        $this->redis->select(1);        //使用数据库1
        $this->redis->del($key);
        $result = $this->redis->exec();

        return $result[1];
    }

    /**
     * 从sql数据库中查询
     * @param $querystr 查询语句
     * @return mix 查询结果
     */
    private function sqlQuery($querystr){
        $stmt = $this->stmtExcute($querystr);

        return $this->fetch_array($stmt);
    }

    /**
     * 从缓存或者sql数据库中查询
     * @param $querystr 查询语句
     * @param $cache    缓存
     * @param $expire   缓存过期时间
     * @return mix 查询结果
     */
    public function query($querystr,$cache = false,$cacheKey = "",$expire = 3600)
    {
        if($cache){
            //如果设置了缓存，先从缓存中查询
            if($cacheKey == "")
                $cacheKey = md5($querystr);
            
            $result = unserialize($this->getCache($cacheKey));
            if($result == FALSE){
                //如果缓存不存在
                $result = $this->sqlQuery($querystr);                              //从数据库中查询
                $this->setCache($cacheKey,serialize($result),Array('nx', 'ex'=>$expire));          //存放到缓存中
            }
            
            return $result;
        }else{
            return $this->sqlQuery($querystr);
        }
    }

    /**
     * 从stmt中取出数组的结果
     * @param $stmt 查询句柄
     * @return mix 查询结果
     */
    public function fetch_array($stmt)
    {
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * sql prepare语句
     * @param $str 语句
     * @return object prepare好的语句
     */
    protected function prepare($str)
    {
        if (!$this->link)
        {
            $this->connect();
        }

        if (!$this->link)
        {
            return null;
        }
        return $this->link->prepare($str);
    }

    private function stmtExcute($sql,$params = array()){
        try{
            $stmt = $this->prepare($sql);
            $result = $stmt->execute($params);
        }catch(\Exception $e){
            if($e->getCode() == "HY000"){
                $this->connect();
                $stmt = $this->prepare($sql);
                $result = $stmt->execute($params);
            }else{
                throw $e;
            }
        }

        return $stmt;
    }

    /**
     * 更新语句
     * @param $updatestr 更新的sql语句
     * @param $cache  是否更新缓存
     * @return int 更新的记录数
     */
    public function update($updatestr,$cache = false,$cacheKey = "")
    {
        if($cache){
            if($cacheKey == "")
                $cacheKey = md5($updatestr);

            $this->delCache($cacheKey);     //直接删除缓存，下次查询时就会更新最新的缓存
        }

        $stmt = $this->stmtExcute($updatestr);

        return $stmt->rowCount(); 	
    }

    public function insert($insertstr)
    {
        $stmt = $this->stmtExcute($insertstr);
        
        return $this->link->lastinsertid();
    }

    public function deletesql($deletestr)
    {
        $stmt = $this->stmtExcute($deletestr);

        return $stmt->rowCount();
    }
    
    /**
     * 返回单个结果或者空
     * @param $queryStr 查询字符串
     * @param $cache    是否缓存
     * @param $expire   缓存时间
     * @return mix 查询结果或者null
     */
    public function uniqueResult($queryStr,$cache = false,$expire = 3600){
        $result = $this->query($queryStr,$cache,$expire);

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
    
    //开启事务处理
    public function beginTransaction(){
        $this->connect();
        $this->link->beginTransaction();
    }
    
    //事务处理失败，开始回滚
    public function rollback(){
        $this->link->rollBack();
    }
    
    //提交当前事务
    public function commit(){
        $this->link->commit();
    }

}