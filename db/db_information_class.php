<?php
    /**
     * 资讯模块，使用zset实现的
     * @author jiangpengfei
     * @date 2018-11-26
     */
    
    namespace DB;

    use DB\CDBManager;
    use DB\RedisConnect;
    use Util\TaskQueue\Consumer;

    class InfoDB
    {
        public $redis = null;

        public function __construct()
        {
            $this->redis = RedisConnect::getInstance();

        }

        /**
         * 获取用户的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getInformationTimelineKey($userId){
            return 'infot'.$userId;
        }

        /**
         * 获取推送推文的任务
         * @return mix 推送结果
         */
        public function getPushInfoTask($callback){
            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_info'], $callback);
        }

        /**
         * 获取删除推文的任务
         */
        public function getDeleteInfoTask($callback){
            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_info_delete'], $callback);
        }


        /**
         * 将资讯id推送到所有用户的timeline中
         * @param $pushUserIds 要推送的用户id
         * @param $infoId 推文id
         * @param $postTime 推文时间
         * @return boolean  true操作成功
         */
        public function pushToTimeline($pushUserIds,$infoId,$postTime){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $addResult = $this->redis->zAdd($this->getInformationTimelineKey($value),$postTime,$infoId);
            }
            $this->redis->exec();
            return true;
        }

        /**
         * 将推文从用户timeline上删除
         * @param $pushUserIds 要删除推文的用户id
         * @param $infoId 推文Id
         * @return boolean  true操作成功
         */
        public function deleteFromTimeline($pushUserIds,$infoId){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $this->redis->zRem($this->getInformationTimelineKey($value),$infoId);
            }
            $this->redis->exec();
            return true;
        }
        
        public function close(){
            $this->redis->close();
        }

        
    };