<?php
namespace DB;
use DB\RedisConnect;
use Util\TaskQueue\Consumer;

class PhoneDB{
    private $redis;
    private $messageDB;

    public function __construct(){
        $this->messageDB = $GLOBALS['redis_message'];
        $this->connect();
    }

    public function connect(){
        $this->redis = RedisConnect::getInstance();
    }

    /**
     * 从队列中返回当前需要处理的短信
     * @return string 需要处理的短信
     */
    public function popMessage($callback){

        $consumer = new Consumer();
        $consumer->consume($this->messageDB, $callback);

        return true;

        // $this->redis->select(0);        //选择数据库
        // $result = $this->redis->blPop($this->messageDB, 0);
        
        // return $result[1];
    }
    

};