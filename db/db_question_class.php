<?php
    
    namespace DB;

    use DB\RedisConnect;
    use Util\TaskQueue\Consumer;

    class QuestionDB
    {
        public $redis = null;

        public function __construct()
        {
            $this->redis = RedisConnect::getInstance();

        }

        /**
         * 获取用户的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getUserTimelineKey($userId){
            return 'qt'.$userId;
        }

        /**
         * 获取推送问题的任务
         * @return mix 推送结果
         */
        public function getPushQuestionsTask($callback){

            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_question'], $callback);

            // $this->redis->select(0);        //选择数据库
            // $result = $this->redis->blPop($GLOBALS['redis_question'], 0);

            // return $result[1];
        }

        /**
         * 获取删除问题的任务
         */
        public function getDeletePushQuestionsTask($callback){
            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_question_delete'], $callback);


            // $this->redis->select(0);        //选择数据库
            // $result = $this->redis->blPop($GLOBALS['redis_question_delete'], 0);

            // return $result[1];
        }



        /**
         * 将推文id推送到所有用户的timeline中
         * @param $pushUserIds 要推送的用户id
         * @param $questionId 问题id
         * @param $postTime 推文时间
         * @param $type     类型，1为问题，2为悬赏
         * @return boolean  true操作成功
         */
        public function pushToTimeline($pushUserIds, $questionId, $postTime, $type){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $addResult = $this->redis->zAdd($this->getUserTimelineKey($value), $postTime, $type.$questionId);
            }
            $this->redis->exec();
            return true;
        }

        /**
         * 将推文从用户timeline上删除
         * @param $pushUserIds 要删除推文的用户id
         * @param $questionId 问题Id
         * @return boolean  true操作成功
         */
        public function deleteFromTimeline($pushUserIds, $questionId, $type){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $this->redis->zRem($this->getUserTimelineKey($value), $type.$questionId);
            }
            $this->redis->exec();
            return true;
        }
        
        public function close(){
            $this->redis->close();
        }
    };