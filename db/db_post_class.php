<?php
    /**
     * 人物个人空间模块，使用zset实现的
     * @author jiangpengfei
     * @date 2016-12-25
     */
    
    namespace DB;

    use DB\CDBManager;
    use DB\RedisConnect;
    use Util\TaskQueue\Consumer;

    class PostDB
    {
        public $redis = null;

        public function __construct()
        {
            $this->redis = RedisConnect::getInstance();

        }

        /**
         * 获取用户的timeline的key
         * @param $userId 用户id
         * @return string key值
         */
        private function getUserTimelineKey($userId){
            return 'pt'.$userId;
        }

        /**
         * 获取推送推文的任务
         * @return mix 推送结果
         */
        public function getPostTask($callback){
            // $this->redis->select(0);        //选择数据库
            // $task = $this->redis->blPop($GLOBALS['redis_post'], 0);

            // return $task[1];

            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_post'], $callback);
        }

        /**
         * 获取删除推文的任务
         */
        public function getDeletePostTask($callback){
            // $this->redis->select(0);        //选择数据库
            // $task = $this->redis->blPop($GLOBALS['redis_post_delete'], 0);

            // return $task[1];
            
            $consumer = new Consumer();
            $consumer->consume($GLOBALS['redis_post_delete'], $callback);
        }



        /**
         * 将推文id推送到所有用户的timeline中
         * @param $pushUserIds 要推送的用户id
         * @param $postId 推文id
         * @param $postTime 推文时间
         * @return boolean  true操作成功
         */
        public function pushToTimeline($pushUserIds,$postId,$postTime){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $addResult = $this->redis->zAdd($this->getUserTimelineKey($value),$postTime,$postId);
            }
            $this->redis->exec();
            return true;
        }

        /**
         * 将推文从用户timeline上删除
         * @param $pushUserIds 要删除推文的用户id
         * @param $postId 推文Id
         * @return boolean  true操作成功
         */
        public function deleteFromTimeline($pushUserIds,$postId){
            $this->redis->multi();
            $this->redis->select(0);        //选择数据库
            foreach($pushUserIds as $value){
                $this->redis->zRem($this->getUserTimelineKey($value),$postId);
            }
            $this->redis->exec();
            return true;
        }
        
        public function close(){
            $this->redis->close();
        }

        
    };