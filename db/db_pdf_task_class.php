<?php
/**
 * Created by PhpStorm.
 * User: xuliulei
 * Date: 18-6-28
 * Time: 下午5:54
 */

namespace DB;

use DB\CDBManager;
use DB\RedisConnect;
use Util\Util;
use Util\TaskQueue\Consumer;

class CDBPdfTask
{
    public $pdo = null;

    public $TABLE = 'gener_pdf_task';

    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {

    }

    /**
     * @codeCoverageIgnore
     */
    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }

    /**
     * 获取OCR的任务队列
     * @codeCoverageIgnore
     * @return string key值
     */
    private function getPdfKey()
    {
        return 'pdf_task';
    }

    public function getFamilyPdfTask($callback) {
        $consumer = new Consumer();
        $consumer->consume($GLOBALS['redis_family_pdf_task'], $callback, true);
    }

    /**
     * 更新pdf的结果
     *
     * @param $pdfId
     * @param $previewPdf
     * @param $printPdf
     * @return mixed
     */
    public function updatePdf($pdfId, $previewPdf, $printPdf)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET preview_pdf = '$previewPdf',print_pdf = '$printPdf' , update_time = now(),
        finish_percent = '100%', finish_status = 'pdf导出完成', finish='1' WHERE id = '$pdfId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 更新pdf的进度
     *
     * @param $pdfId
     * @param $previewPdf
     * @param $printPdf
     * @return mixed
     */
    public function updatePdfWorking($pdfId, $percent, $status = '')
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET finish_percent = '$percent', update_time = now(), finish_status='$status' WHERE id = '$pdfId' ";
        return $this->pdo->update($sql);
    }
    /**
     * 更新失败的pdf的结果
     *
     * @param $pdfId
     * @param $previewPdf
     * @param $printPdf
     * @return mixed
     */
    public function updatePdfFailed($pdfId, $failed_detail)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "UPDATE $this->TABLE SET failed_detail = '$failed_detail', update_time = now(), finish='2' WHERE id = '$pdfId' ";
        return $this->pdo->update($sql);
    }

    /**
     * 获取pdf的结果
     * @param $taskId
     * @return mixed
     */
    public function getPdf($taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id, familyId, preview_pdf,print_pdf, finish, create_by, create_time,update_by,update_time FROM $this->TABLE WHERE id = '$taskId' ";
        return $this->pdo->uniqueResult($sql);
    }

    /**
     * 获取任务详情
     * @param $taskId
     * @return mixed
     */
    public function getFamilyPdfTaskDetail($taskId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,familyId,preview_pdf,print_pdf,familyTreeName,direction,eventIds,photoIdOnes,photoIds,graveIds,isHD,options,finish,startPersonId,create_time,create_by,update_time,update_by FROM $this->TABLE WHERE id='$taskId'";
        return $this->pdo->uniqueResult($sql);
    }

    public function getEventsByEventIds($eventIds) {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $eventStr = Util::arrayToString($eventIds);

        $sql = "SELECT id,exp_time as expTime,exp_name as expName,exp_content as expContent FROM gener_significantEvent WHERE id in ($eventStr) AND is_delete = '0'";

        return $this->pdo->query($sql);
    }

    public function getPhotosByPhotoIds($photoIds) {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $photoStr = Util::arrayToString($photoIds);

        $sql = "SELECT id,albumId,photo,description,address FROM gener_family_photo WHERE id in ($photoStr) AND is_delete = '0' ";
    
        return $this->pdo->query($sql);
    }

    /**
     * 根据graveId数组获取grave
     */
    public function getGravesByGraveIds($graveIds) {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $graveStr = Util::arrayToString($graveIds);

        $sql = "SELECT id,person_name,address,description,photo FROM gener_grave WHERE id in ($graveStr)";
    
        return $this->pdo->query($sql);
    }



      


}