<?php
namespace DB;

use DB\RedisConnect;
use Util\TaskQueue\Consumer;

class InstantMessageDB{
    private $imDB;

    public function __construct(){
        $this->imDB = $GLOBALS['redis_instant_message'];
    }

    /**
     * 从队列中返回当前需要处理的注册任务
     * @return string 需要处理的注册任务
     */
    public function pop($callback){
        $consumer = new Consumer();
        $consumer->consume($this->imDB, $callback);

        // $this->redis->select(0);        //选择数据库
        // $result = $this->redis->blPop($this->imDB, 0);
        
        // return $result[1];
    }

    

};