<?php

namespace DB;

use Model\Person;
use Util\RelationParser;
use Util\Util;

class CDBPerson
{
    var $pdo = null;
    public $TABLE = "gener_person";
    public $TABLE_USER = "gener_user";
    public $TABLE_FAMILY = "gener_family";

    public function init()
    {
        if (!$this->pdo) {
            if (!isset($GLOBALS['pdo'])) {
                $this->pdo = new CDBManager($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pwd'], $GLOBALS['db_name']);
                if ($this->pdo)
                    $GLOBALS['pdo'] = $this->pdo;
            } else {
                $this->pdo = $GLOBALS['pdo'];
            }
        }

        return true;
    }

    /**
     * 获取家族的所有人员,供前端家族树使用
     * @param $familyId
     * @return array 所有人员的数组
     */
    public function getFamilyPersons($familyId, int $levelLimit = null, $otherField = [], $orderByBirthday=false)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $levelLimitSql = "";
        if (!is_null($levelLimit)) {
            //如果levelLimit不为空，则查询要通过level过滤
            $levelLimitSql .= " AND tb.level <= $levelLimit ";
        }

        $fieldKey = null;
        $fieldValue =null;
        $orderByBranchNameSql = null;

        if ($otherField){
            foreach ($otherField as $k => $v){
                if ($v){
                    $fieldKey = $k;
                    $fieldValue = $v;
                    break;
                }
            }
            $hasTitlePeopleSql = "select $fieldKey as $fieldValue, person_id as personId,tp.family_index as familyIndex, tp.level 
                                    from gener_person_pdf_title td left join 
                                    (select id,family_index,level,is_delete from $this->TABLE where familyId = $familyId) tp 
                                    on td.person_id = tp.id
                                    where family_id = $familyId and td.is_delete = 0 and tp.is_delete = 0 and {$fieldKey} > '' order by tp.family_index desc";
            $hasTitlePeople = $this->pdo->query($hasTitlePeopleSql);
            print_r($hasTitlePeopleSql."\n");
            $branchNameList = array_reverse(array_column($hasTitlePeople, $fieldValue));
            $branchNameList = array_map(function ($v){return "'$v'";}, $branchNameList);

            if ($branchNameList){
                $orderByBranchNameSql = " order by field($fieldValue, " . implode(',', $branchNameList) . "), tb.level, tb.family_index desc";
            }else{
                $orderByBranchNameSql = " order by tb.level, tb.family_index desc";
            }
//             = $branchNameList . "order by field(";

            if ($hasTitlePeople){
                $pdfTitleSql = " , case ";
                foreach ($hasTitlePeople as $k => $v) {
                    $lastIndex = $this->getLastPersonFamilyIndex($familyId, $v['level'], $v['familyIndex']);
//                    $hasTitlePeople[$k]['lastIndex'];
                    $pdfTitleSql .= " when family_index between {$v['familyIndex']} and {$lastIndex} then '{$v[$fieldValue]}' ";
                }
                $pdfTitleSql .= " else null end as $fieldValue ";
            }else{
                $pdfTitleSql = " , null as $fieldValue ";
            }

        }


//        $pdfTitleJoinSql = $needTitle ? " left join (select  * from gener_person_pdf_title where family_id = $familyId and is_delete = 0) td on td.person_id = tb.id ":"";
        $pdfTitleSql = $otherField ? $pdfTitleSql : "";
        $joinAnotherTable = $fieldKey === 'branch_name' ? " left join (select * from gener_person_pdf_title where is_delete = 0) ta on ta.person_id = tb.id " : "";
//        $filterByAnotherTable = $fieldKey === 'branch_name' ? " and ta.is_delete = 0  " : "";
        $anotherFields = $fieldKey === 'branch_name' ? " ,zan, zan_type as zanType, origin_name as originName " : "";
//        print_r($pdfTitleSql);
        $orderBySql = $orderByBirthday ? " ,IF(ISNULL(tb.birthday),0,1), tb.birthday desc, tb.ranking " : "";
        $sql = "SELECT tb.id,tb.ranking,tb.same_personId as samePersonId,tb.name,tb.zi,tb.remark,tb.type,tb.country_name as countryName,tb.province_name as provinceName,tb.city_name as cityName,tb.area_name as areaName,tb.town_name as townName,tb.address,tb.birthday,tb.gender,tb.phone,tb.photo,tb.share_url as shareUrl,tb.is_dead as isDead,tb.dead_time as deadTime,tb.familyId,
                    tb.ref_familyId as refFamilyId,tb.father,tb.mother,tb.son,tb.daughter,tb.spouse,tb.sister,tb.brother,
                    tb.level,tb.sideText,tb.profileText,tb.userId,tb.create_by as createBy,tb.create_time as createTime,tb.update_by as updateBy,tb.update_time as updateTime, 
                    tb.ref_personId as refPersonId,is_adoption as isAdoption, tb.family_index as familyIndex, 
                    tu.username,
                    tu.photo as userPhoto,
                    tu.nickname,
                    tu.hx_username as hxUsername $pdfTitleSql $anotherFields
                    FROM $this->TABLE tb
                    left join $this->TABLE_USER tu on tb.userId = tu.id $joinAnotherTable
                    WHERE familyId = '$familyId' AND tb.is_delete = '0'  $levelLimitSql" .
                (!$orderByBranchNameSql ? "order by tb.level$orderBySql" : $orderByBranchNameSql);
//        print_r($sql."\n");
        $result = $this->pdo->query($sql);
        print_r($sql."\n");
        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }
        print_r(json_encode($result,JSON_UNESCAPED_UNICODE));
        return $result;
    }

    /**
     * 获取家族的相关信息,供前端家族树使用
     * @param $familyId
     * @return array start_level
     */
    public function getFamilyInfoById($familyId){
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $sql = "SELECT start_level FROM $this->TABLE_FAMILY WHERE id = '$familyId' and is_delete = '0' ";
        return $this->pdo->uniqueResult($sql);
    }



     /**
     * 获取多个家族需要拼接的情况
     * 主家族 A和B 合并
     * @param  personsA 合并的第一个家族
     * @param  personIdA 第一个关联Id
     * @param familyIdB 第二个家族B
     * @param refPersonsStartId B家族的拼接起始人物
     * @return array  拼接完的数组
     * 
     */
    public function getMultiFamilyPersons($familyPeopleA,$personIdA,$familyIdB,$refPersonsStartId,$filter = 0){
        $endIndex = 9999999;

       //  拿出familyPeopleAHead
        $num = 0;
        $flag = 0;
        $deleteNum = 0;
        $personA =  $this->getPersonById2($personIdA);
        foreach($familyPeopleA as $key => $person){
            if($personIdA == $person['id']){
                $num = $key;

         
               for($i = $num;$i<count($familyPeopleA);$i++){
                   // 计算familyPeopleATail
                   if($familyPeopleA[$i]['level']<$personA->level){
                       $deleteNum ++;
                   }else{
                   break;
                   }
               }
           break;
            }elseif ($person['level'] == $personA->level){
               if(in_array($personIdA,$person['brother']) ){
                   $familyPeopleA[$key]['brother'] = Util::array_remove($familyPeopleA[$key]['brother'],$personIdA);
                }elseif( in_array($personIdA,$person['sister'])){
                   $familyPeopleA[$key]['sister'] = Util::array_remove($familyPeopleA[$key]['sister'],$personIdA);
                }
            } 
        }

        
        $familyPeopleAHead =array_slice($familyPeopleA,0,$num);
        $familyPeopleATail = array_slice($familyPeopleA,$deleteNum  + $num+1 ,count($familyPeopleA));


       //  处理B家族
       // 根据PersonId获取startIndex
         $personB =  $this->getPersonById2($refPersonsStartId);
         $familyBFamilyIndex = $this->getLastPersonFamilyIndex($familyIdB,$personB->level,$personB->familyIndex);
         $familyPeopleB = $this->getFamilyPersonsByFamilyIndex($familyIdB,$personB->familyIndex,$familyBFamilyIndex,$filter);
    
       //B 家族人物辈分处理
        if($familyPeopleB != null){
           $level =  $familyPeopleB[0]['level']  -   $personA->level  ;
           foreach($familyPeopleB as $key => $person){
               $familyPeopleB[$key]['level'] -= $level;
           }
          }

       //B 家族人物关系处理
         $familyPeopleB[0]['father'] = $personA->father;
         if($personA ->gender == 0 ){
               $familyPeopleB[0]['sister'] =  Util::array_remove($personA->sister,$personIdA);
               $familyPeopleB[0]['brother'] = $personA->brother;
         }else{
              $familyPeopleB[0]['brother'] =  Util::array_remove($personA->brother,$personIdA);
              $familyPeopleB[0]['sister'] = $personA->sister;
         }

          //  处理Ahead父母的儿子属性
          $parents = array();
          if($filter == 0){
             $parents = array_merge($personA->father,$person['mother']);
          }else{
             $parents = array_merge($personA->father);
          }
          $parentsCount = count($parents);
          $i =count($familyPeopleAHead) - 1 ;
        while($parentsCount>0){
            if(in_array($familyPeopleAHead[$i]['id'],$parents)){
              foreach($familyPeopleAHead[$i]['son'] as $key=>$value){
                  if($value == $personIdA){ 
                          $familyPeopleAHead[$i]['son'][$key]=$familyPeopleB[0]['id']; 
                          $parentsCount--;
                  break;
                  } 
              }
            }
            $i--;
         }


       //    处理Ahead和Atail内兄弟姐妹的属性
     
    //    foreach($familyPeopleAHead as $key  => $person){
    //        if($personA ->level == $person['level']){
    //            if(in_array($personA->id,$person['brother'])){
    //                   $familyPeopleAHead[$key]['brother'] = Util::array_remove($familyPeopleAHead[$key]['brother'],$personIdA);
    //                   $familyPeopleAHead[$key]['brother'] = array_push($familyPeopleAHead[$key]['brother'],$refPersonsStartId);
    //            }else  if(in_array($personA->id,$person['sister'])){
    //                $familyPeopleAHead[$key]['sister'] = Util::array_remove($familyPeopleAHead[$key]['sister'],$personIdA);
    //                $familyPeopleAHead[$key]['sister'] = array_push($familyPeopleAHead[$key]['sister'],$refPersonsStartId);
    //         }
    //        }
    //    }

    //    foreach($familyPeopleATail as $key  => $person){
    //        if($personA ->level == $person['level']){
    //            if(in_array($personA->id,$person['brother'])){
    //                   $familyPeopleATail[$key]['brother'] = Util::array_remove($familyPeopleATail[$key]['brother'],$personIdA);
    //                   $familyPeopleATail[$key]['brother'] = array_push($familyPeopleATail[$key]['brother'],$refPersonsStartId);
    //            }else  if(in_array($personA->id,$person['sister'])){
    //                $familyPeopleATail[$key]['sister'] = Util::array_remove($familyPeopleATail[$key]['sister'],$personIdA);
    //                $familyPeopleATail[$key]['sister'] = array_push($familyPeopleATail[$key]['sister'],$refPersonsStartId);
    //         }
    //        }
    //    }
      
     
   $persons = array_merge($familyPeopleAHead,$familyPeopleB,$familyPeopleATail);
   return $persons;
}


    /**
     * 根据当前人物的level , familyId,familyIndex 查出当前人物深度遍历下最后一人的familyIndex
     * 1. 权限的判断
     * 2.人物添加的位置判断
     * 3. 家族合并被覆盖的人物范围,当前人物的孩子节点数
     * @param familyId
     * @param corePersonLevel
     * @param corePersonFamilyIndex
     * @return int familyIndex
     * @author yuanxin
     * @version 3
     */
    public function getLastPersonFamilyIndex($familyId, $corePersonLevel, $corePersonFamilyIndex): int
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }

        $sql = "SELECT min(family_index) as familyIndex
            FROM $this->TABLE 
               WHERE familyId = $familyId and is_delete = 0 and type = 1 and  level <= $corePersonLevel and  family_index > $corePersonFamilyIndex ";
        $lastPersonFamilyIndex = $this->pdo->query($sql);


        if ($lastPersonFamilyIndex[0]['familyIndex'] == null) {
            $sql = "SELECT family_index as familyIndex FROM $this->TABLE WHERE familyId = $familyId  and is_delete = 0  order by family_index desc limit 1;";
            $lastPersonFamilyIndex = $this->pdo->query($sql);
            return $lastPersonFamilyIndex[0]['familyIndex'];
        } else {
            return $lastPersonFamilyIndex[0]['familyIndex'] - 1;
        }
    }
    
    
        /**
         * 根据起始FamilyIndex 和结束FamilyIndex获取那一段的数据.Filter用于过滤配偶
         * @param familyId 
         * @param startFamilyIndex
         * @param endFamilyIndex
         * @param filter
         * @return persons
         */
    
        public function getFamilyPersonsByFamilyIndex($familyId,$startFamilyIndex,$endFamilyIndex, $filter)
        {
            if (!$this->init()) {
                Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
                exit;
            }
            $filterSql = "";
            if($filter == 1 ){
                $filterSql = "AND type = 1";
            }
    
            $sql = "SELECT   family_index AS familyIndex,id,name,level,father,mother,gender,son,daughter,type,spouse,ranking,birthday,
                            address,profileText,photo,is_adoption as isAdoption,sideText
                            FROM  $this->TABLE 
                            WHERE familyId  = $familyId  AND is_delete = 0 $filterSql AND  family_index   BETWEEN  $startFamilyIndex AND $endFamilyIndex  
                            order by family_index;";
    
            $result = $this->pdo->query($sql);
    
            for ($i = 0; $i < count($result); $i++) {
                $person = new Person($result[$i]);        //转换为person对象
                $parser = new RelationParser($person);
                $result[$i]['father'] = $parser->getFathers();
                $result[$i]['mother'] = $parser->getMothers();
                $result[$i]['son'] = $parser->getSons();
                $result[$i]['daughter'] = $parser->getDaughters();
                $result[$i]['spouse'] = $parser->getSpouses();
                $result[$i]['brother'] = $parser->getBrothers();
                $result[$i]['sister'] = $parser->getSisters();
            }
    
            
            return $result ;
        }

      /**
      * V3 对应的获取人物
     * @param $personId
     * @return object Person对象
     */
    public function getPersonById2($corePersonId)
    {
        if (!$this->init()) {
            //@codeCoverageIgnoreStart
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
            //@codeCoverageIgnoreEnd
        }

        $sql = "SELECT id,same_personId,is_adoption,name,type,level,gender,familyId,userId,father,mother,brother,sister,spouse,son,daughter,family_index AS familyIndex
                        FROM $this->TABLE
                        WHERE id = $corePersonId and is_delete = 0;";
        $queryResult = $this->pdo->uniqueResult($sql);
        if ($queryResult != null) {
            $person = new Person($queryResult);
            $parser = new RelationParser($person);
            $person->father = $parser->getFathers();
            $person->mother = $parser->getMothers();
            $person->son = $parser->getSons();
            $person->daughter = $parser->getDaughters();
            $person->spouse = $parser->getSpouses();
            $person->brother = $parser->getBrothers();
            $person->sister = $parser->getSisters();
            return $person;
        }
        //@codeCoverageIgnoreStar
        return null;
        //@codeCoverageIgnoreEnd
    }


    /**
     * 根据起始FamilyIndex 和结束FamilyIndex获取那一段的数据.Filter用于过滤配偶
     * @param $familyId
     * @param $filter
     * @return mixed
     */

    public function getAllFamilyPersonsOrderByFamilyIndex($familyId,$filter)
    {
        if (!$this->init()) {
            Util::printResult($GLOBALS['ERROR_SQL_INIT'], "SQL初始化失败");
            exit;
        }
        $filterSql = "";
        if($filter == 1 ){
            $filterSql = "AND type = 1";
        }

        $sql = "SELECT   family_index AS familyIndex,id,name,level,father,mother,gender,son,daughter,type,spouse,ranking,birthday,
                            address,profileText,photo,is_adoption as isAdoption,sideText
                            FROM  $this->TABLE 
                            WHERE familyId  = $familyId  AND is_delete = 0 $filterSql
                            order by family_index;";

        $result = $this->pdo->query($sql);

        for ($i = 0; $i < count($result); $i++) {
            $person = new Person($result[$i]);        //转换为person对象
            $parser = new RelationParser($person);
            $result[$i]['father'] = $parser->getFathers();
            $result[$i]['mother'] = $parser->getMothers();
            $result[$i]['son'] = $parser->getSons();
            $result[$i]['daughter'] = $parser->getDaughters();
            $result[$i]['spouse'] = $parser->getSpouses();
            $result[$i]['brother'] = $parser->getBrothers();
            $result[$i]['sister'] = $parser->getSisters();
        }


        return $result ;
    }
}
