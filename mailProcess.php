<?php
/**
 * 从Redis队列中轮询要发送的邮件，并执行发送任务
 *
 */
require_once('vendor/autoload.php');
date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use DB\MailDB;
use Util\Util;

/**
 * 异常处理函数
 * 
 * @param Exception $exception 异常
 * 
 * @return null
 */
function Exception_handler($exception)
{
    global $logger;
    $logger->error('mailProcess:'.Util::exceptionFormat($exception));
    exit;
}

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

/**
 * 在这里注册异常捕获函数，如果出现异常则进行处理
 */
set_exception_handler('Exception_handler');

$mailDB = new MailDB();

$callback = function($msg) use ($logger) {
    $mailData = json_decode($msg->body, true);
    $mailAddress = $mailData['e'];
    $mailBody = $mailData['c'];
    $times = $mailData['t'];
    $mail = new PHPMailer;

    $mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtpdm.aliyun.com';                        // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'system@notify.izuqun.com';            // SMTP username
    $mail->Password = 'IzuqunALi528529';                // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    $mail->CharSet = "utf-8";                           //设置字符集编码
    $mail->setFrom('system@notify.izuqun.com', 'izuqun_mailer');
    $mail->addAddress($mailAddress, '邮箱验证');  // Add a recipient

    $mail->Subject = '邮箱验证';
    $mail->Body    = $mailBody;
    $mail->AltBody = $mailBody;

    if (!$mail->send()) {
        $context['tags'] = array('alert'=>'no');
        $context['filename'] = 'mailProcess';
        $logger->addWarning('Mailer Error: ['. $mailAddress . ']'.  $mail->ErrorInfo,$context);
        return true;        // 这里发送不出去，可能是因为邮箱格式的问题，所以不能返回false
    } else {
        $context['tags'] = array('alert'=>'no');
        $context['filename'] = 'mailProcess';
        $logger->addInfo('['. $mailAddress . ']---发送成功',$context);
        return true;
    }
};

$mailDB->popMail($callback);