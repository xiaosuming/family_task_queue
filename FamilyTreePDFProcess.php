<?php
/**
 * FamilyTreePDFProcess
 * Author: jiangpengfei
 * Date: 2018-8-24
 */
require_once('vendor/autoload.php');

use DB\CDBPdfTask;
use Monolog\Logger;
use Util\Util;
use DB\CDBPerson;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use FamilyTreePDF\Factory\BigEventPaintFactory;
use FamilyTreePDF\Factory\DescPaintFactory;
use FamilyTreePDF\Factory\GravePaintFactory;
use FamilyTreePDF\Factory\PhotoOnePaintFactory;
use FamilyTreePDF\Factory\PhotoTwoPaintFactory;
use FamilyTreePDF\Factory\TreePaintFactory;
use FamilyTreePDF\Paint\PaintContext;
use FamilyTreePDF\Model\EventData;
use FamilyTreePDF\Model\GraveData;
use FamilyTreePDF\Model\PaintData;
use FamilyTreePDF\Model\PhotoData;
use FamilyTreePDF\Util\PDFBuilder;
use FamilyTreePDF\Util\A4PDFBuilder;
use FamilyTreePDF\Util\DataFilter;
use FamilyTreePDF\Factory\StyleConst;
use FamilyTreePDF\Paint\FontManager;
use FamilyTreePDF\Paint\NewPaint\A4Traditional7Level;
use FamilyTreePDF\Paint\newTree\Level7PaintConfig;
use FamilyTreePDF\Util\Level7A4PDFbuilder;

date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);


function Exception_handler($exception)
{
    global $logger;
    $logger->error('FamilyTreePDFProcess:'.Util::exceptionFormat($exception));
    exit;
}

$pdfDB = new CDBPdfTask();
//查阅日志 如果有日志表示上次失败了
if(!is_dir("tempLogFiles")){
    mkdir("tempLogFiles", 0755, true);
}
$file = "tempLogFiles/familyTreePDFProcess.log";
if(!file_exists($file)){
    fopen($file, 'w');
    fclose($file);
}else{
    $lastTaskContent = file_get_contents($file);
    if(!empty($lastTaskContent)){
        $lastTaskArray = json_decode($lastTaskContent, true);
        foreach($lastTaskArray as $id => $detail){
            $pdfDB->updatePdfFailed($id, $detail);
        }
        $empty = '';
        file_put_contents($file, $empty, LOCK_EX);//清空文件内容
    }
}


$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

set_exception_handler('Exception_handler');

$personDB = new CDBPerson();

$callback = function($msg) use ($pdfDB, $personDB) {
    $pdfTask = json_decode($msg->body, true);

    $taskId = $pdfTask['t'];

    $file = __DIR__."/tempLogFiles/familyTreePDFProcess.log";
    $newTaskRecord = [$taskId => ''];
    file_put_contents($file, json_encode($newTaskRecord), LOCK_EX);//添加当前任务id

    $percent = "5%";
    $status = "正在验证参数";

    $pdfDB->updatePdfWorking($taskId, $percent, $status);

    // 根据taskId查询
    $taskDetail = $pdfDB->getFamilyPdfTaskDetail($taskId);

    if ($taskDetail == null) {
        $newTaskRecord[$taskId] = "error taskId";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);//记录错误信息
        return true;
    }

    $familyId = $taskDetail['familyId'];
    $titleText = $taskDetail['familyTreeName'];


    $eventIds = json_decode($taskDetail['eventIds'], true);
    $photoIdOnes = json_decode($taskDetail['photoIdOnes']);
    $photoIdTwos = json_decode($taskDetail['photoIds']);
    $graveIds = json_decode($taskDetail['graveIds']);
    $direction = $taskDetail['direction'];
    $hd = $taskDetail['isHD'];              // 是否高清
    $options = json_decode($taskDetail['options'], true);
    $hallName = $options['hallName'];            //堂号


    $blankImg = $options['blankImg']??1;  //中缝样式


    //添加封面图 传参
    $surname = $options['surname'];
    $coverId = $options['coverId'];
    $pinyin = $options['pinyin'] ?? '';
    //end

    # 是否显示嗣子
    $options['showAdoption'] = $options['showAdoption'] ? true:false;
    $showAdoption = $options['showAdoption'] ? true:false;
    # end


    # 欧式版2的上下更大间距
    $moreSpaceBetweenTopAndBottom = false;
//    $moreSpaceBetweenTopAndBottom = $options['moreSpaceBetweenTopAndBottom'] ? true:false;
    # end

    //添加支系谱名 全局
    $ChildName = $options['ChildName'] ?? '';
    //end

    //添加分页成册页数 全局
    $pageLength = $options['pageLength'] ?? '';
    //end

    //添加是否导出吊线图 全局
    $addTree = $options['addTree'] ?? true;
    //end

    //同一世代是否按出生日期从小到大排序全局
    $orderByBirthday = $options['orderByBirthday'] ?? false;

    //添加是否仅导出吊线图 全局
    $onlyTree = $options['onlyTree'] ?? false;
    if($onlyTree == true){
        // 如果仅导出吊线图, 那么强制设置addTree 为true
        $addTree = true;
    }
    /**********************************版本3新加开始**************************************************** */
    // 多家族导出状态
    $multiOutput = false;
    $refFamilys = $options['refFamilys'] ?? '';
    $refPersons = $options['refPersons'] ?? '';
    $refStartPersonId = $options['refStartPersonId'] ?? '';
    if($refFamilys  != ''  ){
        $multiOutput = true;
    }



    /**********************************版本3新加结束**************************************************** */

    //end

    if (isset($options['template'])) {
        $template = $options['template'];
        $treeTemplate = $template['tree'];
        $descTemplate = $template['desc'];
        $bigEventTemplate = $template['bigEvent'];
        $photoOneTemplate = $template['photoOne'];
        $photoTwoTemplate = $template['photoTwo'];
        $graveTemplate = $template['grave'];
    } else {
        $treeTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
        $descTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
        $bigEventTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
        $photoOneTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
        $photoTwoTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
        $graveTemplate = StyleConst::$PAINT_STYLE_TRADITIONAL_SU;
    }

    // $pageSize支持A3, A4
    $pageSize = StyleConst::$PAINT_PAGESIZE_A3;
    if (isset($options['pageSize'])) {
        $pageSize = $options['pageSize'];
    }
    print_r([185, $pageSize]);
    // 正文字体的选择
    $font = null;
    if (isset($options['font'])) {
        $font = FontManager::getFont($options['font']);
    } else {
        $font = FontManager::getFont(FontManager::$FONT_DEFAULT);
    }

    $disablePageNum = false;
    if (isset($options['disablePageNum'])) {
        $disablePageNum = $options['disablePageNum'] == 1;
    }

    $startPersonId = $taskDetail['startPersonId'];
    var_dump($startPersonId);


    // 初始化文件夹
    \FamilyTreePDF\Util\Util::initDir();

    // 初始化context
    $context = new PaintContext();
    $context->setOptions($options);

    // $persons = $personDB->getFamilyPersons($familyId);
    $generation = $options['generation'] ?? 8;
    try {

    /***************************************版本三改动开始************************************ */
    if($treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL ||  $treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL_WithIndentation){
        if ($startPersonId == 0){
            $persons = $personDB->getAllFamilyPersonsOrderByFamilyIndex($familyId,1);
        }else{
            $startPerson = $personDB->getPersonById2($startPersonId);
            $lastFamilyIndex = $personDB->getLastPersonFamilyIndex($startPerson->familyId,$startPerson->level,$startPerson->familyIndex);
            $persons  =  $personDB->getFamilyPersonsByFamilyIndex($familyId,$startPerson->familyIndex,$lastFamilyIndex,1);
        }
        if ($generation < 2){
            $generation = 5;
        }elseif($generation>20){
            $generation = 20;
        }
    }
    else{
        # 如果是新版欧式2，需要加上title字段
        $needTitle = $descTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU2 ? true:false;
        $needBranchName = $descTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU3 ? true:false;
        if (!$needBranchName){
            $needBranchName = $treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL7;
        }
        $otherField = [];
        $needTitle ? $otherField['title'] = 'title' : 0 ;
        $needBranchName ? $otherField['branch_name'] = 'branchName' : 0 ;
//        $otherField = [
//            'title' => $needTitle ? 'title' : false,
//            'branch_name' => $needBranchName ? 'branchName': false,
//        ];
//        ];
        $persons = $personDB->getFamilyPersons($familyId,null, $otherField, $orderByBirthday);
        $personsData = $persons;
    }
    }catch (Exception $e){
        print_r($e);
    }

    /***************************************版本三改动结束************************************ */

    $familyInfo = $personDB->getFamilyInfoById($familyId);
    $startLevel = $familyInfo['start_level'] ?? 1;
    // 启用数据过滤
    if (count($persons) == 0) {
        return true;
    }
    $dataFilter = new DataFilter($persons);
    //添加startPersonId 和 isShowStartPersonFathers
    $dataFilter->setOptions($options);
    if ($startPersonId != 0) {
        $dataFilter->setStartPersonId($startPersonId);
    }
    $persons = $dataFilter->getPersonSet();

    //更新最新记录
    $newTaskRecord[$taskId] = "steps: 获取参数 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

    $percent = "10%";
    $status = "正在实例化绘制吊线图";

    $pdfDB->updatePdfWorking($taskId, $percent, $status);
    // 吊线图实例化
    if($treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL7){
//        $direction = SysConst::$RIGHT_TO_LEFT;
        $direction = 1;

        $isHD = true;

        $isPageNumDisabled = false;
        $branchPersonMap = $dataFilter->preInput($persons);

        $config = \FamilyTreePDF\Paint\newTree\Level7PaintConfig::getHDR2LConfig();

        $branchPage=array();

        $page =1;
        foreach ($branchPersonMap as $k=>$v){
            $branchPage[$k][]=$page;

            $TreePaint = new A4Traditional7Level();
            $TreePaint->setContext($context);
            $TreePaint->setConfig($config);
            $TreePaint->setDirection($direction);
            $TreePaint->input($v,$page);

            $page += $TreePaint->paint(1,$page,$k);

            $branchPage[$k][]=$page-1;
            //$page++;
        }


        var_dump("page".$page);
        $newTaskRecord[$taskId] .= " 吊线图实例化 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        //更新最新记录
        var_dump(" 合并pdf ");


        $newTaskRecord[$taskId] .= " 合并pdf ".date("Y-m-d H:i:s") . "；";

        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "60%";
        $status = "正在生成pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);
        $others['ChildName'] = $ChildName;
        $builder = new Level7A4PDFbuilder($direction, $isHD, false);

        $builder->toCover($surname, $coverId, $pinyin, $others);
        $total = $builder->merge($titleText, $branchPage , $hallName);
        $builder->toPdf($total);
        $builder->clear();


        $percent = "70%";
        var_dump(" 正在上传预览版pdf ");

        $status = "正在上传预览版pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        $client = new StorageFacadeClient();
        $client->setBucket(StorageBucket::$STORAGE);

        //上传预览版
        $num = 0;
        $previewPdf = [];
        while($num < $page){
            if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf")){
                echo "preview_".$num.".pdf 文件不存在\n";
                break;
            }
            $status = 0;
            while($status < 3){//重新上传三次
                $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf", 'pdf');
                if($fileInfo->success){
                    $previewPdf[$num] = $fileInfo->location;
                    $time = date('h:i:s');
                    echo "上传预览版:print_$num.pdf，$time\n";
                    break;
                }
                $status++;
            }

            $num++;
        }
        $previewPdf = json_encode($previewPdf);

        //更新最新记录
        $newTaskRecord[$taskId] .= " 上传预览版 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "85%";
        var_dump(" 正在上传打印版pdf ");
        $status = "正在上传打印版pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        //上传打印版
        $num = 0;
        $printPdf = [];
        while($num < $page ){

            if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf")){
                echo "preview_".$num.".pdf 文件不存在\n";
                break;
            }
            $status = 0;
            while($status < 3){//重新上传三次
                $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf", 'pdf');
                if($fileInfo->success){
                    $printPdf[$num] = $fileInfo->location;
                    $time = date('h:i:s');
                    echo "上传打印版:print_$num.pdf，$time\n";
                    break;
                }
                $status++;
            }

            $num++;
        }
        $printPdf = json_encode($printPdf);

        //更新最新记录
        $newTaskRecord[$taskId] .= " 上传打印版 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $pdfDB->updatePdf($taskId, $previewPdf, $printPdf);

        file_put_contents($file, '', LOCK_EX);//成功完成pdf任务，清空文件内容

        return true;

    }
    $treePaint = TreePaintFactory::getTreePaint($treeTemplate, $direction, $hd);


//    $treePaint->setContext($context);
//    $treePaint->setDirection($direction);
    $treePaint->setFont($font);


    if ($treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL || $treeTemplate ==  StyleConst::$PAINT_STYLE_TRADITIONAL_LEVEL_WithIndentation){
        var_dump(" 吊线图实例化 ");
        $treePaint->setGeneration($generation);
        $treePaint->input($persons);
        $treePaint->setFamilyName($titleText);
        $treePaint->setGeneration($generation);
        $countPDF = $treePaint->paint();
        var_dump("page".$countPDF);
        $newTaskRecord[$taskId] .= " 吊线图实例化 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        //更新最新记录
        var_dump(" 合并pdf ");


        $newTaskRecord[$taskId] .= " 合并pdf ".date("Y-m-d H:i:s") . "；";

        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "60%";
        $status = "正在生成pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);
        $builder = new PDFBuilder($direction, $hd);
        $others['ChildName'] = $ChildName;
        $builder->toCover($surname, $coverId, $pinyin, $others);
        $builder->toLevelPdf(0, $countPDF);
        $builder->clear();


        $percent = "70%";
        var_dump(" 正在上传预览版pdf ");

        $status = "正在上传预览版pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        $client = new StorageFacadeClient();
        $client->setBucket(StorageBucket::$STORAGE);

        //上传预览版
        $num = 0;
        $previewPdf = [];
        while($num < $countPDF){
            if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf")){
                echo "preview_".$num.".pdf 文件不存在\n";
                break;
            }
            $status = 0;
            while($status < 3){//重新上传三次
                $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf", 'pdf');
                if($fileInfo->success){
                    $previewPdf[$num] = $fileInfo->location;
                    $time = date('h:i:s');
                    echo "上传预览版:print_$num.pdf，$time\n";
                    break;
                }
                $status++;
            }

            $num++;
        }
        $previewPdf = json_encode($previewPdf);

        //更新最新记录
        $newTaskRecord[$taskId] .= " 上传预览版 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "85%";
        var_dump(" 正在上传打印版pdf ");
        $status = "正在上传打印版pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        //上传打印版
        $num = 0;
        $printPdf = [];
        while($num < $countPDF ){

            if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf")){
                echo "preview_".$num.".pdf 文件不存在\n";
                break;
            }
            $status = 0;
            while($status < 3){//重新上传三次
                $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf", 'pdf');
                if($fileInfo->success){
                    $printPdf[$num] = $fileInfo->location;
                    $time = date('h:i:s');
                    echo "上传打印版:print_$num.pdf，$time\n";
                    break;
                }
                $status++;
            }

            $num++;
        }
        $printPdf = json_encode($printPdf);

        //更新最新记录
        $newTaskRecord[$taskId] .= " 上传打印版 ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $pdfDB->updatePdf($taskId, $previewPdf, $printPdf);

        file_put_contents($file, '', LOCK_EX);//成功完成pdf任务，清空文件内容

        return true;

    }else{
        $treePaint->setContext($context);
        $treePaint->setDirection($direction);
    }

    if ($treeTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU) {
        if (isset($options['mottos'])) {
            $treePaint->setMottos($options['mottos']);
        }
    } else {
        $treePaint->setMottos(['    ']);
    }
//    var_dump(sizeof($persons));
//exit;

    $treePaint->input($persons);

    //更新最新记录
    $newTaskRecord[$taskId] .= " 吊线图实例化 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);
    $percent = "20%";
    $status = "正在实例化绘制行传";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);
    // 行传实例化
    $descPaint = DescPaintFactory::getDescPaint($descTemplate, $direction, $hd);
    if($addTree !== true){
        # 去掉吊线图
        $context->setTotalPage(0);
    }

    $descPaint->setContext($context);
    $descPaint->setDirection($direction);
    $descPaint->setFont($font);
    $descPaint->setPageNum($disablePageNum);
//    var_dump($disablePageNum);
    if($onlyTree == true){
        // TODO 此处需要做一些修改
        $context->setTotalPage(0);
    }else{
        if ($descTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU3){
            $descPaint->input($personsData, true);
        }elseif ($descTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU2){
            $descPaint->input($personsData, $orderByBirthday);
        }

        $descPaint->paint($startLevel);//世代详情中，添加自定义初始世代
    }

    if($addTree === true){
        $treePage = $treePaint->paint($startLevel);//绘制树中，添加自定义初始世代
    }

    //更新最新记录
    $newTaskRecord[$taskId] .= " 行传实例化 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);
    $percent = "30%";
    $status = "正在绘制大事件";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);

    // 大事件的绘制
    if (count($eventIds) > 0) {
        $events = $pdfDB->getEventsByEventIds($eventIds);

        $eventDatas = [];
        foreach($events as $event) {
            $eventData = new EventData($event['expName'], $event['expContent'], $event['expTime']);
            $eventDatas[] = $eventData;
        }

        $bigEventPaint = BigEventPaintFactory::getBigEventPaint($bigEventTemplate, $direction, $hd);
        $bigEventPaint->setContext($context);
        $bigEventPaint->setFont($font);
        $bigEventPaint->input($eventDatas);
        $bigEventPaint->paint();
    }

    //更新最新记录
    $newTaskRecord[$taskId] .= " 大事件的绘制 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

    $percent = "40%";
    $status = "正在绘制相册";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);
    // 一页一张的相册绘制
    if (count($photoIdOnes) > 0) {
        $photoOnes = $pdfDB->getPhotosByPhotoIds($photoIdOnes);

        $photoOnePaint = PhotoOnePaintFactory::getPhotoOnePaint($photoOneTemplate, $direction, $hd);
        $photoOnePaint->setContext($context);
        foreach($photoOnes as $photo) {
            $photoData = new PhotoData($photo['photo'], [], $photo['description'], $photo['address']);
            $photoOnePaint->input($photoData);
            $photoOnePaint->paint();
        }
    }

    // 一页两张的相册绘制
    if (count($photoIdTwos) > 0) {
        $photoTwos = $pdfDB->getPhotosByPhotoIds($photoIdTwos);
        $photoTwoPaint = PhotoTwoPaintFactory::getPhotoTwoPaint($photoTwoTemplate, $direction, $hd);
        $photoTwoPaint->setContext($context);

        $len = count($photoTwos);
        $photoDatas = [];
        for($photoI = 0; $photoI < $len; $photoI += 2) {
            $photoOne = $photoTwos[$photoI];
            $photoDataOne = new PhotoData($photoOne['photo'], [], $photoOne['description'], $photoOne['address']);
            $photoDatas[] = $photoDataOne;

            if ($photoI + 1 < $len) {
                $photoTwo = $photoTwos[$photoI+1];
                $photoDataTwo = new PhotoData($photoTwo['photo'], [], $photoTwo['description'], $photoTwo['address']);
                $photoDatas[] = $photoDataTwo;
            }

            $photoTwoPaint->input($photoDatas);
            $photoTwoPaint->paint();
        }
    }

    //更新最新记录
    $newTaskRecord[$taskId] .= " 相册绘制 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);
    $percent = "50%";
    $status = "正在绘制宗祠";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);
    if (count($graveIds) > 0) {

        $graves = $pdfDB->getGravesByGraveIds($graveIds);

        $gravePaint = GravePaintFactory::getGravePaint($graveTemplate, $direction, $hd);
        $gravePaint->setContext($context);
        $gravePaint->setFont($font);

        foreach($graves as $grave) {
            $photo = null;
            $gravePhotos = json_decode($grave['photo'], true);
            if (count($gravePhotos) > 0) {
                $photo = $gravePhotos[0];
            }
            $personName = $grave['person_name'];
            $address = $grave['address'];
            $desc = $grave['description'];

            $graveData = new GraveData($photo, $personName, $desc, $address);
            $gravePaint->input($graveData);
            $gravePaint->paint();
        }
    }
    $others['ChildName'] = $ChildName;
    if ($pageSize == StyleConst::$PAINT_PAGESIZE_A3) {
        var_dump('A3');
        $percent = "55%";
        $status = "正在合并绘制结果";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        $builder = new PDFBuilder($direction, $hd);
        $totalPage = $builder->merge($titleText, $ChildName,$ChildName,$blankImg,$hallName);

        //更新最新记录
        $newTaskRecord[$taskId] .= " 合并pdf ".date("Y-m-d H:i:s") . "；";
        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "60%";
        $status = "正在生成pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        $builder->toCover($surname, $coverId, $pinyin, $others);
        $countPDF = $builder->toPdf($totalPage, $pageLength);
        $builder->destoryGlobals();
        $builder->clear();
    } else {
        var_dump('A4');
        $percent = "55%";
        $status = "正在合并绘制结果";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);


        $builder = new A4PDFBuilder($direction, $hd, $moreSpaceBetweenTopAndBottom,$treePage);
        $totalPage = $builder->merge($titleText, $ChildName);
        $builder->toCover($surname, $coverId, $pinyin, $others);
        $countPDF = $builder->toPdf($totalPage, $pageLength);
        //更新最新记录

        $newTaskRecord[$taskId] .= " 合并pdf ".date("Y-m-d H:i:s") . "；";

        file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

        $percent = "60%";
        $status = "正在生成pdf";
        $pdfDB->updatePdfWorking($taskId, $percent, $status);

        if ($descTemplate == StyleConst::$PAINT_STYLE_TRADITIONAL_OU3){
            $builder->destoryGlobals();
        }
        $builder->clear();
    }

    //更新最新记录
    $newTaskRecord[$taskId] .= " 生成pdf ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

    $percent = "70%";
    $status = "正在上传预览版pdf";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);

    $client = new StorageFacadeClient();
    $client->setBucket(StorageBucket::$STORAGE);

    //上传预览版
    $num = 0;
    $previewPdf = [];
    while($num < $countPDF){

        if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf")){
            echo "preview_".$num.".pdf 文件不存在\n";
            break;
        }
        $status = 0;
        while($status < 3){//重新上传三次
            $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/preview_".$num.".pdf", 'pdf');
            if($fileInfo->success){
                $previewPdf[$num] = $fileInfo->location;
                $time = date('h:i:s');
                echo "上传预览版:print_$num.pdf，$time\n";
                break;
            }
            $status++;
        }

        $num++;
    }
    $previewPdf = json_encode($previewPdf);

    //更新最新记录
    $newTaskRecord[$taskId] .= " 上传预览版 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

    $percent = "85%";
    $status = "正在上传打印版pdf";
    $pdfDB->updatePdfWorking($taskId, $percent, $status);

    //上传打印版
    $num = 0;
    $printPdf = [];
    while($num < $countPDF){

        if(!file_exists(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf")){
            echo "preview_".$num.".pdf 文件不存在\n";
            break;
        }
        $status = 0;
        while($status < 3){//重新上传三次
            $fileInfo = $client->upload(__DIR__."/util/FamilyTreePDF/src/output/pdfoutput/print_".$num.".pdf", 'pdf');
            if($fileInfo->success){
                $printPdf[$num] = $fileInfo->location;
                $time = date('h:i:s');
                echo "上传打印版:print_$num.pdf，$time\n";
                break;
            }
            $status++;
        }

        $num++;
    }
    $printPdf = json_encode($printPdf);

    //更新最新记录
    $newTaskRecord[$taskId] .= " 上传打印版 ".date("Y-m-d H:i:s") . "；";
    file_put_contents($file, json_encode($newTaskRecord, JSON_UNESCAPED_UNICODE), LOCK_EX);

    $pdfDB->updatePdf($taskId, $previewPdf, $printPdf);

    file_put_contents($file, '', LOCK_EX);//成功完成pdf任务，清空文件内容

    return true;
};

$pdfDB->getFamilyPdfTask($callback);
