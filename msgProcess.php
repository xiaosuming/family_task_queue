<?php
/**
 * 从Redis队列中轮询要发送的短信，并执行发送任务
 *
 */
require_once('vendor/autoload.php');
require_once('smsapi/chuanglan/ChuanglanSmsApi.php');
date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use DB\PhoneDB;
use Util\Util;
//use smsapi\YiMei\yimeiApi;

/**
 * 异常处理函数
 *
 * @param Exception $exception 异常
 *
 * @return null
 */
function Exception_handler($exception)
{
    global $logger;
    $logger->error('msgProcess:'.Util::exceptionFormat($exception));
    exit;
}

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

/**
 * 在这里注册异常捕获函数，如果出现异常则进行处理
 */
set_exception_handler('Exception_handler');


$selectCount = 0;
$company = "";      //使用的短信公司

/**
 * 获得当前时间的正确时间戳格式，格式为MMddHHmmss
 */
function formatTime()
{
    return date('mdHis');
}


/**
 * 获得当前的毫秒值，因为smsid不能为空，所以使用此数值
 *
 */
function getMillisecond()
{
    list($s1, $s2) = explode(' ', microtime());
    return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
}

/**
 * 创蓝发送短信
 *
 * @param string $phone   手机号
 * @param string $msg_str 消息内容
 * @param string $order   序号
 *
 * @return string 发送结果
 */
function chuanglanSend($phone, $msg_str, $order)
{
    global $company;
    global $logger;
    $company = "chuanglan";
    $clapi  = new ChuanglanSmsApi();

    //目前只支持国内手机号
    if (substr($phone, 0, 2) != "86") {
        $phone = "86".$phone;
    }

    $result = $clapi->sendInternational($phone, '【爱族群】短信验证码为：'.$msg_str.'，5分钟内有效，短信验证序号'.$order);

    $log = array('company' => $company,'phone' => $phone,'order' => $order, 'msg_str' => $msg_str, 'result' => $result);
    $logger->addInfo("发送短信:", $log);

    return $result;
}

/**
 * 容润发送短信
 *
 * @param string $phone   手机号
 * @param string $msg_str 消息内容
 * @param string $order   序号
 *
 * @return string 发送结果
 */
function rongrunSend($phone, $msg_str, $order)
{
    global $company;
    global $logger;
    $company = "rongrun";
    $timestamp = formatTime(); //时间戳
    $username = "xinhuo1"; //用户名
    $pwd = "xinhuo_yx_187187"; //密码

    $post_data = array();
    $post_data['UserName'] = $username;
    $post_data['Key'] = md5($username.$pwd.$timestamp);
    $post_data['Timestemp'] = $timestamp;
    $post_data['Mobiles'] = $phone;
    $post_data['Content'] = urlencode('【爱族群】短信验证码为：'.$msg_str.'，5分钟内有效，短信验证序号'.$order);
    $post_data['CharSet'] = "utf-8";
    $post_data['SchTime'] = "";
    $post_data['Priority'] = "5";
    $post_data['PackID'] = "";
    $post_data['PacksID'] = "";
    $post_data['ExpandNumber'] = "";
    $post_data['SMSID'] = getMillisecond();//long型数据，此处案例使用了当前的毫秒值，也可根据实际情况进行处理
    $url='http://www.rdsmsvip.com:3077/Platform_Http_Service/servlet/SendSms';
    $o="";

    foreach ($post_data as $k=>$v) {
        $o.= "$k=".$v."&";
    }
    $post_data=substr($o, 0, -1);
    $this_header = array("content-type: application/x-www-form-urlencoded;charset=UTF-8");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);//返回相应的标识，具体请参考我方提供的短信API文档
    curl_close($ch);

    $log = array('company' => $company,'phone' => $phone,'order' => $order, 'msg_str' => $msg_str, 'result' => $result);
    $logger->addInfo("发送短信:", $log);

    return $result;     //返回处理结果
}

/**
 * 阿里发送短信
 *
 * @param string $phone   手机号
 * @param string $msg_str 消息内容
 * @param string $order   序号
 *
 * @return string 发送结果
 */
function aliSend($phone, $msg_str, $order)
{
    global $company;
    global $logger;
    $company = "ali";

    $ch = curl_init();
    $post_data = [];
    $post_data['vcode'] = $msg_str;
    $post_data['order'] = $order;
    $post_data['phone'] = $phone;
//    $url='http://172.17.0.1:8999/api/v1/message/send';
    $url = "http://{$GLOBALS['docker_inet_addr']}:8999/api/v1/message/send";

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    // POST数据

    curl_setopt($ch, CURLOPT_POST, 1);

    // 把post的变量加上

    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

    $result = curl_exec($ch);

    curl_close($ch);

    print_r($result);

    $log = array('company' => $company,'phone' => $phone,'order' => $order, 'msg_str' => $msg_str, 'result' => $result);
    $logger->addInfo("发送短信:", $log);

    return $result;     //返回处理结果
}

/**
 * 亿美 发短信
 */
function yimeiSend($phone, $msg_str, $order)
{
    require_once('smsapi/YiMei/yimeiApi.php');
    require_once('smsapi/YiMei/MagicCrypt.php');
    $yimeiApi = new yimeiApi();
    return $yimeiApi->yimeiSend($phone, $msg_str, $order);
}
/**
 * 发送短信
 *
 * @param string $phone   手机号
 * @param string $msg_str 消息内容
 * @param string $order   序号
 *
 * @return string 发送结果
 */
function sendMsg($phone, $msg_str, $order)
{
    global $selectCount;
    global $logger;
    global $company;

    $randNum = $selectCount;

    //使用亿美短信
    //  $resobj = yimeiSend($phone, $msg_str, $order);
    // if($resobj->result == 'SUCCESS'){
    //      return true;
    //  }

    if ($randNum % 10 <= 0) {    //创蓝(253)的短信发送
        //3
        //9
        $selectCount++;
        $result = chuanglanSend($phone, $msg_str, $order);
        $result_json = json_decode($result, true);
        if ($result_json['code'] == 109) {  //没钱了
            return rongrunSend($phone, $msg_str, $order);
        } else {
            return $result;
        }
        return $result;
    } elseif ($randNum % 10 >=1 && $randNum % 10 < 4) {
        //0,1,2
        $selectCount++;
        $result = rongrunSend($phone, $msg_str, $order);
        $result_json = json_decode($result, true);
        if ($result_json['result'] == -2009) {    //没钱了
            return chuanglanSend($phone, $msg_str, $order);
        } else {
            return $result;
        }
    }elseif ($randNum % 10 >=4) {
        //3,4,5,6,7,8
        $selectCount++;
        $result = aliSend($phone, $msg_str, $order);
        $result_json = json_decode($result, true);
        if (in_array($result_json['data']['Message'], ['isv.AMOUNT_NOT_ENOUGH', 'isv.OUT_OF_SERVICE'])) {    //没钱了
            return chuanglanSend($phone, $msg_str, $order);
        } else {
            return $result;
        }
    }
}



$phoneDB = new PhoneDB();

$callback = function ($rabbitMsg) {
    $pos = stripos($rabbitMsg->body, ",");
    $phone = substr($rabbitMsg->body, 0, $pos);
    $msg = substr($rabbitMsg->body, $pos+1);

    $orderPos = stripos($msg, "|");
    $msg_str = substr($msg, 0, $orderPos);
    $order = substr($msg, $orderPos+1);

    //发送短信
    sendMsg($phone, $msg_str, $order);

    return true;
};

$phoneDB->popMessage($callback);

