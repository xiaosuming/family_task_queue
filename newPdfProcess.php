<?php
/**
 * 于2019-01-23号弃用
 * 新版的pdf导出系统
 * Author: jiangpengfei
 * Date: 2018-8-24
 */
require_once('vendor/autoload.php');

use DB\CDBPdfTask;
use Monolog\Logger;
use Util\Util;
use DB\CDBPerson;
use Util\FamilyTree;
use Util\FamilyTree\DataFilter;
use Util\Storage\StorageFacadeClient;
use Util\Storage\StorageBucket;
use Util\FamilyTree\PhotoData;
use Util\FamilyTree\GraveData;
use Util\FamilyTree\PaintConfig;

date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);


function Exception_handler($exception)
{
    global $logger;
    $logger->error('newPdfProcess:'.Util::exceptionFormat($exception));
    exit;
}

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

set_exception_handler('Exception_handler');

$pdfDB = new CDBPdfTask();
$personDB = new CDBPerson();

$callback = function($msg) use ($pdfDB, $personDB) {
    $pdfTask = json_decode($msg->body, true);   //这里将json递归深度限制为1

    $taskId = $pdfTask['t'];

    // 根据taskId查询
    $taskDetail = $pdfDB->getFamilyPdfTaskDetail($taskId);

    if ($taskDetail == null) {
        return true;
    }

    $familyId = $taskDetail['familyId'];
    $titleText = $taskDetail['familyTreeName'];
    $eventIds = json_decode($taskDetail['eventIds'], true);
    $photoIdOnes = json_decode($taskDetail['photoIdOnes']);
    $photoIdTwos = json_decode($taskDetail['photoIds']);
    $graveIds = json_decode($taskDetail['graveIds']);
    $direction = $taskDetail['direction'];
    $hd = $taskDetail['isHD'];              // 是否高清
    $options = json_decode($taskDetail['options'], true);

    $startPersonId = $taskDetail['startPersonId'];

    if ($direction == 1) {
        if ($hd == 0) {
            $config = PaintConfig::getR2LConfig();
        } else {
            $config = PaintConfig::getHDR2LConfig();
        }
    } else {

        if ($hd == 0) {
            $config = PaintConfig::getL2RConfig();
        } else {
            $config = PaintConfig::getHDL2RConfig();
        }
    }

    $familyTree = new FamilyTree($config);
    $familyTree->initDir();
    $familyTree->setOptions($options);
    $persons = $personDB->getFamilyPersons($familyId);

    // 启用数据过滤
    $dataFilter = new DataFilter($persons);
    $dataFilter->setOptions($options);
    if ($startPersonId != 0) {
        $dataFilter->setStartPersonId($startPersonId);
    }
    $persons = $dataFilter->getPersonSet();

    $familyTree->setDirection($direction);
    $familyTree->input($persons);
    $familyTree->paintDesc();       // 绘制描述页
    $familyTree->paintTree();       // 绘制族谱

    if (count($eventIds) > 0) {
        $events = $pdfDB->getEventsByEventIds($eventIds);
        $familyTree->paintBigEvent($events);   // 绘制大事件
    }

    if (count($photoIdOnes) > 0) {  // 一页一张的相册
        $photoOnes = $pdfDB->getPhotosByPhotoIds($photoIdOnes);

        foreach($photoOnes as $photo) {
            $photoData = new PhotoData($photo['photo'], [], $photo['description'], $photo['address']);
            $familyTree->paintOnePhoto($photoData);
        }
    }

    if (count($photoIdTwos) > 0) {  // 一页两张的相册
        $photoTwos = $pdfDB->getPhotosByPhotoIds($photoIdTwos);

        $len = count($photoTwos);
        $photoDatas = [];
        for($photoI = 0; $photoI < $len; $photoI += 2) {
            $photoOne = $photoTwos[$photoI];
            $photoDataOne = new PhotoData($photoOne['photo'], [], $photoOne['description'], $photoOne['address']);
            $photoDatas[] = $photoDataOne;

            if ($photoI + 1 < $len) {
                $photoTwo = $photoTwos[$photoI+1];
                $photoDataTwo = new PhotoData($photoTwo['photo'], [], $photoTwo['description'], $photoTwo['address']);
                $photoDatas[] = $photoDataTwo;
            }

            $familyTree->paintTwoPhoto($photoDatas);
        }
    }

    if (count($graveIds) > 0) {

        $graves = $pdfDB->getGravesByGraveIds($graveIds);

        foreach($graves as $grave) {
            $photo = null;
            $gravePhotos = json_decode($grave['photo'], true);
            if (count($gravePhotos) > 0) {
                $photo = $gravePhotos[0];
            }
            $personName = $grave['person_name'];
            $address = $grave['address'];
            $desc = $grave['description'];

            $graveData = new GraveData($photo, $personName, $desc, $address);
            $familyTree->paintGrave($graveData);
        }
    }

    $familyTree->merge($titleText);
    $familyTree->toPdf();
    $familyTree->clear();

    $client = new StorageFacadeClient();
    $client->setBucket(StorageBucket::$STORAGE);
    $fileInfo = $client->upload(__DIR__."/util/FamilyTree/pdfoutput/preview.pdf", 'pdf');

    if (!$fileInfo->success) {
        echo "预览文件上传错误";
        return false;
    }
    $previewPdf = $fileInfo->location;

    $fileInfo = $client->upload(__DIR__."/util/FamilyTree/pdfoutput/print.pdf", 'pdf');

    if (!$fileInfo->success) {
        echo "打印文件上传错误";
        return false;
    }
    $printPdf = $fileInfo->location;
    $pdfDB->updatePdf($taskId, $previewPdf, $printPdf);

    return true;
};

$pdfDB->getFamilyPdfTask($callback);
