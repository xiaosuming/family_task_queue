<?php
/**
 * 从Redis队列中轮询要执行的任务，并执行推送
 *
 */
require_once('vendor/autoload.php');
date_default_timezone_set("Asia/Shanghai");
ini_set("default_socket_timeout", -1);

use DB\InstantMessageDB;
use ThirdParty\InstantMessage;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\NativeMailerHandler;
use Util\Util;

/**
 * 异常处理函数
 * 
 * @param Exception $exception 异常
 * 
 * @return null
 */
function Exception_handler($exception)
{
    global $logger;
    $logger->error('IMProcess:'.Util::exceptionFormat($exception));
    exit;
}

$client = new Raven_Client('http://51f938a7db92459086a3f315e889ccee:bf2e982b88a7407b986cdda1e75c98ee@test.xinhuotech.com:8080/2');
$logger = new Logger("logger");         //用来记录全局的异常警告日志
$handler = new Monolog\Handler\RavenHandler($client);
$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
$logger->pushHandler($handler);

$GLOBALS['logger'] = $logger;

/**
 * 在这里注册异常捕获函数，如果出现异常则进行处理
 */
set_exception_handler('Exception_handler');

$imClient = new InstantMessage();
$imDB = new InstantMessageDB();

$callback = function($msg) use ($imClient, $logger) {
    $task = json_decode($msg->body, true);
    if ($task['t'] == 1) {
        $username = $task['u'];
        $password = $task['p'];
        $faceUrl = $task['f'];
        $nick = $task['n'] ?? $username; // 昵称
        //这里执行创建用户
        
        if ($imClient->createUser($username, $password, $faceUrl, $nick)) {
            return true;
        } else {
            $logger->error('创建用户失败');
            return false;
        }

        
    }
};

$imDB->pop($callback);