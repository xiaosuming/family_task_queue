<?php
define("DIRECTION", 1);
define("HD", 0);
require_once('vendor/autoload.php');
use Util\TaskQueue\Consumer;
use Util\TaskQueue\Producer;

function exceptionHandler($e) {
    echo "异常处理";

    var_dump($e);
}

set_exception_handler('exceptionHandler');

$callback = function($msg) {
    echo "开始callback\n";
    sleep(120);
    echo "睡好120秒了\n";
    return true;
};

$producer = new Producer();
$producer->produce('test-long-run-async', 'hhhhh');

$consumer = new Consumer();
$consumer->consume('test-long-run-async', $callback, true);





