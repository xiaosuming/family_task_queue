<?php
define("DIRECTION", 1);
define("HD", 0);
require_once('vendor/autoload.php');
use Util\TaskQueue\Consumer;
use Util\TaskQueue\Producer;

function exceptionHandler($e) {
    echo "异常处理";
    var_dump($e);
}

set_exception_handler('exceptionHandler');

$producer = new Producer();
$producer->produce('test-long-run', 'hhhhh');

